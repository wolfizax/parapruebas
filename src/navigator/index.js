import React from 'react';
import { connect } from 'react-redux';
import { reduxifyNavigator,createReactNavigationReduxMiddleware } from 'react-navigation-redux-helpers';
import {RootNavigator} from './routes';

const navigationMiddleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.navState
);
const AppWithNavigationState = reduxifyNavigator(RootNavigator, 'root');
const mapStateToProps = state => ({
  state: state.navState,
});
const AppNavigator = connect(mapStateToProps)(AppWithNavigationState);

export { RootNavigator, AppNavigator, navigationMiddleware };