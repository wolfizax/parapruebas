import { createStackNavigator,createBottomTabNavigator } from 'react-navigation';

/*Presentational Views */
import MainView from '../views/Main';
import NewUserView from '../views/NewUser';
import NewUserValidationCodeView from '../views/NewUser/ValidationCode';
import NewUserCompletePerfilView from '../views/NewUser/CompletePerfil';
import LoginIdentificationView from '../views/Login/index';
import LoginPasswordView from '../views/Login/Password';
import LoginForgotPasswordView from '../views/Login/Forgot';
import LoginForgotValidationCodeView from '../views/Login/ValidationCode';
import LoginForgotChangePasswordView from '../views/Login/ChangePassword';
import DashboardView from '../views/Dashboard/index';
import LeftMenuView from '../views/LeftMenu/index';
import ExperienceWonderChatt from '../views/WalkWonderChatt/index';
import ScanQRCode from '../views/ScanQRCode/index';
import LogoutView from '../views/Logout/index';
import UserSettingView from '../views/UserSetting/index';
import PrivacyMenuView from '../views/UserSetting/PrivacyMenu';
import PrivacySettingView from '../views/UserSetting/PrivacySetting';
import BlockedContactsView from '../views/UserSetting/BlockedContacts';
import SecurityMenuView from '../views/UserSetting/SecurityMenu';
import EmergencyContactsView from '../views/UserSetting/EmergencyContacts';
import AssociateEmailView from '../views/UserSetting/AssociateEmail';
import ChangeNumberView from '../views/UserSetting/ChangeNumber/index';
import ChangeNumberFormView from '../views/UserSetting/ChangeNumber/Form';
import ChangeNumberValidationCodeView from '../views/UserSetting/ChangeNumber/ValidationCode';
import SoundSettingsView from '../views/UserSetting/SoundSetting';
import StorageSettingsView from '../views/UserSetting/StorageSetting/index';
import StorageSettingsUseOfDataView from '../views/UserSetting/StorageSetting/Useofdata';
import StorageSettingsUseOfStorageView from '../views/UserSetting/StorageSetting/Useofstorage';
import HelpMenuView from '../views/UserSetting/HelpMenu';
import ProfileView from '../views/UserSetting/Profile/index';
import ProfilePersonalInformationView from '../views/UserSetting/Profile/PersonalInformation';
import ProfileProfessionalInformationView from '../views/UserSetting/Profile/ProfessionalInformation';
import MyWalletPointsView from '../views/UserSetting/MyWallet/Points';
import ChangePasswordView from '../views/UserSetting/ChangePassword';
import MessagesView from '../views/Messages/Messages';
import ContactsView from '../views/Contacts/Contacts';
import DiscoverView from '../views/Discover/index';
import AddMembers from '../views/AddMembers/AddMembers';
import NewConversation from '../views/newConversation/NewConversation';
import SearchCommunity from '../views/searchCommunity/SearchCommunity';
import MenuContacts from '../views/menuContacts/MenuContacts';
import InvitationsCode from '../views/invitationsCode/InvitationsCode';
import CreateGroup from '../views/createGroup/CreateGroup';
import AddCategory from '../views/addCategory/AddCategory';
import InfContacts from '../views/infContacts/InfContacts';
import MomentsContacts from '../views/momentsContacts/MomentsContacts';
import MyMoments from '../views/mymoments/MyMoments';
import MomentsPhoto from '../views/momentsPhoto/MomentsPhoto';
import MomentsVideo from '../views/momentsVideo/MomentsVideo';
import WonderTales from '../views/wonderTales/WonderTales';
import NewCategory from '../views/newCategory/NewCategory';
import EditCategory from '../views/editCategory/EditCategory';
import PendingInvitations from '../views/pendingInvitations/PendingInvitations';
import OptionsContacts from '../views/optionsContacts/OptionsContacts';
import ReportContact from '../views/reportContact/ReportContact';
import AssingCategory from '../views/assingCategory/AssingCategory';
import ViewProfession from '../views/viewProfession/ViewProfession';
import InfInviteContacts from '../views/infInviteContacts/InfInviteContacts';
import SearchContacts from '../views/searchContacts/SearchContacts';
import Reputation from '../views/reputation/Reputation';
import InviteFriend from '../views/InviteFriend/InviteFriend'
import ChatView from '../views/Chat/index';

const TabBottomNavigation = createBottomTabNavigator({
  Messages:{screen:MessagesView},
  Contacts:{screen:ContactsView},
  Discover: {screen:DiscoverView},
  Profile:{screen:ProfileView},
},
{
  initialRouteName:'Messages',
  tabBarOptions:
    {
      showLabel:false,
      allowFontScaling:true,
      activeTintColor: '#3CD22F',
      inactiveTintColor: '#C7C7C7',
      style:{borderTopWidth:1,borderTopColor:'#AAAAAA'}
    },
    backBehavior:'initialRoute'
})

const RootNavigator = createStackNavigator({
    Main: { screen: MainView },
    NewUser: { screen: NewUserView },
    NewUserValidationCode: { screen:NewUserValidationCodeView},
    NewUserCompletePerfil: { screen:NewUserCompletePerfilView},
    Login: { screen:LoginIdentificationView},
    LoginPassword: { screen:LoginPasswordView},
    LoginForgotPassword: { screen:LoginForgotPasswordView},
    LoginForgotValidationCode: { screen:LoginForgotValidationCodeView},
    LoginForgotChangePassword: { screen:LoginForgotChangePasswordView},
    Dashboard: {screen:TabBottomNavigation,navigationOptions: {header:null}},
    LeftMenu: {screen:LeftMenuView},
    ExperienceWonderChatt: { screen:ExperienceWonderChatt,navigationOptions: {header:null}},
    ScanQRCode: { screen:ScanQRCode},
    UserSetting: { screen:UserSettingView},
    PrivacyMenu: { screen:PrivacyMenuView},
    PrivacySetting:{ screen:PrivacySettingView},
    BlockedContacts:{ screen:BlockedContactsView},
    SecurityMenu: { screen:SecurityMenuView},
    EmergencyContacts: { screen:EmergencyContactsView},
    AssociateEmail: { screen:AssociateEmailView},
    ChangeNumber: {screen:ChangeNumberView},
    ChangeNumberForm: {screen:ChangeNumberFormView},
    ChangeNumberValidationCode: {screen:ChangeNumberValidationCodeView},
    SoundSettings: { screen:SoundSettingsView},
    StorageSettings: { screen:StorageSettingsView},
    StorageSettingsUseOfData:{ screen:StorageSettingsUseOfDataView},
    StorageSettingsUseOfStorage: {screen:StorageSettingsUseOfStorageView,navigationOptions: {header:null}},
    HelpMenu: {screen:HelpMenuView},
    ProfilePersonalInformation: {screen: ProfilePersonalInformationView},
    ProfileProfessionalInformation: {screen: ProfileProfessionalInformationView},
    MyWalletPoints:{screen: MyWalletPointsView},
    ChangePassword:{screen: ChangePasswordView},
    Logout: { screen:LogoutView,navigationOptions: {header:null}},
    AddMembers: {screen: AddMembers,navigationOptions: {header: null }},
    NewConversation: {screen: NewConversation,navigationOptions: {header: null }},
    SearchCommunity: {screen: SearchCommunity,navigationOptions: {header: null }},
    MenuContacts: {screen: MenuContacts,navigationOptions: {header: null }},
    InvitationsCode: {screen: InvitationsCode,navigationOptions: {header: null }},
    CreateGroup: {screen: CreateGroup,navigationOptions: {header: null }},
    AddCategory: {
    screen: AddCategory,
    navigationOptions: {
      header: null 
    }
  },
  InfContacts: {
    screen: InfContacts,
    navigationOptions: {
      header: null 
    }
  },
  MomentsContacts: {
    screen: MomentsContacts,
    navigationOptions: {
      header: null 
    }
  },
  MyMoments: {
    screen: MyMoments,
    navigationOptions: {
      header: null 
    }
  },
  MomentsPhoto: {
    screen: MomentsPhoto,
    navigationOptions: {
      header: null 
    }
  },
  MomentsVideo: {
    screen: MomentsVideo,
    navigationOptions: {
      header: null 
    }
  },
  WonderTales: {
    screen: WonderTales,
    navigationOptions: {
      header: null 
    }
  },
  NewCategory: {
    screen: NewCategory,
    navigationOptions: {
      header: null 
    }
  },
  EditCategory: {
    screen: EditCategory,
    navigationOptions: {
      header: null 
    }
  },
  PendingInvitations : {
    screen: PendingInvitations,
    navigationOptions: {
      header: null 
    }
  },
  OptionsContacts : {
    screen: OptionsContacts,
    navigationOptions: {
      header: null 
    }
  },
  ReportContact : {
    screen: ReportContact,
    navigationOptions: {
      header: null 
    }
  },
  AssingCategory : {
    screen: AssingCategory,
    navigationOptions: {
      header: null 
    }
  },
  ViewProfession : {
    screen: ViewProfession,
    navigationOptions: {
      header: null 
    }
  },
  InfInviteContacts : {
    screen: InfInviteContacts,
    navigationOptions: {
      header: null 
    }
  },
  SearchContacts : {
    screen: SearchContacts,
    navigationOptions: {
      header: null 
    }
  },
  Reputation : {
    screen: Reputation,
    navigationOptions: {
      header: null 
    }
  },
  InviteFriend : {
    screen: InviteFriend,
    navigationOptions: {
      header: null 
    }
  },
  Chat:{screen:ChatView,navigationOptions:{header:null}}

  });

export {RootNavigator}