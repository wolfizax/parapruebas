import React from 'react';
import {View,StyleSheet,Dimensions,PixelRatio} from 'react-native';

let {width} = Dimensions.get('window');

const Divider = () => <View style={{width:width - 30,height:1 / PixelRatio.getPixelSizeForLayoutSize(1),backgroundColor:'#AAAAAA',marginTop:3,marginBottom:3,marginLeft:15}} />    

const styles = StyleSheet.create({
	divider:{
		height:1,
		width,
		backgroundColor : '#AAAAAA',
		marginTop:15,
		marginBottom:15,
		marginLeft:15
	}

})      

export default Divider;