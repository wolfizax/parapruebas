import React from 'react';
import {StyleSheet} from 'react-native';
import {List, ListItem,Left, Body,Text,Thumbnail} from 'native-base';
import {capitalize} from '../utils/letter';
import Proptypes from 'prop-types';

const ListNavigationContacts = ({contacts,text,navigation})=>(
    <List>
        {contacts.sort((a,b)=>{return a.displayName.localeCompare(b.displayName)}).map((item,i)=>
            {if(item.displayName.toLowerCase().indexOf(text.toLowerCase())!==-1){ 
                return(
                    <ListItem key = {i} onPress = {()=>navigation(item)} > 
                        <Left>
                            <Thumbnail source={require('../assets/usuario.png')} />
                            <Body style={{flexDirection:'column',marginTop:10}}>
                                <Text>{capitalize(item.displayName)} </Text>
                                <Text note>sub-title</Text>                        
                            </Body> 
                        </Left>                                          
                    </ListItem>
                )
            }}
            
        )}  
    </List>   
)


const styles = StyleSheet.create({
    body:{
        flexDirection:'column',
        marginTop:10
    }

})

ListNavigationContacts.proptypes = {
    contacts : Proptypes.array.isRequired,
    text : Proptypes.string,
    navigation:Proptypes.func.isRequired
}


export default ListNavigationContacts;