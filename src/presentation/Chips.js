import React from 'react';
import {View,ScrollView,Image,StyleSheet} from 'react-native';
import {Text, Button} from 'native-base';
import Icon from './Icon/index';
import Proptypes from 'prop-types';

const Chips = ({contacts,handleRemoveContacts})=>(

    <View style={styles.ctn}>
            <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator = {false}
                contentContainerStyle ={{
                alignItems : 'center',
                paddingTop: 5,
                paddingEnd: 5
                }}
            >
                {contacts.map((item,i)=>
                <View key = {i} style={styles.chips}>
                    <Image style={styles.image} source={require('../assets/usuario.png')} />
                    <Text style={styles.text}>{item.displayName }</Text>
                    <Button transparent dark style={styles.icon}  onPress = {()=>handleRemoveContacts(item,i)}><Icon name="Grupo-775" color="#3CD22F"/></Button>
                </View>
                )}
         
            </ScrollView>
    </View>
)

const styles = StyleSheet.create({
    ctn :{
        width:'100%',
        marginRight:10,
        marginLeft:10,
        paddingBottom:3,
        marginTop:3
    },
    chips:{
        backgroundColor:'white',
        marginRight:10,
        flexDirection:'row',
        borderRadius:18,
        paddingRight:5,
        alignItems : 'center',
        justifyContent:'center',
        height:36,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 1,
        shadowRadius: 3.84,
        elevation: 4
    },
    image:{
        width:36,
        height:36,
        borderRadius:18
    },
    text:{
        marginRight:17,
        marginLeft:3
    },
    icon:{
        paddingBottom:12
    }
})


Chips.proptypes = {
    contacts : Proptypes.array.isRequired,
    handleRemoveContacts : Proptypes.func.isRequired
}


export default Chips;