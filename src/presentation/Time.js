import React from 'react';
import {Text} from 'react-native';
import moment from "moment";
import Proptypes from 'prop-types';
import {time} from '../utils/date';

const Time = ({color,date})=>(
	<Text style={{color:color}}>{time('2019-03-13')}</Text>         
)

Time.proptypes = {
    color : Proptypes.string.isRequired
}

export default Time;