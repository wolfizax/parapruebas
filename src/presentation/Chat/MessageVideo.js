import React, { Component } from 'react';
import { TouchableOpacity, ImageBackground } from 'react-native'; 
import { Row, Text } from 'native-base';
import Icon from '../Icon/index';
import PropTypes from 'prop-types';
import style from './style';

export default class MessageVideo extends Component{
    render(){
        const { description, thumbnail } = this.props;
        return(
            <Row>
                <TouchableOpacity>
                    <ImageBackground
                        source={{uri:thumbnail}}
                        style={style.videoMessageThumbnail}>
                    <Row style={style.videoMessageIconContainer}>
                        <Icon
                            name='DI-4'
                            size={25}
                            color={'#3cd22f'}
                            />
                    </Row>
                    <Row style={style.videoMessageDuration}>
                        <Text note style={style.videoMessageDurationText} allowFontScaling>1:35 min</Text>
                    </Row>
                    </ImageBackground>
                    {
                        description != undefined ?
                        <Text allowFontScaling>{description}</Text>
                        :
                        null
                    }
                </TouchableOpacity>
            </Row>
        )
    }
}
MessageVideo.propTypes = {
    url: PropTypes.string.isRequired,
    thumbnail: PropTypes.string.isRequired,
    description: PropTypes.string
}