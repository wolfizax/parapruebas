import React, { Component } from 'react';
import { ScrollView, Platform, TouchableOpacity, Text, Dimensions } from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import { OptimizedFlatList } from 'react-native-optimized-flatlist';
import { emojify } from 'react-emojione';
import emojiCategories from './emojiCategories';
import { View } from 'native-base';
import style from './style';

const EMOJIS_PER_ROW = Platform.OS == 'ios' ? 8 : 9;
const {height, width} = Dimensions.get('window');
export default class EmojiPicker extends Component{
    constructor(props){
        super(props);
        this.size = Math.min(height,width) / EMOJIS_PER_ROW;
    }
    renderCategory(category,index){
        let emojis = emojiCategories.emojisByCategory[category]
        return(
            <OptimizedFlatList
                keyExtractor={(item, index) => index.toString()}
                data={emojis}
                renderItem={({item}) => this.renderItemCategory(item,this.size)}
                numColumns={EMOJIS_PER_ROW}
                initialNumToRender={45}
                getItemLayout={(data, index) => ({ length: this.size, offset: this.size * index, index })}
                removeClippedSubviews
            />
        )
    }
    renderItemCategory(emoji){
        return(
            <TouchableOpacity activeOpacity={0.8} key={emoji}>
                {this.renderEmoji(emoji,this.size)}
            </TouchableOpacity>
        )
    }
    renderEmoji(emoji){
        return(
            <Text style={{height:this.size,width:this.size,fontSize:this.size-14}}>
                {emojify(`:${ emoji }:`, { output: 'unicode' })}
            </Text>
        )
    }
    renderTabBar(){
        return(
            <View style={style.tabContainer}>
                {
                    emojiCategories.tabs.map((tab,index) => 
                        <TouchableOpacity activeOpacity={0.5} key={index} style={style.tab} onPress={() => this.goToPage(index)} >
                            <Text style={{width:this.size,height:this.size,fontSize:this.size-25}}>{emojify(tab.tabLabel,{output:'unicode'})}</Text>
                        </TouchableOpacity>
                    )
                }
            </View>
        )
    }
    goToPage(index){
        this.refs.scrollableTab.goToPage(index)
    }
    render(){
            return(
                <ScrollableTabView
                renderTabBar={() => this.renderTabBar()}
                tabBarPosition='bottom'
                initialPage={0}
                ref='scrollableTab'
                style={style.containerEmojis}
                >
                {
                    emojiCategories.tabs.map((tab,index) => (
                        <ScrollView key={tab.category} tabLabel={tab.tabLabel}>
                            {this.renderCategory(tab.category,index)}
                        </ScrollView>
                    ))
                }
                </ScrollableTabView>
            );
    }
}