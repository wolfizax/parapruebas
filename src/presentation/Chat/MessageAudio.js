import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import { Row, Text } from 'native-base';
import Icon from '../Icon/index';
import Sound from 'react-native-sound';
import * as Progress from 'react-native-progress';
import PropTypes from 'prop-types';
import AudioRecorderPlayer from 'react-native-audio-recorder-player';
import duration from '../../utils/duration';
import style from './style';

export default class MessageAudio extends Component{
    constructor(){
        super();
        this.state = {
            play:false,
            pause:false,
            duration:{seconds:0,text:'00:00:00'},
            currentTime:{seconds:0,text:'00:00:00'},
            error:false
        }
        this.soundRef = null;
        this.audioPlayer = new AudioRecorderPlayer();
    }
    play = async () => {
        const { url } = this.props;
        if(this.state.play){
            this.state.pause ? this.audioPlayer.pausePlayer() : this.audioPlayer.resumePlayer()
            this.setState({...this.state,pause:!this.state.pause});
        }else{
            this.audioPlayer.startPlayer(url);
            this.setState({...this.state,play:!this.state.play})
            this.audioPlayer.addPlayBackListener((e) => {
                if(e.current_position === e.duration){
                    this.audioPlayer.stopPlayer();
                    this.audioPlayer.removePlayBackListener();
                    this.setState({...this.state,play:!this.state.play,currentTime:{text:'00:00:00',minutes:0,hours:0,seconds:0}});
                }else{
                    this.setState({...this.state,currentTime:duration(e.current_position/1000)});
                }
                return;
            });
        }
    }
    componentWillMount(){
        const { url } = this.props;
        this.soundRef = new Sound(url,'',(error) => {
            if(error){
                this.setState({...this.state,error:true});
            }else{
                this.setState({...this.state,duration:duration(this.soundRef.getDuration())});
            }
        });
    }
    render(){
        const { play, duration, currentTime } = this.state;
        let progressValue = Math.round((currentTime.seconds * 100) / duration.seconds)/100;

        return(
            <Row style={style.containerMessageAudio}>
                <TouchableOpacity onPress={this.play}>
                        <Icon
                        name={!play ? 'DI-4' : 'DI-5'}
                        size={25}
                        color={'#C7C7C7'}
                        style={style.iconMessageAudio}
                        />
                </TouchableOpacity>
                <Row style={{justifyContent:'center'}}>
                    <Text note style={{alignSelf:'center'}}>{currentTime.text} - {duration.text}</Text>
                    <Progress.Circle indeterminateAnimationDuration={1} progress={isNaN(progressValue) ? 0 : progressValue} width={40} style={{marginLeft:10}} />
                </Row>
            </Row>
        )
    }
}
MessageAudio.propTypes = {
    url:PropTypes.string.isRequired
}