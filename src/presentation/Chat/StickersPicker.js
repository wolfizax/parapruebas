import React, { Component } from 'react';
import { TouchableOpacity, Platform, Dimensions, FlatList } from 'react-native';
import { Row } from 'native-base';
import Image from 'react-native-image-progress';
import PropTypes from 'prop-types';
import GphApiClient from 'giphy-js-sdk-core';
import style from './style';
var client = GphApiClient('1Kg94X9f69NhkfBIrjxrFUnG7VwP6hu6');

const GIFTS_PER_ROW = Platform.OS == 'ios' ? 8 : 9;
const {height, width} = Dimensions.get('window');
export default class StickersPicker extends Component{
    constructor(props){
        super(props);
        this.state = {
            gifts:[],
            offset:0,
            refreshing:false
        };
        this.size = Math.min(height,width) / GIFTS_PER_ROW;
    }
    componentDidMount(){
        if(this.props.category === ''){
            client.trending("stickers", {limit:GIFTS_PER_ROW})
            .then((response) => {
                this.extractGifts(response);
            })
            .catch((err) => {

            })
        }
    }
    extractGifts = async (data) => {
        let gifts = data.data.map((item) => {
            return item.images.fixed_height_downsampled.url;
        });
        let newGifts = this.state.gifts.concat(gifts);
        this.setState({...this.state,gifts:newGifts});
    }
    renderGift(data){
        return(
            <TouchableOpacity style={style.containerGift}>
                <Image source={{uri:data.item}} style={style.imageGift} />
            </TouchableOpacity>
        );
    }
    loadMore = () => {
        this.state.offset += 10;
        if(this.props.category === ''){
            client.trending("stickers", {offset:this.state.offset})
            .then(res => {
                this.extractGifts(res);
            })
            .catch(err => {

            })
        }
    }
    render(){
        return(
            <Row>
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    numColumns={GIFTS_PER_ROW}
                    data={this.state.gifts}
                    renderItem={(item) => this.renderGift(item)}
                    getItemLayout={(data, index) => ({ length: this.size, offset: this.size * index, index })}
                    removeClippedSubviews
                    onEndReached={this.loadMore}
                    initialNumToRender={4}
                />
            </Row>
        );
    }
}
StickersPicker.propTypes = {
    category: PropTypes.string
}
StickersPicker.defaultProps = {
    category: ''
}