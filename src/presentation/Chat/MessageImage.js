import React, { Component } from 'react';
import { TouchableOpacity, Text } from 'react-native';
import { Row } from 'native-base';
import Modal from 'react-native-modal';
import Image from 'react-native-image-progress';
import ViewTransformer from "react-native-easy-view-transformer";
import PropTypes from 'prop-types';
import style from './style';

export default class MessageImage extends Component{
    constructor(){
        super();
        this.state = {
            modalStatus:false,
        };
    }
    renderModalImage(data){
        return(
            <Modal isVisible={this.state.modalStatus} onBackButtonPress={() => this.handleModal()}>
				<ViewTransformer enableScale={true} enableTranslate={false}>
                	<Image source={{uri:data.url}} style={{height:'90%'}} resizeMode='contain' />
				</ViewTransformer>
            </Modal>
        )
    }
    handleModal = () => {
        this.setState({...this.state,modalStatus:!this.state.modalStatus});
    }
    render(){
        let { url,description } = this.props;
        return(
            <Row>
                {this.renderModalImage({url,description})}
                <TouchableOpacity onPress={this.handleModal}>
                    <Image source={{uri:url}} style={style.typeMessageImage}/>
                    {
                        description != undefined ?
                        <Text allowFontScaling>{description}</Text>
                        :
                        null
                    }
                </TouchableOpacity>
            </Row>
        )
    }
}
MessageImage.propTypes = {
    url: PropTypes.string.isRequired,
    description: PropTypes.string
}