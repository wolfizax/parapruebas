import React, { Component } from 'react';
import { View } from 'react-native';
import MessageImage from './MessageImage';
import MessageAudio from './MessageAudio';
import MessageLocation from './MessageLocation';
import MessageVideo from './MessageVideo';
import MessageMoney from './MessageMoney';
import { Text } from 'native-base';
import PropTypes from 'prop-types';
import style from './style';
import RF from "react-native-responsive-fontsize"

export default class MessageComponent extends Component{
    renderTypeMessage(message){
        switch(message.type){
            case 'text':
                return(<Text allowFontScaling style={[style.messageText,{fontSize:RF(2.8)}]}>
                    {message.message}
                </Text> )
            break;
            case 'image':
                return(<MessageImage url={message.url} description={message.message} />)
            break;
            case 'audio':
                return(<MessageAudio url={message.url} />)
            break;
            case 'location':
                return(<MessageLocation location={message.location} description={message.message} />)
            break;
            case 'video':
                return(<MessageVideo url={message.video.url} description={message.message} thumbnail={message.video.thumbnail} />)
            break;
            case 'money':
                return(<MessageMoney money={message.money} description={message.message} />)
            break;
            default:
            break;
        }
    }
    render(){
        const { message, user } = this.props;
        user.id === message.sender.id ? direction = 'left' : direction = 'right'
        const messageStyle = direction === 'left' ? style.messageLeft : style.messageRight;
        return(
            <View style={{flexDirection:'row',justifyContent:direction === 'left' ? 'flex-start' : 'flex-end'}}>
                <View style={{marginVertical:2}}>
                    <View style={[style.messageContainer,messageStyle]}>
                        <View>
                            {this.renderTypeMessage(message)}
                            <Text allowFontScaling note style={[{textAlign:direction},style.messageDate]}>{message.date}</Text>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}
MessageComponent.propTypes = {
    message:PropTypes.object.isRequired,
    user: PropTypes.object.isRequired
}