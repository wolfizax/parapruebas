import React, { Component } from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import style from './style';
import { Row, Text, Grid } from 'native-base';

export default class MessageMoney extends Component{
    render(){
        const { money, description } = this.props;
        return(
            <Row>
                <TouchableOpacity>
                    <Row style={{width:240,flex:1,flexDirection:'row',flexWrap:'wrap',backgroundColor:'#d2efeb',borderRadius:12}}>
                        <Grid style={{padding:10}}>
                            <Row>
                                <Text style={{color:'#b4cdb3'}}>Transfer ID {money.id}</Text>
                            </Row>
                            <Row>
                                <Text allowFontScaling style={{fontSize:20}}>USD {money.mount}</Text>
                            </Row>
                            <Row>
                                <Text style={{color:'#b4cdb3'}}>From: {money.from}</Text>
                            </Row>
                        </Grid>
                    </Row>
                    {
                        description != undefined ?
                        <Text allowFontScaling>{description}</Text>
                        :
                        null
                    }
                </TouchableOpacity>
            </Row>
        )
    }
}
MessageMoney.propTypes = {
    money: PropTypes.object.isRequired,
    description: PropTypes.string
}