import React, { Component } from 'react';
import { TouchableOpacity, Platform, Dimensions, FlatList } from 'react-native';
import { Row } from 'native-base';
import Image from 'react-native-image-progress';
import PropTypes from 'prop-types';
import GphApiClient from 'giphy-js-sdk-core';
import style from './style';
var client = GphApiClient('1Kg94X9f69NhkfBIrjxrFUnG7VwP6hu6');

const GIFS_PER_ROW = Platform.OS == 'ios' ? 8 : 9;
const {height, width} = Dimensions.get('window');
export default class GiftPicker extends Component{
    constructor(props){
        super(props);
        this.state = {
            gifs:[],
            offset:0
        };
        this.size = Math.min(height,width) / GIFS_PER_ROW;
    }
    componentDidMount(){
        if(this.props.search === ''){
            client.trending("gifs", {limit:GIFS_PER_ROW})
            .then((response) => {
                this.extractGifs(response);
            })
            .catch((err) => {

            })
        }
    }
    extractGifs = async (data) => {
        let newGifs = data.data.map((item) => {
            return item.images.fixed_height_downsampled.url;
        });
        let gifs = this.state.gifs.concat(newGifs);
        this.setState({...this.state,gifs});
    }
    renderGif(data){
        return(
            <TouchableOpacity style={style.containerGift}>
                <Image source={{uri:data.item}} style={style.imageGift} />
            </TouchableOpacity>
        );
    }
    loadMore = () => {
        this.state.offset += 10;
        if(this.props.search === ''){
            client.trending("gifs", {offset:this.state.offset})
            .then(res => {
                this.extractGifs(res);
            })
            .catch(err => {

            })
        }
    }
    render(){
        return(
            <Row>
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    numColumns={GIFS_PER_ROW}
                    data={this.state.gifs}
                    renderItem={(item) => this.renderGif(item)}
                    getItemLayout={(data, index) => ({ length: this.size, offset: this.size * index, index })}
                    removeClippedSubviews
                    onEndReached={this.loadMore}
                    initialNumToRender={4}
                />
            </Row>
        );
    }
}
GiftPicker.propTypes = {
    search: PropTypes.string
}
GiftPicker.defaultProps = {
    search: ''
}