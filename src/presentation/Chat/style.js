import { StyleSheet } from 'react-native';

const style = StyleSheet.create({
    containerEmojis:{
        backgroundColor:'white',
        marginTop:5
    },
    tabContainer:{
        borderTopWidth:1,
        borderTopColor:'gray',
        height:35,
        flexDirection:'row',
        padding:5,
        alignContent:'center'
    },
    tab:{
        flex:1,
        flexDirection:'column',
    },
    activeTab:{
            backgroundColor: '#007aff',
    },
    containerGift:{
        width:105,
        height:90
    },
    imageGift:{
        width:105,
        height:90,
        borderRadius:2,
        marginRight:1
    },
    messageContainer:{
        borderRadius:10,
        borderColor:'#000000',
        marginTop:8,
        marginRight:10,
        marginLeft:10,
        paddingHorizontal:10,
        paddingVertical:10,
        flexDirection:'row',
        flex:1
    },
    messageLeft:{
        backgroundColor:'#E3FFFB'
    },
    messageRight:{
        backgroundColor:'#FFFFFF'
    },
    messageText:{
        fontFamily:'Roboto',
    },
    messageDate:{
        fontFamily:'Roboto',
        fontSize:12
    },
    typeMessageImage:{
        width: 240,
        minHeight: 130,
        borderRadius: 2,
        margin: 5,
        padding: 5,
        resizeMode: 'cover'
    },
    containerMessageAudio: {
        width: 240,
        height: 40,
        borderRadius: 2,
        margin: 3,
        padding: 5,
        alignItems: 'center',
    },
    iconMessageAudio:{
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 0.5,
        backgroundColor: "transparent",
        marginRight: 5
    },
    mapStyle: {
        borderRadius: 12,
        width: 240,
        minHeight: 130,
    },
    videoMessageThumbnail:{
        width:240,
        minHeight:130,
        justifyContent:'center',
        alignItems:'center'
    },
    videoMessageIconContainer:{
        width:60,
        height:60,
        backgroundColor:'white',
        borderRadius:50,
        justifyContent:'center',
        alignItems:'center'
    },
    videoMessageDuration:{
        backgroundColor:'black',
        left:0,
        bottom:0,
        position:'absolute',
        padding:6,
        margin:5,
        borderRadius:10
    },
    videoMessageDurationText:{
        color:'white'
    }
});

export default style;