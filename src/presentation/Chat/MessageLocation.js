import React, { Component } from 'react';
import { TouchableOpacity, Linking } from 'react-native';
import { Row, Text } from 'native-base';
import PropTypes from 'prop-types';
import style from './style';
import MapView from 'react-native-maps';

export default class MessageLocation extends Component{
    openMap = () => {
      const { latitude, longitude } = this.props.location;
      const url = `http://maps.google.com/?q=${latitude},${longitude}`;
      Linking.openURL(url);
    }
    render(){
        const { location, description } = this.props;
        return(
                <Row>
                    <TouchableOpacity>
                        <MapView region={{
                            latitude:location.latitude,
                            longitude:location.longitude,
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421}}
                            scrollEnabled={false}
                            zoomEnabled={false}
                            style={style.mapStyle}
                            onPress={this.openMap} />
                        {
                            description != undefined ?
                            <Text allowFontScaling>{description}</Text>
                            :
                            null
                        }
                    </TouchableOpacity>
                </Row>
            )
    }
}
MessageLocation.propTypes = {
  location:PropTypes.object.isRequired,
  description:PropTypes.string
}