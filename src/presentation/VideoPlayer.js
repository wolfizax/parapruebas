import React,{Component} from 'react';
import {View,Text,StyleSheet,Dimensions,TouchableOpacity,ActivityIndicator} from 'react-native';
import Video from 'react-native-video';
import Icon from './Icon/'

let {width,height} = Dimensions.get('window');

export default class VideoPlayer extends Component{
	constructor(props){
		super(props);
		this.state = {
			duration:null,
			currentTime: null,
			time : null,
			paused : false,
			error:false,
			isBuffering: false,
			image : props.image || null
		}
	}

	onLoad({duration}) {
		this.setState({duration,time:duration,error:false})
	}

	onProgress({currentTime}) {
		this.setState({time:this.state.duration - currentTime});
	}

	onBuffer({ isBuffering } : { isBuffering: boolean }) {
		if(isBuffering){
			this.setState({isBuffering})
		} 

	}

	onEnd(){
		this.setState({time : this.state.duration})
	}

	onError(){
		thi.setState({error:true})
		console.log('error')
	}

	handlePlayVideo(){
		this.setState({paused:!this.state.paused})
	}

		renderTime = (time) =>{
	
		let  minutes = Math.floor( time / 60 );
		let seconds = time % 60;
 
		//Anteponiendo un 0 a los minutos si son menos de 10 
		minutes = minutes < 10 ? '0' + minutes : minutes;
 
		//Anteponiendo un 0 a los segundos si son menos de 10 
		seconds = seconds < 10 ? '0' + seconds : seconds;
 
		result = minutes + ":" + Math.floor(seconds);  // 161:30
		return result;
	}


	render(){
		return(

		<TouchableOpacity 
			style={{flex:1}} 
			onPress={this.handlePlayVideo.bind(this)}
			activeOpacity ={0.8}
		>
		<View style={{flex:1,backgroundColor:'#C7C7C7'}}>
		
			<View style={styles.ctn}>

				{(this.state.isBuffering && !this.state.paused) && <View style={styles.play}><Icon size = {16} name="DI-4" color="#17D6B6" /></View>}

					<Video source={{uri: this.props.source}}   // Can be a URL or a localfile.
       				ref={(ref) => {
         				this.player = ref
      		 		}}     
      		 		paused = {!this.state.paused}  
      		 		resizeMode = 'cover'
      		 		fullscreen = {true}
      		 		onLoad={this.onLoad.bind(this)} 
      		 		pictureInPicture = {true}                         // Store reference
       				onBuffer={this.onBuffer.bind(this)}                // Callback when remote video is buffering
       				onEnd={this.onEnd.bind(this)}                      // Callback when playback finishes
       				onError={this.onError.bind(this)}  
       				onProgress={this.onProgress.bind(this)} 
       				poster =  {this.state.isBuffering && this.state.image }
       				ocultarShutterView = {true}
					style={styles.backgroundVideo}	
					posterResizeMode="cover"
					controls = {false}			
       				/>

       			{this.state.error && <View style={styles.error}><Text style={styles.errorText}>An error has occurred</Text></View>}
       			{this.state.duration && <View style={styles.time}><Text style={styles.timeText}>{this.renderTime(this.state.time)}</Text></View>}
				{!this.state.isBuffering && <View style={styles.loading}><ActivityIndicator size="large" color="#17D6B6" /></View>}
			
			</View>
			

		</View>
		</TouchableOpacity>

		)
	}
}

const styles = StyleSheet.create({
	ctn:{
		flex:1,
		position:'relative',
		justifyContent:'center',
		alignItems:'center'
	},
	backgroundVideo: {
    	position: 'absolute',
    	top: 0,
    	left: 0,
    	bottom: 0,
    	right: 0,
    	width:'100%',
    	zIndex:-1
  	},
	time:{
		position:'absolute',
		bottom:10,
		left:10,
		backgroundColor:'rgba(0,0,0,.6)',
		paddingLeft:7,
		paddingRight:7,
		paddingTop:4,
		paddingBottom:4,
		borderRadius:4
	},
	timeText:{
		color:'white'
	},
	play : {
		width:36,
		height:36,
		backgroundColor:'white',
		borderRadius:18,
		justifyContent:'center',
		alignItems:'center'
	},	
	loading:{
		flex:1,
		position:'absolute',
		width:'100%',
		height:'100%',
		justifyContent:'center',
		alignItems:'center'
	},
	error:{
		position:'absolute',
		bottom:10,
		left:10,
		backgroundColor:'#d43232',
		paddingLeft:7,
		paddingRight:7,
		paddingTop:4,
		paddingBottom:4,
		borderRadius:7
	},
	errorText:{
		color:'white'
	}

})