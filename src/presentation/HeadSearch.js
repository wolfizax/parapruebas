import React from 'react';
import {StyleSheet} from 'react-native';
import {Header, Item, Input} from 'native-base';
import Proptypes from 'prop-types';
import Icon from './Icon/'


const HeadSearch = ({handleSearch,activateSearch,goBack})=>(

	<Header searchBar style={styles.head}>  
        	<Item style={{backgroundColor:'transparent'}}>
            	<Icon name="Enmascarar-grupo-63" color="black" style={{marginRight:6}}/>
            	<Input placeholder="Search..." onChangeText={(text)=>handleSearch(text)}/>
            	<Icon name="Grupo-775" size={16} color="#3CD22F" onPress = {()=>goBack ? goBack() : activateSearch()} />
        	</Item>           
   	
    </Header>

)

const styles = StyleSheet.create({
    head:{
        backgroundColor:'white',
        width:'100%',
        height:50,
        paddingHorizontal:10
    }
})

HeadSearch.proptypes = {
    handleSearch: Proptypes.func.isRequired
}


export default HeadSearch;