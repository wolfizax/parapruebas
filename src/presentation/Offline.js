import React, {Component} from 'react';
import {View, Text, StyleSheet, NetInfo} from 'react-native';
import axios from 'axios';
import ConfigService from '../services/config';

const styles = StyleSheet.create({
    containerMessague:{
        backgroundColor:'red',
        padding:20
    },
    textMessague:{
        color:'white',
        fontFamily:'Roboto',
        fontWeight:'bold',
        textAlign:'center'
    }
});
export default class OfflineNetwork extends Component{
    constructor(){
        super();
        this.state = {
            connectionStatus: null,
            serverStatus: null,
        }
    }
    componentDidMount(){
        this.handlePing = setInterval(this.handlePing,60000)
        NetInfo.isConnected.addEventListener('connectionChange',this.handleConnectionChange);
    }
    componentWillMount(){
        NetInfo.removeEventListener('connectionChange');
    }
    componentWillUpdate(nextProps,nextState){
        if(nextState == this.state){
            return false;
        }
    }
    handlePing = async () => {
        axios.get(ConfigService.BASE_URL)
        .then(result => {
            if(result.request._hasError){
                this.setState({
                    ...this.sate,
                    serverStatus:false
                });
            }else{
                this.setState({
                    ...this.sate,
                    serverStatus:true
                });
            }
        })
        .catch((err) => {
            if(err.request._hasError){
                this.setState({
                    ...this.state,
                    serverStatus:false
                });
            }
        });
    }
    handleConnectionChange = (newconnectionStatus) => {
        this.setState({
            ...this.state,
            connectionStatus: newconnectionStatus
        });
    }
    render(){
        if(this.state.connectionStatus === false){
            return(
                <View style={styles.containerMessague}>
                    <Text allowFontScaling={true} style={styles.textMessague}>Sorry your device not have access internet network</Text>
                </View>)
        }
        if(this.state.serverStatus === false){
            return(
                <View style={styles.containerMessague}>
                    <Text allowFontScaling={true} style={styles.textMessague}>Oh, I'm sorry our servers are outside of service. Try again</Text>
                </View>)
        }
        if((this.state.serverStatus || this.state.serverStatus == null) || (this.state.connectionStatus || this.state.connectionStatus == null))
        {
            return(null)
        }
    }
}