import React, { Component } from 'react';
import {  Spinner, View} from 'native-base';
import { StyleSheet,Modal } from 'react-native';

const style = StyleSheet.create({
    container:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    containerSpinner:{
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        position: 'absolute',
        borderRadius: 8,
        padding: 20.5,
        width:100
    }
});
export default class Loading extends Component{
    render(){
        return(
            <Modal
            visible={this.props.visible}
            transparent
            onRequestClose={() => {}}
        >
            <View style={style.container}>
                <View style={style.containerSpinner}>
                    <Spinner size="large" color="white" />
                </View>
            </View>
        </Modal>
        );
    }
}