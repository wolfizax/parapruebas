import React from 'react';
import {StyleSheet} from 'react-native';
import {List, ListItem,Left, Body,Text,Thumbnail} from 'native-base';
import {capitalize} from '../utils/letter';
import Proptypes from 'prop-types';

const ListContants = ({contacts})=>(
    <List>
        {contacts.map((item,i)=>
            <ListItem key = {i} > 
                <Left>
                    <Thumbnail source={require('../assets/usuario.png')} />
                    <Body style={{flexDirection:'column',marginTop:10}}>
                        <Text>{capitalize(item.displayName)} </Text>
                        <Text note>sub-title</Text>                        
                    </Body> 
                </Left>                                          
            </ListItem>
        )}
    </List>   
)


const styles = StyleSheet.create({
    body:{
        flexDirection:'column',
        marginTop:10
    }

})

ListContants.proptypes = {
    contacts : Proptypes.array.isRequired
}


export default ListContants;