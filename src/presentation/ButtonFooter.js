import React from 'react';
import {Text,View,TouchableOpacity} from 'react-native';
import Proptypes from 'prop-types';

const ButtonFooter = ({text,handleSucess}) =>(
	<View style={{marginBottom:5,paddingHorizontal:10}}>
    	<TouchableOpacity onPress = {()=>handleSucess()} >
        	<View style={{height:55,alignItems:'center',justifyContent:'center',borderRadius:5,backgroundColor:'#3CD22F'}}><Text style={{color:'white',fontSize:20,fontWeight:'bold'}}>{text}</Text></View>      	
    	</TouchableOpacity>    
    </View>         
)

ButtonFooter.proptypes = {
    text : Proptypes.string.isRequired,
    handleSucess: Proptypes.func.isRequired
}


export default ButtonFooter;