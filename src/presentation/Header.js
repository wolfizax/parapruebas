import React,{Component} from 'react';
import {StyleSheet,TouchableOpacity} from 'react-native';
import {Header,Title, Button,Input, Thumbnail, View} from 'native-base'
import Icon from './Icon/index';
import { Row } from 'react-native-easy-grid';
import {fileService} from '../services/file';
import PropTypes from 'prop-types';

const style = StyleSheet.create({
    header:{
        backgroundColor:'#262626'
    },
    sidesMenu:{
        maxWidth:50
    },
    containerCenter:{
        justifyContent:'center',
        alignItems:'center'
    },
    fontBold:{
        fontWeight:'bold'
    },
    fontRoboto:{
        fontFamily:'Roboto'
    },
    icon:{
        color:'white'
    },
    search:{
        backgroundColor:'white',
        height:30,
        color:'#262626',
        borderRadius:20,
        padding:5,
        fontFamily:'Roboto'
    },
    avatarContainer:{
        alignItems:'center',
        position:'relative',
        marginBottom:25,
    },
    avatar:{
        width:'100%',
        height:50,
        backgroundColor:'#262626',
        position:'relative',
        alignItems:'center'
    },
    avatarImageStyle:{
        borderRadius:25
    }
})
export default class HeaderView extends Component{
    constructor(){
        super();
        this.state = {
            showSearch:false,
            searchText:''
        }
    }
    search = (text) => {
        this.setState({...this.state,searchText:text})
        this.props.searchBar.function(text);
    }
    renderMiddleComponent = () => {
        if(this.state.showSearch){
            return (<Input autoFocus placeholder="Search" value={this.state.searchText} onChangeText={(text) => this.search(text)} onBlur={() => {this.setState({...this.state,showSearch:!this.state.showSearch});this.props.searchBar.onClose()}} style={this.props.searchBar.style ? this.props.searchBar.style : style.search} />)
        }else if(this.props.title){
            return (<Title style={[style.fontBold,style.fontRoboto]}>{this.props.title}</Title>)
        }
    }
    renderRightComponent = () => {
        if(this.props.rightComponent){
            return (<Button transparent onPress={() => this.props.rightComponent.function()}>
                <Icon name={this.props.rightComponent.icon} size={this.props.rightComponent.size ? this.props.rightComponent.size : 20} style={this.props.rightComponent.style ? this.props.rightComponent.style : style.icon} />
            </Button>)
        }if(this.props.searchBar){
            return (<Button transparent onPress={() => {this.setState({...this.state,showSearch:!this.state.showSearch,searchText:''});this.props.searchBar.onClose()}}>
                <Icon name='SI-2' size={20} style={style.icon} />
            </Button>)
        }
    }
    render(){
        return(
            <View>
                <Header style={style.header}>
                    <Row style={[style.sidesMenu,{justifyContent:'flex-start'}]}>
                        {
                            this.props.leftComponent ?
                                <Button  transparent onPress={() => this.props.leftComponent.function()}>
                                    <Icon name={this.props.leftComponent.icon} size={this.props.leftComponent.size ? this.props.leftComponent.size : 20} style={this.props.leftComponent.style ? this.props.leftComponent.style : style.icon} />
                                </Button>
                            :
                                null
                        }
                    </Row>
                    <Row style={[style.containerCenter]}>
                        {this.renderMiddleComponent()}
                    </Row>
                    <Row style={[style.sidesMenu,{justifyContent:'flex-end'}]}>
                        {this.renderRightComponent()}
                    </Row>
                </Header>
                {
                    this.props.avatar ? 
                    <View style={style.avatarContainer}>
                        <View style={style.avatar}>
                            {
                                this.props.avatar.onPress ?
                                <TouchableOpacity onPress={() => this.props.avatar.onPress()}>
                                    <Thumbnail style={style.avatarImageStyle} large source={this.props.avatar.url != 'assets/avatar.png' ? {uri:fileService.view(this.props.avatar.url)} : require('../assets/userPlaceholder.jpeg')} />
                                </TouchableOpacity>
                                :
                                <Thumbnail style={style.avatarImageStyle} large source={this.props.avatar.url != 'assets/avatar.png' ? {uri:fileService.view(this.props.avatar.url)} : require('../assets/userPlaceholder.jpeg')} />
                            }
                        </View>
                    </View>
                    :
                    null
                }
            </View>
        );
    }
}
HeaderView.propTypes = {
    leftComponent: PropTypes.shape({
        icon: PropTypes.string.isRequired,
        function: PropTypes.func.isRequired,
        size: PropTypes.number,
        style: PropTypes.object
    }),
    rightComponent: PropTypes.shape({
        icon: PropTypes.string.isRequired,
        function: PropTypes.func.isRequired,
        size: PropTypes.number,
        style: PropTypes.object
    }),    
    title: PropTypes.string,
    searchBar: PropTypes.shape({
        style: PropTypes.object,
        function: PropTypes.func.isRequired,
        onClose: PropTypes.func
    }),
    avatar: PropTypes.shape({
        url: PropTypes.string,
        onPress: PropTypes.func
    })
};