import React from 'react';
import {List, ListItem, Thumbnail,Left, Body,Text} from 'native-base';
import Proptypes from 'prop-types';

const ListAddContacts = ({contacts,text, handleAddContacts,handleAddContactsActivate})=>(
    
    <List>
        {contacts.sort((a,b)=>{return a.displayName.localeCompare(b.displayName)}).map((item,i)=>
            {if(item.displayName.toLowerCase().indexOf(text.toLowerCase())!==-1){ 
                return(
                    <ListItem key = {i} onPress={()=>handleAddContacts(item,i)} onLongPress={()=>handleAddContactsActivate(item,i)} > 
                        <Left>
                            <Thumbnail source={require('../assets/usuario.png')} />
                            <Body style={{flexDirection:'column',marginTop:10}}>
                                <Text>{item.displayName} </Text>
                                <Text note>sub-title</Text>                        
                            </Body> 
                        </Left>                                          
                    </ListItem>
                )
            }}
            
        )}  
    </List>    

)

ListAddContacts.proptypes = {
    contacts : Proptypes.array.isRequired,
    text :  Proptypes.string.isRequired,
    handleAddContacts : Proptypes.func,
    handleAddContactsActivate : Proptypes.func
}


export default ListAddContacts;