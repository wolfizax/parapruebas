import React from 'react';
import {View,StyleSheet,Text} from 'react-native';
import {Header,Button} from 'native-base';
import Icon from './Icon/index';
import LinearGradient from 'react-native-linear-gradient';
import Proptypes from 'prop-types';

const HeadGoBack = ({title,goBack})=>(

    <Header style={styles.head}>  
        
        <Button transparent style={styles.icon} onPress = {()=>goBack()}>
            <Icon name='left-arrow' size={18} color="#3CD22F"  />
        </Button>

        <View >
            <Text style={styles.title}>{title}</Text>
        </View>
        
        <View />
                
    </Header>
   
)

const styles = StyleSheet.create({
    head:{
        height:50,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:'white',
        paddingLeft:10,
        paddingRight:10,
        borderBottomWidth:1,
        borderBottomColor:'#c7c7c7'
    },
    icon:{
        marginTop:4
    },
    title:{
        color:'#3CD22F',
        fontSize:20,
    }

})

HeadGoBack.proptypes = {
    title : Proptypes.string.isRequired,
    goBack : Proptypes.func.isRequired
}

export default HeadGoBack;
