import React, {Component} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import {Button, Text} from 'native-base';
import {StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

const style = StyleSheet.create({
    textButton:{
        color:'white',
        textAlign:'center'
}
});
export default class ButtonGradient extends Component{
    render(){
        return(
            <LinearGradient colors={this.props.gradientColors} start={this.props.gradientStart} end={this.props.gradientEnd} >
                <Button full={this.props.full} disabled={this.props.disabled} style={this.props.buttonStyle ? {...this.props.buttonStyle} : null} transparent onPress={() => this.props.onSubmit()}>
                    <Text uppercase={this.props.uppercase} style={this.props.titleStyle ? {...this.props.titleStyle} : style.textButton}>{this.props.title}</Text>
                </Button>
            </LinearGradient>
        )
    }
}
ButtonGradient.defaultProps = {
    uppercase:true
};

ButtonGradient.propTypes = {
    gradientColors: PropTypes.array.isRequired,
    gradientStart: PropTypes.object.isRequired,
    gradientEnd: PropTypes.object.isRequired,
    title: PropTypes.string.isRequired,
    titleStyle: PropTypes.object,
    buttonStyle:PropTypes.object,
    onSubmit: PropTypes.func.isRequired,
    full: PropTypes.bool,
    disabled: PropTypes.bool,
    uppercase: PropTypes.bool
};