import React,{Component} from 'react';
import {View,Text,StyleSheet} from 'react-native';
import Proptypes from 'prop-types';

const BadgeCircle = ({number})=>(

    <View style={ styles.badge}><Text style={styles.textBadge}>{number}</Text></View> 
)


const styles = StyleSheet.create({
    badge : {
        width:18,
        height:18,
        backgroundColor: '#3CD22F',
        borderRadius:9,
        justifyContent:'center',
        alignItems:'center'
    },
    textBadge:{
        fontSize:9,
        color:'white'
    }
    
});


BadgeCircle.proptypes = {
    number : Proptypes.number.isRequired
}

export default BadgeCircle;