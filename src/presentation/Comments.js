import React from 'react';
import {View} from 'react-native';
import {List, ListItem, Left, Body, Thumbnail, Text } from 'native-base';
import Time from './Time';
import Proptypes from 'prop-types';

const Comments = ({comments})=>(
	<List>
        {comments.map((item,i)=>
        <ListItem Thumbnail key ={i} style={{marginTop:15,marginRight:10,marginLeft:10}} >
            <Left>
               {!item.avatar ? <Thumbnail  source={require('../assets/usuario.png')} /> : <Thumbnail  source={{uri: item.avatar }} />} 
                <Body>
                    <Text>{item.displayName}</Text>
                    <Text note> {item.text} </Text>
                </Body>
            </Left>
            <View style={{justifyContent:'flex-start'}}>
                <Time date ={item.date} color='black'/>
            </View>
        </ListItem>

        )}
    </List>
)

Comments.proptypes = {
    comments : Proptypes.array.isRequired
}



export default Comments;