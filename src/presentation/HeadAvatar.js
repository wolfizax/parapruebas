import React,{Component} from 'react';
import {View,StyleSheet} from 'react-native';
import { Thumbnail} from 'native-base';
import Proptypes from 'prop-types';

const HeadAvatar = ({avatar})=>(

    <View style={styles.ctn}>
        <View style={styles.bg}/>
        {!avatar ? <Thumbnail large  source={require('../assets/usuario.png')} /> : <Thumbnail large  source={{uri: avatar }} />} 
    </View>

)


const styles = StyleSheet.create({
    ctn: {
        alignItems:'center',
        position:'relative',
        marginBottom:20
    },
    bg:{
        width:'100%',
        height:45,
        backgroundColor:'transparent',
        position:'absolute',
        borderBottomWidth:2,
        borderBottomColor:'#c7c7c7'
    }
    
});


HeadAvatar.proptypes = {
    avatar : Proptypes.string
}

export default HeadAvatar;