import {actionTypeUser} from '../actions/action-types'; 
const initialState = {
    isFetching:false,
    _id:'',
    role:'',
    confirmed:0,
    isConnected:0,
    status:0,
    name:'',
    email:'',
    sex:'',
    dateBirth:'',
    countryCode:'',
    numberPhone:'',
    idWonder:'',
    createdAt:'',
    selectAvatar:{},
    idAvatar:'',
    token:'',
    skills:[],
    profession:''
};

export default function user(state = initialState,action){
    switch(action.type){
        case actionTypeUser.isFetching:
            return Object.assign({},state,{isFetching:action.status});
            break;
        case actionTypeUser.LOGIN_SUCCESS:
            return Object.assign({},state,{...action.data});
            break;
        case actionTypeUser.RESTORE_SESSION:
            return Object.assign({},state,{...action.data});
            break;
        case actionTypeUser.SAVE_PERSONAL_INFORMATION:
            return Object.assign({},state,{...action.data});
            break;
        case actionTypeUser.UPDATE_AVATAR:
            return Object.assign({},state,{idAvatar:action.data})
            break;
        case actionTypeUser.UPDATE_SKILLS_LIST:
            return Object.assign({},state,{skills:action.data});
            break;
        case actionTypeUser.DELETE_SKILLS_LIST:
            return Object.assign({},state,{skills:[]});
            break;
        case actionTypeUser.UPDATE_PROFESSION:
            return Object.assign({},state,{},{profession:action.data});
            break;
        default:
            return state;
    }
}