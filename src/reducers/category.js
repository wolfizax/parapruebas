const initial =  [{label:'all'},{label:'family'},{label:'hobby'},{label:'work'},{label:'friends'}];

export default (state = initial, action) =>{
    
    switch (action.type) {
        case 'ADD_CATEGORY':
            return state.concat({label:action.text.toLowerCase()});
        case 'DELETE_CATEGORY':
        	return state.filter((item,i)=>{return i!=action.index});
         case 'EDIT_CATEGORY':
         	let index = state.findIndex(obj => obj.label==action.category);
         	return [
        		...state.slice(0, index),
        			// Copia el objeto antes de modificarlo
       			Object.assign({}, state[index], {
          			label: action.text
        		}),
        		...state.slice(index + 1)
      			]
        default:
            return state;
    }

}  
