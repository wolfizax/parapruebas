import { NavigationActions } from 'react-navigation';
import {actionTypeRoutes} from '../actions/action-types';
import { RootNavigator } from '../navigator/routes';


// Start with two routes: The Main screen, with the Login screen on top.
const firstAction = RootNavigator.router.getActionForPathAndParams('Main');
const tempNavState = RootNavigator.router.getStateForAction(firstAction);
const secondAction = RootNavigator.router.getActionForPathAndParams('HelpMenu');
const initialNavState = RootNavigator.router.getStateForAction(
  //secondAction, //SOLO PARA USO DE DESARROLLO
  tempNavState
);
function nav(state = initialNavState, action) {
  let nextState;
  switch (action.type) {
    case actionTypeRoutes.Main:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({routeName:'Main',params:action.params}),
        state
      );
      break;
    case actionTypeRoutes.NewUser:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'NewUser',params:action.params }),
        state
      );
      break;
    case actionTypeRoutes.NewUserValidationCode:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'NewUserValidationCode',params:action.params }),
        state
      );
      break;
    case actionTypeRoutes.NewUserCompletePerfil:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'NewUserCompletePerfil',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.Login:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Login',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.Logout:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Logout',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.LoginPassword:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'LoginPassword',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.LoginForgotPassword:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'LoginForgotPassword',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.LoginForgotValidationCode:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'LoginForgotValidationCode',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.LoginForgotChangePassword:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'LoginForgotChangePassword',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.Dashboard:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Dashboard',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.LeftMenu:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'LeftMenu',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.UserSetting:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'UserSetting',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.PrivacyMenu:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'PrivacyMenu',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.PrivacySetting:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'PrivacySetting',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.BlockedContacts:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'BlockedContacts',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.SecurityMenu:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'SecurityMenu',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.EmergencyContacts:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'EmergencyContacts',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.AssociateEmail:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'AssociateEmail',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.ChangeNumber:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ChangeNumber',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.ChangeNumberForm:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ChangeNumberForm',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.ChangeNumberValidationCode:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ChangeNumberValidationCode',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.SoundSettings:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'SoundSettings',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.StorageSettings:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'StorageSettings',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.StorageSettingsUseOfData:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'StorageSettingsUseOfData',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.StorageSettingsUseOfStorage:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'StorageSettingsUseOfStorage',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.HelpMenu:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'HelpMenu',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.Profile:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Profile',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.ProfilePersonalInformation:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ProfilePersonalInformation',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.ProfileProfessionalInformation:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ProfileProfessionalInformation',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.MyWalletPoints:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'MyWalletPoints',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.ChangePassword:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'ChangePassword',params:action.params }),
        state
      );
      break;
      case actionTypeRoutes.Chat:
      nextState = RootNavigator.router.getStateForAction(
        NavigationActions.navigate({ routeName: 'Chat',params:action.params }),
        state
      );
      break;
    default:
      nextState = RootNavigator.router.getStateForAction(action, state);
      break;
  }
  return nextState || state;
}
export default nav;