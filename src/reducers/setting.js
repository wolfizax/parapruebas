import {actionTypeSetting} from '../actions/action-types';

const initialState = {
    blockedUsersList: null,
    emergencyUsersList: null,
    messageTone:'Email',
    messageVibration:'Default',
    groupTone:'Email',
    groupVibration:'Default',
    callTone:'Email',
    callVibration:'Default',
    storage:{
        mobileData:{
            photos:false,
            audio:false,
            video:false,
            documents:false
        },
        wifi:{
            photos:false,
            audio:false,
            video:false,
            documents:false
        }
    }
};

export default function setting(state = initialState,action){
    switch(action.type){
        case actionTypeSetting.LOAD_SETTINGS:
            return Object.assign({},state,{...action.data});
            break;
        case actionTypeSetting.ADD_USER_BLOCKED_LIST:
            return Object.assign({},state,{blockedUsersList:action.data});
            break;
        case actionTypeSetting.REMOVE_USER_BLOCKED_LIST:
            return Object.assign({},state,{blockedUsersList:action.data});
            break;
        case actionTypeSetting.DELETE_USER_BLOCKED_LIST:
            return Object.assign({},state,{blockedUsersList:null});
            break;
        case actionTypeSetting.ADD_USER_EMERGENCY_LIST:
            return Object.assign({},state,{emergencyUsersList:action.data});
            break;
        case actionTypeSetting.REMOVE_USER_EMERGENCY_LIST:
            return Object.assign({},state,{emergencyUsersList:action.data});
            break;
        case actionTypeSetting.DELETE_USER_EMERGENCY_LIST:
            return Object.assign({},state,{emergencyUsersList:null});
            break;
        case actionTypeSetting.CHANGE_MESSAGE_NOTIFICATION_TONE:
            return Object.assign({},state,{messageTone:action.data});
            break
        case actionTypeSetting.CHANGE_MESSAGE_VIBRATION:
            return Object.assign({},state,{messageVibration:action.data});
            break;
        case actionTypeSetting.CHANGE_GROUP_NOTIFICATION_TONE:
            return Object.assign({},state,{groupTone:action.data});
            break;
        case actionTypeSetting.CHANGE_GROUP_VIBRATION:
            return Object.assign({},state,{groupVibration:action.data});
            break;
        case actionTypeSetting.CHANGE_CALL_NOTIFICATION_TONE:
            return Object.assign({},state,{callTone:action.data});
            break;
        case actionTypeSetting.CHANGE_CALL_VIBRATION:
            return Object.assign({},state,{callVibration:action.data});
            break;
        case actionTypeSetting.CHANGE_STORAGE_OPTIONS:
            return Object.assign({},state,{storage:action.data});
            break;
        default:
            return state;
    }
}