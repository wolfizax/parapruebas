import {combineReducers} from 'redux';
import app from './app';
import nav from './nav'; 
import user from './user';
import setting from './setting';
import category from './category';
import contacts from './contacts';
import members from './members';


const AppReducers = combineReducers({
    app,
    navState:nav,
    user,
    setting,
    category,
  	contacts,
  	members
});
export default AppReducers;