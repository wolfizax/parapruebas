const initial =  [
				{
					displayName:'ricardo perez',
					category :'family',
					number : '+584126534991',
					block : true
				},
				{
					displayName:'yessica marchi',
					category : 'work',
					number : '+584121234567',
					block : true
				},
				{
					displayName:'steven peralta',
					category :'family',
					number : '+584128912345',
					block : false
				},
				{
					displayName:'edgardo avendañoooo000',
					category : 'friends',
					number : '+584126531234',
					block : false
				},
				{
					displayName:'edgar marquez',
					category : 'friends',
					number : '+584126531232',
					block : false
				},
				{
					displayName:'mama',
					category : 'family',
					number : '+584126531234',
					block : false
				}

			];

export default (state = initial, action) =>{
    
    switch (action.type) {
    	case 'EDIT_CONTACTS':
    	 	return action.contacts;
    	case 'EDIT_CONTACTS_CATEGORY':
    	 	let index = state.findIndex(obj => obj.number==action.user.number);
         	return [
        		...state.slice(0, index),
        			// Copia el objeto antes de modificarlo
       			Object.assign({}, state[index],action.user),
        		...state.slice(index + 1)
      		];
      	case 'EDIT_BLOCK_CONTACTS':
      		return [
        		...state.slice(0, action.index),
        			// Copia el objeto antes de modificarlo
       			Object.assign({}, state[action.index],{
       				block : action.block
       			}),
        		...state.slice(action.index + 1)
      		];
      	case 'DELETE_CONTACT':  
      		return state.filter((item,i)=>{return i!=action.index});
        default:
            return state;
    }

}  
