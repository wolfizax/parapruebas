import { FOREGROUND, BACKGROUND, INACTIVE } from 'redux-enhancer-react-native-appstate';
import { actionTypeApp } from '../actions/action-types';

const initialState = {
    root:null,
	starting: true,
	ready: false,
	inactive: false,
	background: false
};

export default function app(state = initialState, action) {
	switch (action.type) {
		case FOREGROUND:
			return {
				...state,
				inactive: false,
				foreground: true,
				background: false
			};
		case BACKGROUND:
			return {
				...state,
				inactive: false,
				foreground: false,
				background: true
			};
		case INACTIVE:
			return {
				...state,
				inactive: true,
				foreground: false,
				background: false
			};
		case actionTypeApp.INIT:
			return {
				...state,
				ready: false,
				starting: true
			};
		case actionTypeApp.READY:
			return {
                ...state,
                root: action.state,
				ready: true,
				starting: false
			};
		default:
			return state;
	}
}