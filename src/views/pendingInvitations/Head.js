import React from 'react';
import {View,StyleSheet,Text} from 'react-native';
import {Header,Button} from 'native-base';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

const Head = ({title,goBack})=>(

      <Header style={styles.head}>  
        
        <Button transparent style={styles.icon} onPress = {()=>goBack()}>
            <Icon name='left-arrow' size={18} color="#3CD22F"  />
        </Button>

        <View >
            <Text style={styles.title}>{title}</Text>
        </View>
        
        <View />
                
    </Header>
   
)

const styles = StyleSheet.create({
    head:{
        height:50,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:'white',
        paddingLeft:10,
        paddingRight:10
    },
    icon:{
        marginTop:4
    },
    title:{
        color:'#3CD22F',
        fontSize:20,
    }

})

Head.proptypes = {
    title : Proptypes.string.isRequired,
    goBack : Proptypes.func.isRequired
}

export default Head;
