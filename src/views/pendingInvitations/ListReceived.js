import React from 'react';
import {StyleSheet,View,ActivityIndicator} from 'react-native';
import {List, ListItem, Left, Body, Thumbnail, Text,Badge,Button } from 'native-base';
import Icon from '../../presentation/Icon/index';
import {capitalize} from '../../utils/letter';
import Proptypes from 'prop-types';


const ListReceived = ({list,handleAcceptInvitation,handleDenyInvitation})=>(
	<List>
        {list.map((item,i)=>
            {if(item.status != "sending"){
            return(
                <ListItem key ={i} style={styles.list}>
                    <Left>
                        {!item.avatar ? <Thumbnail source={require('../../assets/usuario.png')} /> : <Thumbnail source={{uri: item.avatar }} />} 
                         <Body>
                            <Text>{capitalize(item.name)}</Text>
                            <Text note> sub-title </Text>
                        </Body>
                    </Left>

                    <View>
                        {item.progress ? <ActivityIndicator size="small" color="#22B307" /> : <View style={{flexDirection:'row'}} ><Button transparent onPress ={()=>handleAcceptInvitation(item,i)} style={{marginRight:20}}><Icon name="Acept" size={18} color="#22B307"/></Button><Button transparent onPress={()=>handleDenyInvitation(item,i)}  style={{marginRight:16}}><Icon name="SI-6" size={18} color="red"/></Button></View>}
                    </View>
                </ListItem>
            )}}

        )}
    </List>
)

const styles = StyleSheet.create({
    list:{
        marginLeft:10,
        marginRight:10
    }

})

ListReceived.proptypes = {
    list : Proptypes.array.isRequired,
    handleAcceptInvitation : Proptypes.func.isRequired,
    handleDenyInvitation : Proptypes.func.isRequired
}

export default ListReceived;