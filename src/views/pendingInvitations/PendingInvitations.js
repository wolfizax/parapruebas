import React,{Component} from 'react';
import {View,StyleSheet} from 'react-native';
import {Container,Content,Body,Text,Tab, Tabs,TabHeading} from 'native-base';
import ListSending from './ListSending';
import ListReceived from './ListReceived'
import Head from './Head';
import Badge from '../../presentation/BadgeCircle';

class PendingInvitations extends Component{
	constructor(props){
		super(props);
		this.state = {
			tab : 0,
			contacts : [
				{name:'ricado perez',status:'sending'},
				{name:'yessica marchi'},
			],
			groups : [
				{name:'wonderchatt'},
				{name:'ventas y subastas',status:'sending'},
			]
		}
	}

	handleContactsCancelInvitation(item,index){
		let contacts = this.state.contacts;
		contacts[index].progress = true;
		this.setState({...this.state,contacts});

		//aqui va el end-point
		let cont = 0;
		let interval =  setInterval(()=>{ 

			if(cont==4){
				contacts[index].progress = false;
				this.setState({...this.state,contacts});
				clearInterval(interval);
			}
			
			cont ++;
		}, 1000);
	}

	handleGroupsCancelInvitation(item,index){
		let groups = this.state.groups;
		groups[index].progress = true;
		this.setState({...this.state,groups});

		//aqui va el end-point
		let cont = 0;
		let interval =  setInterval(()=>{ 

			if(cont==4){
				groups[index].progress = false;
			this.setState({...this.state,groups});
				clearInterval(interval);
			}
			
			cont ++;
		}, 1000);
	}

	handleContactAcceptInvitation(item,index){
		let contacts = this.state.contacts;
		contacts[index].progress = true;
		this.setState({...this.state,contacts});

		//aqui va el end-point
		let cont = 0;
		let interval =  setInterval(()=>{ 

			if(cont==4){
				contacts[index].progress = false;
				this.setState({...this.state,contacts});
				clearInterval(interval);
			}
			
			cont ++;
		}, 1000);
	}

	handleContactDenyInvitation(item,index){
		let contacts = this.state.contacts;
		contacts[index].progress = true;
		this.setState({...this.state,contacts});

		//aqui va el end-point
		let cont = 0;
		let interval =  setInterval(()=>{ 

			if(cont==4){
				contacts[index].progress = false;
				this.setState({...this.state,contacts});
				clearInterval(interval);
			}
			
			cont ++;
		}, 1000);
	}

	handleGroupAcceptInvitation(item,index){
		let groups = this.state.groups;
		groups[index].progress = true;
		this.setState({...this.state,groups});

		//aqui va el end-point
		let cont = 0;
		let interval =  setInterval(()=>{ 

			if(cont==4){
				groups[index].progress = false;
			this.setState({...this.state,groups});
				clearInterval(interval);
			}
			
			cont ++;
		}, 1000);
	}

	handleGroupDenyInvitation(item,index){
		let groups = this.state.groups;
		groups[index].progress = true;
		this.setState({...this.state,groups});

		//aqui va el end-point
		let cont = 0;
		let interval =  setInterval(()=>{ 

			if(cont==4){
				groups[index].progress = false;
			this.setState({...this.state,groups});
				clearInterval(interval);
			}
			
			cont ++;
		}, 1000);
	}
		
	

	render(){
		return(
		<Container>
			<Head
				title = "Pending invitations"
				goBack = {()=>this.props.navigation.goBack()}
			/>

			<Content >

				<Tabs 
				tabContainerStyle={{elevation:0,borderBottomWidth:1,borderBottomColor:'#c7c7c7'}} 
				tabBarBackgroundColor={'white'} 
				tabBarUnderlineStyle={{backgroundColor:'black',height:1.5}}  
				initialPage = {this.state.tab}
				onChangeTab= {({i})=>this.setState({tab:i})}
				>
                	
					<Tab heading={<TabHeading style={{backgroundColor:'white'}}>{(this.state.tab == 0) ? <Text style={{color:'black'}}>Contacts</Text> : <Text style={{color:'#c7c7c7'}}>Contacts</Text>}<Badge number ={2} /></TabHeading>} tabStyle={{backgroundColor:'#2B2B2B'}} activeTabStyle={{ backgroundColor: '#2B2B2B',color:'#17D6B6' }} >
						<View style={styles.ctn}><Text style={styles.bold}>Sent</Text></View>
                    	<ListSending
                			list = {this.state.contacts}
                			handleCancelInvitation = {this.handleContactsCancelInvitation.bind(this)}
                			
                		/>
                		<View style={styles.ctn}><Text style={styles.bold}>Received</Text></View>
                		<ListReceived
                			list = {this.state.contacts}
                			handleAcceptInvitation = {this.handleContactAcceptInvitation.bind(this)}
                			handleDenyInvitation = {this.handleContactDenyInvitation.bind(this)}
                		/>

                	</Tab>
                	<Tab heading={<TabHeading style={{backgroundColor:'white'}}>{(this.state.tab == 1) ? <Text style={{color:'black'}}>Groups</Text> : <Text style={{color:'#c7c7c7'}}>Groups</Text>}<Badge number={2} /></TabHeading>} tabStyle={{backgroundColor:'#000000'}} activeTabStyle={{ backgroundColor: '#000000',color:'#17D6B6' }} >
                		<View style={styles.ctn}><Text style={styles.bold}>Sent</Text></View>
                    	<ListSending
                			list = {this.state.groups}
                			handleCancelInvitation = {this.handleGroupsCancelInvitation.bind(this)}
                		
                		/>
                		<View style={styles.ctn}><Text style={styles.bold}>Received</Text></View>
                		<ListReceived
                			list = {this.state.groups}
                			handleAcceptInvitation = {this.handleGroupAcceptInvitation.bind(this)}
                			handleDenyInvitation = {this.handleGroupDenyInvitation.bind(this)}
                		/>

                	</Tab>

                </Tabs>
			</Content>
		</Container>

		)
	}
}


const styles = StyleSheet.create({
	ctn:{
		paddingLeft:10,
		paddingRight:10,
		marginTop:10
	},
	bold:{
		fontWeight :'bold'
	},
	badge : {
        width:18,
        height:18,
        backgroundColor: '#17D6B6',
        borderRadius:9,
        justifyContent:'center',
        alignItems:'center'
    },
    textBadge:{
        fontSize:9,
        color:'white'
    }
})


export default PendingInvitations;