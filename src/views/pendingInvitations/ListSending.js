import React from 'react';
import {StyleSheet,View,ActivityIndicator} from 'react-native';
import {List, ListItem, Left, Body, Thumbnail, Text,Badge,Button } from 'native-base';
import Icon from '../../presentation/Icon/index';
import {capitalize} from '../../utils/letter';
import Proptypes from 'prop-types';


const ListSending = ({list,handleCancelInvitation})=>(
	<List>
        {list.map((item,i)=>
            {if(item.status == "sending"){
            return(
                <ListItem key ={i} style={styles.list}>
                    <Left>
                        {!item.avatar ? <Thumbnail source={require('../../assets/usuario.png')} /> : <Thumbnail source={{uri: item.avatar }} />} 
                        <Body>
                            <Text>{capitalize(item.name)}</Text>
                            <Text note> sub-title </Text>
                        </Body>
                    </Left>
                    
                    <View>
                        {item.progress ? <ActivityIndicator size="small" color="#22B307" /> : <Button transparent onPress={()=>handleCancelInvitation(item,i)}><Text style={{color:'#22B307'}}>Cancel</Text></Button>}
                    </View> 
                </ListItem>
            )}}

        )}
    </List>
)

const styles = StyleSheet.create({
    list:{
        marginLeft:10,
        marginRight:10,
    }

})

ListSending.proptypes = {
    list : Proptypes.array.isRequired,
    handleCancelInvitation : Proptypes.func.isRequired
}



export default ListSending;