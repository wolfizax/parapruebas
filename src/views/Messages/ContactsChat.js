import React from 'react';
import {StyleSheet,View} from 'react-native';
import {Content,List, ListItem, Left, Body, Thumbnail, Text } from 'native-base';
import Badge from '../../presentation/BadgeCircle';
import Time from '../../presentation/Time';
import {capitalize} from '../../utils/letter';
import Proptypes from 'prop-types';


const ContactsChat = ({messages,handleSelectChat})=>(
<Content>
	<List>
        {messages.map((item,i)=>
        <ListItem key ={i} style={styles.list} onPress={()=>handleSelectChat(i,item)}>
            <Left>
               {!item.avatar ? <Thumbnail  source={require('../../assets/usuario.png')} /> : <Thumbnail  source={{uri: item.avatar }} />} 
                <Body style={{flex:1}}>
                    <Text numberOfLines = { 1 }>{capitalize(item.displayName)}</Text>
                    <Text note numberOfLines = { 1 }> {item.text} </Text>
                </Body>
            </Left>
           
            <View style={styles.right}>
                <View style={styles.date}><Time date = {item.date} color = '#c7c7c7'/></View>
                <View style={{flex:1,alignItems:'flex-end'}}>{item.nroMsm !=0 && <Badge number={item.nroMsm} />}</View>
            </View>
        </ListItem>
        )}
    </List>
</Content>
)

const styles = StyleSheet.create({
    list:{
        marginRight:10,
        marginLeft:10,
        justifyContent:'space-between'
    },
    right:{
        height:40,
        justifyContent:'flex-start',
        alignItems:'flex-end',
        marginBottom:10,
        marginLeft:5
    },
    date:{
        height:16,
        justifyContent:'flex-start',
        marginBottom:3
    }

})

ContactsChat.proptypes = {
    messages : Proptypes.array.isRequired,
    handleSelectChat: Proptypes.func.isRequired
}



export default ContactsChat;