import React from 'react';
import {View,StyleSheet} from 'react-native';
import {Content,List, ListItem, Left, Body, Right, Thumbnail, Text} from 'native-base';
import Proptypes from 'prop-types';
import Badge from '../../presentation/BadgeCircle';
import Time from '../../presentation/Time';
import {capitalize} from '../../utils/letter';


const GroupsChat = ({messages,handleSelectGroupChat})=>(
<Content>
	<List>

        {messages.map((item,i)=>

        <ListItem  key = {i} style={styles.list} onPress={()=>handleSelectGroupChat(i,item)}>
            <Left>
                {!item.avatar ? <Thumbnail source={require('../../assets/usuario.png')} /> : <Thumbnail  source={{uri: item.avatar }} />} 
                <Body style={{flex:1}}>
                    <Text numberOfLines = {1}>{capitalize(item.name)}</Text>
                    <Text note numberOfLines = {1}>{item.displayName} : {item.text} </Text>
                </Body>
            </Left>

            <View style={styles.right}>
                <View style={styles.date}><Time date = {item.date} color = '#c7c7c7'/></View>
                <View style={{flex:1,alignItems:'flex-end'}}>{item.nroMsm !=0 && <Badge number={item.nroMsm} />}</View>
            </View>
            
        </ListItem>

        )}
    </List>
</Content>
)

const styles = StyleSheet.create({
    list:{
        marginRight:10,
        marginLeft:10,
        justifyContent:'space-between'
    },
    right:{
        height:40,
        justifyContent:'flex-start',
        alignItems:'flex-end',
        marginBottom:10
    },
    date:{
        height:16,
        justifyContent:'flex-start',
        marginBottom:3
    }

})

GroupsChat.proptypes = {
    messages : Proptypes.array.isRequired,
    handleSelectGroupChat : Proptypes.func.isRequired
}



export default GroupsChat;