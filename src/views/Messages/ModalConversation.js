import React from 'react';
import {StyleSheet,View,TouchableOpacity,Text,PixelRatio} from 'react-native';
import Modal from 'react-native-modal';
import Proptypes from 'prop-types';

const ModalConversation = ({modal,handleModal,navigation})=>(

	<Modal style={styles.modal} onBackdropPress={() => handleModal()} onBackButtonPress={() => handleModal()} isVisible={modal}>
        <View style={styles.modalContent}>
            <View style={styles.modalContentRow}>
                <TouchableOpacity onPress={()=>navigation('NewConversation')}><View style={styles.listOne}><Text>New conversation</Text></View></TouchableOpacity>
                <View style={styles.divider}/>
                <TouchableOpacity onPress={()=>navigation('AddMembers')}><View style={styles.listTwo}><Text>Create group</Text></View></TouchableOpacity>
            </View>
            
        </View>             
    </Modal>

)
const styles = StyleSheet.create({
    modal:{
        justifyContent:'flex-start'
    },
	modalContent:{
		width:'80%',
		marginLeft:'10%',
        backgroundColor:'white',
        borderRadius:7,
        justifyContent:'flex-start'
    },
    modalContentRow:{
        width:'100%',
        height:90
    },
    listOne:{
        paddingLeft:10,
        paddingRight:10,
        marginTop:10,
        marginBottom:10
    },
    listTwo:{
        paddingLeft:10,
        paddingRight:10,
        marginTop:10
    },
    divider:{
        width:'100%',
        height:1 / PixelRatio.getPixelSizeForLayoutSize(1),
        backgroundColor:'#AAAAAA',
        marginBottom:4,
        marginTop:4
    }
    
})


ModalConversation.proptypes = {
    modal: Proptypes.bool.isRequired,
    handleModal : Proptypes.func.isRequired,
    navigation : Proptypes.func.isRequired,
}



export default ModalConversation;