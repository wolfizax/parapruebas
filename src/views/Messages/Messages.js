import React,{Component} from 'react';
import { Container,Tab, Tabs,TabHeading,Text} from 'native-base';
import Head from './Head';
import ContactsChat from './ContactsChat';
import GroupsChat from './GroupsChat';
import ModalConversation from './ModalConversation';
import Icon from '../../presentation/Icon/index';
import {countChat} from '../../services/countChat';
import Badge from '../../presentation/BadgeCircle';

class Messages extends Component{

	 static navigationOptions ={
        tabBarIcon : ({tintColor})=>(
        	<Icon name='chat' size ={20} color={tintColor} />
        )
    }

    

	constructor(props){
		super(props);
		this.state = {
			modal : false,
			tab :0,
			groupChats : [
			{	
					name : 'wc',
					displayName:'edgar',
					text : 'e pale e pale e pale e pale e pale e palee pale e pale e pale e pale',
					nroMsm:2,
					date : new Date()
				},
				{
					name : 'subastas',
					displayName:'gean carlos',
					text : 'a como?',
					nroMsm:3,
					date :  new Date()

				}
			],
			chats : [
				{
					displayName:'ricardo perez',
					text : 'e pale e pale e pale e pale e pale e palee pale e pale e pale e pale',
					nroMsm:2,
					date : new Date()
				},
				{
					displayName:'yessica marchi',
					text : 'saludo',
					nroMsm:3,
					date :  new Date()

				},
				{
					displayName:'edgar wc',
					text : 'e pale',
					nroMsm:1,
					date :  new Date()

				},
				{
					displayName:'dervis wc',
					text : 'saludo',
					nroMsm:0,
					date : new Date()
				},
				{
					displayName:'pedro perez',
					text : 'e pale',
					nroMsm:1,
					date :  new Date()

				},
				{
					displayName:'maria db',
					text : 'saludo',
					nroMsm:0,
					date : new Date()
				}
			]
		}
		
	}

	handleSelectChat(i,message){
		let item = this.state.chats;
		item[i].nroMsm = 0;
		this.setState({...this.state,item})
	}

	handleSelectGroupChat(i,message){
		let item = this.state.groupChats;
		item[i].nroMsm = 0;
		this.setState({...this.state,item})
	}

	handleNavigation(routeName){
		this.setState({modal : false });
		this.props.navigation.navigate(routeName)
	}

	render(){

		let countChats = countChat(this.state.chats);
		let countgroupChats= countChat(this.state.groupChats);

		return(
			<Container>
				<Head 
					handleModal = {()=>this.setState({modal:!this.state.modal})}
				/>
				
					<Tabs 
					tabBarBackgroundColor={'white'} 
					tabBarUnderlineStyle={{backgroundColor:'black',height:1.5,color:'red'}} 
					tabContainerStyle={{elevation:1,backgroundColor:'white'}} 
					initialPage = {this.state.tab}
				 	onChangeTab= {({i})=>this.setState({tab:i})}
					>
                		<Tab 
                			heading={<TabHeading style={{backgroundColor:'white'}}>{(this.state.tab == 0) ? <Text style={{color:'black'}}>Chat</Text> : <Text style={{color:'#c7c7c7'}}>Chat</Text>}{countChats != 0 &&  <Badge number={countChats} />}</TabHeading>} >

                    		<ContactsChat
                    			style={{flex:1}}
                    			messages = {this.state.chats}
                    			handleSelectChat = {this.handleSelectChat.bind(this)}
                    		/>

                		</Tab>
                		<Tab heading={<TabHeading style={{backgroundColor:'white'}}>{(this.state.tab == 1) ? <Text style={{color:'black'}}>Groups</Text> : <Text style={{color:'#c7c7c7'}}>Groups</Text>}{countgroupChats != 0 &&  <Badge number ={countgroupChats} />}</TabHeading>}  >
        
                    		<GroupsChat      				
                    			messages = {this.state.groupChats}
                    			handleSelectGroupChat = {this.handleSelectGroupChat.bind(this)}
                    		/>

                		</Tab>
                	</Tabs>
				

				<ModalConversation 
					modal = {this.state.modal}
					handleModal = {()=>this.setState({modal:!this.state.modal})}
					navigation = {this.handleNavigation.bind(this)}
				/>

			</Container>

		)
	}
}

export default Messages;