import React, { Component } from 'react';
import LoggedView from '../../index';
import Icon from '../../../presentation/Icon/index';
import style from '../style';
import Modal from 'react-native-modal';
import {TouchableOpacity,TextInput} from 'react-native';
import ProfessionsList from '../../../utils/professions.js';
import { Container, Content, Row, Grid, Text, Item, Input, List, ListItem, Left, View, Col, Header, Right,Toast} from 'native-base';
import {connect} from 'react-redux';
import Loading from '../../../presentation/Loading';
import { setFetching, UPDATE_SKILLS_LIST,DELETE_SKILLS_LIST, UPDATE_PROFESSION} from '../../../actions/user';
import { userService } from '../../../services/user';
import ButtonGradient from '../../../presentation/ButtonGradient';

@connect(state => ({
    user:state.user,
    skills:typeof(state.user.skills) == 'string' && state.user.skills != '' ? JSON.parse(state.user.skills) : state.user.skills
}))
/** @extends Component */
export default class ProfileView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        headerLeft: (<TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ paddingLeft: 12 }}>
            <Icon name="SI-8" size={20} style={{color:'white'}} />
          </TouchableOpacity>),
        headerTintColor:'white',
        headerStyle:{backgroundColor:'#2B2B2B'},
        headerRight: (<Row></Row>),
        title:'Professional Informatión',
        headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}
      });
    constructor(props){
        super(props);
        this.state = {
            modalProfessions: false,
            professionSelectValue: this.props.user.profession == null ? 'Select' : this.props.user.profession,
            professionInputValue:'',
            professionsData: ProfessionsList,
            searchData:null,
            searchText:'',
            skillInputValue:'',
            skillsListDisplay:true
        }
    }
    componentDidMount(){
        this.getData();
    }
    getData = async () => {
        const {_id,token,role,idWonder} = this.props.user;
        this.props.dispatch(setFetching(true));
        let skills = await userService.getProfessionalSkills(_id,token,role);
        let profession = await userService.query(_id,token,role,idWonder);
        this.props.dispatch(setFetching(false));
        if(JSON.stringify(skills) != JSON.stringify(this.props.skills)){
            this.props.dispatch(UPDATE_SKILLS_LIST(skills));
        }
        if(this.props.user.profession != profession.profession){
            if(profession.profession == null){
                this.props.dispatch(UPDATE_PROFESSION(''));
            }else{
                this.props.dispatch(UPDATE_PROFESSION(profession.profession));
            }
        }
    }
    onSubmit = async () => {
        const {_id,token,role} = this.props.user;
        let result;
        if(this.state.professionSelectValue == 'others'){
            if(this.state.professionInputValue != ''){
                this.props.dispatch(setFetching(true));
                result = await userService.update(_id,token,role,{profession:this.state.professionInputValue});
                this.props.dispatch(setFetching(false));
                if(result){
                    this.props.dispatch(UPDATE_PROFESSION(this.state.professionInputValue));
                    Toast.show({
                        text:'Your professional profession was updated successfully.',
                        type:'success',
                        duration:4000,
                        position:'bottom'
                    });
                }else{
                    Toast.show({
                        text:'Sorry, a problem occurred in the process of update your professional profession. Try again.',
                        type:'warning',
                        duration:4000,
                        position:'bottom'
                    });
                }
            }else{
                Toast.show({
                    text:'The profession field is empty. Try again.',
                    type:'danger',
                    duration:4000,
                    position:'bottom'
                });
            }
        }else{
            this.props.dispatch(setFetching(true));
            result = await userService.update(_id,token,role,{profession:this.state.professionSelectValue});
            this.props.dispatch(setFetching(false));
            if(result){
                this.props.dispatch(UPDATE_PROFESSION(this.state.professionSelectValue));
                Toast.show({
                    text:'Your professional profession was updated successfully.',
                    type:'success',
                    duration:4000,
                    position:'bottom'
                });
            }else{
                Toast.show({
                    text:'Sorry, a problem occurred in the process of update your professional profession. Try again.',
                    type:'warning',
                    duration:4000,
                    position:'bottom'
                });
            }
        }
    }
    handleModal = () => {
        this.setState({...this.state,modalProfessions:!this.state.modalProfessions,searchText:'',searchData:null});
    }
    removeSkill = async (skill,id) => {
        const {_id,token,role} = this.props.user;
        this.props.dispatch(setFetching(true));
        result = await userService.removeProfessionalSkill(_id,token,role,{skill});
        this.props.dispatch(setFetching(false));
        if(result){
            let data = this.props.skills.filter((element,index) => {return index != id});
            if(data.length > 0){
                this.props.dispatch(UPDATE_SKILLS_LIST(data));
            }else{
                this.props.dispatch(DELETE_SKILLS_LIST());
            }
        }else{
            Toast.show({
                text:'Sorry, a problem occurred in the process of delete skill. Try again.',
                duration:4000,
                type:'danger',
                position:'bottom'
            }); 
        }
    }
    renderRowSkillsList = (skill,sectionID,id) => {
        return(<ListItem noBorder>
            <Left><Text allowFontScaling style={[style.fontRoboto]}>{skill}</Text></Left><Right><Icon name='SI-6' onPress={() => this.removeSkill(skill,id)} /></Right>
        </ListItem>)
    }
    renderModalProfessionsRow = (profession) => {
        return(
            <ListItem onPress={() => this.onSelectProfession(profession)} noBorder>
                <Left><Text allowFontScaling style={[style.fontRoboto]}>{profession.charAt(0).toUpperCase() + profession.slice(1)}</Text></Left>
            </ListItem>
        )
    }
    filterSearch = (text) => {
        let data = this.state.professionsData.filter((item) => {
            const name = item.toLowerCase();
            const textSearch = text.toLowerCase();
            return name.indexOf(textSearch) > -1
        })
        this.setState({...this.state,searchData:data,searchText:text})
    }
    onSelectProfession = (profession) => {
        this.setState({...this.state,professionSelectValue:profession,modalProfessions:!this.state.modalProfessions});
    }
    addSkill = async (text) => {
        const {_id,token,role} = this.props.user;
        if(!this.props.skills.find(skill => {return skill == text})){
            let data = this.props.skills.concat(text)
            this.props.dispatch(setFetching(true));
            result = await userService.addProfessionalSkill(_id,token,role,{skill:text});
            this.props.dispatch(setFetching(false));
            if(result){
                this.props.dispatch(UPDATE_SKILLS_LIST(data));
            }else{
                Toast.show({
                    text:'Sorry, a problem occurred in the process of add skill. Try again.',
                    duration:4000,
                    type:'danger',
                    position:'bottom'
                }); 
            }
            this.setState({...this.state,skillInputValue:''});
        }else{
            Toast.show({
                text:'Sorry, there is already an ability with the same name.',
                duration:4000,
                type:'warning',
                position:'bottom'
            });
        }
    }
    renderModalProfessions = () => {
        return(
            <View style={style.modalContainer}>
                <Modal isVisible={this.state.modalProfessions} onBackdropPress={() => this.handleModal()} onBackButtonPress={() => this.handleModal()}>
                    <View style={style.modalContentBlockedContacts}>
                        <Header style={style.searchBar} searchBar rounded noShadow>
                            <Item>
                                <Icon onPress={() => this.handleModal()} name="SI-8" />
                                <Input  placeholder="Search" value={this.state.searchText} onChangeText={(text) => {this.filterSearch(text)}} />
                                <Icon onPress={() => this.setState({...this.state,searchText:'',searchData:null})} name="SI-6" />
                            </Item>
                        </Header>
                        <View style={[style.modalContentRow,{height:'90%'}]}>
                            <List dataArray={this.state.searchData ? this.state.searchData : this.state.professionsData} renderRow={this.renderModalProfessionsRow} scrollEnabled />
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    render(){
        return(
            <Container>
                <Content padder>
                    {this.renderModalProfessions()}
                    <Grid style={style.containerForm}>
                        <Row style={[style.containerItem]}>
                            <Col size={0.4} style={{justifyContent:'center'}}>
                                <Text style={[style.fontBold,style.fontRoboto]}>Professión</Text>
                            </Col>
                            <Col>
                                <TouchableOpacity onPress={() => this.handleModal()}>
                                    <Text>{this.state.professionSelectValue.charAt(0).toUpperCase() + this.state.professionSelectValue.slice(1)}  <Icon name='SI-1' /></Text>
                                    {
                                        this.state.professionSelectValue == 'others' ?
                                        <TextInput value={this.state.professionInputValue} onChangeText={(text) => this.setState({...this.state,professionInputValue:text.replace(/[^a-zA-Z \s]/g, '')})} underlineColorAndroid='#bcbcbc' placeholder='¿What is your professión?' autoCapitalize='characters' returnKeyType='done' textContentType='jobTitle' />
                                        :
                                        null
                                    }
                                </TouchableOpacity>
                            </Col>
                        </Row>
                        <Row style={style.containerForm}>
                            <Col size={0.4}>
                                <Text style={[style.fontBold,style.fontRoboto]}>Descriptión of skills</Text>
                            </Col>
                            <Col>
                                <Item>
                                    <Input value={this.state.skillInputValue} onChangeText={(text) => this.setState({...this.state,skillInputValue:text.replace(/[^a-zA-Z \s]/g, '')})} placeholder='Title of your skill'/>
                                    <Icon onPress={() => this.state.skillInputValue == '' ? null : this.addSkill(this.state.skillInputValue)} name='SI-5' />
                                </Item>
                                <Row style={{marginVertical:10,padding:10}}><Text>Your skills ({this.props.skills != null ? this.props.skills.length : '0'})</Text><Right><Icon onPress={() => this.setState({...this.state,skillsListDisplay:!this.state.skillsListDisplay})} name={this.state.skillsListDisplay ? 'SI-0' : 'SI-1'} /></Right></Row>
                                <List scrollEnabled style={{display:this.state.skillsListDisplay ? 'flex' : 'none'}} dataArray={this.props.skills} renderRow={this.renderRowSkillsList} />
                            </Col>
                        </Row>
                    </Grid>
                </Content>
                {
                    this.state.professionSelectValue != this.props.user.profession && this.state.professionSelectValue != 'Select'
                    ?
                    <View>
                        <ButtonGradient full title='Save' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />  
                    </View>
                    :
                    null
                }
                <Loading visible={this.props.user.isFetching} />
            </Container>
        )
    }
}