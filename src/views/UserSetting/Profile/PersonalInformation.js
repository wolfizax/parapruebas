import React, {Component} from 'react';
import {Container, Content, Text, CheckBox, Toast, View} from 'native-base';
import {Grid,Row,Col} from 'react-native-easy-grid';
import {TouchableOpacity, TextInput, Image, Animated} from 'react-native';
import Permissions from 'react-native-permissions';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker'
import LoggedView from '../../index';
import {setFetching,SAVE_PERSONAL_INFORMATION,UPDATE_AVATAR} from '../../../actions/user';
import {Validation} from '../../../utils/validation';
import {userService} from '../../../services/user';
import {fileService} from '../../../services/file';
import ButtonGradient from '../../../presentation/ButtonGradient';
import Loading from '../../../presentation/Loading';
import Icon from '../../../presentation/Icon/index';
import moment from 'moment';
import {connect} from 'react-redux';
import style from '../style';

@connect(state => ({
    user:state.user
}))
/** @extends Component */
export default class ProfilePersonalInformationView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        headerLeft: (<TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ paddingLeft: 12 }}>
            <Icon name="SI-8" size={20} style={{color:'white'}} />
          </TouchableOpacity>),
        headerTintColor:'white',
        headerStyle:{backgroundColor:'#2B2B2B'},
        headerRight: (<Row></Row>),
        title:'Personal Information',
        headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}
      });

    constructor(props){
        super(props);
        this.state = {
            user:{
                name:props.user.name,
                dateBirth:props.user.dateBirth,
            },
            sexFemaleCheckbox:props.user.sex == 'F' ? true : false,
            sexMaleCheckbox:props.user.sex == 'M' ? true : false,
            errorInput:{},
            bounceValue: new Animated.Value(150),
            MultimediaOptionsBox:false,
            selectAvatar:{},
        };
    }
    isValid = (type) => {
        const { name, dateBirth} = this.state.user;
        this.setState({...this.state,errorInput:{name:''}});
        switch (type) {
            case 'name':
                if(Validation.name(name) === false){
                    this.setState({...this.state,errorInput:{name:'name'}});
                    Toast.show({
                        text:'The name field is empty or has no minimum 4 characters.',
                        type:'danger',
                        duration:4000,
                        position:'bottom'
                    });
                }else{
                    return true;
                }
                break;
            case 'sex':
                if(this.state.sexFemaleCheckbox == false && this.state.sexMaleCheckbox == false){
                    this.setState({...this.state,errorInput:{name:'sex'}});
                    Toast.show({
                        text:'You must choose a sexual gender to continue.',
                        type:'danger',
                        duration:4000,
                        position:'bottom'
                    });
                    return false;
                }else{
                    return true;
                }
                break;
            case 'dateBirth':
            if(moment().diff(dateBirth,'years') < 13){
                this.setState({...this.state,errorInput:{name:'dateBirth'}});
                Toast.show({
                    text:'You must be at least 13 years old.',
                    type:'danger',
                    duration:4000,
                    position:'bottom'
                });
                return false;
            }else{ return true}
            break;
            default:
                break;
        }
    }
    onSubmit = async () => {
        const {_id,token,role} = this.props.user;
        const {name,dateBirth} = this.state.user;
        let result;
        if(this.isValid('name')){
            if(this.isValid('dateBirth')){
                if(this.isValid('sex')){
                    this.props.dispatch(setFetching(true));
                    result = await userService.update(_id,token,role,{name,sex:this.state.sexFemaleCheckbox ? 'F' : 'M',dateBirth});
                    if(result != null){
                        this.props.dispatch(SAVE_PERSONAL_INFORMATION({name,sex:this.state.sexFemaleCheckbox ? 'F' : 'M',dateBirth}));
                        if(this.state.selectAvatar.type != undefined){
                            result = await userService.updateAvatar(_id,token,role,{uri:this.state.selectAvatar.uri,name:this.state.selectAvatar.name,type:this.state.selectAvatar.type})
                            if(result){
                                this.props.dispatch(UPDATE_AVATAR(result.idAvatar));
                                this.props.dispatch(setFetching(false));
                                Toast.show({
                                    text:'Your profile information was updated successfully.',
                                    type:'success',
                                    duration:4000,
                                    position:'bottom'
                                });
                            }else{
                                this.props.dispatch(setFetching(false));
                                Toast.show({
                                    text:'Sorry, an error occurred trying to communicate with the server. Try again',
                                    type:'danger',
                                    duration:4000,
                                    position:'bottom'
                                });
                            }
                        }else{
                            this.props.dispatch(setFetching(false));
                            Toast.show({
                                text:'Your profile information was updated successfully.',
                                type:'success',
                                duration:4000,
                                position:'bottom'
                            });
                        } 
                    }else{
                        this.props.dispatch(setFetching(false));
                        Toast.show({
                            text:'Sorry, an error occurred trying to communicate with the server. Try again',
                            type:'danger',
                            duration:4000,
                            position:'bottom'
                        });
                    }
                }
            }
        }
    }
    hadleMultimediaBox = () => {
        var toValue = 150;

        if(!this.state.MultimediaOptionsBox) {
          toValue = 0;
        }
        Animated.spring(
          this.state.bounceValue,
          {
            toValue: toValue,
            velocity: 3,
            tension: 2,
            friction: 8,
          }
        ).start();
    
        this.setState({...this.state,MultimediaOptionsBox: !this.state.MultimediaOptionsBox});
    }
    requestGalleryPermiss = async () => {
        let result;
        result = await Permissions.check('photo');
        if(result != 'authorized')
        {
            result = await Permissions.request('photo',{
                rationale: {
                    title: 'WonderChatt Gallery Permission',
                    message:
                        'WonderChatt needs access to gallery ' +
                        'of your phone device to continue with the registration.',
                    }
            });
            if(result == 'authorized')
            {
                const options = {
                    title: 'Select Avatar',
                    storageOptions: {
                        skipBackup: true,
                        path: 'images'
                    }};
                ImagePicker.launchImageLibrary(options, (result) => {
                this.setState({...this.state,selectAvatar:{type:result.type,name:result.fileName,data:result.data,uri:result.uri}});
                this.hadleMultimediaBox();
                });
            }else{
                Toast.show({
                    text:'Sorry, a problem occurred in the permission request process to access the photos on your mobile device.',
                    type:'warning',
                    duration:4000,
                    position:'bottom'
                });
                console.log('Error in checking permiss: PHOTO',err);
            }
        }else{
            const options = {
                title: 'Select Avatar',
                storageOptions: {
                    skipBackup: true,
                    path: 'images',
                },
                };
                ImagePicker.launchImageLibrary(options, (result) => {
                    this.setState({...this.state,selectAvatar:{type:result.type,name:result.fileName,data:result.data,uri:result.uri}});
                    this.hadleMultimediaBox();
                });
        }
    }
    requestCameraPermission = async () => 
    {
        let result;
        result = await Permissions.check('camera')
        if(result != 'authorized')
        {
            result = await Permissions.request('camera',{
            rationale: {
                title: 'WonderChatt Camera Permission',
                message:
                    'WonderChatt needs access to camera ' +
                    'of your phone device to continue with the registration.',
                }
            });
            if(result == 'authorized')
            {
                const options = {
                    title: 'Select Avatar',
                    storageOptions: {
                        skipBackup: true,
                        path: 'images',
                    },
                    };
                    ImagePicker.launchCamera(options, (result) => {
                        this.setState({...this.state,selectAvatar:{type:result.type,name:result.fileName,data:result.data,uri:result.uri}});
                        this.hadleMultimediaBox();
                    });
            }else{
                Toast.show({
                    text:'Sorry, there was a problem in the permission request process to use your mobile devices camera',
                    type:'warning',
                    duration:4000,
                    position:'bottom'
                });
                console.log('Error in checking permiss: CAMERA',err);
            }
        }else{
            const options = {
                title: 'Select Avatar',
                storageOptions: {
                    skipBackup: true,
                    path: 'images',
                },
                };
                ImagePicker.launchCamera(options, (result) => {
                    this.setState({...this.state, selectAvatar:{type:result.type,name:result.fileName,data:result.data,uri:result.uri}});
                    this.hadleMultimediaBox();
                });
        }
    }
    renderMultimediaBox = () =>{
        return (
            <Animated.View
            style={[style.multimediaBox,
              {transform: [{translateY: this.state.bounceValue}]}]}
          >
            <Row style={{height:10}}>
                <Col style={{alignItems:'flex-end',padding:10}} >
                    <Icon onPress={() => this.hadleMultimediaBox()} name="SI-6" size={17} style={{color:'black'}} />
                </Col>
            </Row>
            <Row>
                <Col style={[style.multimediaBoxItem,{alignItems:'flex-end'}]}>
                    <TouchableOpacity onPress={() => this.requestCameraPermission()}>
                        <Image source={require('../../../assets/photo-camera.png')} />
                    </TouchableOpacity>
                </Col>
                <Col style={style.multimediaBoxItem}>
                    <TouchableOpacity onPress={() => this.requestGalleryPermiss()}>
                        <Image source={require('../../../assets/gallery.png')} />
                    </TouchableOpacity>
                </Col>
                </Row>
          </Animated.View>
        );
    }
    render(){
        return(
            <Container>
                <Loading visible={this.props.user.isFetching} />
                <Content padder>
                    <Grid style={style.containerForm}>
                        <Row style={style.containerItem} >
                            <Col size={0.2} style={style.containerAvatar}>
                                <TouchableOpacity onPress={() => this.hadleMultimediaBox()}>
                                {
                                    this.props.user.idAvatar == '' && this.state.selectAvatar.type == undefined ?
                                        <Image style={{width:65,height:64}} source={require('../../../assets/userAvatar.png')} />
                                    :
                                        <Image style={{width:65,height:64}} source={this.props.user.idAvatar != '' && this.state.selectAvatar.type == undefined ? {uri:fileService.view(this.props.user.idAvatar)} : {uri:`data:${this.state.selectAvatar.type};base64,${this.state.selectAvatar.data}`}} />
                                }
                                </TouchableOpacity>
                            </Col>
                            <Col style={{justifyContent:'center'}}>
                                <Text style={[style.fontBold,style.fontRoboto]}>Name</Text>
                                <TextInput onFocus={() => this.state.MultimediaOptionsBox ? this.hadleMultimediaBox() : null} onBlur={() => this.isValid('name')} autoCorrect={false} returnKeyType='next' textContentType="name"  value={this.state.user.name} underlineColorAndroid={this.state.errorInput.name == 'name' ? 'red' : '#bcbcbc'} onChangeText={(text) => this.setState({...this.state,user:{...this.state.user,name:text.replace(/[^a-zA-Z0-9\s]/g, '')}})} />
                            </Col>
                        </Row>
                        <Row style={[style.containerItem,style.containerBirthdate]}>
                            <Col size={0.4} style={{justifyContent:'center'}}>
                                <Text style={[style.fontBold,style.fontRoboto]}>Birthdate</Text>
                            </Col>
                            <Col>
                                <DatePicker
                                    style={{width: 200,margin:10}}
                                    date={this.state.user.dateBirth}
                                    mode="date"
                                    placeholder="select date"
                                    format="YYYY-MM-DD"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                    dateText:{
                                        color:'white',
                                        fontWeight:'bold',
                                        fontSize:19,
                                    },
                                    dateInput: {
                                        borderRadius:3,
                                        borderColor:this.state.errorInput.name == 'dateBirth' ? 'red' : 'transparent',
                                        backgroundColor:'#17D6B6'
                                    }}}
                                    onDateChange={(date) => {this.setState({...this.state,user:{...this.state.user,dateBirth:date}});this.isValid('dateBirth')}}
                                />
                            </Col>
                        </Row>
                        <Row style={[style.containerSexs]}>
                            <Col size={0.5}><Text style={[style.fontBold,style.fontRoboto]}>Sex</Text></Col>
                            <CheckBox color={this.state.errorInput.name == 'sex' ? 'red' : '#17D6B6'} style={style.checkBoxsSex} checked={this.state.sexFemaleCheckbox} onPress={() => this.state.sexFemaleCheckbox ? this.setState({...this.state,sexFemaleCheckbox:!this.state.sexFemaleCheckbox}) : this.setState({...this.state,sexFemaleCheckbox:true,sexMaleCheckbox:false})} />
                            <Text style={style.fontRoboto} onPress={() => this.state.sexFemaleCheckbox ? this.setState({...this.state,sexFemaleCheckbox:!this.state.sexFemaleCheckbox}) : this.setState({...this.state,sexFemaleCheckbox:true,sexMaleCheckbox:false})}>Female</Text>
                            <CheckBox color={this.state.errorInput.name == 'sex' ? 'red' : '#17D6B6'} style={style.checkBoxsSex} checked={this.state.sexMaleCheckbox}  onPress={() => this.state.sexMaleCheckbox ? this.setState({...this.state,sexMaleCheckbox:!this.state.sexMaleCheckbox}) : this.setState({...this.state,sexMaleCheckbox:true,sexFemaleCheckbox:false})} />
                            <Text style={style.fontRoboto} onPress={() => this.state.sexMaleCheckbox ? this.setState({...this.state,sexMaleCheckbox:!this.state.sexMaleCheckbox}) : this.setState({...this.state,sexMaleCheckbox:true,sexFemaleCheckbox:false})}>Male</Text>
                        </Row>
                    </Grid>
                </Content>      
                <View>
                    <ButtonGradient uppercase={false} full title='Save' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />  
                </View>
                {this.renderMultimediaBox()}
            </Container>
        );
    }
}