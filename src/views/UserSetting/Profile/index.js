import React, { Component } from 'react';
import LoggedView from '../../index';
import Icon from '../../../presentation/Icon/index';
import HeaderView from '../../../presentation/Header';
import style from '../style';
import {connect} from 'react-redux';
import { Container, Content, Row, Text, Grid, H2, List, ListItem, Left, Right, Switch} from 'native-base';
import {ProfilePersonalInformation,ProfileProfessionalInformation,LeftMenu,ChangePassword} from '../../../actions/nav';

@connect(state => ({
    user:state.user,
}))
/** @extends Component */
export default class ProfileView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    tabBarIcon: ({tintColor}) => (<Icon name='avatar' color={tintColor ? tintColor : '#C7C7C7'} size={20} />)
    });
    constructor(props){
        super(props);
        this.state ={
            pushNotification:true,
            myWallet:true,
            nearby:true
        }
    }
    render(){
        return(
            <Container>
                <HeaderView avatar={{url:this.props.user.idAvatar}} title='Wonder Profile' leftComponent={{icon:'I-30-Menu',function:() => this.props.navigation.dispatch(LeftMenu())}} />
                <Content>
                    <Grid>
                        <Row style={[style.contentCenter]}>
                            <H2 allowFontScaling style={[style.fontRoboto,style.fontBold,style.textCenter]}>{this.props.user.name}</H2>
                        </Row>
                        <Row style={[style.contentCenter,{margin:0}]}>
                            <Text allowFontScaling style={[style.fontRoboto,{fontSize:20,color:'#C7C7C7'}]}>{`+${this.props.user.countryCode} ${this.props.user.numberPhone}`}</Text>
                        </Row>
                        <List style={{marginVertical:10}}>
                            <ListItem style={style.listItem} onPress={() => this.props.navigation.dispatch(ProfilePersonalInformation())}>
                                <Left><Text allowFontScaling style={[style.fontRoboto]}>Personal informatión</Text></Left><Right><Icon style={style.listItemIcon} name='SI-7' size={14}/></Right>
                            </ListItem >
                            <ListItem style={style.listItem} onPress={() => this.props.navigation.dispatch(ProfileProfessionalInformation())}>
                                <Left><Text allowFontScaling style={[style.fontRoboto]}>Professional informatión</Text></Left><Right><Icon style={style.listItemIcon} name='SI-7' size={14}/></Right>
                            </ListItem>
                            <ListItem style={style.listItem} onPress={() => this.props.navigation.dispatch(ChangePassword())}>
                                <Left><Text allowFontScaling style={[style.fontRoboto]}>Change Password</Text></Left><Right><Icon style={style.listItemIcon} name='SI-7' size={14}/></Right>
                            </ListItem>
                            <ListItem style={style.listItem}>
                                <Left><Text allowFontScaling style={[style.fontRoboto]}>My QR code</Text></Left><Right><Icon style={style.listItemIcon} name='I-17-Moments' size={14}/></Right>
                            </ListItem>
                        </List>
                        <Row style={{marginLeft:10}}>
                            <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Settings</Text>
                        </Row>
                        <List>
                            <ListItem style={style.listItem}>
                                <Left><Text allowFontScaling style={[style.fontRoboto]}>Push notificatión</Text></Left><Right><Switch value={this.state.pushNotification} onValueChange={() => this.setState({...this.state,pushNotification:!this.state.pushNotification})} trackColor={{true:'#17D6B6'}} /></Right>
                            </ListItem>
                            <ListItem style={style.listItem}>
                                <Left><Text allowFontScaling style={[style.fontRoboto]}>Nearby</Text></Left><Right><Switch value={this.state.nearby} onValueChange={() => this.setState({...this.state,nearby:!this.state.nearby})} trackColor={{true:'#17D6B6'}} /></Right>
                            </ListItem>
                            {/* <ListItem onPress={() => this.props.navigation.dispatch(MyWalletPoints())}>
                                <Left><Text allowFontScaling style={[style.fontRoboto]}>25.000 USD</Text></Left><Right><Switch value={this.state.myWallet} onValueChange={() => this.setState({...this.state,myWallet:!this.state.myWallet})} trackColor={{true:'#17D6B6'}} /></Right>
                            </ListItem> */}
                        </List>
                    </Grid>
                </Content>
            </Container>
        )
    }
}