import React, { Component } from 'react';
import {TouchableOpacity, TextInput} from 'react-native';
import LoggedView from '../index';
import { Container, Content, Row, Text, Grid,H3, Col, Toast } from 'native-base';
import ButtonGradient from '../../presentation/ButtonGradient';
import {Validation} from '../../utils/validation';
import Icon from '../../presentation/Icon/index';
import style from './style';

/** @extends Component */
export default class AssociateEmailView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'Associate Email',
    headerTitleStyle: {flex: 1, textAlign: 'center', color:'white'},
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerLeft: (
        <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ paddingLeft: 12 }}>
            <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>
    ),
    headerRight: (<Row></Row>)});
    constructor(props){
        super(props);
        this.state = {
            inputEmail:'',
            errorInput:{}
        }
    }
    isValid = () => {
        this.setState({...this.state,errorInput:{}})
        if(Validation.email(this.state.inputEmail) === false){
            this.setState({...this.state,errorInput:{name:'email'}});
            Toast.show({
                text:'The email field is empty or does not have a correct format.',
                type:'danger',
                duration:4000,
                position:'bottom'
            });
        }else{
            return true;
        }
    }
    render(){
        return(
            <Container>
                <Content padder>
                    <Grid>
                        <Row style={{marginVertical:20,borderBottomColor:'#C7C7C7',borderBottomWidth:2}}>
                            <H3 allowFontScaling style={[style.fontRoboto,{padding:10}]}>Enter your primary email</H3>
                        </Row>
                        <Row style={style.verticalCenter}>
                            <Col size={0.2}>
                                <Text style={[style.fontBold,style.fontRoboto,style.textCenter]}>Email</Text>
                            </Col>
                            <Col>
                            <TextInput underlineColorAndroid={this.state.errorInput.name == 'email' ? 'red' : '#bcbcbc'} value={this.state.inputEmail} onBlur={() => this.isValid()} onChangeText={(text) => this.setState({...this.state,inputEmail:text})} autoCapitalize='none' autoCorrect={false} returnKeyType='next'  textContentType="emailAddress" keyboardType="email-address" />
                            </Col>
                        </Row>
                        <Row style={style.associateEmailFooterForm}>
                            <ButtonGradient disabled={this.state.inputEmail ? false : true}  onSubmit={() => null} title='Associate' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} />
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}