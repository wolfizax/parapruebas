import React, { Component } from 'react';
import {TouchableOpacity, Linking} from 'react-native';
import LoggedView from '../index';
import { Container, Content, List, ListItem, Left, Row, Text } from 'native-base';
import Icon from '../../presentation/Icon/index';
import style from './style';

/** @extends Component */
export default class HelpMenuView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'Help',
    headerTitleStyle: {flex: 1, textAlign: 'center', color:'white'},
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerLeft: (
        <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ paddingLeft: 12 }}>
            <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>
    ),
    headerRight: (<Row></Row>)});
    render(){
        return(
            <Container>
                <Content padder>
                    <List>
                        <ListItem onPress={() => Linking.openURL('https://www.wonderchatt.com')}><Left><Text>Frequent questions</Text></Left></ListItem>
                        <ListItem><Left><Text>Contact us</Text></Left></ListItem>
                        <ListItem onPress={() => Linking.openURL('https://www.wonderchatt.com')}><Left><Text>Condition and privacy</Text></Left></ListItem>
                        <ListItem ><Left><Text>Info of the applicatión</Text></Left></ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}