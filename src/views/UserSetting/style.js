import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    contentCenter:{
        justifyContent:'center',
        margin:10
    },
    fontRoboto:{
        fontFamily:'Roboto'
    },
    textCenter:{
        textAlign:'center'
    },
    verticalCenter:{
        alignItems:'center'
    },
    fontBold:{
        fontWeight:'bold'
    },
    iconOptions:{
        backgroundColor:'#E3FFFB',
        borderRadius:100,
        padding:5,
        marginRight:5
    },
    btnDeleteAccount:{
        backgroundColor:'red',
        marginTop:30
    },
    bottomBorder:{
        borderBottomWidth:2,
        borderBottomColor:'#C7C7C7',
    },
    textDescription:{
        fontFamily:'Roboto',
        fontSize:18,
        color:'#2B2B2B'
    },
    rowSetting:{
        margin:10,
        paddingLeft:10,
    },
    settingTitle:{
        fontWeight:'bold',
        fontSize:19,
        fontFamily:'Roboto',
        color:'#2B2B2B',
    },
    settingOption:{
        fontFamily:'Roboto',
        fontSize:18,
        marginLeft:10
    },
    settingSubOption:{
        fontFamily:'Roboto',
        fontSize:18,
        marginLeft:20
    },
    settingOptionValue:{
        color:'#C7C7C7'
    },
    modalContainer:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    modalContent:{
        backgroundColor:'white',
        padding:20,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:5,
        borderColor:"rgba(0, 0, 0, 0.1)"
    },
    modalContentBlockedContacts:{
        backgroundColor:'white',
        borderRadius:5,
        borderColor:"rgba(0, 0, 0, 0.1)"
    },
    modalContentRow:{
        width:'100%',
        flexWrap:'wrap',
        alignItems:'flex-start',
        flexDirection:'row',
        alignItems:'center',
        padding:5
    },
    modalHeaderBTNClose:{
        right:0,
        position:'absolute'
    },
    modalBTNClose:{
       width:'100%',
       flexDirection:'row',
       justifyContent:'flex-end',
    },
    modalFooter:{
        borderTopColor:'#C7C7C7',
        borderTopWidth:2
    },
    modalFooterBTN:{
        margin:10
    },
    checkBoxOption:{
        borderColor:'#C7C7C7',
        marginRight:15
    },
    textJustify:{
        textAlign:'justify'
    },
    textStatusUser:{
        color: '#C7C7C7',
        marginTop:5
    },
    itemContactOptions:{
        alignItems:'center',
        flex:0.3,
        flexDirection:'row'
    },
    icon:{
        color:'#16D6B6',
        fontSize:20,
        marginHorizontal:10
    },
    searchBar:{
        backgroundColor:'transparent',
        borderWidth: 0, //no effect
        shadowColor: 'white', //no effect
        borderBottomColor: 'transparent',
        borderTopColor: 'transparent'
    },
    listBlockContainer:{
        height:58,
        margin:10,
        marginLeft:20
    },
    listBlockedContactsContainer:{
        backgroundColor:'#EEEEEE',
        alignItems:'center',
        borderRadius:30,
        paddingRight:5,
        marginHorizontal:5
    },
    itemAvatarListBlock:{
        borderRadius:30,
        marginRight:5
    },
    iconItemListBlock:{
        marginHorizontal:5
    },
    associateEmailFooterForm:{
        justifyContent:'flex-end',
        marginVertical:20
    },
    checkBoxCircleOption:{
        borderColor:'#C7C7C7',
        marginRight:15,
        borderRadius:10
    },
    itemModalSound:{
        paddingVertical:20
    },
    containerForm:{
        padding:10
    },
    containerItem:{
        marginVertical:5,
        justifyContent:'center'
    },
    containerAvatar:{
        marginRight:10
    },
    containerBirthdate:{
        borderBottomColor:'#bcbcbc',
        borderBottomWidth:2,
        paddingBottom:5
    },
    contaierFormPhoneInfo:{
        marginVertical:20
    },
    containerSexs:{
        paddingVertical:10,
        borderBottomColor:'#bcbcbc',
        borderBottomWidth:2,
    },
    checkBoxsSex:{
        marginHorizontal:20
    },
    multimediaBox: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#FFFFFF",
        height: 150,
    },
    multimediaBoxItem:{
        justifyContent:'center',
        marginHorizontal:10
    },
    walletPointsTitle:{
        fontSize:30
    },
    walletPointIconItem:{
        marginRight:10
    },
    walletPointTextOption:{
        color:'#16D6B6'
    },
    walletModalOptionsButtonActivated:{
        backgroundColor:'#17D6B6',
        borderColor:'#17D6B6',
        borderWidth:1,
        marginHorizontal:10
    },
    walletModalOptionsButtonDesactivated:{
        backgroundColor:'transparent',
        borderColor:'#17D6B6',
        borderWidth:1,
        marginHorizontal:10
    },
    walletModalOptionsCardTextDesactivated:{
        color:'#17D6B6'
    },
    walletModalOptionsCardTextActivated:{
        color:'white'
    },
    itemForm:{
        borderColor:'#bcbcbc'
    },
    listItem:{
        marginLeft:10,
        marginRight:10
    },
    listItemIcon:{
        marginRight:5
    }
});