import React, { Component } from 'react';
import LoggedView from '../../index';
import Icon from '../../../presentation/Icon/index';
import style from '../style';
import Modal from 'react-native-modal';
import {TouchableOpacity, Image, TextInput} from 'react-native';
import { Container, Content, Row, Grid, Text, H1, List, ListItem, Left, Right, View, Button, Col, Input} from 'native-base';


/** @extends Component */
export default class MyWalletPoints extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        headerLeft: (<TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ paddingLeft: 12 }}>
            <Icon name="SI-8" size={20} style={{color:'white'}} />
          </TouchableOpacity>),
        headerTintColor:'white',
        headerStyle:{backgroundColor:'#2B2B2B'},
        headerRight: (<Row></Row>),
        title:'My Wallet Points',
        headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}
      });
    constructor(props){
        super(props);
        this.state = {
            modalAddCard:false,
            modalViewCard:false,
            modalDeleteCard:false,
            modalConfiguration:false,
            maxDailyAmount:'100',
            maxTransactionAmount:'150',
            numberCard:'',
            nameCard:'',
            expireDateCard:'',
            csvCard:'',
            typeCard:'credit'
        }
    }
    renderTypeCardImage = () => {
        let typeCard = this.state.numberCard.substr(0,1);
        if(typeCard == 4){
            return(<Col><Image source={require('../../../assets/visa-icon.png')} /></Col>)
        }else if(typeCard == 5){
            return(<Col><Image source={require('../../../assets/mastercard-icon.png')} /></Col>)
        }
    }
    handleModal = (type) => {
        if(type == 'add'){
            this.setState({...this.state,modalAddCard:!this.state.modalAddCard,nameCard:'',numberCard:'',expireDateCard:'',csvCard:''})
        }else if(type == 'view'){
            this.setState({...this.state,modalViewCard:!this.state.modalViewCard,numberCard:'5'})
        }else if(type == 'delete'){
            this.setState({...this.state,modalDeleteCard:!this.state.modalDeleteCard})
        }else if(type == 'configuration'){
            this.setState({...this.state,modalConfiguration:!this.state.modalConfiguration})
        }
    }
    renderModalConfiguration = () => {
        return(
            <View style={style.modalContainer}>
                <Modal isVisible={this.state.modalConfiguration} onBackdropPress={() => this.handleModal('configuration')} onBackButtonPress={() => this.handleModal('configuration')}>
                    <View style={style.modalContent}>
                        <View style={style.modalContentRow}>
                            <View style={style.modalContentRow}>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Maximum daily amount</Text>
                            </View>
                            <View style={style.modalContentRow}>
                                <Col size={5}>
                                    <TextInput keyboardType='number-pad' value={this.state.maxDailyAmount} onChangeText={(text) => this.setState({...this.state,maxDailyAmount:text.replace(/[^0-9]/g,'')})}  underlineColorAndroid='#bcbcbc' />
                                </Col>
                                <Col size={10}>
                                    <Text allowFontScaling>Points</Text>
                                </Col>
                            </View>
                            <View style={style.modalContentRow}>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Maximum transaction amount</Text>
                            </View>
                            <View style={style.modalContentRow}>
                                <Col size={5}>
                                    <TextInput keyboardType='number-pad' value={this.state.maxTransactionAmount} onChangeText={(text) => this.setState({...this.state,maxTransactionAmount:text.replace(/[^0-9]/g,'')})}  underlineColorAndroid='#bcbcbc' />
                                </Col>
                                <Col size={10}>
                                    <Text allowFontScaling>Points</Text>
                                </Col>
                            </View>
                            <View style={[style.modalContentRow,style.contentCenter]}>
                                <Button style={style.modalFooterBTN} onPress={() => this.handleModal('configuration')} primary><Text allowFontScaling>Save</Text></Button>
                                <Button style={style.modalFooterBTN} onPress={() => this.handleModal('configuration')} light><Text allowFontScaling>Cancel</Text></Button>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    renderViewCardModal = () => {
        return(
            <View style={style.modalContainer}>
                <Modal isVisible={this.state.modalViewCard} onBackdropPress={() => this.handleModal('view')} onBackButtonPress={() => this.handleModal('view')}>
                    <View style={style.modalContent}>
                        <View style={style.modalContentRow}>
                            <Col size={10}>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Number</Text>
                                <TextInput value='5105105105105100' editable={false} selectTextOnFocus={false} underlineColorAndroid='#bcbcbc' />
                            </Col>
                            {this.renderTypeCardImage()}
                        </View>
                        <View style={style.modalContentRow}>
                            <Col>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Name</Text>
                                <TextInput value='Edgar Francisco Marquez Cardenas' editable={false} selectTextOnFocus={false}  underlineColorAndroid='#bcbcbc' />
                            </Col>
                        </View>
                        <View style={style.modalContentRow}>
                            <Col>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Expiratión date</Text>
                                <TextInput value='****/**' editable={false} selectTextOnFocus={false} underlineColorAndroid='#bcbcbc'/>
                            </Col>
                            <Col>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>CSV</Text>
                                <TextInput value='****' editable={false} selectTextOnFocus={false} underlineColorAndroid='#bcbcbc' />
                            </Col>
                        </View>
                        <View style={[style.modalContentRow,style.contentCenter]}>
                            <Button style={style.modalFooterBTN} onPress={() => this.handleModal('view')} light><Text allowFontScaling>Close</Text></Button>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    renderDeleteCardModal = () => {
        return(
            <View style={style.modalContainer}>
                <Modal isVisible={this.state.modalDeleteCard} onBackdropPress={() => this.handleModal('delete')} onBackButtonPress={() => this.handleModal('delete')}>
                    <View style={style.modalContent}>
                        <View style={[style.modalContentRow,style.contentCenter]}>
                            <Text allowFontScaling>¿Are you sure you want to delete your credit card ************5100 registration?</Text>
                        </View>
                        <View style={[style.modalContentRow,style.contentCenter]}>
                            <Button style={style.modalFooterBTN} onPress={() => this.handleModal('delete')} primary><Text allowFontScaling>Accept</Text></Button>
                            <Button style={style.modalFooterBTN} onPress={() => this.handleModal('delete')} light><Text allowFontScaling>Cancel</Text></Button>
                        </View>
                    </View>
                </Modal>
            </View>)
    }
    renderAddCardModal = () => {
        return(
            <View style={style.modalContainer}>
                <Modal isVisible={this.state.modalAddCard} onBackdropPress={() => this.handleModal('add')} onBackButtonPress={() => this.handleModal('add')}>
                    <View style={style.modalContent}>
                        <View style={style.modalContentRow}>
                            <Button onPress={() => this.setState({...this.state,typeCard:'credit'})} rounded transparent small style={this.state.typeCard == 'credit' ? style.walletModalOptionsButtonActivated : style.walletModalOptionsButtonDesactivated}><Text allowFontScaling style={this.state.typeCard == 'credit' ? [style.fontBold,style.fontRoboto,style.walletModalOptionsCardTextActivated] : [style.fontBold,style.fontRoboto,style.walletModalOptionsCardTextDesactivated]}>Credit</Text></Button>
                            <Button onPress={() => this.setState({...this.state,typeCard:'debit'})} rounded transparent small style={this.state.typeCard == 'debit' ? style.walletModalOptionsButtonActivated : style.walletModalOptionsButtonDesactivated}><Text allowFontScaling style={this.state.typeCard == 'debit' ? [style.fontBold,style.fontRoboto,style.walletModalOptionsCardTextActivated] : [style.fontBold,style.fontRoboto,style.walletModalOptionsCardTextDesactivated]}>Debit</Text></Button>
                        </View>
                        <View style={style.modalContentRow}>
                            <Col size={10}>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Number</Text>
                                <TextInput value={this.state.numberCard} onChangeText={(text) => this.setState({...this.state,numberCard:text.replace(/[^0-9]/g,'')})} underlineColorAndroid='#bcbcbc' />
                            </Col>
                            {this.renderTypeCardImage()}
                        </View>
                        <View style={style.modalContentRow}>
                            <Col>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Name</Text>
                                <TextInput value={this.state.nameCard} onChangeText={(text) => this.setState({...this.state,nameCard:text.replace(/[^a-zA-Z \s]/g,'')})} underlineColorAndroid='#bcbcbc' />
                            </Col>
                        </View>
                        <View style={style.modalContentRow}>
                            <Col>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Expiratión date</Text>
                                <TextInput value={this.state.expireDateCard} onChangeText={(text) => this.setState({...this.state,expireDateCard:text.replace(/[^0-9\/]/g,'')})} underlineColorAndroid='#bcbcbc'  maxLength={6} />
                            </Col>
                            <Col>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>CSV</Text>
                                <TextInput value={this.state.csvCard} onChangeText={(text) => this.setState({...this.state,csvCard:text.replace(/[^0-9]+/g,'')})} maxLength={4} underlineColorAndroid='#bcbcbc' />
                            </Col>
                        </View>
                        <View style={[style.modalContentRow,style.contentCenter]}>
                            <Button style={style.modalFooterBTN} onPress={() => this.handleModal('add')} primary><Text allowFontScaling>Accept</Text></Button>
                            <Button style={style.modalFooterBTN} onPress={() => this.handleModal('add')} light><Text allowFontScaling>Cancel</Text></Button>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    render(){
        return(
            <Container>
                <Content padder>
                {this.renderAddCardModal()}
                {this.renderViewCardModal()}
                {this.renderDeleteCardModal()}
                {this.renderModalConfiguration()}
                    <Grid style={style.containerForm}>
                        <Row style={[style.contentCenter]}>
                            <H1 allowFontScaling style={[style.fontBold,style.fontRoboto,style.walletPointsTitle]}>200.000 </H1><Text>USD</Text>
                        </Row>
                        <Row>
                            <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>My cards</Text>
                        </Row>
                        <List>
                            <ListItem noBorder>
                                <Left><Image source={require('../../../assets/visa-icon.png')} style={{marginRight:10}} /><Text allowFontScaling style={[style.fontRoboto]}>Credit *****1234</Text></Left><Row style={{justifyContent:'flex-end'}}><Icon style={style.iconItemListBlock} onPress={() => this.handleModal('view')} name='SI-0' size={20} /><Icon onPress={() => this.handleModal('delete')} name='DI-27' size={20} /></Row>
                            </ListItem>
                            <ListItem  noBorder>
                                <Left>
                                    <Image source={require('../../../assets/mastercard-icon.png')} style={{marginRight:10}} /><Text allowFontScaling style={[style.fontRoboto]}>Credit *****1234</Text></Left><Row style={{justifyContent:'flex-end'}}><Icon style={style.iconItemListBlock} onPress={() => this.handleModal('view')} name='SI-0' size={20} /><Icon onPress={() => this.handleModal('delete')} name='DI-27' size={20} /></Row> 
                            </ListItem>
                            <ListItem  noBorder onPress={() => this.handleModal('add')}>
                                <Left><Icon name='DI-22' style={style.walletPointIconItem} size={25} /><Text allowFontScaling style={[style.fontRoboto,style.fontBold,style.walletPointTextOption]}>Add credit card</Text></Left>
                            </ListItem>
                            <ListItem  noBorder  onPress={() => this.handleModal('configuration')}>
                                <Left><Icon name='SI-7' style={style.walletPointIconItem} size={25} /><Text allowFontScaling style={[style.fontRoboto,style.fontBold,style.walletPointTextOption]}>Configuratión</Text></Left>
                            </ListItem>
                        </List>
                    </Grid>
                </Content>
            </Container>
        )
    }
}