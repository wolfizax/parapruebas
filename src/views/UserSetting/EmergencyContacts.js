import React, {Component} from 'react';
import {Content, Container, Text, Grid, Row, Header, Item, Input, List, ListItem, Left, Body, Thumbnail, View, Toast} from 'native-base';
import {TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {setFetching} from '../../actions/user';
import Loading from '../../presentation/Loading';
import {ADD_USER_EMERGENCY_LIST,REMOVE_USER_EMERGENCY_LIST,DELETE_USER_EMERGENCY_LIST} from '../../actions/setting';
import ButtonGradient from '../../presentation/ButtonGradient';
import Icon from '../../presentation/Icon/index';
import style from './style';
import {categoryFriendshipService} from '../../services/categoryFriendship';
import {friendshipService} from '../../services/friendShip';
import LoggedView from '../index';
import { fileService } from '../../services/file';

@connect(state => ({
    user:state.user,
    emergencyUsersList:typeof(state.setting.emergencyUsersList) == 'string' ? JSON.parse(state.setting.emergencyUsersList) : state.setting.emergencyUsersList
}))
/** @extends Component */
export default class EmergencyContactsView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        title:'Emergency Contacts', 
        headerRight:(
            <TouchableOpacity
                onPress={() => {navigation.state.params.handleModal()}}
                style={{ paddingRight: 12 }}>
                <Icon name='DI-16' size={20} style={{color: '#fff'}} />
            </TouchableOpacity>),
        headerLeft: (
        <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ paddingLeft: 12 }}>
            <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>),
        headerTintColor:'white', 
        headerStyle:{backgroundColor:'#2B2B2B'},
        headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}})
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false,
            searchText:'',
            searchData:null,
            newContactsEmergencyList:null,
            onPressLongUser:null,
            usersData:[]
        }
    }
    async componentDidMount(){
        this.props.navigation.setParams({
            handleModal: this.handleModal
        });
        this.getData();
    }
    getData = async () => {
        const {_id,token,role} = this.props.user;
        let result;
        let result2;
        let friends = [];
        let emergencyContacts = [];
        this.props.dispatch(setFetching(true));
        result = await categoryFriendshipService.getEmergencyContacts(_id,token,role);
        result2 = await friendshipService.getUserList(_id,token,role);
        this.props.dispatch(setFetching(false));
        if(result.length > 0){
            result.map(item => {
                emergencyContacts.push(item.idfriend);
            });
            if(JSON.stringify(emergencyContacts) != JSON.stringify(this.props.emergencyUsersList)){
                this.props.dispatch(ADD_USER_EMERGENCY_LIST(emergencyContacts));
            }
        }else{
            this.props.dispatch(DELETE_USER_EMERGENCY_LIST());
        }
        if(result2.length > 0){
            result2.map(item => {
                friends.push(item.idfriend);
            });
        }
        this.setState({...this.state,usersData:friends})
    }
    onRemoveUserEmergencyList = async () => {
        const {_id,token,role} = this.props.user;
        this.props.dispatch(setFetching(true));
        result = await categoryFriendshipService.removeEmergencyContact(_id,token,role,{idfriend:this.state.onPressLongUser._id});
        this.props.dispatch(setFetching(false));
        if(result.ok){
            let newUserEmergencyList = this.props.emergencyUsersList.filter((element) => {return element._id != this.state.onPressLongUser._id});
            this.setState({...this.state,onPressLongUser:null});
            newUserEmergencyList.length > 0 ? this.props.navigation.dispatch(REMOVE_USER_EMERGENCY_LIST(newUserEmergencyList)) : this.props.navigation.dispatch(DELETE_USER_EMERGENCY_LIST())
        }else{
            Toast.show({
                text:'Sorry, a problem occurred in the process of elimination the emergency contact. Try again.',
                duration:4000,
                type:'warning',
                position:'bottom'
            }); 
        }
    }
    onAddUserEmergencyList = async () => {
        const {_id,token,role} = this.props.user;
        let friendsString = '';
        let oldData = this.props.emergencyUsersList != null ? this.props.emergencyUsersList : [];
        let newData = oldData.concat(this.state.newContactsEmergencyList);
        this.state.newContactsEmergencyList.map(item =>{
            if(friendsString != ''){
                friendsString = `${friendsString},${item._id}`
            }else{
                friendsString = item._id
            }
        });
        this.props.dispatch(setFetching(true));
        result = await categoryFriendshipService.addEmergencyContact(_id,token,role,{idfriend:friendsString});
        this.props.dispatch(setFetching(false));
        if(result.ok){
            this.props.navigation.dispatch(ADD_USER_EMERGENCY_LIST(newData));
        }else{
            Toast.show({
                text:'Sorry, a problem occurred in the process of elimination the blocked contact. Try again.',
                duration:4000,
                type:'warning',
                position:'bottom'
            }); 
        }
        this.handleModal();
    }
    handleModal = () => {
        let backupUsers = this.state.newContactsEmergencyList != null ? this.state.newContactsEmergencyList.concat(this.state.usersData) : this.state.usersData
        this.setState({
            ...this.state,
            modalVisible:!this.state.modalVisible,
            searchText:'',
            searchData:null,
            newContactsEmergencyList:null,
            usersData:backupUsers,
            onPressLongUser:null
        });
    }
    filterSearch = (text) => {
        let data = this.state.usersData.filter((item) => {
            const name = item.name.toLowerCase();
            const textSearch = text.toLowerCase();
            return name.indexOf(textSearch) > -1
        })
        this.setState({...this.state,searchData:data,searchText:text})
    }
    renderListContactsModal = item => {
        if(this.props.emergencyUsersList != null){
            if(!this.props.emergencyUsersList.find((element) => {return item._id == element._id}) && this.props.emergencyUsersList.length <= 5){
                return(<ListItem onPress={() => this.addUserEmergencyList(item)} avatar noBorder>
                <Left>
                    <Thumbnail style={style.itemAvatar} source={item.idAvatar == 'assets/avatar.png' || item.idAvatar == undefined ? require('../../assets/userPlaceholder.jpeg') : {uri:fileService.view(item.idAvatar)}} />
                </Left>
                <Body>
                    <Text style={[style.fontBold,style.fontRoboto]}>{item.name}</Text>
                    <Text style={[style.textStatusUser,style.fontRoboto]}>{item.isConnected ? 'Online' : 'Offline'}</Text>
                </Body>
            </ListItem>)
            }else{
                return(<ListItem disabled avatar noBorder style={{opacity:0.3}}> 
                <Left>
                    <Thumbnail style={style.itemAvatar} source={item.idAvatar == 'assets/avatar.png' || item.idAvatar == undefined ? require('../../assets/userPlaceholder.jpeg') : {uri:fileService.view(item.idAvatar)}} />
                </Left>
                <Body>
                    <Text style={[style.fontBold,style.fontRoboto]}>{item.name}</Text>
                    <Text style={[style.textStatusUser,style.fontRoboto]}>{item.isConnected ? 'Online' : 'Offline'}</Text>
                </Body>
            </ListItem>)
            }
        }else{
            return(<ListItem onPress={() => this.addUserEmergencyList(item)} avatar noBorder>
                <Left>
                    <Thumbnail style={style.itemAvatar} source={item.idAvatar == 'assets/avatar.png' || item.idAvatar == undefined ? require('../../assets/userPlaceholder.jpeg') : {uri:fileService.view(item.idAvatar)}} />
                </Left>
                <Body>
                    <Text style={[style.fontBold,style.fontRoboto]}>{item.name}</Text>
                    <Text style={[style.textStatusUser,style.fontRoboto]}>{item.isConnected ? 'Online' : 'Offline'}</Text>
                </Body>
            </ListItem>)
        }
    }
    addUserEmergencyList = (item) => {
        let data = this.state.newContactsEmergencyList ? this.state.newContactsEmergencyList : [];
        if(!data.find((element) => {return item._id == element._id})){
            data.push({...item});
            let newUsersData = this.state.usersData.filter(element => item._id !== element._id)
            this.setState({...this.state,newContactsEmergencyList:data,usersData:newUsersData});
        }
    }
    removeUserEmergencyList = (item) => {
        let data = this.state.newContactsEmergencyList.filter(element => element._id !== item._id);
        let newUsersData = this.state.usersData.push({...item});
        data.length == 0 ? this.setState({...this.state,newContactsEmergencyList:null}) : this.setState({...this.state,newContactsEmergencyList:data,newUsersData})
    }
    renderListNewEmergencyList = (item) => {
        return(
            <Row style={style.listBlockedContactsContainer}>
                <Thumbnail style={style.itemAvatarListBlock} source={item.idAvatar == 'assets/avatar.png' || item.idAvatar == undefined ? require('../../assets/userPlaceholder.jpeg') : {uri:fileService.view(item.idAvatar)}} />
                <Text style={style.fontRoboto}>{item.name}</Text>
                <Icon onPress={() => this.removeUserEmergencyList(item)} style={style.iconItemListBlock} name="SI-6" />
            </Row>
        )
    }
    renderAddEmergencyContactsList = () =>{
        return(
            <View style={style.modalContainer}>
                <Modal isVisible={this.state.modalVisible} onBackdropPress={() => this.handleModal()} onBackButtonPress={() => this.handleModal()}>
                    <View style={style.modalContentBlockedContacts}>
                        <Header style={style.searchBar} searchBar rounded noShadow>
                            <Item>
                                <Icon onPress={() => this.handleModal()} name="SI-8" />
                                <Input  placeholder="Search" value={this.state.searchText} onChangeText={(text) => {this.filterSearch(text)}} />
                                <Icon onPress={() => this.setState({...this.state,searchText:'',searchData:null})} name="SI-6" />
                            </Item>
                        </Header>
                        <View style={style.modalContentRow}>
                            {
                                this.state.newContactsEmergencyList ? 
                                (<View style={style.listBlockContainer}>
                                    <List showsHorizontalScrollIndicator={false} horizontal={true} dataArray={this.state.newContactsEmergencyList} renderRow={this.renderListNewEmergencyList} />
                                </View>)
                                :
                                (null)
                            }
                            {
                                this.state.usersData.length > 0 ?
                                ( <View style={style.modalContentRow}>
                                    <List dataArray={this.state.searchData ? this.state.searchData : this.state.usersData} renderRow={this.renderListContactsModal} scrollEnabled></List>
                                </View>)
                                :
                                ( <Text style={[style.textJustify,style.textDescription,style.fontRoboto]}>
                                    Sorry, at this time you do not have available contacts to add to the block list.
                                </Text>)
                            }
                        </View>
                        <ButtonGradient full onSubmit={this.onAddUserEmergencyList} disabled={!this.state.newContactsEmergencyList ? true : false} title='Next' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} />
                    </View>
                </Modal>
            </View>
        );
    }
    renderListsContactsEmergency = (item) => {
        return(
            <ListItem onPress={() => this.state.onPressLongUser != null ? this.setState({...this.state,onPressLongUser:null}) : null } avatar noBorder onLongPress={() => this.setState({...this.state,onPressLongUser:item})}>
                <Left>
                    <Thumbnail source={item.idAvatar == 'assets/avatar.png' || item.idAvatar == undefined ? require('../../assets/userPlaceholder.jpeg') : {uri:fileService.view(item.idAvatar)}} />
                </Left>
                <Body>
                    <Text style={[style.fontBold,style.fontRoboto]}>{item.name}</Text>
                    <Text style={[style.textStatusUser,style.fontRoboto]}>{item.isConnected ? 'Online' : 'Offline'}</Text>
                </Body>
            </ListItem>
        )
    }
    render(){
        return(
           <Container>
               <Content padder>
                    <Grid>
                        {this.renderAddEmergencyContactsList()}
                        {
                            this.props.emergencyUsersList != null ? 
                            <Row>
                                <List dataArray={this.props.emergencyUsersList} renderRow={this.renderListsContactsEmergency} />
                             </Row>
                            :
                            <Row style={{marginVertical:40}}>
                                <Text style={[style.textJustify,style.textDescription,style.fontRoboto]}>
                                Currently you do not have any contacts added to your list of emergency contacts.
                                </Text>
                            </Row>
                        }
                    </Grid>
               </Content>
               {
                   this.state.onPressLongUser ? 
                   <View>
                        <ButtonGradient onSubmit={this.onRemoveUserEmergencyList} full title={`To unlock ${this.state.onPressLongUser.name}`} gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} />
                    </View>
                    :
                    null
               }
            <Loading visible={this.props.user.isFetching} />
           </Container>
        )
    }
}