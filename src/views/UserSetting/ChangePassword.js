import React, {Component} from 'react';
import {Container,Content,Grid,Row,Text, H1, Form, Item, Input, Label, View, Toast, H3} from 'native-base';
import {TouchableOpacity, Image} from 'react-native';
import Icon from '../../presentation/Icon/index';
import style from './style';
import ButtonGradient from '../../presentation/ButtonGradient';
import {Validation} from '../../utils/validation';
import LoggedView from '../index';
import {userService} from '../../services/user';
import { setFetching } from '../../actions/user';
import { connect } from 'react-redux';

@connect(state => ({
    user:state.user
}))
export default class ChangePasswordView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        title:'Change Password',
        headerTitleStyle: {flex: 1, textAlign: 'center', color:'white'},
        headerStyle:{backgroundColor:'#2B2B2B'},
        headerLeft: (
            <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{ paddingLeft: 12 }}>
                <Icon name='SI-8' size={20} style={{color: '#fff'}} />
            </TouchableOpacity>
        ),
        headerRight: (<Row></Row>)});
    constructor(props){
        super(props);
        this.state = {
            finaliceProcess:false,
            currentPassword:'',
            newPassword:'',
            confirmNewPassword:'',
            errorInput:{},
        }
    }
    isValid = (type) => {
        const { currentPassword, newPassword, confirmNewPassword} = this.state;
        this.setState({...this.state,errorInput:{name:''}});
        switch (type) {
            case 'currentPassword':
                if(Validation.password(currentPassword,8) === false){
                    this.setState({...this.state,errorInput:{name:'currentPassword'}});
                    Toast.show({
                        text:'The current password field is empty or has no minimum 8 characters.',
                        type:'danger',
                        duration:4000,
                        position:'bottom'
                    });
                }else{
                    return true;
                }
                break;
            case 'newPassword':
                if(Validation.password(newPassword,8) === false){
                    this.setState({...this.state,errorInput:{name:'newPassword'}});
                    Toast.show({
                        text:'The new password field is empty or has no minimum 8 characters.',
                        type:'danger',
                        duration:4000,
                        position:'bottom'
                    });
                }else{
                    return true;
                }
                break;
            case 'confirmNewPassword':
                if(Validation.compare(newPassword,confirmNewPassword) === false && Validation.password(confirmNewPassword,8) === false){
                    this.setState({...this.state,errorInput:{name:'confirmNewPassword'}});
                    Toast.show({
                        text:'The confirm new password field is different the field new password.',
                        type:'danger',
                        duration:4000,
                        position:'bottom'
                    });
                }else{
                    return true;
                }
                break;
            default:
                break;
        }
    }
    onSubmit = async () => {
        const { currentPassword,newPassword} = this.state;
        const {_id,token,role} = this.props.user;
        let result;
        if(this.isValid('currentPassword')){
            if(this.isValid('newPassword')){
                if(this.isValid('confirmNewPassword')){
                    this.props.dispatch(setFetching(true));
                    result = await userService.verifyPassword(_id,token,role,currentPassword);
                    this.props.dispatch(setFetching(false));
                    if(result){
                        this.props.dispatch(setFetching(true));
                        result = await userService.chagePassword(_id,token,role,{currentPassword,newPassword});
                        this.props.dispatch(setFetching(false));
                        if(result){
                            this.setState({...this.state,finaliceProcess:true});
                        }else{
                            Toast.show({
                                text:'Sorry, there was a problem with the password change process. Try again.',
                                type:'danger',
                                duration:4000,
                                position:'bottom'
                            });
                        }
                    }else{
                        this.setState({...this.state,errorInput:{name:'currentPassword'}});
                        Toast.show({
                            text:'The current password is not correct.',
                            type:'danger',
                            duration:4000,
                            position:'bottom'
                        });
                    }
                }
            }
        }
    }
    render(){
        if(!this.state.finaliceProcess){
            return(
                <Container>
                    <Content>
                        <Grid>
                            <Form>
                                <Item stackedLabel style={[this.state.errorInput.name == 'currentPassword' ? {borderColor:'red'} : style.itemForm,style.listItem]}>
                                    <Label><Text allowFontScaling style={style.fontRoboto}>Current Password</Text></Label>
                                    <Input secureTextEntry={true} onChangeText={(text) => this.setState({...this.state,currentPassword:text})} onBlur={() => this.isValid('currentPassword')} textContentType='password' returnKeyType='next' autoCorrect={false} autoCapitalize='none' />
                                </Item>
                                <Item stackedLabel style={[this.state.errorInput.name == 'newPassword' ? {borderColor:'red'} : style.itemForm,style.listItem]}>
                                    <Label><Text allowFontScaling style={style.fontRoboto}>New Password</Text></Label>
                                    <Input secureTextEntry={true} onChangeText={(text) => this.setState({...this.state,newPassword:text})} onBlur={() => this.isValid('newPassword')} textContentType='password' returnKeyType='next' autoCorrect={false} autoCapitalize='none' />
                                </Item>
                                <Item stackedLabel style={[this.state.errorInput.name == 'confimeNewPassword' ? {borderColor:'red'} : style.itemForm,style.listItem]}>
                                    <Label><Text allowFontScaling style={style.fontRoboto}>Confirm New Password</Text></Label>
                                    <Input secureTextEntry={true} onChangeText={(text) => this.setState({...this.state,confirmNewPassword:text})} onBlur={() => this.isValid('confirmNewPassword')} textContentType='password' returnKeyType='done' autoCorrect={false} autoCapitalize='none' />
                                </Item>
                            </Form>
                        </Grid>
                    </Content>
                    <View>
                        <ButtonGradient uppercase={false} full title='Next' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit}/>  
                    </View>
                </Container>
            )
        }else{
            return(
                <Container>
                    <Content padder style={{paddingVertical:50}}>
                        <Row style={[style.contentCenter,{marginVertical:30}]}>
                            <Image source={require('../../assets/rocket.png')} />
                        </Row>
                        <Row style={style.contentCenter}>
                            <H1 allowFontScaling style={[style.fontBold,style.fontRoboto,style.textCenter]}>Password Changed</H1>
                        </Row>
                        <Row style={[style.contentCenter,{margin:0}]}>
                            <H3 style={[style.textCenter,style.textDescription,style.textCenter]}>Your password has been changed successfully</H3>
                        </Row>
                    </Content>
                </Container>
            )
        }
        
    }
}