import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';
import LoggedView from '../index';
import { Container, Content, List, ListItem, Left, Row, Text } from 'native-base';
import {PrivacyMenu,SoundSettings,StorageSettings, HelpMenu} from '../../actions/nav';
import Icon from '../../presentation/Icon/index';
import style from './style';

/** @extends Component */
export default class UserSettingView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'User Setting',
    headerTitleStyle: {flex: 1, textAlign: 'center', color:'white'},
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerLeft: (
        <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ paddingLeft: 12 }}>
            <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>
    ),
    headerRight: (<Row></Row>)});
    render(){
        return(
            <Container>
                <Content>
                    <List>
                        <ListItem onPress={() => this.props.navigation.dispatch(PrivacyMenu())}>
                            <Left><Icon name='I-13-Perfil' size={14} style={style.iconOptions} /><Text>User Account</Text></Left>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.dispatch(SoundSettings())}>
                            <Left><Icon name='SI-13' size={14} style={style.iconOptions} /><Text>Sound Configuration</Text></Left>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.dispatch(StorageSettings())}>
                            <Left><Icon name='SI-14' size={14} style={style.iconOptions} /><Text>Storage Data</Text></Left>
                        </ListItem>
                        <ListItem>
                            <Left><Icon name='SI-15' size={14} style={style.iconOptions} /><Text>User Reputation</Text></Left>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.dispatch(HelpMenu())}>
                            <Left><Icon name='SI-16' size={14} style={style.iconOptions} /><Text>Help</Text></Left>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}