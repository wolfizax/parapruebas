import React, { Component } from 'react';
import {Content, Container, Row, Text, List, ListItem, Left, Button, View, Grid, Toast} from 'native-base';
import Modal from 'react-native-modal';
import Icon from '../../presentation/Icon/index';
import {TouchableOpacity} from 'react-native';
import LoggedView from '../index';
import {PrivacySetting,SecurityMenu,ChangeNumber} from '../../actions/nav';
import style from './style';
import { setFetching,LOGIN_LOGOUT } from '../../actions/user';
import { userService } from '../../services/user';
import {NavigationActions} from 'react-navigation';
import {connect} from 'react-redux';

@connect(state => ({
    user:state.user
}))
/** @extends Component */
export default class PrivacyMenuView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'User Account', 
    headerRight:<Row></Row>,
    headerLeft: (
    <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{ paddingLeft: 12 }}>
        <Icon name='SI-8' size={20} style={{color: '#fff'}} />
    </TouchableOpacity>),
    headerTintColor:'white', 
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}})
    
    constructor(props){
        super(props);
        this.state = {
            modalVisible:false
        };
    }
    handleModal = () => {
        this.setState({...this.state,modalVisible:!this.state.modalVisible});
    }
    onDeactiveAccount = async () => {
        const {_id,token,role} = this.props.user;
        this.props.dispatch(setFetching(true));
        result = await userService.deactive(_id,token,role);
        this.props.dispatch(setFetching(false));
        this.handleModal();
        if(result){
            await userService.logout(_id,token,role);
            this.props.dispatch(LOGIN_LOGOUT());
            this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Main' })], 0);
        }else{
            Toast.show({
                text:'Sorry, a problem occurred in the process of elimination of your account. Try again',
                duration:4000,
                type:'danger',
                position:'bottom'
            }); 
        }
    }
    renderModalDeleteAccount = () => {
        return(
            <View style={style.modalContainer}>
                <Modal isVisible={this.state.modalVisible} onBackdropPress={() => this.handleModal()} onBackButtonPress={() => this.handleModal()}>
                    <View style={style.modalContent}>
                        <View style={style.modalContentRow}>
                            <View style={style.modalContentRowItem}>
                                <Text allowFontScaling style={style.fontRoboto}>¿Sure you want to delete your account?</Text>
                           </View>
                           <View style={[style.modalContentRowItem,style.modalHeaderBTNClose]}>
                                <Icon onPress={() => this.handleModal()} name='SI-6' size={14} style={{color: '#C7C7C7'}} />
                           </View>
                        </View>
                        <View style={style.modalContentRow}>
                            <Text allowFontScaling style={[style.fontRoboto,style.textJustify]}>
                                This action is irreversible, after performing this action it will be impossible to recover the information of this account. Are you sure?
                            </Text>
                        </View>
                        <View style={[style.modalContentRow,style.contentCenter,style.modalFooter]}>
                            <Button style={style.modalFooterBTN} onPress={() => this.onDeactiveAccount()} primary><Text allowFontScaling>Accept</Text></Button>
                            <Button style={style.modalFooterBTN} onPress={() => this.handleModal()} light><Text allowFontScaling>Cancel</Text></Button>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    render(){
        return(
            <Container>
                <Content padder>
                    <Grid>
                        {this.renderModalDeleteAccount()}
                       
                    </Grid>
                    <List>
                            <ListItem onPress={() => this.props.navigation.dispatch(PrivacySetting())}><Left><Text>Privacy</Text></Left></ListItem>
                            <ListItem onPress={() => this.props.navigation.dispatch(SecurityMenu())}><Left><Text>Security</Text></Left></ListItem>
                            <ListItem onPress={() => this.props.navigation.dispatch(ChangeNumber())}><Left><Text>Change of number</Text></Left></ListItem>
                    </List>
                    <Button onPress={() => this.handleModal()} full style={style.btnDeleteAccount}><Text>Delete Account</Text></Button>
                </Content>
            </Container>
        );
    }
}