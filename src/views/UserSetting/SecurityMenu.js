import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';
import LoggedView from '../index';
import { Container, Content, List, ListItem, Left, Row, Text } from 'native-base';
import {EmergencyContacts, AssociateEmail} from '../../actions/nav';
import Icon from '../../presentation/Icon/index';
import style from './style';

/** @extends Component */
export default class SecurityMenuView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'Security',
    headerTitleStyle: {flex: 1, textAlign: 'center', color:'white'},
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerLeft: (
        <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ paddingLeft: 12 }}>
            <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>
    ),
    headerRight: (<Row></Row>)});
    render(){
        return(
            <Container>
                <Content>
                    <List>
                        <ListItem onPress={() => this.props.navigation.dispatch(EmergencyContacts())}>
                            <Left><Icon name='I-13-Perfil' size={14} style={style.iconOptions} /><Text>Emergency Contacts</Text></Left>
                        </ListItem>
                        <ListItem>
                            <Left><Icon name='SI-13' size={14} style={style.iconOptions} /><Text>Manage Device</Text></Left>
                        </ListItem>
                        <ListItem onPress={() => this.props.navigation.dispatch(AssociateEmail())}>
                            <Left><Icon name='SI-14' size={14} style={style.iconOptions} /><Text>Asociate Mail</Text></Left>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}