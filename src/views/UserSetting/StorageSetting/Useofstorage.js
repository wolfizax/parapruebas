import React, { Component } from 'react';
import Modal from 'react-native-modal';
import {Container, Content, List, ListItem, Left, Body, Right, Thumbnail, Text, View, Button} from 'native-base';
import LoggedView from '../../index';
import Icon from '../../../presentation/Icon/index';
import style from '../style';
import HeaderView from '../../../presentation/Header';

/** @extends Component */
export default class StorageSettingsUseOfStorageView extends LoggedView{
    constructor(props){
        super(props);
        this.state = {
            usersData:[{
                id:1,
                avatarUrl:'http://i.pravatar.cc/40?img=3',
                name:'Kumar Lopez',
                status: 'Online',
                usedSpace:'400',
            },{
            id:2,
            avatarUrl:'http://i.pravatar.cc/40?img=2',
            name:'Josmer Crespo',
            status: 'Online',
            usedSpace:'500'
            }],
            searchData:null,
            modalVisible: false
        };
    }
    onCloseSearch = () => {
        this.setState({...this.state,searchData:null})
    }
    search = (text) => {
        let data = this.state.usersData.filter((item) => {
            const name = item.name.toLowerCase();
            const textSearch = text.toLowerCase();
            return name.indexOf(textSearch) > -1
        })
        this.setState({...this.state,searchData:data})
    }
    handleModal = () => {
        this.setState({...this.state,modalVisible:!this.state.modalVisible});
    }
    renderModal = () => {
        return(
            <View style={style.modalContainer}>
                <Modal isVisible={this.state.modalVisible} onBackdropPress={() => this.handleModal()} onBackButtonPress={() => this.handleModal()}>
                    <View style={style.modalContent}>
                        <View>
                            <Text allowFontScaling style={[style.fontRoboto,style.textJustify,style.textJustify]}>
                                ¿Surely you want to delete messages records, photos and all information associate with this messages?
                            </Text>
                        </View>
                        <View style={[style.modalContentRow,style.contentCenter,style.modalFooter]}>
                            <Button style={style.modalFooterBTN} onPress={() => this.handleModal()} primary><Text allowFontScaling>Accept</Text></Button>
                            <Button style={style.modalFooterBTN} onPress={() => this.handleModal()} light><Text allowFontScaling>Cancel</Text></Button>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    renderItem = (item) => {
        return(
            <ListItem avatar noBorder>
                <Left>
                    <Thumbnail style={style.itemAvatar} source={{ uri: item.avatarUrl }} />
                </Left>
                <Body>
                    <Text style={[style.fontBold,style.fontRoboto]}>{item.name}</Text>
                    <Text style={[style.textStatusUser,style.fontRoboto]}>{item.status}</Text>
                </Body>
                <Right style={style.contentCenter}>
                    <Text note>{`${item.usedSpace} MB`}</Text>
                </Right>
                <Right style={style.contentCenter}>
                    <Icon onPress={() => this.handleModal()} name="DI-27" size={20} color='black' />
                </Right>
            </ListItem>
        )
    }
    render(){
        return(
           <Container>
               <HeaderView title="Use of storage" leftComponent={{icon:'SI-8',function:this.props.navigation.goBack}} searchBar={{function:this.search,onClose:this.onCloseSearch}} />
               <Content padder>
                    {this.renderModal()}
                    <List dataArray={this.state.searchData ? this.state.searchData : this.state.usersData} renderRow={this.renderItem} />
               </Content>
           </Container>)
    }
}