import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';
import {Container, Content, Row, Col, Text, Grid} from 'native-base';
import LoggedView from '../../index';
import Icon from '../../../presentation/Icon/index';
import style from '../style';

/** @extends Component */
export default class StorageSettingsUseOfDataView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'Use of data', 
    headerRight:<Row></Row>,
    headerLeft: (
    <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{ paddingLeft: 12 }}>
        <Icon name='SI-8' size={20} style={{color: '#fff'}} />
    </TouchableOpacity>),
    headerTintColor:'white', 
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}})
    render(){
        return(<Container>
            <Content>
                <Grid>
                    <Row style={style.bottomBorder}>
                        <Col style={style.rowSetting}>
                            <Text allowFontScaling style={style.settingTitle}>Sent messages</Text>
                            <Text allowFontScaling style={[style.settingSubOption,{marginLeft:0}]}>11875 Menssages</Text>
                        </Col>
                    </Row>
                    <Row style={style.bottomBorder}>
                        <Col style={style.rowSetting}>
                            <Text allowFontScaling style={style.settingTitle}>Received messages</Text>
                            <Text allowFontScaling style={[style.settingSubOption,{marginLeft:0}]}>2000 Menssages</Text>
                        </Col>
                    </Row>
                    <Row style={style.bottomBorder}>
                        <Col style={style.rowSetting}>
                            <Text allowFontScaling style={style.settingTitle}>Multimedia bytes sent</Text>
                            <Text allowFontScaling style={[style.settingSubOption,{marginLeft:0}]}>10 GB</Text>
                        </Col>
                    </Row>
                    <Row style={style.bottomBorder}>
                        <Col style={style.rowSetting}>
                            <Text allowFontScaling style={style.settingTitle}>Multimedia bytes received</Text>
                            <Text allowFontScaling style={[style.settingSubOption,{marginLeft:0}]}>1 GB</Text>
                        </Col>
                    </Row>
                    <Row style={style.bottomBorder}>
                        <Col style={style.rowSetting}>
                            <Text allowFontScaling style={style.settingTitle}>Multimedia bytes received</Text>
                            <Text allowFontScaling style={[style.settingSubOption,{marginLeft:0}]}>1 GB</Text>
                        </Col>
                    </Row>
                    <Row style={style.bottomBorder}>
                        <Col style={style.rowSetting}>
                            <Text allowFontScaling style={style.settingTitle}>Bytes of sent messages</Text>
                            <Text allowFontScaling style={[style.settingSubOption,{marginLeft:0}]}>100 MB</Text>
                        </Col>
                    </Row>
                    <Row style={style.bottomBorder}>
                        <Col style={style.rowSetting}>
                            <Text allowFontScaling style={style.settingTitle}>Bytes of received messages</Text>
                            <Text allowFontScaling style={[style.settingSubOption,{marginLeft:0}]}>1 GB</Text>
                        </Col>
                    </Row>
                </Grid>
            </Content>
        </Container>);
    }
}