import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {Container, Content, Row, Col, Text, Grid, CheckBox, View} from 'native-base';
import {CHANGE_STORAGE_OPTIONS} from '../../../actions/setting';
import {StorageSettingsUseOfData,StorageSettingsUseOfStorage} from '../../../actions/nav';
import {connect} from 'react-redux';
import LoggedView from '../../index';
import Icon from '../../../presentation/Icon/index';
import style from '../style';

@connect(state => ({
    settings:typeof(state.setting.storage) == 'string' ? JSON.parse(state.setting.storage) : state.setting.storage
}))
/** @extends Component */
export default class StorageSettingsView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'Storage Data', 
    headerRight:<Row></Row>,
    headerLeft: (
    <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{ paddingLeft: 12 }}>
        <Icon name='SI-8' size={20} style={{color: '#fff'}} />
    </TouchableOpacity>),
    headerTintColor:'white', 
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}})
    constructor(props){
        super(props)
        this.state = {
            modalVisible:false,
            typeModal:'mobileData',
            settings:props.settings
        }
    }
    handleModal = (type='mobileData') => {
        this.state.modalVisible ?
            this.setState({...this.state, modalVisible:!this.state.modalVisible,settings:this.props.settings})
        :
            this.setState({...this.state, modalVisible:!this.state.modalVisible,settings:this.props.settings,typeModal:type})
    }
    onSubmit = () => {
        this.props.navigation.dispatch(CHANGE_STORAGE_OPTIONS(this.state.settings));
        this.setState({...this.state,modalVisible:!this.state.modalVisible});
    }
    renderOptionsActivesMobileData = () => {
       let texto = '';
       let optionsActives = [];
       for (let prop in this.state.settings.mobileData){
           if(this.state.settings.mobileData[prop] == true){
                optionsActives.push(prop);
           }
       }
       optionsActives.map((option) => {
           if(optionsActives.length > 1){
               texto = `${texto} ${option.substring(0, 1).toUpperCase() + option.substring(1)}`
           }else{
               texto = `${option.substring(0, 1).toUpperCase() + option.substring(1)}`
           }
           if(optionsActives.length == 4){
               texto = 'All the files';
           }
       })
       return(
            <Text allowFontScaling style={style.settingSubOption}>{texto == '' ? 'None' : texto}</Text>       )
    }
    renderOptionsActivesWifi = () => {
        let texto = '';
        let optionsActives = [];
        for (let prop in this.state.settings.wifi){
            if(this.state.settings.wifi[prop] == true){
                 optionsActives.push(prop);
            }
        }
        optionsActives.map((option) => {
            if(optionsActives.length > 1){
                texto = `${texto} ${option.substring(0, 1).toUpperCase() + option.substring(1)}`
            }else{
                texto = `${option.substring(0, 1).toUpperCase() + option.substring(1)}`
            }
            if(optionsActives.length == 4){
                texto = 'All the files';
            }
        })
        return(
             <Text allowFontScaling style={style.settingSubOption}>{texto == '' ? 'None' : texto}</Text>
        )
     }
    renderModalOptionsDowloads = () => {
        return(
            <View style={style.modalContainer}>
                <Modal onBackdropPress={() => this.handleModal()} isVisible={this.state.modalVisible} onBackButtonPress={() => this.handleModal()}>
                    <View style={style.modalContent}>
                        <View style={[style.modalContentRow]}>
                            <View style={style.modalContentRowItem}>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Connect to {this.state.typeModal == 'mobileData' ? 'Mobile Data' : 'WI-FI'}</Text>
                           </View>
                           <View style={[style.modalContentRowItem,style.modalHeaderBTNClose]}>
                                <Icon onPress={() => this.handleModal()} name='SI-6' size={14} style={{color: '#C7C7C7'}} />
                           </View>
                        </View>
                        <View style={style.modalContentRow}>
                            <View style={style.modalContentRowItem}>
                                <CheckBox style={[style.checkBoxOption]} checked={this.state.settings[this.state.typeModal].photos} onPress={() => this.setState({...this.state,settings:{...this.state.settings,[this.state.typeModal]:{...this.state.settings[this.state.typeModal],photos:!this.state.settings[this.state.typeModal].photos}}})} />
                            </View>
                            <View style={style.modalContentRowItem}>
                                <Text allowFontScaling style={style.profileOptionsText} onPress={() => this.setState({...this.state,settings:{...this.state.settings,[this.state.typeModal]:{...this.state.settings[this.state.typeModal],photos:!this.state.settings[this.state.typeModal].photos}}})} >Photos</Text>
                            </View>
                        </View>
                        <View style={style.modalContentRow}>
                            <View style={style.modalContentRowItem}>
                                <CheckBox style={style.checkBoxOption} checked={this.state.settings[this.state.typeModal].audio} onPress={() => this.setState({...this.state,settings:{...this.state.settings,[this.state.typeModal]:{...this.state.settings[this.state.typeModal],audio:!this.state.settings[this.state.typeModal].audio}}})} />
                            </View>
                            <View style={style.modalContentRowItem}>
                                <Text allowFontScaling style={style.profileOptionsText} onPress={() => this.setState({...this.state,settings:{...this.state.settings,[this.state.typeModal]:{...this.state.settings[this.state.typeModal],audio:!this.state.settings[this.state.typeModal].audio}}})}>Audio</Text>
                            </View>
                        </View>
                        <View style={style.modalContentRow}>
                            <View style={style.modalContentRowItem}>
                                <CheckBox style={style.checkBoxOption} checked={this.state.settings[this.state.typeModal].video} onPress={() => this.setState({...this.state,settings:{...this.state.settings,[this.state.typeModal]:{...this.state.settings[this.state.typeModal],video:!this.state.settings[this.state.typeModal].video}}})}/>
                            </View>
                            <View style={style.modalContentRowItem}>
                                <Text allowFontScaling style={style.profileOptionsText} onPress={() => this.setState({...this.state,settings:{...this.state.settings,[this.state.typeModal]:{...this.state.settings[this.state.typeModal],video:!this.state.settings[this.state.typeModal].video}}})}>Video</Text>
                            </View>
                        </View>
                        <View style={style.modalContentRow}>
                            <View style={style.modalContentRowItem}>
                                <CheckBox style={style.checkBoxOption} checked={this.state.settings[this.state.typeModal].documents} onPress={() => this.setState({...this.state,settings:{...this.state.settings,[this.state.typeModal]:{...this.state.settings[this.state.typeModal],documents:!this.state.settings[this.state.typeModal].documents}}})} />
                            </View>
                            <View style={style.modalContentRowItem}>
                                <Text allowFontScaling style={style.profileOptionsText}onPress={() => this.setState({...this.state,settings:{...this.state.settings,[this.state.typeModal]:{...this.state.settings[this.state.typeModal],documents:!this.state.settings[this.state.typeModal].documents}}})}>Documents</Text>
                            </View>
                        </View>
                        <View style={[style.modalContentRow,{marginTop:20}]}>
                            <View style={style.modalBTNClose}>
                                <Text onPress={() => this.onSubmit()} style={[style.fontRoboto,{color:'#17D6B6',marginHorizontal:20}]} allowFontScaling>SAVE</Text>
                                <Text onPress={() => this.handleModal()} style={[style.fontRoboto,{color:'#17D6B6'}]} allowFontScaling>CANCEL</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    render(){
        return(<Container>
            <Content>
                <Grid>
                    {this.renderModalOptionsDowloads()}
                    <TouchableOpacity onPress={() => this.props.navigation.dispatch(StorageSettingsUseOfData())}>
                        <Row style={style.bottomBorder}>
                            <Col style={style.rowSetting}>
                                <Text allowFontScaling style={style.settingTitle}>Use of data</Text>
                            </Col>
                        </Row>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.dispatch(StorageSettingsUseOfStorage())}>
                        <Row style={style.bottomBorder}>
                            <Col style={style.rowSetting}>
                                <Text allowFontScaling style={style.settingTitle}>Use of storage</Text>
                            </Col>
                        </Row>
                    </TouchableOpacity>
                        <Row>
                            <Col style={[style.rowSetting,{margin:0}]}>
                                <Text allowFontScaling style={style.settingTitle}>Automatic Download</Text>
                                <TouchableOpacity onPress={() => this.handleModal('mobileData')}>
                                    <Text allowFontScaling style={style.settingOption}>Conected to mobile data</Text>
                                    {this.renderOptionsActivesMobileData()}
                                </TouchableOpacity>
                            </Col>
                        </Row>
                        <Row style={style.bottomBorder}>
                            <Col style={[style.rowSetting,{margin:0}]}>
                                <TouchableOpacity onPress={() => this.handleModal('wifi')}>
                                    <Text allowFontScaling style={style.settingOption}>Conected to WI-FI</Text>
                                    {this.renderOptionsActivesWifi()}
                                </TouchableOpacity>
                            </Col>
                        </Row>
                </Grid>
            </Content>
        </Container>);
    }
}