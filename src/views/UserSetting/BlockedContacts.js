import React, {Component} from 'react';
import {Content, Container, Text, Grid, Row, Header, Item, Input, List, ListItem, Left, Body, Thumbnail, View, Toast} from 'native-base';
import {TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {ADD_USER_BLOCKED_LIST,REMOVE_USER_BLOCKED_LIST,DELETE_USER_BLOCKED_LIST} from '../../actions/setting';
import {setFetching} from '../../actions/user';
import {connect} from 'react-redux';
import ButtonGradient from '../../presentation/ButtonGradient';
import Icon from '../../presentation/Icon/index';
import style from './style';
import LoggedView from '../index';
import Loading from '../../presentation/Loading';
import { friendshipService } from '../../services/friendShip';
import { fileService } from '../../services/file';

@connect(state => ({
    user:state.user,
    blockedUsersList:typeof(state.setting.blockedUsersList) == 'string' ? JSON.parse(state.setting.blockedUsersList) : state.setting.blockedUsersList
}))
/** @extends Component */
export default class BlockedContactsView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        title:'Blocked Contacts', 
        headerRight:(
            <TouchableOpacity
                onPress={() => {navigation.state.params.handleModal()}}
                style={{ paddingRight: 12 }}>
                <Icon name='DI-16' size={20} style={{color: '#fff'}} />
            </TouchableOpacity>),
        headerLeft: (
        <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{ paddingLeft: 12 }}>
            <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>),
        headerTintColor:'white', 
        headerStyle:{backgroundColor:'#2B2B2B'},
        headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}})
    constructor(props){
        super(props);
        this.state = {
            modalVisible: false,
            searchText:'',
            searchData:null,
            newContactsBlockList:null,
            onPressLongUser:null,
            usersData:[]
        }
    }
    componentDidMount(){
        this.props.navigation.setParams({
            handleModal: this.handleModal
        });
        this.getData();
    }
    getData = async () => {
        const {_id,token,role} = this.props.user;
        let result;
        let result2;
        let friends = [];
        let blockedContacts = [];
        this.props.dispatch(setFetching(true));
        result = await friendshipService.getUserBlockedList(_id,token,role);
        result2 = await friendshipService.getUserList(_id,token,role);
        this.props.dispatch(setFetching(false));
        if(result.length > 0){
            result.map(item => {
                blockedContacts.push(item.idfriend);
            });
            if(JSON.stringify(blockedContacts) != JSON.stringify(this.props.blockedUsersList)){
                this.props.dispatch(ADD_USER_BLOCKED_LIST(blockedContacts));
            }
        }else{
            this.props.dispatch(DELETE_USER_BLOCKED_LIST());
        }
        if(result2.length > 0){
            result2.map(item => {
                friends.push(item.idfriend);
            });
        }
        this.setState({...this.state,usersData:friends})
    }
    onRemoveUserBlockedList = async () => {
        const {_id,token,role} = this.props.user;
        this.props.dispatch(setFetching(true));
        result = await friendshipService.unblockContact(_id,token,role,{idfriend:this.state.onPressLongUser._id});
        this.props.dispatch(setFetching(false));
        if(result){
            let newUserBlockedList = this.props.blockedUsersList.filter((element) => {return element._id != this.state.onPressLongUser._id});
            this.setState({...this.state,onPressLongUser:null});
            newUserBlockedList.length > 0 ? this.props.navigation.dispatch(REMOVE_USER_BLOCKED_LIST(newUserBlockedList)) : this.props.navigation.dispatch(DELETE_USER_BLOCKED_LIST())
        }else{
            Toast.show({
                text:'Sorry, a problem occurred in the process of elimination the blocked contact. Try again.',
                duration:4000,
                type:'warning',
                position:'bottom'
            }); 
        }
    }
    onAddUserBlockedList = async () => {
        const {_id,token,role} = this.props.user;
        let result;
        //let blockString = '';
        let oldData = this.props.blockedUsersList != null ? this.props.blockedUsersList : [];
        let newData = oldData.concat(this.state.newContactsBlockList);
        /* this.state.newContactsBlockList.map(item => {
            if(blockString != ''){
                blockString = `${blockString},${item._id}`
            }else{
                blockString = item._id
            }
        }); */
        this.props.dispatch(setFetching(true));
        this.state.newContactsBlockList.map(item => {
            result = friendshipService.blockedContact(_id,token,role,{idfriend:item._id});
        })
        this.props.dispatch(setFetching(false));
        if(result){
            this.props.navigation.dispatch(ADD_USER_BLOCKED_LIST(newData));
        }else{
            Toast.show({
                text:'Sorry, a problem occurred in the process of add the blocked contact. Try again.',
                duration:4000,
                type:'warning',
                position:'bottom'
            }); 
        }
        this.handleModal();
    }
    handleModal = () =>{
        let backupUsers = this.state.newContactsBlockList != null ? this.state.newContactsBlockList.concat(this.state.usersData) : this.state.usersData
        this.setState({
            ...this.state,
            modalVisible:!this.state.modalVisible,
            searchText:'',
            searchData:null,
            newContactsBlockList:null,
            usersData:backupUsers,
            onPressLongUser:null,
        });
    }
    filterSearch = (text) => {
        let data = this.state.usersData.filter((item) => {
            const name = item.name.toLowerCase();
            const textSearch = text.toLowerCase();
            return name.indexOf(textSearch) > -1
        })
        this.setState({...this.state,searchData:data,searchText:text})
    }
    renderListContactsModal = item => {
        if(this.props.blockedUsersList != null){
            if(!this.props.blockedUsersList.find(element => {return element._id == item._id}) && this.props.blockedUsersList.length <= 5){
                return(<ListItem onPress={() => this.addUserListBlocked(item)} avatar noBorder>
                    <Left>
                        <Thumbnail style={style.itemAvatar} source={item.idAvatar == 'assets/avatar.png' || item.idAvatar == undefined ? require('../../assets/userPlaceholder.jpeg') : {uri:fileService.view(item.idAvatar)}} />
                    </Left>
                    <Body>
                    <Text style={[style.fontBold,style.fontRoboto]}>{item.name}</Text>
                        <Text style={[style.textStatusUser,style.fontRoboto]}>{item.isConnected ? 'Online' : 'Offline'}</Text>
                    </Body>
                </ListItem>)
            }else{
                return(<ListItem disabled avatar noBorder style={{opacity:0.3}}> 
                    <Left>
                        <Thumbnail style={style.itemAvatar} source={item.idAvatar == 'assets/avatar.png' || item.idAvatar == undefined ? require('../../assets/userPlaceholder.jpeg') : {uri:fileService.view(item.idAvatar)}} />
                    </Left>
                    <Body>
                        <Text style={[style.fontBold,style.fontRoboto]}>{item.name}</Text>
                        <Text style={[style.textStatusUser,style.fontRoboto]}>{item.isConnected ? 'Online' : 'Offline'}</Text>
                    </Body>
                </ListItem>)
            }
        }else{
            return(<ListItem onPress={() => this.addUserListBlocked(item)} avatar noBorder>
                <Left>
                    <Thumbnail style={style.itemAvatar} source={item.idAvatar == 'assets/avatar.png' || item.idAvatar == undefined ? require('../../assets/userPlaceholder.jpeg') : {uri:fileService.view(item.idAvatar)}} />
                </Left>
                <Body>
                    <Text style={[style.fontBold,style.fontRoboto]}>{item.name}</Text>
                    <Text style={[style.textStatusUser,style.fontRoboto]}>{item.isConnected ? 'Online' : 'Offline'}</Text>
                </Body>
            </ListItem>)
        }
    }
    addUserListBlocked = (item) => {
        let data = this.state.newContactsBlockList ? this.state.newContactsBlockList : [];
        if(!data.find((element) => {return item._id == element._id})){
            data.push({...item});
            let newUsersData = this.state.usersData.filter(element => item._id !== element._id)
            this.setState({...this.state,newContactsBlockList:data,usersData:newUsersData});
        }
    }
    removeUserListBlocked = (item) => {
        let data = this.state.newContactsBlockList.filter(element => element._id !== item._id);
        let newUsersData = this.state.usersData.push({...item});
        data.length == 0 ? this.setState({...this.state,newContactsBlockList:null}) : this.setState({...this.state,newContactsBlockList:data,newUsersData})
    }
    renderListNewBlockedUser = (item) => {
        return(
            <Row style={style.listBlockedContactsContainer}>
                <Thumbnail style={style.itemAvatarListBlock} source={{uri:item.avatarUrl}} />
                <Text style={style.fontRoboto}>{item.name}</Text>
                <Icon onPress={() => this.removeUserListBlocked(item)} style={style.iconItemListBlock} name="SI-6" />
            </Row>
        )
    }
    renderAddBlockedContactsList = () =>{
        return(
            <View style={style.modalContainer}>
                <Modal isVisible={this.state.modalVisible} onBackdropPress={() => this.handleModal()} onBackButtonPress={() => this.handleModal()}>
                    <View style={style.modalContentBlockedContacts}>
                        <Header style={style.searchBar} searchBar rounded noShadow>
                            <Item>
                                <Icon onPress={() => this.handleModal()} name="SI-8" />
                                <Input  placeholder="Search" value={this.state.searchText} onChangeText={(text) => {this.filterSearch(text)}} />
                                <Icon onPress={() => this.setState({...this.state,searchText:'',searchData:null})} name="SI-6" />
                            </Item>
                        </Header>
                        <View style={style.modalContentRow}>
                            {
                                this.state.newContactsBlockList ? 
                                (<View style={style.listBlockContainer}>
                                    <List showsHorizontalScrollIndicator={false} horizontal={true} dataArray={this.state.newContactsBlockList} renderRow={this.renderListNewBlockedUser} />
                                </View>)
                                :
                                (null)
                            }
                            {
                                this.state.usersData.length > 0 ?
                                ( <View style={style.modalContentRow}>
                                    <List dataArray={this.state.searchData ? this.state.searchData : this.state.usersData} renderRow={this.renderListContactsModal} scrollEnabled></List>
                                </View>)
                                :
                                ( <Text style={[style.textJustify,style.textDescription,style.fontRoboto]}>
                                    Sorry, at this time you do not have available contacts to add to the block list.
                                </Text>)
                            }
                        </View>
                        <ButtonGradient full onSubmit={this.onAddUserBlockedList} disabled={!this.state.newContactsBlockList ? true : false} title='Next' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} />
                    </View>
                </Modal>
            </View>
        );
    }
    renderListContactsBlocked = (item) => {
        return(
            <ListItem onPress={() => this.state.onPressLongUser != null ? this.setState({...this.state,onPressLongUser:null}) : null } avatar noBorder onLongPress={() => this.setState({...this.state,onPressLongUser:item})}>
                <Left>
                    <Thumbnail source={item.idAvatar == 'assets/avatar.png' || item.idAvatar == undefined ? require('../../assets/userPlaceholder.jpeg') : {uri:fileService.view(item.idAvatar)}} />
                </Left>
                <Body>
                    <Text style={[style.fontBold,style.fontRoboto]}>{item.name}</Text>
                    <Text style={[style.textStatusUser,style.fontRoboto]}>{item.isConnected ? 'Online' : 'Offline'}</Text>
                </Body>
            </ListItem>
        )
    }
    render(){
        return(
           <Container>
               <Content padder>
                    <Grid>
                        {this.renderAddBlockedContactsList()}
                        {
                            this.props.blockedUsersList != null ? 
                            <Row>
                                <List dataArray={this.props.blockedUsersList} renderRow={this.renderListContactsBlocked} />
                             </Row>
                            :
                            <Row style={{marginVertical:40}}>
                                <Text style={[style.textJustify,style.textDescription,style.fontRoboto]}>
                                    Touch add to select the contacts yu want to block, they will not be able to call yur or send you a message.
                                </Text>
                            </Row>
                        }
                        
                    </Grid>
               </Content>
               {
                   this.state.onPressLongUser ? 
                   <View>
                        <ButtonGradient onSubmit={this.onRemoveUserBlockedList} full title={`To unlock ${this.state.onPressLongUser.name}`} gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} />
                    </View>
                    :
                    null
               }
               <Loading visible={this.props.user.isFetching} />
           </Container>
        )
    }
}