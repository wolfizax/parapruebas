import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';
import LoggedView from '../../index';
import { Container, Content, Row, Text, Grid} from 'native-base';
import ButtonGradient from '../../../presentation/ButtonGradient';
import {ChangeNumberForm} from '../../../actions/nav';
import Icon from '../../../presentation/Icon/index';
import style from '../style';


/** @extends Component */
export default class ChangeNumberView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'Change Number',
    headerTitleStyle: {flex: 1, textAlign: 'center', color:'white'},
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerLeft: (
        <TouchableOpacity
            onPress={() => navigation.goBack(navigation.getParam('backRoute'))}
            style={{ paddingLeft: 12 }}>
            <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>
    ),
    headerRight: (<Row></Row>)});
    render(){
        return(
            <Container>
                <Content padder>
                    <Grid>
                        <Row style={style.contentCenter}>
                            <Text style={[style.textDescription,style.textJustify]} allowFontScaling>
                                When you change your phone number, the groups, the settings and the informatión of your account will be migrated.
                            </Text>
                        </Row>
                        <Row style={style.contentCenter}>
                            <Text style={[style.textDescription,style.textJustify]} allowFontScaling>
                                Before starting this step, make sure you can receive sms messages in your new number.
                            </Text>
                        </Row>
                        <Row style={style.associateEmailFooterForm}>
                            <ButtonGradient onSubmit={() => this.props.navigation.dispatch(ChangeNumberForm({backRoute:this.props.navigation.state.key}))} title='Next' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} />
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}