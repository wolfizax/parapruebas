import React, { Component } from 'react';
import {TouchableOpacity, TextInput, Platform} from 'react-native';
import LoggedView from '../../index';
import DeviceInfo from 'react-native-device-info';
import CountryPicker, {getAllCountries} from 'react-native-country-picker-modal'
import {ChangeNumberValidationCode} from '../../../actions/nav';
import { Container, Content, Row, Text, Grid, Toast, Col } from 'native-base';
import ButtonGradient from '../../../presentation/ButtonGradient';
import {Validation} from '../../../utils/validation';
import Icon from '../../../presentation/Icon/index';
import style from '../style';
import {connect} from 'react-redux';

@connect(state => ({
    countryCode: state.user.countryCode,
    numberPhone: state.user.numberPhone
}))
/** @extends Component */
export default class ChangeNumberForm extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'Change Number',
    headerTitleStyle: {flex: 1, textAlign: 'center', color:'white'},
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerLeft: (<Row></Row>),
    headerRight: (<TouchableOpacity
        onPress={() => navigation.goBack(navigation.getParam('backRoute'))}
        style={{ paddingRight: 12 }}>
        <Icon name='SI-6' size={20} style={{color: '#fff'}} />
    </TouchableOpacity>)});
    constructor(props){
        super(props);
        let userLocaleCountryCode = DeviceInfo.getDeviceCountry()
        const userCountryData = getAllCountries().filter(country => country.cca2 === userLocaleCountryCode).pop();
        let newCallingCode = null;
        let oldcca2 = getAllCountries().filter(country => country.callingCode === this.props.countryCode).pop();
        let newcca2 = userLocaleCountryCode;
        if (!newcca2 || !userCountryData) {
            newcca2 = 'US'
            newCallingCode = '1'
        } 
        else {
            newCallingCode = userCountryData.callingCode
        }
        this.state = {
            oldcca2,
            newcca2,
            newCallingCode,
            newNumberPhone:'',
            errorInput:{}
        }
    }
    isValid = () => {
        this.setState({...this.state,errorInput:{}});
        if(Validation.isNumerPhone(this.state.newNumberPhone) === false){
            this.setState({...this.state,errorInput:{name:'newNumberPhone'}});
            Toast.show({
                text:'The new number phone field is empty or does not have a correct format.',
                type:'danger',
                duration:4000,
                position:'bottom'
            });
        }else{
            return true;
        }
    }
    onSubmit = () => {
        if(this.isValid() == true && this.props.numberPhone != this.state.newNumberPhone){
            this.props.navigation.dispatch(ChangeNumberValidationCode({backRoute:this.props.navigation.getParam('backRoute')}));
        }else{
            Toast.show({
                text:'The new telephone number entered is the same as the current one. Please enter a different phone number.',
                type:'danger',
                duration:4000,
                position:'bottom'
            });
        }
    }
    render(){
        return(
            <Container>
                <Content padder>
                    <Grid>
                        <Row style={style.contentCenter}>
                            <Text allowFontScaling style={[style.fontRoboto,style.fontBold,{fontSize:19}]}>Current telephone number</Text>
                        </Row>
                        <Row style={style.modalContent}>
                            <Col style={{width:'35%'}}>
                                <CountryPicker onChange={null} disabled cca2={this.state.oldcca2.cca2} />
                            </Col>
                            <Col>
                                <TextInput
                                    editable={false} 
                                    name={'oldPhoneNumber'}
                                    style={style.inputPhoneText}
                                    underlineColorAndroid='#BCBCBC'
                                    value={this.props.numberPhone}
                                    />
                            </Col>
                        </Row>
                        <Row style={style.contentCenter}>
                            <Text allowFontScaling style={[style.fontRoboto,style.fontBold,{fontSize:19}]}>Enter new number</Text>
                        </Row>
                        <Row style={style.modalContent}>
                            <Col style={{width:'35%'}}>
                                <CountryPicker hideAlphabetFilter filterable  onChange={(value) => this.setState({...this.state,newcca2:value.cca2,newCallingCode:value.callingCode})} cca2={this.state.newcca2} showCallingCode closeable />
                            </Col>
                            <Col>
                                <TextInput 
                                    name={'newPhoneNumber'}
                                    placeholder="Phone Number" 
                                    style={style.inputPhoneText}
                                    autoCapitalize={'none'}
                                    autoCorrect={false} 
                                    returnKeyType='go'
                                    underlineColorAndroid='#BCBCBC'
                                    keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'phone-pad'}
                                    textContentType="telephoneNumber"
                                    onChangeText={(text) => this.setState({...this.state,newNumberPhone:text.replace(/(^0+)|([^0-9])/g,'')})}
                                    maxLength={11}
                                    onBlur={() => this.isValid()}
                                    value={this.state.newNumberPhone}
                                    />
                            </Col>
                        </Row>
                        <Row style={style.associateEmailFooterForm}>
                            <ButtonGradient disabled={this.state.errorInput ? false : true}  onSubmit={() => this.onSubmit()} title='Next' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} />
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}