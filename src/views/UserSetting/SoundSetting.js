import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';
import {Container, Content, Row, Col, Text, Grid, View, Radio } from 'native-base';
import {CHANGE_MESSAGE_NOTIFICATION_TONE,CHANGE_MESSAGE_VIBRATION,CHANGE_GROUP_NOTIFICATION_TONE,CHANGE_GROUP_VIBRATION,CHANGE_CALL_NOTIFICATION_TONE,CHANGE_CALL_VIBRATION} from '../../actions/setting';
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import LoggedView from '../index';
import Icon from '../../presentation/Icon/index';
import Sound from 'react-native-sound';
import style from './style';

@connect(state => ({
    settings:{
        Message:{
            Tone:state.setting.messageTone,
            Vibration:state.setting.messageVibration,
        },
        Group:{
            Tone:state.setting.groupTone,
            Vibration:state.setting.groupVibration,
        },
        Call:{
            Tone:state.setting.callTone,
            Vibration:state.setting.callVibration,
        }
    }
}))
/** @extends Component */
export default class SoundSettingView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'Sound Configuration', 
    headerRight:<Row></Row>,
    headerLeft: (
    <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{ paddingLeft: 12 }}>
        <Icon name='SI-8' size={20} style={{color: '#fff'}} />
    </TouchableOpacity>),
    headerTintColor:'white', 
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}})
    
    constructor(props){
        super(props);
        this.state = {
            modalNotificationTone:false,
            modalVibration:false,
            notificationType:'Message',
            typeOption:'Tone',
            settings:props.settings,
            onPlaySound:''
        }
    }
    handleModalNotificationsTone = (notificationType='Message',typeOption='Vibration') => {
        this.state.modalNotificationTone ?
            this.setState({...this.state,onPlaySoud:'',modalNotificationTone:!this.state.modalNotificationTone,notificationType,typeOption,settings:this.props.settings})
        :
            this.setState({...this.state,onPlaySoud:'',modalNotificationTone:!this.state.modalNotificationTone,notificationType,typeOption})
    }
    handleModalVibration = (notificationType='Message',typeOption='Vibration') => {
        this.state.modalVibration ?
            this.setState({...this.state,modalVibration:!this.state.modalVibration,notificationType,typeOption,settings:this.props.settings})
        :
            this.setState({...this.state,modalVibration:!this.state.modalVibration,notificationType,typeOption,})
    }
    onChangedValue = (value) => {
       this.setState({...this.state,settings:{...this.state.settings,[this.state.notificationType]:{...this.state.settings[this.state.notificationType],[this.state.typeOption]:value}}});
    }
    playSound = (option,file) => {
        this.setState({...this.state,onPlaySound:option});
        let sound = new Sound(`${file}.mp3`,Sound.MAIN_BUNDLE,(error) => {
            sound.stop();
            if(error){
                console.log(error);
            }
            sound.play(() => {
                this.setState({...this.state,onPlaySound:''});
            });
        });
    }
    onSubmit = () => {
        switch (this.state.notificationType) {
            case 'Message':
                if(this.state.typeOption == 'Tone'){
                    this.props.navigation.dispatch(CHANGE_MESSAGE_NOTIFICATION_TONE(this.state.settings.Message.Tone));
                }else{
                    this.props.navigation.dispatch(CHANGE_MESSAGE_VIBRATION(this.state.settings.Message.Vibration));
                }
            break;
            case 'Group':
                if(this.state.typeOption == 'Tone'){
                    this.props.navigation.dispatch(CHANGE_GROUP_NOTIFICATION_TONE(this.state.settings.Group.Tone));
                }else{
                    this.props.navigation.dispatch(CHANGE_GROUP_VIBRATION(this.state.settings.Group.Vibration));
                }
            break;
            case 'Call':
                if(this.state.typeOption == 'Tone'){
                    this.props.navigation.dispatch(CHANGE_CALL_NOTIFICATION_TONE(this.state.settings.Call.Tone));
                }else{
                    this.props.navigation.dispatch(CHANGE_CALL_VIBRATION(this.state.settings.Call.Vibration));
                }
            break;
            default:
                break;
        }
        this.state.modalNotificationTone ? this.setState({...this.state,modalNotificationTone:!this.state.modalNotificationTone}) : this.setState({...this.state,modalVibration:!this.state.modalVibration})
    }
    renderModalVibration = () => {
        return(
            <View style={style.modalContainer}>
                <Modal onBackdropPress={() => this.handleModalVibration()} isVisible={this.state.modalVibration} onBackButtonPress={() => this.handleModalVibration()}>
                    <View style={[style.modalContent,{paddingHorizontal:10,paddingVertical:20}]}>
                        <View style={[style.modalContentRow]}>
                            <View>
                            <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Vibration</Text>
                           </View>
                        </View>
                        <View style={[style.modalContentRow,style.itemModalSound]}>
                            <View>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Desactivated</Text>
                            </View>
                            <View style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => this.onChangedValue('Desactivated')} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'Desactivated' ? true : false}  />
                            </View>
                        </View>
                        <View style={[style.modalContentRow,style.itemModalSound]}>
                            <View>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Default</Text>
                            </View>
                            <View style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => this.onChangedValue('Default')} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'Default' ? true : false}  />
                            </View>
                        </View>
                        <View style={[style.modalContentRow,style.itemModalSound]}>
                            <View>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Short</Text>
                            </View>
                            <View style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => this.onChangedValue('Short')} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'Short' ? true : false}  />
                            </View>
                        </View>
                        <View style={[style.modalContentRow,style.itemModalSound]}>
                            <View>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Long</Text>
                            </View>
                            <View style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => this.onChangedValue('Long')} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'Long' ? true : false}  />
                            </View>
                        </View>
                        <View style={[style.modalContentRow,{marginTop:20}]}>
                            <View style={style.modalBTNClose}>
                                <Text onPress={() => this.onSubmit()} style={[style.fontRoboto,{color:'#17D6B6',marginHorizontal:20}]} allowFontScaling>SAVE</Text>
                                <Text onPress={() => this.handleModalVibration()} style={[style.fontRoboto,{color:'#17D6B6'}]} allowFontScaling>CANCEL</Text>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    renderModalNotificationTone = () => {
        return(
            <View style={style.modalContainer}>
                <Modal onBackdropPress={() => this.handleModalNotificationsTone()} isVisible={this.state.modalNotificationTone} onBackButtonPress={() => this.handleModalNotificationsTone()}>
                    <View style={[style.modalContent,{paddingHorizontal:10,paddingVertical:20}]}>
                        <View style={[style.modalContentRow]}>
                            <View>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Notification tone</Text>
                           </View>
                        </View>
                        <Row style={[style.modalContentRow,style.itemModalSound]}>
                            <Row>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Email</Text>
                            </Row>
                            <Row style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => {this.playSound('Email','notification');this.onChangedValue('Email')}} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'Email' ? true : false} />
                            </Row>
                        </Row>
                        <Row style={[style.modalContentRow,style.itemModalSound]}>
                            <Row>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Eternal</Text>
                            </Row>
                            <Row style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => {this.playSound('Eternal','notification');this.onChangedValue('Eternal')}} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'Eternal' ? true : false} />
                            </Row>
                        </Row>
                        <Row style={[style.modalContentRow,style.itemModalSound]}>
                            <Row>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Greetings</Text>
                            </Row>
                            <Row style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => {this.playSound('Greetings','notification');this.onChangedValue('Greetings')}} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'Greetings' ? true : false} />
                            </Row>
                        </Row>
                        <Row style={[style.modalContentRow,style.itemModalSound]}>
                            <Row>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Hangounts Call</Text>
                            </Row>
                            <Row style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => {this.playSound('HangountsCall','notification');this.onChangedValue('HangountsCall')}} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'HangountsCall' ? true : false} />
                            </Row>
                        </Row>
                        <Row style={[style.modalContentRow,style.itemModalSound]}>
                            <Row>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Hangounts Message</Text>
                            </Row>
                            <Row style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => {this.playSound('hangountsMessage','notification');this.onChangedValue('hangountsMessage')}} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'hangountsMessage' ? true : false} />
                            </Row>
                        </Row>
                        <Row style={[style.modalContentRow,style.itemModalSound]}>
                            <Row>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Harmonica</Text>
                            </Row>
                            <Row style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => {this.playSound('Harmonica','notification');this.onChangedValue('Harmonica')}} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'Harmonica' ? true : false} />
                            </Row>
                        </Row>
                        <Row style={[style.modalContentRow,style.itemModalSound]}>
                            <Row>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Headline</Text>
                            </Row>
                            <Row style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => {this.playSound('Headline','notification');this.onChangedValue('Headline')}} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'Headline' ? true : false} />
                            </Row>
                        </Row>
                        <Row style={[style.modalContentRow,style.itemModalSound]}>
                            <Row>
                                <Text allowFontScaling style={[style.fontRoboto,{paddingLeft:20}]} >Hello</Text>
                            </Row>
                            <Row style={[{right:0,position:'absolute',paddingRight:20}]}>
                                <Radio onPress={() => {this.playSound('Hello','notification');this.onChangedValue('Hello')}} selected={this.state.settings[this.state.notificationType][this.state.typeOption] == 'Hello' ? true : false} />
                            </Row>
                        </Row>
                        <Row style={[style.modalContentRow,{marginTop:20}]}>
                            <Row style={style.modalBTNClose}>
                                <Text onPress={() => this.onSubmit()} style={[style.fontRoboto,{color:'#17D6B6',marginHorizontal:20}]} allowFontScaling>SAVE</Text>
                                <Text onPress={() => this.handleModalNotificationsTone()} style={[style.fontRoboto,{color:'#17D6B6'}]} allowFontScaling>CANCEL</Text>
                            </Row>
                        </Row>
                    </View>
                </Modal>
            </View>
        );
    }
    render(){
        return(<Container>
            <Content>
                {this.renderModalVibration()}
                {this.renderModalNotificationTone()}
                <Grid>
                    <TouchableOpacity onPress={() => this.handleModalNotificationsTone('Message','Tone')}>
                        <Row>
                            <Col style={style.rowSetting}>
                                <Text allowFontScaling style={style.settingTitle}>Message notifications</Text>
                                <Text allowFontScaling style={style.settingOption}>Notification tone</Text>
                                <Text allowFontScaling style={[style.settingSubOption,style.settingOptionValue]}>{this.state.settings.Message.Tone == '' ? 'none' : this.state.settings.Message.Tone}</Text>
                            </Col>
                        </Row>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.handleModalVibration('Message','Vibration')}>
                        <Row>
                            <Col style={style.rowSetting}>
                                <Text allowFontScaling style={style.settingOption}>Vibration</Text>
                                <Text allowFontScaling style={[style.settingSubOption,style.settingOptionValue]}>{this.state.settings.Message.Vibration == '' ? 'none' : this.state.settings.Message.Vibration}</Text>
                            </Col>
                        </Row>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.handleModalNotificationsTone('Group','Tone')}>
                        <Row>
                            <Col style={style.rowSetting}>
                                <Text allowFontScaling style={style.settingTitle}>Group Notification</Text>
                                <Text allowFontScaling style={style.settingOption}>Notification tone</Text>
                                <Text allowFontScaling style={[style.settingSubOption,style.settingOptionValue]}>{this.state.settings.Group.Tone == '' ? 'none' : this.state.settings.Group.Tone}</Text>
                            </Col>
                        </Row>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.handleModalVibration('Group','Vibration')}>
                        <Row>
                            <Col style={style.rowSetting}>
                                <Text allowFontScaling style={style.settingOption}>Vibration</Text>
                                <Text allowFontScaling style={[style.settingSubOption,style.settingOptionValue]}>{this.state.settings.Group.Vibration == '' ? 'none' : this.state.settings.Group.Vibration}</Text>
                            </Col>
                        </Row>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.handleModalNotificationsTone('Call','Tone')}>
                        <Row>
                            <Col style={style.rowSetting}>
                                <Text allowFontScaling style={style.settingTitle}>Call Notification</Text>
                                <Text allowFontScaling style={style.settingOption}>Notification tone</Text>
                                <Text allowFontScaling style={[style.settingSubOption,style.settingOptionValue]}>{this.state.settings.Call.Tone == '' ? 'none' : this.state.settings.Call.Tone}</Text>
                            </Col>
                        </Row>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.handleModalVibration('Call','Vibration')}>
                        <Row>
                            <Col style={style.rowSetting}>
                                <Text allowFontScaling style={style.settingOption}>Vibration</Text>
                                <Text allowFontScaling style={[style.settingSubOption,style.settingOptionValue]}>{this.state.settings.Call.Vibration == '' ? 'none' : this.state.settings.Call.Vibration}</Text>
                            </Col>
                        </Row>
                    </TouchableOpacity>
                    <Row></Row>
                </Grid>
            </Content>
        </Container>);
    }
}