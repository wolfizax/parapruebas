import React, { Component } from 'react';
import {TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {Container, Content, Row, Col, Text, Grid, CheckBox, View, Button} from 'native-base';
import {BlockedContacts} from '../../actions/nav';
import LoggedView from '../index';
import Icon from '../../presentation/Icon/index';
import style from './style';
import {connect} from 'react-redux';

@connect(state => ({
    blockedUsersList:typeof(state.setting.blockedUsersList) == 'string' ? JSON.parse(state.setting.blockedUsersList) : state.setting.blockedUsersList
}))
/** @extends Component */
export default class PrivacySettingView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
    title:'Privacy', 
    headerRight:<Row></Row>,
    headerLeft: (
    <TouchableOpacity
        onPress={() => navigation.goBack()}
        style={{ paddingLeft: 12 }}>
        <Icon name='SI-8' size={20} style={{color: '#fff'}} />
    </TouchableOpacity>),
    headerTintColor:'white', 
    headerStyle:{backgroundColor:'#2B2B2B'},
    headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}})
    constructor(props){
        super(props)
        this.state = {
            modalVisible:false,
            checkBoxEveryone:false,
            checkBoxContacts:false,
            checkBoxNoOne:false
        }
    }
    handleModal = () => {
        this.setState({...this.state, modalVisible:!this.state.modalVisible});
    }
    renderModalProfile = () => {
        return(
            <View style={style.modalContainer}>
                <Modal onBackdropPress={() => this.handleModal()} isVisible={this.state.modalVisible} onBackButtonPress={() => this.handleModal()}>
                    <View style={style.modalContent}>
                        <View style={[style.modalContentRow]}>
                            <View style={style.modalContentRowItem}>
                                <Text allowFontScaling style={[style.fontBold,style.fontRoboto]}>Profile</Text>
                           </View>
                           <View style={[style.modalContentRowItem,style.modalHeaderBTNClose]}>
                                <Icon onPress={() => this.handleModal()} name='SI-6' size={14} style={{color: '#C7C7C7'}} />
                           </View>
                        </View>
                        <View style={style.modalContentRow}>
                            <View style={style.modalContentRowItem}>
                                <CheckBox style={[style.checkBoxOption]} checked={this.state.checkBoxEveryone} onPress={() => {this.state.checkBoxEveryone ? this.setState({...this.state,checkBoxEveryone:!this.state.checkBoxEveryone}) : this.setState({...this.state,checkBoxEveryone:!this.state.checkBoxEveryone,checkBoxContacts:false,checkBoxNoOne:false})}} />
                            </View>
                            <View style={style.modalContentRowItem}>
                                <Text allowFontScaling style={style.profileOptionsText} onPress={() => {this.state.checkBoxEveryone ? this.setState({...this.state,checkBoxEveryone:!this.state.checkBoxEveryone}) : this.setState({...this.state,checkBoxEveryone:!this.state.checkBoxEveryone,checkBoxContacts:false,checkBoxNoOne:false})}} >Everyone</Text>
                            </View>
                        </View>
                        <View style={style.modalContentRow}>
                            <View style={style.modalContentRowItem}>
                                <CheckBox style={style.checkBoxOption} checked={this.state.checkBoxContacts} onPress={() => {this.state.checkBoxContacts ? this.setState({...this.state,checkBoxContacts:!this.state.checkBoxContacts}) : this.setState({...this.state,checkBoxContacts:!this.state.checkBoxContacts,checkBoxEveryone:false,checkBoxNoOne:false})}} />
                            </View>
                            <View style={style.modalContentRowItem}>
                                <Text allowFontScaling style={style.profileOptionsText} onPress={() => {this.state.checkBoxContacts ? this.setState({...this.state,checkBoxContacts:!this.state.checkBoxContacts}) : this.setState({...this.state,checkBoxContacts:!this.state.checkBoxContacts,checkBoxEveryone:false,checkBoxNoOne:false})}}>My contacts</Text>
                            </View>
                        </View>
                        <View style={style.modalContentRow}>
                            <View style={style.modalContentRowItem}>
                                <CheckBox style={style.checkBoxOption} checked={this.state.checkBoxNoOne} onPress={() => {this.state.checkBoxNoOne ? this.setState({...this.state,checkBoxNoOne:!this.state.checkBoxNoOne}) : this.setState({...this.state,checkBoxNoOne:!this.state.checkBoxNoOne,checkBoxContacts:false,checkBoxEveryone:false})}} />
                            </View>
                            <View style={style.modalContentRowItem}>
                                <Text allowFontScaling style={style.profileOptionsText} onPress={() => {this.state.checkBoxNoOne ? this.setState({...this.state,checkBoxNoOne:!this.state.checkBoxNoOne}) : this.setState({...this.state,checkBoxNoOne:!this.state.checkBoxNoOne,checkBoxContacts:false,checkBoxEveryone:false})}}>No one</Text>
                            </View>
                        </View>
                        <View style={style.modalBTNClose}>
                            <Button onPress={() => this.handleModal()} light><Text allowFontScaling style={[style.fontRoboto,style.fontBold]}>CANCEL</Text></Button>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
    render(){
        return(<Container>
            <Content>
                <Grid>
                    {this.renderModalProfile()}
                    <Row style={style.bottomBorder}>
                        <Col style={style.rowSetting}>
                            <Text allowFontScaling style={[style.textDescription]}>Who can access my personal information WonderChatt</Text>
                        </Col>
                    </Row>
                    <TouchableOpacity onPress={() => this.handleModal()}>
                        <Row style={style.bottomBorder}>
                            <Col style={style.rowSetting}>
                                <Text allowFontScaling style={style.settingTitle}>Profile</Text>
                                <Text allowFontScaling style={style.settingOption}>Everyone</Text>
                            </Col>
                        </Row>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.props.navigation.dispatch(BlockedContacts())}>
                    <Row style={style.bottomBorder}>
                        <Col style={style.rowSetting}>
                            <Text allowFontScaling style={style.settingTitle}>Messenger service</Text>
                            <Text allowFontScaling style={style.settingOption}>Blocked contacts: {this.props.blockedUsersList != null ? this.props.blockedUsersList.length : 'none'}</Text>
                            <Text allowFontScaling style={style.settingSubOption}>List of blocked contacts</Text>
                        </Col>
                    </Row>
                    </TouchableOpacity>
                </Grid>
            </Content>
        </Container>);
    }
}