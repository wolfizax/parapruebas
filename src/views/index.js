import React from 'react';
import {BackHandler} from 'react-native';
import { Answers } from 'react-native-fabric';
export default class LoggedView extends React.Component {
	
  _didFocusSubscription;
  _willBlurSubscription;
  constructor(props) {
    super(props);
    this._didFocusSubscription = props.navigation.addListener('didFocus', payload =>
      BackHandler.addEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  componentDidMount() {
    this._willBlurSubscription = this.props.navigation.addListener('willBlur', payload =>
      BackHandler.removeEventListener('hardwareBackPress', this.onBackButtonPressAndroid)
    );
  }

  onBackButtonPressAndroid = () => {
      if(this.props.navigation.state.routeName === 'Main' || this.props.navigation.state.routeName === 'Dashboard'){
        BackHandler.exitApp();
      }else{
        if(this.props.navigation.getParam('backRoute') != undefined){
          this.props.navigation.goBack(this.props.navigation.getParam('backRoute'))

        }else{
          this.props.navigation.goBack();
        }
        return true;
      }
  };

  componentWillUnmount() {
    this._didFocusSubscription && this._didFocusSubscription.remove();
    this._willBlurSubscription && this._willBlurSubscription.remove();
  };
  
  componentDidCatch(err,info){
    Answers.logCustom(err,info);
  }
    
}
