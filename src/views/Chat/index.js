import React, { Component } from 'react';
import LoggedView from '../index';
import { TouchableWithoutFeedback, TextInput, TouchableOpacity } from 'react-native';
import { Container, Content, Button, Header, Row, Thumbnail, Col, Text, Tabs, Tab, Right } from 'native-base';
import Icon from '../../presentation/Icon/index';
import MessageContent from '../../presentation/Chat/Message';
import LinearGradient from 'react-native-linear-gradient';
import moment from 'moment';
import style from './style';
import EmojiPicker from '../../presentation/Chat/EmojiPicker';
import GiftPicker from '../../presentation/Chat/GiftPicker';
import StickersPicker from '../../presentation/Chat/StickersPicker';
import { OptimizedFlatList } from 'react-native-optimized-flatlist';

/** @extends Component  */
export default class ChatView extends LoggedView{
    constructor(props){
        super(props);
        this.state = {
            boxView:false,
            currentTab:0,
            messages:[{
                id:1,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Hey there!',
                date:moment().format('LT'),
                type:'text',
            },{
                id:2,
                sender:{
                    id:1,
                    name:'Edgar Perez'
                },
                message:'Hey man',
                date:moment().format('LT'),
                type:'text'
            },{
                id:3,
                sender:{
                    id:1,
                    name:'Edgar Perez'
                },
                message:'What are you up to?',
                date:moment().format('LT'),
                type:'text'
            },{
                id:4,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Im just setting everything up!',
                date:moment().format('LT'),
                type:'text'
            },{
                id:4,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Dont forget the weekend trip',
                date:moment().format('LT'),
                type:'text'
            },{
                id:5,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Dont forget the weekend trip',
                date:moment().format('LT'),
                type:'text'
            },{
                id:6,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Dont forget the weekend trip',
                date:moment().format('LT'),
                type:'text'
            },{
                id:7,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Dont forget the weekend trip',
                date:moment().format('LT'),
                type:'text'
            },{
                id:8,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Dont forget the weekend trip',
                date:moment().format('LT'),
                type:'text'
            },{
                id:9,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Dont forget the weekend trip',
                date:moment().format('LT'),
                type:'text'
            },{
                id:10,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Dont forget the weekend trip',
                date:moment().format('LT'),
                type:'text'
            },{
                id:11,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Dont forget the weekend trip',
                url:'http://i.pravatar.cc/400?img=70',
                date:moment().format('LT'),
                type:'image'
            },{
                id:12,
                sender:{
                    id:1,
                    name:'Edgar Marquez'
                },
                message:'Dont forget the weekend trip',
                url:'https://raw.githubusercontent.com/zmxv/react-native-sound-demo/master/advertising.mp3',
                date:moment().format('LT'),
                type:'audio'
            },{
                id:13,
                sender:{
                    id:1,
                    name:'Edgar Marquez'
                },
                message:'Dont forget the weekend trip',
                location:{
                    latitude:7.8579175,
                    longitude:-72.4979058
                },
                date:moment().format('LT'),
                type:'location'
            },{
                id:14,
                sender:{
                    id:2,
                    name:'Ricardo Perez'
                },
                message:'Dont forget the weekend trip',
                video:{
                    url:'https://www.youtube.com/watch?v=5kO2eWq6-ts',
                    thumbnail:'https://img.youtube.com/vi/5kO2eWq6-ts/default.jpg'
                },
                date:moment().format('LT'),
                type:'video'
            },{
                id:15,
                sender:{
                    id:1,
                    name:'Edgar Marquez'
                },
                message:'Dont forget the weekend trip',
                money:{
                    id:'000001',
                    mount:Number.parseFloat('200.00').toFixed(2),
                    from:'Ricardo Perez'
                },
                date:moment().format('LT'),
                type:'money'
            }]
        }
    }
    renderHeader(){
        return(
            <Header style={style.header}>
                <Row style={[style.sidesMenu,{justifyContent:'flex-start'}]}>
                    <Button  transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name='SI-0' size={20} style={style.icon} />
                    </Button>
                </Row>
                <Row style={[style.containerCenter]}>
                    <Col style={{maxWidth:50}}>
                        <Thumbnail style={{borderRadius:10}} height={40} width={40} source={{uri:'http://i.pravatar.cc/400?img=70'}} />
                    </Col>
                    <Col style={{justifyContent:'center'}}>
                        <Text style={[style.fontRoboto,{color:'white'}]}>James smith</Text>
                        <Text note>Online</Text>
                    </Col>
                </Row>
                <Row style={[style.sidesMenu,{justifyContent:'flex-end'}]}>
                    <Button  transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name='DI-5' size={20} style={style.icon} />
                    </Button>
                    <Button  transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name='I-30-Menu' size={20} style={style.icon} />
                    </Button>
                </Row>
            </Header>
        );
    }
    handleBoxView(){
        if(this.state.boxView){
            this.refs.inputMessage.focus();
        }
        this.setState({...this.state,boxView:!this.state.boxView});
    }
    renderFooter(){
        return(
            <Row style={style.containerFooter}>
                <Col style={style.containerMultimediaIcon}>
                    <TouchableWithoutFeedback>
                        <Button rounded style={{padding:12}} light><Icon name='SI-3' size={20} color='#17d6b6' /></Button>
                    </TouchableWithoutFeedback>
                </Col>
                <Row style={style.containeBarInput}>
                    <Col style={style.containerReactionIcon}>
                        <TouchableWithoutFeedback onPress={() => this.handleBoxView()}>
                            <Icon name={this.state.boxView ? 'DI-12' : 'I-18-Emojis'} size={20} color={this.state.boxView ? 'black' : '#C7C7C7'} />
                        </TouchableWithoutFeedback>
                    </Col>
                    <Col>
                        <TextInput placeholder='Type your messag'
                            ref='inputMessage'
                            multiline={true}
                            onFocus={() => this.state.boxView ? this.setState({...this.state,boxView:!this.state.boxView}) : null}
                            />
                    </Col>
                    <Col style={style.containerVoiceNoteIcon}>
                        <TouchableWithoutFeedback>
                            <Icon name='I-3-Microfono' size={20} color='#C7C7C7' />
                        </TouchableWithoutFeedback>
                    </Col>
                </Row>
                <Col style={style.containerSendButton}>
                    <TouchableWithoutFeedback>
                        <LinearGradient colors={['#30bfff','#0dffc7']} style={style.btnSend}>
                            <Icon name={'DI-4'} size={20} color='white' />
                        </LinearGradient>
                    </TouchableWithoutFeedback>
                </Col>
            </Row>
        )
    }
    renderTabs(){
        return(
            <Row style={style.containerTabs}>
                <Col style={style.containerOptionTab}>
                    <TouchableOpacity style={[style.tab,this.state.currentTab == 0 ? style.tabActive : null]} activeOpacity={0.5} onPress={() => this.goToPage(0)}>
                        <Icon name='I-18-Emojis' size={20} color={this.state.currentTab == 0 ? '#17d6b6' : '#c7c7c7'} />
                    </TouchableOpacity>
                </Col>
                <Col style={style.containerOptionTab}>
                    <TouchableOpacity activeOpacity={0.5} style={[style.tab,this.state.currentTab == 1 ? style.tabActive : null]}  onPress={() => this.goToPage(1)}>
                        <Icon name='DI-13' size={20} color={this.state.currentTab == 1 ? '#17d6b6' : '#c7c7c7'} />
                    </TouchableOpacity>
                </Col>
                <Col style={style.containerOptionTab}>
                    <TouchableOpacity activeOpacity={0.5} style={[style.tab,this.state.currentTab == 2 ? style.tabActive : null]} onPress={() => this.goToPage(2)}>
                        <Icon name='DI-0' size={20} color={this.state.currentTab == 2 ? '#17d6b6' : '#c7c7c7'} />
                    </TouchableOpacity>
                </Col>
                <Right>
                    <Col style={style.containerOptionTab}>
                        <TouchableOpacity>
                            <Icon name='DI-15' size={20} color='black' />
                        </TouchableOpacity>
                    </Col>
                </Right>
            </Row>
        )
    }
    goToPage(index){
        this.refs.scrollableTab.goToPage(index);
    }
    renderBox(){
        return(
            <Tabs style={style.reactionsTab} ref='scrollableTab' renderTabBar={()=> this.renderTabs()} onChangeTab={({ i }) => this.setState({...this.state,currentTab:i})}>
                <Tab heading="Emojis">
                    <EmojiPicker />
                </Tab>
                <Tab heading="Gifs">
                    <GiftPicker />
                </Tab>
                <Tab heading="Stickers">
                    <StickersPicker />
                </Tab>
            </Tabs>
        )
    }
    renderMessage(message){
        return(
            <MessageContent key={message.item.id} user={{id:1,name:'Edgar Marquez'}} message={message.item} />
        )
    }
    render(){
        return(
            <Container style={style.chatContainer}>
                {
                    this.renderHeader()
                }
                <Content>
                    <OptimizedFlatList keyExtractor={(item, index) => index.toString()} data={this.state.messages} renderItem={this.renderMessage} />
                </Content>
                {this.renderFooter()}
                {
                    this.state.boxView ?
                    this.renderBox()
                    :
                    null
                }
            </Container>
        )
    }
}