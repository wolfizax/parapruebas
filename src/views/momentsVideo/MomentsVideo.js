import React,{Component} from 'react';
import {View,Text,Image,Dimensions,TouchableOpacity} from 'react-native'
import {Container,Content,Thumbnail} from 'native-base';
import VideoPlayer from '../../presentation/VideoPlayer';
import moment from "moment";
import Comments from '../../presentation/Comments';
import HeadGoBack from '../../presentation/HeadGoBack';

let {width} = Dimensions.get('window');
let height = 0.56 * width;

class MomentsVideo extends Component{
	constructor(props){
		super(props);

		this.state= {
			showComment : false,
			text : 'i look this photo this morning',
			comments : [
			{
				displayName : 'displayName',
				date :  new Date(),
				text : 'comments'
			},
			{
				displayName : 'displayName',
				date :  new Date(),
				text :'comments'
			},
			{
				displayName : 'displayName',
				date :  new Date(),
				text :  'comments'
			}
			]
		}
	}

	render(){
		return(
			<Container>
				<HeadGoBack 
					title="Video"
					goBack = {()=>this.props.navigation.goBack()}
				/>
				<Content>
					<View style={{flexDirection:'row',paddingLeft:10,marginTop:20}}>
						<Thumbnail source={require('../../assets/bg.jpeg')} />
						<View style={{marginLeft:10}}>
							<Text style={{fontWeight:'bold'}}>Ricardo Perez</Text>
							<Text>{moment(new Date()).fromNow()}</Text>
						</View>
					</View>

					<View style={{width,height,marginBottom:20,marginTop:10}}>
						<VideoPlayer 
							source='https://vjs.zencdn.net/v/oceans.mp4'
							image = "https://baconmockup.com/300/200/"
						/>
					</View>
					
					<View style={{flexDirection:'row',marginBottom:10,marginLeft:10,marginRight:10}}>
						<View style={{flex:1,flexDirection:'row'}} >
							<TouchableOpacity><Text style={{fontWeight:'bold',fontSize:18,marginRight:18}}>Share</Text></TouchableOpacity>
							<TouchableOpacity><Text style={{fontWeight:'bold',fontSize:18}}>Like 45</Text></TouchableOpacity>
						</View>
						<TouchableOpacity onPress={()=>this.setState({showComment:!this.state.showComment})}>
						<View><Text  style={{fontSize:18}}>{this.state.comments.length} comments</Text></View>
						</TouchableOpacity>
					</View>
					<View style={{marginLeft:10,marginRight:10}}><Text>{this.state.text}</Text></View>
					{this.state.showComment && <Comments comments = {this.state.comments}/>}

				</Content>
			</Container>

		)
	}
}

export default MomentsVideo;