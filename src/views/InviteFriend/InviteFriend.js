import React,{Component} from 'react';
import Contacts from 'react-native-contacts';
import DeviceInfo from 'react-native-device-info';
import {View,Text,StyleSheet,TextInput,Platform } from 'react-native';
import CountryPicker, {getAllCountries} from 'react-native-country-picker-modal';
import {Container,Content,Tab, Tabs,Form,Item,Input,Label,Toast} from 'native-base';
import MyContacts from './MyContacts';
import Head from './Head';
import HeadSearch from '../../presentation/HeadSearch';
import ButtonFooter from '../../presentation/ButtonFooter';
import {Validation} from '../../utils/validation';

class InviteFriend extends Component{
	constructor(props){
		super(props);
		let userLocaleCountryCode = DeviceInfo.getDeviceCountry();
        const userCountryData = getAllCountries().filter(country => country.cca2 === userLocaleCountryCode).pop()
        let callingCode = null
        let cca2 = userLocaleCountryCode
        if (!cca2 || !userCountryData) {
            cca2 = 'US'
            callingCode = '1'
        } 
        else {
            callingCode = userCountryData.callingCode
        }
		this.state = {
			tab  : 0,
			searchActive:false,
			contacts : [],
			search : '',
			number : '',
			cca2,
            callingCode
		}
	}

	componentDidMount(){
		Contacts.requestPermission((err, permission) => {
      		Contacts.getAll((err, contacts) => {
  				this.setState({
  					contacts
  				})
			})
    	})
      
	}

	handleSentInvitation(contact){
		console.log(contact.phoneNumbers[0].number)
	}

	handleSearch(text){
		this.setState({search: text });
        console.log(this.state.search)
		
	}

	handleChangeText(text){
		text = text.replace(/^0+/, '');
        this.setState({...this.state,number: text.replace(/[^0-9]/g, '')})
	}

	handleSendNumber(){
		if(this.state.number.length<10){
			Toast.show({
                text:'Your telephone number is invalid format. It must have a minimum of 10 digits.',
                duration:4000,
                type:'danger',
                position:'bottom'
            });
		}else{
            console.log(this.state.callingCode,this.state.number)
			
		}
	}


	render(){
		return(
		<Container>

				{(this.state.searchActive && this.state.tab ==1)
				? 	<HeadSearch 
                    	handleSearch = {(search)=>this.setState({search})} 
                    	activateSearch = {()=>this.setState({searchActive:false})}
                	/>
                :	<Head
						goBack = {()=>this.props.navigation.goBack()}
						search = {()=>this.setState({searchActive:!this.state.searchActive})}
						searchActive = {this.state.tab}
					/>
				}
			
				
				<Tabs 

				 	tabBarBackgroundColor={'white'} 
				 	tabBarUnderlineStyle={{backgroundColor:'black',height:1.5}} 
				 	tabContainerStyle={{elevation:0}} 
				 	initialPage = {this.state.tab}
				 	onChangeTab= {({i})=>this.setState({tab:i})}
				 	>
                	<Tab 
                        heading="Number"
                        tabStyle={{backgroundColor:'white'}} 
                        textStyle={{color:'#c7c7c7'}} 
                        activeTabStyle={{ backgroundColor: 'white' }} 
                        activeTextStyle={{color:'black'}}>
          
                        	<Content >
                            <View style={styles.ctn}>
								<View style={styles.top}>
									<Text style={styles.h1}>Invite a friend</Text>
								</View>

								<View><Text>Region</Text></View>

								<Form style={styles.form} >
									<View style={{marginBottom:10}}><Text>Phone number</Text></View>

									<Item>
										<CountryPicker hideAlphabetFilter filterable  onChange={(value) => this.setState({...this.state,cca2:value.cca2,callingCode:value.callingCode})} cca2={this.state.cca2} showCallingCode closeable />
									</Item>

									<Item>
              							<TextInput 
              							placeholder = "Phone number"
                                    	keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'phone-pad'}
              							onChangeText = {this.handleChangeText.bind(this)}
              							maxLength={11}
              							value={this.state.number}
              							onSubmitEditing = {this.handleSendNumber.bind(this)}
             							/>
            						</Item>

								</Form>

								<View><Text>An sms will be sent to the phone number,so that your friend can register in wonderchatt and be your contact</Text></View>

                            </View>
							</Content>
                        
                       		{this.state.number == '' ? <View /> : <ButtonFooter text = "Send Invitation" handleSucess = {this.handleSendNumber.bind(this)}/>}


                	</Tab>
                	<Tab  
                        heading="My Contacts"
                        tabStyle={{backgroundColor:'white'}} 
                        textStyle={{color:'#c7c7c7'}} 
                        activeTabStyle={{ backgroundColor: 'white' }} 
                        activeTextStyle={{color:'black'}}>  

                			<MyContacts 
                				search = {this.state.search}
                				contacts = {this.state.contacts}
                				handleSentInvitation = {this.handleSentInvitation.bind(this)}
                			/>


                	</Tab>

                </Tabs>
         
		</Container>
		)
	}
}

const styles = StyleSheet.create({
	ctn:{
		flex:1,
		marginLeft:10,
		marginRight:10
	},
    top :{
    	marginTop:20,
    	marginBottom:20,
    	justifyContent:'center',
    	alignItems:'center'
    },
    h1:{
    	fontSize:20,
    	fontWeight:'bold'
    },
    form:{
    	marginTop:10,
    	marginBottom:25
    },
    region:{
    	fontWeight:'bold',
    	fontSize:20
    },
    icon:{
    	marginLeft:10,
    	marginTop:5
    }
})


export default InviteFriend;
