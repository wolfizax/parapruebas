import React from 'react';
import {View,TouchableOpacity,StyleSheet,ActivityIndicator} from 'react-native';
import {Content,List,ListItem,Left,Body,Text,Right,Thumbnail} from 'native-base';
import {OptimizedFlatList} from 'react-native-optimized-flatlist'
import Proptypes from 'prop-types';

const MyContacts = ({contacts,handleSentInvitation,search}) =>(
    <Content  style={{borderBottomWidth:1,borderBottomColor:'#c7c7c7'}}>

        <OptimizedFlatList
            data={contacts}
            extraData={search}
            initialNumToRender = {10}
            keyExtractor={(item, index) => item.recordID}
            renderItem={ ({item}) => 

                {if(item.givenName.toLowerCase().indexOf(search.toLowerCase())!==-1 || search == ''){ 
                    return(
                    <ListItem style={styles.list}>
				        <Left>
                            {item.thumbnailPath ? <Thumbnail source={{uri : item.thumbnailPath}} /> : <Thumbnail source={require('../../assets/usuario.png')}/>}
                            <Body style={{marginTop:17}}><Text numberOfLines = { 1 }>{item.givenName &&  item.givenName} {item.familyName && item.familyName}</Text></Body> 
                        </Left>         
                        <View>
                	       {
                            item.sent 
                            ? <ActivityIndicator size="small" color="#3CD22F" /> 
                            : 
                            <View>
                                {!item.invite ? <TouchableOpacity onPress={()=>handleSentInvitation(item)}><Text style={styles.buttonText}> Sent invitation </Text></TouchableOpacity> : <View><Text style={{color:'#CCCCCC'}}>sent</Text></View>}
                            </View>
                            }
                        </View>           			
                    </ListItem>
                    )
                }}

            }
        />
    </Content>


)

const styles = StyleSheet.create({
    list:{
        marginLeft:10,
        marginRight:10,
        justifyContent:'space-around',
        flexDirection:'row'
    },
    buttonText: {
        color: '#3CD22F'
    }
})

MyContacts.proptypes = {
    contacts : Proptypes.array.isRequired,
    handleSentInvitation : Proptypes.func.isRequired
}



export default MyContacts;