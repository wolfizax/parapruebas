import React from 'react';
import {View,StyleSheet,Text,PixelRatio} from 'react-native';
import {Button} from 'native-base';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

const Head = ({search,goBack,searchActive})=>(
    <View style={styles.head}>

        <Button transparent onPress = {()=>goBack()} style={{marginTop:10}}>
            <Icon name='left-arrow' size={16} color="#3CD22F" />
        </Button>

        <View >
            <Text style={styles.title}>invite</Text>
        </View>
        
        {searchActive 
        ?<Button transparent onPress = {()=>search()} style={{marginTop:10}}><Icon name='Enmascarar-grupo-63' size={18} color="#3CD22F" /></Button>
        :<View />
        }

    </View>
   
)

const styles = StyleSheet.create({
    head:{
        height:56,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:'white',
        color:'white',
        paddingLeft:10,
        paddingRight:10,
    },
    icon:{
        color:'white',
        marginLeft:10,
        fontSize: 30
    },
    title:{
        color:'#3CD22F',
        fontSize:20
    }

})

Head.proptypes = {
    goBack : Proptypes.func.isRequired,
    search : Proptypes.func.isRequired
}

export default Head;
