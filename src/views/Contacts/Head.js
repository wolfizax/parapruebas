import React from 'react';
import {View,StyleSheet} from 'react-native';
import {Button,Text} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

const Head = ({navigation})=>(
   
     <LinearGradient style={styles.head} colors={['#52ED52', '#3CD22F' ,'#22B307']} start={{x: 0, y: 0}} end={{x: 1, y: 0}} >  
        <Button style={styles.icon} transparent onPress={()=>navigation('MenuContacts')}><Icon name='list-menu' size={18} color="black" /></Button>
        <Text style={styles.title}>Weed Utopia</Text> 
        <Button style={styles.icon} transparent onPress={()=>navigation('SearchContacts')}><Icon name='Enmascarar-grupo-63' size={18} color="black" /></Button>       
    </LinearGradient> 

)

const styles = StyleSheet.create({
    head:{
        height:50,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        color:'white',
        paddingLeft:10,
        paddingRight:10
    },
    icon:{
        marginTop:4
    },
    title:{
        color:'white',
        fontSize:20
    }
})

Head.proptypes = {
    navigation: Proptypes.func.isRequired
}


export default Head;