import React,{Component} from 'react';
import {View} from 'react-native';
import { Container,Content,Tab, Tabs, ScrollableTab} from 'native-base';
import Head from './Head';
import ListContact from './ListContacts';
import Alphabetic from './Alphabetic'
import Icon from '../../presentation/Icon/index';
import {connect} from 'react-redux';
import {capitalize} from '../../utils/letter';

class Contacts extends Component{

    constructor(props){
        super(props);
        this.state = {
            alphabetic : null
        }
    }

    static navigationOptions ={
        tabBarIcon : ({tintColor})=>(
        <Icon name='users' size ={20} color={tintColor} />
        )
    }

    handleSelectLetter(label){
        label = this.state.alphabetic == label ? null : label;
        this.setState({alphabetic :label});
    }


    render(){

        return(
            <Container>
                <Head 
                    navigation = {(routeName)=>this.props.navigation.navigate(routeName)}
                />
                <Tabs tabBarBackgroundColor={'white'} tabBarUnderlineStyle={{backgroundColor:'black',height:1.5}}  renderTabBar={()=> <ScrollableTab />}>
                    {this.props.category.map((item,i)=>
                    <Tab 
                        key = {i} 
                        heading={capitalize(item.label)} 
                        tabStyle={{backgroundColor:'white'}} 
                        textStyle={{color:'#c7c7c7'}} 
                        activeTabStyle={{ backgroundColor: 'white' }} 
                        activeTextStyle={{color:'black'}}
                    >
                        
                        <View style={{flexDirection:'row',position:'relative',flex:1}}>
                            <Content>
                                <ListContact 
                                    contacts = {this.props.contacts}
                                    category = {item.label}
                                    handleSelectContact = {(item,index)=>this.props.navigation.push('InfContacts',{item,index})}
                                    alphabetic =  {this.state.alphabetic}
                                />
                           
                            </Content>

                            <Alphabetic 
                                handleSelectLetter = {this.handleSelectLetter.bind(this)}
                                alphabetic  =  {this.state.alphabetic}
                                contacts = {this.props.contacts}
                                category = {item.label}
                            />

                        </View>

                    </Tab>
                    )}
                                   
                </Tabs>

            </Container>

        )
    }
}

const mapStateToProps = (state,props) =>{
    return{
        category: state.category,
        contacts : state.contacts
    }
} 

  
export default connect(mapStateToProps,null)(Contacts) 
