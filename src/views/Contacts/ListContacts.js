import React from 'react';
import {StyleSheet,View,TouchableOpacity,Dimensions} from 'react-native';
import {List, ListItem, Left, Body, Right, Thumbnail, Text,Button,Badge} from 'native-base';
import Icon from '../../presentation/Icon/index';
import {capitalize,letter} from '../../utils/letter';
import Proptypes from 'prop-types';

let {width} = Dimensions.get('window');

const ListContacts = ({contacts,category,handleSelectContact,alphabetic})=>(
    <List style={{width : width - 30}}>
        {contacts.sort((a,b)=>{return a.displayName.localeCompare(b.displayName)}).map((item,i)=>
            {if((item.category == category || category == 'all') && (alphabetic == letter(item.displayName) || alphabetic == null)){
            return(
                <ListItem Thumbnail key ={i} style={styles.list}>
                    <Left style={{marginRight:3}}>
                        <TouchableOpacity onPress={()=>handleSelectContact(item,i)}>
                            {!item.avatar ? <Thumbnail source={require('../../assets/usuario.png')} /> : <Thumbnail source={{uri: item.avatar }} />} 
                        </TouchableOpacity>
                        <Body style={{flex:1}}>
                            <Text numberOfLines = { 1 }>{capitalize(item.displayName)}</Text>
                            <Text note>{item.category}</Text>
                        </Body>
                    </Left>
                    <View style={{flexDirection:'row',paddingLeft:5}} >
                        <Button transparent style={{marginRight:19}}><Icon name="chat" size={20} color="#3CD22F"/></Button>
                        <Button transparent><Icon name="telephone" size={20} color="#3CD22F"/></Button>
                    </View>
                </ListItem>
            )}}

        )}
    </List>
)

const styles = StyleSheet.create({
    list:{
        paddingBottom:10,
        marginRight:10,
        marginLeft:10,
        justifyContent:'space-between'
    }

})

ListContacts.proptypes = {
    handleSelectContact: Proptypes.func.isRequired,
    category : Proptypes.string.isRequired,
    contacts : Proptypes.array.isRequired
}



export default ListContacts;