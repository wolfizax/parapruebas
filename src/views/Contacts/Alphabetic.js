import React from 'react';
import {View,Text,StyleSheet,Dimensions,ScrollView,TouchableOpacity}  from 'react-native';
import {alphabet,findAlphabetic} from '../../utils/letter';
import Proptypes from 'prop-types';

const Alphabetic = ({alphabetic,handleSelectLetter,contacts,category}) =>(

	<View>
	{findAlphabetic(contacts,category) &&

	<ScrollView style={styles.list}>
		{alphabet.map((label,i)=>
			<TouchableOpacity key={i} onPress={()=>handleSelectLetter(label)}>
				<View  style={alphabetic == label ? styles.badge : styles.itemList}>
					<Text style={alphabetic == label ? styles.textBadge : styles.textList }>{label.toUpperCase()}</Text>
				</View>
			</TouchableOpacity>
		)}
	</ScrollView>
	}
	</View>

)

const styles = StyleSheet.create({
	list:{
		marginTop:10,
		width:40,
		position:'absolute',
		right:0,
		top:0,
		bottom:0
	},
    itemList:{
        marginBottom:5,
        justifyContent:'center',
        alignItems:'center',
        width:34
    },
    textList:{
    	color:'#C7C7C7'
    },
    badge:{
        width:34,
        height:34,
        borderRadius:17,
        backgroundColor: '#3CD22F',
        alignItems:'center',
        justifyContent: 'center',
        marginBottom:5
    },
    textBadge:{
        color:'white',
        fontSize:12
    }

})

Alphabetic.proptypes = {
	alphabetic : Proptypes.string,
	handleSelectLetter : Proptypes.func.isRequired,
	contacts : Proptypes.array.isRequired,
	category : Proptypes.string.isRequired,

}

export default Alphabetic;