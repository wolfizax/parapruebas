import React,{Component} from 'react';
import HeaderView from '../../presentation/Header'
import LoggedView from '../index';
import { Image, ImageBackground,View } from 'react-native';
import { Container, Content, H1, Body, H2, H3 } from 'native-base';
import { Grid, Row} from 'react-native-easy-grid'
import { connect } from 'react-redux';
import {LeftMenu} from '../../actions/nav';
import Icon from '../../presentation/Icon';
import style from './style';

@connect(state => ({
    user: state.user
}))
/** @extends Component */
export default class DashboardView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        tabBarIcon: ({tintColor}) => (<Icon name='DI-1' color={tintColor ? tintColor : '#C7C7C7'} size={20} />)
        });
    constructor(props){
        super(props)
        props.navigation.setParams({title:'WonderChatt'});
    }
    openMenu = () => {
        this.props.navigation.dispatch(LeftMenu());
    }
    login = () => {
        return(
        <Content padder>
            <Grid>
                <Row style={[style.contentCenter,{marginVertical:20}]}>
                    <Body>
                        <Image resizeMode='contain' style={style.animationLogo} source={require('../../assets/dashboard_animation.gif')} />
                    </Body>
                </Row>
                <Row style={[style.contentCenter,{marginTop:40}]}>
                <H1>{this.props.user.name}</H1>
                </Row>
                <Row style={style.contentCenter}>
                    <H2 style={style.fontBold}>Welcome back</H2>
                </Row>
            </Grid>
        </Content>
        );
    }
    firstLogin = () =>{
        return(
            <View style={{flex:1,width:'100%',height:'100%'}}>
            <ImageBackground resizeMode='stretch' resizeMethod='auto' style={{width:'100%',height:'100%'}} source={require('../../assets/dashboard_bg.jpeg')}>
            <Grid>
                <Row style={[style.contentCenter,{marginTop:20}]}>
                    <H1 style={[style.fontBold,{color:'white'}]}>{this.props.user.name}</H1>
                </Row>
            </Grid>
            </ImageBackground>
        </View>
        );
    }
    render(){
        return(
            <Container style={this.props.navigation.getParam('firstLogin') == undefined ? {backgroundColor:'#E2E6E5'} : null} >
                <HeaderView title='WonderChatt' leftComponent={{icon:'I-30-Menu',function:this.openMenu}} />
                <Content padder>
                    {
                        this.props.navigation.getParam('firstLogin') == undefined ? 
                        this.login()
                        :
                        this.firstLogin()
                    }
                </Content>
            </Container>
        );
    }
}