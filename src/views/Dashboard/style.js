import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    contentCenter:{
        justifyContent:'center',
        margin:3
    },
    textCenter:{
        textAlign:'center'
    },
    fontBold:{
        fontWeight:'bold'
    },
    fontItalic:{
        fontStyle:'italic'
    },
    logoImage:{
        height:200,
        width:200
    },
    animationLogo:{
        width:350
    },
    textLink:{
        color:'#17d6b6'
    }
});