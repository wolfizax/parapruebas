import React from 'react';
import {View,Switch} from 'react-native';
import {Body,Text,Left,Right,ListItem } from 'native-base';
import { TextInputMask } from 'react-native-masked-text'
import Icon from '../../presentation/Icon/index'
import Proptypes from 'prop-types';

const Config = ({groupPrivate,handlegroupPrivate,groupFree,amount,handleAmount,handlePrice})=>(

    <View style={{marginTop:20,marginBottom:20}}>
        <View style={{marginLeft:10}}><Text style={{fontWeight:'bold'}}>Group type</Text></View>

        <ListItem >  
            <Left>
                <Body style={{flexDirection:'column',marginTop:10}}><Text>Public/Private</Text></Body> 
            </Left>                      
            <Right style={{justifyContent: 'center',flexDirection:'row'}}>
                <Switch style={{marginRight:3}} value = {groupPrivate} onValueChange = {()=>handlegroupPrivate()}/>
                {groupPrivate ?  <Icon name="PublicGroup" size={20}/>  : <Icon name="PrivateGroup"  size={20} color="black"/> }        
            </Right>
                                     
        </ListItem>

        <View style={{marginLeft:10,marginTop:5}}><Text style={{fontWeight:'bold'}}>Price</Text></View>

        {groupPrivate &&
        <ListItem>
            <Left>        
                {groupFree ? <TextInputMask type={'money'} value={amount} onChangeText={(text)=>handlePrice(price)} options={{ unit: 'US$' }}  style={{borderBottomWidth:0.5,borderBottomColor:'#c7c7c7'}} /> : <Body style={{flexDirection:'column',marginTop:10}}><Text>Free</Text></Body>}
            </Left> 
            <Right style={{justifyContent: 'center',flexDirection:'row'}}>
                <Switch style={{marginRight:3}}  value = {groupFree} onValueChange = {()=>handleAmount()}/>
                <Icon name="I-35-Money" size={20} color="#17D6B6"/> 
            </Right>
        </ListItem>
        }
    </View>

)

Config.proptypes = {
    amount : Proptypes.string.isRequired,
    groupPrivate : Proptypes.bool.isRequired,
    groupFree : Proptypes.bool.isRequired,
    handlegroupPrivate : Proptypes.func.isRequired,
    handlePrice : Proptypes.func.isRequired
}


export default Config;