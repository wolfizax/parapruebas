import React,{Component} from 'react';
import {View,StyleSheet,TouchableOpacity,Text} from 'react-native';
import Modal from 'react-native-modal';
import {Button,Right} from 'native-base';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

const Modalbox = ({modal,handleGalery,handleCamera,handleModal})=>(
  <Modal isVisible={modal} style={styles.bottomModal} onBackdropPress={()=>handleModal()} onBackButtonPress ={()=>handleModal()}>
    <View style={styles.modalContent}> 
      <TouchableOpacity onPress = {()=>handleCamera()}><View style={{width:80,height:80,borderRadius:30, alignItems:'center',justifyContent:'center',backgroundColor:'#30BFFF'}}><Text><Icon name="I-16-Camara" size={30} color="white" /></Text></View></TouchableOpacity> 
      <TouchableOpacity onPress = {()=>handleGalery()}><View style={{width:80,height:80,borderRadius:30, alignItems:'center',justifyContent:'center', backgroundColor:'#FF9E2E'}}><Text><Icon name="I-32-Galeria" size={30} color="white" /></Text></View></TouchableOpacity> 
    </View>
  </Modal>
          
)

const styles = StyleSheet.create ({
  bottomModal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  modalContent: {
    backgroundColor: 'white',
    padding: 22,
    justifyContent: 'space-around',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
    flexDirection:'row'
  }
})


Modalbox.proptypes = {
    modal : Proptypes.bool.isRequired,
    handleModal : Proptypes.func.isRequired,
    handleCamera : Proptypes.func.isRequired,
    handleGalery : Proptypes.func.isRequired
}

export default Modalbox