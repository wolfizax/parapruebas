import React,{Component} from 'react';
import {View} from 'react-native';
import {Container,Content,Text} from 'native-base';
import ImagePicker from 'react-native-image-crop-picker';
import Head from './Head';
import Members from '../../presentation/ListContacts';
import Modalbox from './Modalbox';
import Config from  './Config';
import Infogroup from './Infogroup';
import ButtonFooter from '../../presentation/ButtonFooter';
import {connect} from 'react-redux';

class CreateGroup extends Component{
    constructor(props){
        super(props);
        this.state = {
            modal : false,
            groupPrivate : false,
            groupFree :false,
            amount : '1,00',
            nameGroup : '',
            image : null
        }
    }

    handleGalery(){
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
        }).then(image => {
            this.setState({
                image : image.path,
                modal : false
            })
        });
    }

    handleCamera(){
        ImagePicker.openCamera({
            width: 400,
            height: 400,
            cropping: true,
        }).then(image => {
            this.setState({
                image : image.path,
                modal : false
            })
        });
    }

    handleAmount(event, maskedvalue, floatvalue){
        this.setState({
            amount: maskedvalue
        })
    }

    handleSucess (){
        console.log('end point create group ')
    }

    render(){
        return(
            <Container>
                <Head
                title = "New group"
                goBack = {()=>this.props.navigation.goBack()}
                />
                <Content>
                    <Infogroup
                        nameGroup = {this.state.nameGroup}
                        image = {this.state.image}
                        handleModal = {()=>this.setState({modal:!this.state.modal})}
                        handleText = {(text)=>this.setState({nameGroup:text})}
                    />

                    <Config 
                        handlegroupPrivate = {()=>{this.setState({groupPrivate:!this.state.groupPrivate})}}
                        groupPrivate = {this.state.groupPrivate}
                        groupFree = {this.state.groupFree}
                        amount = {this.state.amount}
                        handleAmount = {()=>{this.setState({groupFree:!this.state.groupFree})}}
                        handlePrice = {(amount)=>this.setState({amount})}
                   />
                    <View style={{marginLeft:10}}><Text style={{fontWeight:'bold'}}>Members</Text></View>                 
                    <Members
                        contacts = {this.props.members}
                    />
                </Content>
                <Modalbox
                    handleModal = {()=>this.setState({modal:!this.state.modal})}
                    modal = {this.state.modal}
                    handleGalery  = {this.handleGalery.bind(this)}
                    handleCamera = {this.handleCamera.bind(this)}
                />
                {(this.state.image != null && this.state.nameGroup!='') && <ButtonFooter text = 'Create' handleSucess = {this.handleSucess.bind(this)} />}
            </Container>
        )
    }
}

const mapStateToProps = (state,props) =>{
    return{
        members : state.members
    }
} 

  
export default connect(mapStateToProps,null)(CreateGroup);