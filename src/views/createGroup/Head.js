import React from 'react';
import {View,StyleSheet,Text} from 'react-native';
import {Header,Button} from 'native-base';
import Proptypes from 'prop-types';

const Head = ({title,goBack})=>(

    <Header style={styles.head}>  
        
        <Button transparent style={styles.icon} onPress = {()=>goBack()}>
            <Text style={styles.cancel}>cancel</Text>
        </Button>

        <View >
            <Text style={styles.title}>{title}</Text>
        </View>
        
        <View />
                
    </Header>
   
)

const styles = StyleSheet.create({
    head:{
        height:50,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        color:'white',
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:'white',
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    icon:{
        marginTop:4
    },
    title:{
        color:'black',
        fontSize:20
    },
    cancel:{
        color:'#3CD22F'
    }

})

Head.proptypes = {
    title : Proptypes.string.isRequired,
    goBack : Proptypes.func.isRequired
}

export default Head;
