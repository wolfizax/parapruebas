import React,{Component} from 'react';
import {View,TouchableHighlight,Image} from 'react-native';
import {Item, Input, Label} from 'native-base';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

const Infogroup = ({image,handleModal,nameGroup,handleText})=>(
    <View style={{paddingHorizontal:10,marginTop:10}}>
        <View style={{flexDirection:'row'}}>
            <TouchableHighlight style={{width:80,height:80,borderRadius:30}}  onPress = {()=>handleModal()}>
                <View style={{width:80,height:80,borderRadius:30,backgroundColor:'#eee',alignItems:'center',justifyContent:'center',position:'relative'}} >
                    {image != null ? <Image source={{uri: image}} style={{width:80,height:80,borderRadius:30}}/> : <Icon name="I-16-Camara" size={30} color="#c7c7c7" />}
                    <View style={{backgroundColor:'#3CD22F',width:25,height:25,borderRadius:12.5,position:'absolute',bottom:3,left:-5,alignItems:'center',justifyContent:'center',shadowColor: "#000",shadowOffset: {width: 0,height: 0},shadowOpacity: 0.34,shadowRadius: 6.27,elevation: 10}}><Icon name ="SI-3" color="white" size={15} /></View>
                </View>
            </TouchableHighlight>
            <View style={{flex:1,marginLeft:10}}>
                <Item floatingLabel>
                    <Label>Group-name</Label>
                    <Input value={nameGroup} onChangeText = {(text)=>handleText(text)} />
                </Item>
            </View>                   
        </View>
    </View>

)

Infogroup.proptypes = {
    image : Proptypes.string.isRequired,
    handleModal : Proptypes.func.isRequired,
    nameGroup : Proptypes.string.isRequired,
    handleText : Proptypes.func.isRequired,
}



export default Infogroup;