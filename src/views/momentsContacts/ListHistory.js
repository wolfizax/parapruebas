import React from 'react';
import {View,Image,Dimensions,TouchableOpacity,StyleSheet} from 'react-native';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

let {width} = Dimensions.get('window');
width = width - 50;

const ListHistory = ({history,navigation})=>(

	<View style={styles.ctn}>
		{history.map((item,i)=>
			<TouchableOpacity key={i}  onPress = {()=>item.type == 'video' ? navigation('MomentsVideo') : navigation('MomentsPhoto')}>
				<View styles={styles.parentImg}>
					{item.type=='video' && <Icon name ="I-14-Video" size={16} color="white" style={styles.icon} /> }
					<Image source={require('../../assets/bg.jpeg')} style={styles.image} />
				</View>
			</TouchableOpacity>
		)}
	</View>
)

const styles = StyleSheet.create({
	ctn:{
		marginTop:20,
		marginLeft:10,
		marginRight:5,
		flexDirection:'row',
		flexWrap:'wrap'
	},
	parentImg:{
		position:'relative',
		width:width/3,
		height:width/3,
	},
	image:{
		width:width/3,
		height:width/3,
		borderRadius:3,
		marginLeft:5,
		marginRight:5,
		marginBottom:10
	},
	icon:{
		position:'absolute',
		top:5,
		right:5,
		zIndex:1000
	}

})

ListHistory.proptypes = {
    history : Proptypes.array.isRequired,
    navigation : Proptypes.func.isRequired
}

export default ListHistory;