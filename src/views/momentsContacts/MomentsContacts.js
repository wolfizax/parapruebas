import React,{Component} from 'react';
import {View,Text} from 'react-native';
import {Container,Content,Thumbnail} from 'native-base';
import ListMoments from './ListMoments';
import ListHistory from './ListHistory';
import HeadGoBack from '../../presentation/HeadGoBack';
import Divider from '../../presentation/Divider';


class MomentsContacts extends Component{

	constructor(props){
		super(props);
		this.state = {
			moments : [1,2,3,4,5,6,7,8,9,10,11,12,13,14],
			history : [
				{
					type:'photo'
				},
				{
					type:'photo'
				},
				{
					type:'video'
				},
				{
					type:'video'
				},
				{
					type:'photo'
				},
				{
					type:'video'
				},
				{
					type:'video'
				}
			
			]
		}
	}

	render(){
		return(

			<Container>
				<HeadGoBack 
					title="Moments Contacts"
					goBack = {()=>this.props.navigation.goBack()}
				/>
				<Content> 
					<View style={{flexDirection:'row',paddingLeft:10,marginTop:20,marginBottom:5}}>
						<Thumbnail large source={require('../../assets/bg.jpeg')} />
						<View style={{marginLeft:10}}>
							<Text style={{fontWeight:'bold',fontSize: 26}}>Ricardo Perez</Text>
							<Text>developers</Text>
						</View>
					</View>

					<Divider />
					
					<ListMoments moments = {this.state.moments} navigation = {()=>this.props.navigation.navigate('MyMoments')} />
					
					<Divider/>	

					<ListHistory history = {this.state.history} navigation = {(routeName)=>this.props.navigation.navigate(routeName)} />
				
				</Content>
			</Container>

		)
	}
}

export default MomentsContacts;