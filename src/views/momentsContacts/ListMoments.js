import React from 'react';
import {View,ScrollView,TouchableOpacity} from 'react-native';
import { Thumbnail} from 'native-base';
import Proptypes from 'prop-types';

const ListMoments = ({moments,navigation})=>(

	 <ScrollView 
        horizontal={true}
        showsHorizontalScrollIndicator = {false}
        contentContainerStyle ={{
        	alignItems : 'center',
        	paddingTop: 5,
        	paddingEnd: 5
        }}
    >
        {moments.map((item,i)=>
        	<View key={i} style={{marginTop:10,marginBottom:10}}>
        		<TouchableOpacity onPress = {()=>navigation()}>
            		<Thumbnail source={require('../../assets/bg.jpeg')} style={{marginLeft:10,marginRight:10}} /> 
        		</TouchableOpacity>
        	</View>
        )}
         
    </ScrollView>
)


ListMoments.proptypes = {
  	moments : Proptypes.array.isRequired,
	navigation : Proptypes.func.isRequired

}

export default ListMoments;