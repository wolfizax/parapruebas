import React from 'react';
import {StyleSheet,View} from 'react-native';
import {Text,Right,Button} from 'native-base';
import Modal from 'react-native-modal';
import Proptypes from 'prop-types';

const MessageModal = ({modal,handleModal})=>(

	<Modal onBackdropPress={() => handleModal()} onBackButtonPress={() => handleModal()} isVisible={modal}>
        <View style={styles.modalContent}>     
            <View>
                <Text>This category already exists!!</Text>
            </View>
            <View style={styles.btn}>
                <View style={{flex:1}}/>
                <Button transparent dark onPress={()=>handleModal()}><Text>Ok</Text></Button>
            </View>
        </View>
             
    </Modal>


)
const styles = StyleSheet.create({

	modalContent:{
		width:'80%',
		marginLeft:'10%',
        backgroundColor:'white',
        padding:15,
        borderRadius:5,
        borderColor:"rgba(0, 0, 0, 0.1)"

    },
    modalContentRow:{
        width:'100%',
        flexWrap:'wrap',
        flexDirection:'row',
        alignItems:'center',
        padding:5
    },
    btn:{
        flexDirection:'row'
    }
})


MessageModal.proptypes = {
    modal: Proptypes.bool.isRequired,
    handleModal : Proptypes.func.isRequired
}



export default MessageModal;