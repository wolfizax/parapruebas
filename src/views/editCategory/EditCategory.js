import React,{Component} from 'react';
import {Container,Content, Item, Input, Label } from 'native-base';
import HeadGoBack from '../../presentation/HeadGoBack';
import ButtonFooter from '../../presentation/ButtonFooter';
import {editCategory } from '../../actions/category';
import {editContacts } from '../../actions/contacts';
import {editCategoryC} from '../../utils/contacts';
import {connect} from 'react-redux';
import MessageModal from './MessageModal';

class EditCategory extends Component{
	constructor(props){
		super(props)
		this.state = {
			input : this.props.navigation.getParam('category'),
			modal:false,
			category : this.props.navigation.getParam('category')
		}
	}

	handleSucess(){
		if(this.state.input == this.state.category){
			this.props.navigation.goBack();
		}else{
			if(this._handleChecked(this.props.category,this.state.input)){
				this.setState({modal:!this.state.modal})
			}else{
				this.props.handleEditCategory(this.state.input,this.state.category);
				this.props.handleEditContacts(editCategoryC(this.props.contacts,this.state.category,this.state.input))
				this.setState({input : ''});
				this.props.navigation.goBack();
			}
		
		}
			
	}

	_handleChecked(categories,text){
		for(let i = 0;i<categories.length - 1;i++){
			if(categories[i]==text.toLowerCase()) return true;
		}
		return false;
	}

	render(){
		return(
			<Container>
				<HeadGoBack 
					title="Edit category"
					goBack = {()=>this.props.navigation.goBack()}
				/>
				<Content>
            			<Item floatingLabel style={{marginLeft:10,marginRight:10,marginTop:20}}>
              				<Label  style={{fontWeight:'bold'}}>Modify category</Label>
              				<Input 
              					value={this.state.input}
              					onChangeText = {(input)=>this.setState({input})}
              				/>
            			</Item>  				

				</Content>

				{this.state.input != '' && <ButtonFooter text = 'Save' handleSucess = {this.handleSucess.bind(this)} />}
			
				<MessageModal 
					modal = {this.state.modal}
					handleModal = {()=>this.setState({modal:!this.state.modal})}
				/>
			</Container>
		)
	}
}

const mapStateToProps = (state,props) =>{
    return{
        category: state.category,
        contacts : state.contacts
    }
} 


const mapDispatchToProps = dispatch =>{
    return{
        handleEditCategory(text,category){       
            dispatch(editCategory(text,category))
        },
        handleEditContacts(contacts){
        	dispatch(editContacts(contacts))
        }
  
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(EditCategory);