import React from 'react';
import {View,StyleSheet,Text} from 'react-native';
import {Header,Button} from 'native-base';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';


const Head = ({goBack,activateSearch})=>(

	<Header style={styles.head}>
        <Button transparent style={styles.icon} onPress={()=>goBack()}>
            <Text style={{color:"#3CD22F"}}>cancel</Text>                
        </Button>
        <Text style={styles.title}>New conversation</Text>
        <Button transparent style={styles.icon}  onPress={()=>activateSearch()}>
            <Icon name="Enmascarar-grupo-63" size = {18} color="#3CD22F" />               
        </Button>
    
    </Header>

)

const styles = StyleSheet.create({
    head:{
        height:50,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:'white',
        color:'white',
        paddingLeft:10,
        paddingRight:10
    },
    icon:{
        marginTop:4
    },
    title:{
        color:'black',
        fontSize:20,
        paddingLeft:3
    }

})

Head.proptypes = {
    goBack : Proptypes.func.isRequired,
    activateSearch : Proptypes.func.isRequired
}

export default Head;
