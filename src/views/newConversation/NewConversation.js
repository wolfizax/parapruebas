import React,{Component} from 'react';
import {Container,Content} from 'native-base';
import HeadSearch from '../../presentation/HeadSearch';
import Head from './Head';
import Contacts from '../../presentation/ListNavigationContacts';

class NewConversation extends Component{
	constructor(props){
        super(props);
        this.state = {
            activate : false,
            search : '',
            activateAddContacts : false,
            members : [],
            contacts :[
                {
                    displayName : 'juan'
                },
                {
                    displayName : 'pedro'
                },
                {
                    displayName : 'ricardo'
                },
                {
                    displayName : 'patricio'
                }
            ]
        }
    }

	render(){
		return(
			<Container>
				{this.state.activate ?  <HeadSearch handleSearch = {(search)=>this.setState({search})} activateSearch={()=>this.setState({activate : !this.state.activate,search:''})} /> : <Head activateSearch={()=>this.setState({activate :!this.state.activate})} goBack = {()=>this.props.navigation.goBack()} />}
               		
				<Content>

					<Contacts
                    	text ={this.state.search}
                    	contacts = {this.state.contacts}
                        navigation = {()=>console.log('navigation chat ')}
                    />

				</Content>
			</Container>
		)
	}
}

export default NewConversation;