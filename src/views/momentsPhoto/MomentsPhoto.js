import React,{Component} from 'react';
import {View,Text,Dimensions,TouchableOpacity} from 'react-native'
import {Container,Content,Thumbnail} from 'native-base';
import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Circle';
import moment from "moment";
import Comments from '../../presentation/Comments';
import HeadGoBack from '../../presentation/HeadGoBack';

let {width} = Dimensions.get('window');
let height = 0.56 * width;

class MomentsPhoto extends Component{
	constructor(props){
		super(props);

		this.state= {
			showComment : false,
			text : 'i look this photo this morning',
			comments : [
			{
				displayName : 'displayName',
				date :  new Date(),
				text : 'comments'
			}
			]
		}
	}

	render(){
		return(
			<Container>
				<HeadGoBack 
					title="Photo"
					goBack = {()=>this.props.navigation.goBack()}
				/>
				<Content>
					<View style={{flexDirection:'row',paddingLeft:10,marginTop:20}}>
						<Thumbnail source={require('../../assets/bg.jpeg')} />
						<View style={{marginLeft:10}}>
							<Text style={{fontWeight:'bold'}}>Ricardo Perez</Text>
							<Text> {moment(new Date()).fromNow()}</Text>
						</View>
					</View>


					  <Image 
                        source={{uri: "https://cdn.cinemascomics.com/wp-content/uploads/2019/02/cambio-hulk-los-vengadores.jpg" }}
                        indicator={ProgressBar} 
                        style={{width,height,marginBottom:20,marginTop:10}}
                    />

					<View style={{flexDirection:'row',marginBottom:10,marginLeft:10,marginRight:10}}>
						<View style={{flex:1,flexDirection:'row'}} >
							<TouchableOpacity><Text style={{fontWeight:'bold',fontSize:18,marginRight:18}}>Share</Text></TouchableOpacity>
							<TouchableOpacity><Text style={{fontWeight:'bold',fontSize:18}}>Like 45</Text></TouchableOpacity>
						</View>
						<TouchableOpacity onPress={()=>this.setState({showComment:!this.state.showComment})}>
						<View><Text  style={{fontSize:18}}>{this.state.comments.length} comments</Text></View>
						</TouchableOpacity>
					</View>
					<View style={{marginLeft:10,marginRight:10}}><Text>{this.state.text}</Text></View>
					{this.state.showComment && <Comments comments = {this.state.comments}/>}

				</Content>
			</Container>

		)
	}
}

export default MomentsPhoto;