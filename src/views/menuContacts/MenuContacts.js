import React,{Component} from 'react';
import {StyleSheet} from 'react-native';
import {Container,Content,List, ListItem, Text,Body,Left} from 'native-base';
import HeadGoBack from '../../presentation/HeadGoBack';
import Icon from '../../presentation/Icon/index';

class MenuContacts extends Component{
	constructor(props){
		super(props);
		this.state = {
			list :[
				{text:'Search community',icon:'SI-2',routeName:'SearchCommunity',color:'black'},
				{text:'Invite friend',icon:'AddMembers',routeName:'InviteFriend',color:'#17D6B6'},
				{text:'See pending invitations',icon:'PendingNotif',routeName:'PendingInvitations',color:'black'},
				{text:'Add contact category',icon:'AddMembers',routeName:'AddCategory',color:'#17D6B6'},
				{text:'External pending invitations',icon:'PendingNotif',routeName:'InvitationsCode',color:'black'}
			]
		}
	}

	render(){
		return(
			<Container>
				<HeadGoBack 
					title ="Weed Utopia"
					goBack = {()=>this.props.navigation.goBack()}
				/>

				<Content>

					<List>

						{this.state.list.map((item,i)=>

						<ListItem key={i} style={styles.list} onPress = {()=>this.props.navigation.navigate(item.routeName)}>
           		 			<Left>
              					<Icon active name={item.icon} size={16} style={styles.icon} color={item.color} />
             					<Body><Text>{item.text}</Text></Body>
            				</Left>				
            			</ListItem>

						)}

					</List>

				</Content>

			</Container>
		)
	}
}

const styles = StyleSheet.create({
  list:{
    marginRight:10,
    marginLeft:10,
  },
  icon:{
  	marginTop:3
  }
})

export default MenuContacts;