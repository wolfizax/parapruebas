import React from 'react';
import {StyleSheet,TouchableOpacity,View,ActivityIndicator} from 'react-native';
import {List, ListItem,Left, Body,Text,Thumbnail,Right,Content} from 'native-base';
import {capitalize} from '../../utils/letter';
import Proptypes from 'prop-types';

const Contacts = ({contacts,text,navigation,handleInvite})=>(

<Content>
    <List>
        {contacts.sort((a,b)=>{return a.displayName.localeCompare(b.displayName)}).map((item,i)=>
            {if(item.displayName.toLowerCase().indexOf(text.toLowerCase())!==-1){ 
                return(
                    <ListItem  key = {i} style={styles.list}>
                        <Left>
                            <Thumbnail source={require('../../assets/usuario.png')} />
                            <Body style={{flexDirection:'column',marginTop:10}}>
                                <Text>{capitalize(item.displayName)} </Text>
                                <Text note>sub-title</Text>                        
                            </Body> 
                        </Left>                        
                       
                        <View>                     
                            {
                                item.invite 
                                ? <TouchableOpacity onPress={()=>navigation(item)}><Text style={styles.buttonText}> Invite </Text></TouchableOpacity> 
                                : <View>{item.progress ? <ActivityIndicator size="small" color="#3CD22F" /> : <TouchableOpacity onPress={()=>handleInvite(item,i)}><Text style={styles.buttonText}> Sent invitation </Text></TouchableOpacity>}</View>
                            }
                        </View>                                        
                    </ListItem>
                )
            }}
            
        )}  
    </List> 
</Content>

)


const styles = StyleSheet.create({
    list:{
        marginLeft:10,
        marginRight:10,
        justifyContent:'space-around',
        flexDirection:'row'
    },
    buttonText: {
        color: '#3CD22F'
  },

})

Contacts.proptypes = {
    contacts : Proptypes.array.isRequired,
    text : Proptypes.string,
    navigation : Proptypes.func.isRequired,
    handleInvite : Proptypes.func.isRequired
}


export default Contacts;