import React from 'react';
import {View,StyleSheet} from 'react-native';
import {List, ListItem,Left,Right, Body,Text,Thumbnail,Content} from 'native-base';
import Proptypes from 'prop-types';
import Icon from '../../presentation/Icon/index'

const Groups = ({contacts,text})=>(
<Content>
    <List>
        {contacts.sort((a,b)=>{return a.name.localeCompare(b.name)}).map((item,i)=>
            {if(item.name.toLowerCase().indexOf(text.toLowerCase())!==-1){ 
                return(
                    <ListItem key = {i} style={styles.list} > 
                        <Left style={styles.left}>
                            <Thumbnail source={require('../../assets/usuario.png')} />
                            <Body style={styles.body}>
                                <Text>{item.name}</Text>
                                <Text note>sub-title</Text>                        
                            </Body> 
                        </Left>
                        
                        <View>                          
                            {i ==0 ? <View style={styles.right}><Icon name='PrivateGroup' size={20} style={{color: 'black'}} /><Text style={styles.text}>Send request to join</Text></View> : <View style={styles.right}><Icon name='PublicGroup' size={20} style={{color: 'black'}} /><Text style={styles.text}>Join the group</Text></View>}
                        </View>                                  
                    </ListItem>
                )
            }}
            
        )}  
    </List>

</Content>   
)


const styles = StyleSheet.create({
    list:{
        marginLeft:10,
        marginRight:10
    },
    body:{
        flexDirection:'column',
        marginTop:10
    },
    text : {
        color : '#17D6B6'
    },
    left:{
        flex:1
    },
    right:{
        justifyContent:'center',
        alignItems:'center'
    }
})

Groups.proptypes = {
    contacts : Proptypes.array.isRequired,
    text : Proptypes.string.isRequired
}


export default Groups;