import React,{Component} from 'react';
import {Container,Tab, Tabs,TabHeading,Text} from 'native-base';
import HeadSearch from '../../presentation/HeadSearch';
import Contacts from './Contacts';
import Groups from './Groups';

class SearchCommunity extends Component{
	constructor(props){
		super(props);
		this.state = {
			search : '',
            contacts : [
            {
                displayName : 'juan pereira',
                number : '+584126534991',
                invite : true,
            },
             {
                displayName : 'diego valdez',
                number : '+584166534991',
                invite : false,
            }
            ],
            groups : [
            {
            	name :' group 1'
            },
            {
            	name :' group 2'
            }
            ]
		}
	}

    handleInvite(item,index){
        let contacts = this.state.contacts;
        contacts[index].progress = true;
        this.setState({...this.state,contacts});

        //aqui va el end-point
        let cont = 0;
        let interval =  setInterval(()=>{ 

            if(cont==4){
                contacts[index].invite = true;
                contacts[index].progress = false;
                this.setState({...this.state,contacts});
                clearInterval(interval);
            }
            
            cont ++;
        }, 1000);
      
    }

	render(){
		return(
			<Container>
				<HeadSearch 
                    handleSearch = {(search)=>this.setState({search})} 
                    goBack = {()=>this.props.navigation.goBack()}
                />
				<Tabs tabBarBackgroundColor={'white'} tabBarUnderlineStyle={{backgroundColor:'black',height:1.5}} tabContainerStyle={{elevation:0,borderBottomWidth:1,borderBottomColor:'#c7c7c7'}}  >
                	<Tab 
                        heading="Contacts"
                        tabStyle={{backgroundColor:'white'}} 
                        textStyle={{color:'#C7C7C7'}} 
                        activeTabStyle={{ backgroundColor: 'white' }} 
                        activeTextStyle={{color:'black'}}>

                    		<Contacts
                    			text ={this.state.search}
                    			contacts = {this.state.contacts}
                                navigation = {(item)=>this.props.navigation.navigate('InfInviteContacts',{item})}
                    		    handleInvite ={this.handleInvite.bind(this)}
                            />
                	</Tab>
                	<Tab 
                        heading="Groups"
                        tabStyle={{backgroundColor:'white'}} 
                        textStyle={{color:'#C7C7C7'}} 
                        activeTabStyle={{ backgroundColor: 'white' }} 
                        activeTextStyle={{color:'black'}}>                   	
                		<Groups
                			text ={this.state.search}
                    		contacts = {this.state.groups}
                    	/>
                	</Tab>

                </Tabs>
				
			</Container>
			
		)
	}
}


export default SearchCommunity;