import React,{Component} from 'react';
import {View,TouchableOpacity,Dimensions} from 'react-native';
import Image from 'react-native-image-progress';
import ProgressBar from 'react-native-progress/Circle';
import {Container} from 'native-base';
import Head from './Head';

let {width,height} = Dimensions.get('window');

class Moments extends Component{	
    constructor(props){
        super(props);
        this.state= {
        	position : 0,
        	percent : 0,
        	active : true,
            moments : [
            	'https://okdiario.com/img/2018/04/03/vengadores-primer-equipo-infinity-war-655x368.jpg',
            	'https://www.ecestaticos.com/imagestatic/clipping/981/237/9812379af1063c770a37c82b19ada0f6/los-vengadores-sociedad-anonima.jpg?mtime=1430317539',
            	'https://cdn.cinemascomics.com/wp-content/uploads/2019/02/cambio-hulk-los-vengadores.jpg'
            ],

        }
    }

    componentDidMount(){
    	this._handleInterval();
    }

    _handleInterval(){
        let interval;
        let number = 0;
        setInterval(()=>{ 
            if(!this.state.active) return null;
            interval = this.state.percent + 1;
            this.setState({percent:interval});

            if(interval/10 == 10){
                number = this.state.position;
                if((this.state.moments.length - 1) > number){         
                    number = this.state.position + 1;
                    this.setState({position:number,percent:0});
                }else{
                    this.props.navigation.goBack();
                }
            }
            
        }, 100);
    }

    nextBackImage(evt){
        let number = this.state.position;
        if(evt.nativeEvent.locationX > 0 && evt.nativeEvent.locationX < width*0.25){
            number -- ;

        }else if(evt.nativeEvent.locationX > width * 0.75  && evt.nativeEvent.locationX < width){
            number ++;
        }

        if(number < 0 || number >= this.state.moments.length){
            this.props.navigation.goBack()
        }else{
            if(!(evt.nativeEvent.locationX > 50 && evt.nativeEvent.locationX <  width - 50)){
                this.setState({position:number,percent:0});
            }
            
        }
 
    }

	render(){
		return(
			<Container>

                <Head 
                        goBack = {()=>this.props.navigation.goBack()} 
                        lengthMoments = {this.state.moments.length}
                        percent = {this.state.percent}
                        position = {this.state.position}
                />
               
				<TouchableOpacity
                    onPress = {this.nextBackImage.bind(this)}
					onPressIn={()=>this.setState({active:false})}
      				onPressOut={()=>this.setState({active:true})}
                    delay = {100}
      				activeOpacity = {1}
                    style={{position:'absolute',zIndex:-1}}
      		    >
                   

                    <Image 
                        source={{uri: `${this.state.moments[this.state.position]}` }}
                        indicator={ProgressBar} 
                        style={{width, height}}
                    />

				</TouchableOpacity>

			</Container>
		)
	}
}

export default Moments;
