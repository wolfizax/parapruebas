import React from 'react';
import {View,StyleSheet,Text} from 'react-native';
import {Button,Thumbnail} from 'native-base';
import ProgressCircle from 'react-native-progress-circle';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

const Head = ({goBack,lengthMoments,percent,position})=>(

    <View transparent style={styles.head}>
        <Button transparent style={styles.icon} onPress = {()=>goBack()}>
            <Icon name="left-arrow" size={18} color="#3CD22F" />                  
        </Button>
        <Thumbnail small source={require('../../assets/usuario.png')} style={styles.avatar} />
        <ProgressCircle
            percent={percent}
            radius={18}
            borderWidth={4}
            color="#fff"
            shadowColor="#2B2B2B"
            bgColor="#2B2B2B"
        >
            <Text style={{ fontSize: 10,color:'white' }}>{lengthMoments - position}</Text>
        </ProgressCircle>
    </View>

)


const styles = StyleSheet.create({
    head:{
        height:60,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        paddingLeft:10,
        paddingRight:10,
        backgroundColor:'transparent'
    },
    avatar:{
        marginTop:4,
        marginLeft:45
    },
    icon:{
        marginTop:10
    }

})

Head.proptypes = {
    goBack : Proptypes.func.isRequired,
    lengthMoments : Proptypes.number.isRequired,
    percent : Proptypes.number.isRequired,
    position : Proptypes.number.isRequired
}


export default Head;