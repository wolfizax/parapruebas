import React, { Component } from 'react';
import {Container, Content, List, ListItem, Text, Row, Left, Right} from 'native-base';
import {TouchableOpacity} from 'react-native';
import Icon from '../../presentation/Icon/index';
import {Logout} from '../../actions/nav';
import LoggedView from '../index'
import {UserSetting} from '../../actions/nav';

/** @extends Component */
export default class LeftMenuView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({title:'Settings',headerTintColor:'#FFF', headerRight:<Row></Row>, headerStyle:{backgroundColor:'#2B2B2B'},headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}, headerLeft: (
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{ paddingLeft: 12 }}>
           <Icon name="SI-0" size={20} style={{color:'white'}} />
        </TouchableOpacity>
    )})
    render(){
        return(<Container> 
            <Content>
                <List>
                    <ListItem onPress={() => this.props.navigation.dispatch(UserSetting())}>
                        <Left><Text>User Setting</Text></Left>
                    </ListItem>
                    <ListItem>
                        <Left><Text>Scan Qr code</Text></Left>
                    </ListItem>
                    <ListItem onPress={() => this.props.navigation.dispatch(Logout())}>
                        <Left><Text>Logout</Text></Left>
                    </ListItem>
                </List>
            </Content>
        </Container>)
    }
}