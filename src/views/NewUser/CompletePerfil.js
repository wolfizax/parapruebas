import React, {Component} from 'react';
import {Container, Content, Text, CheckBox, Toast} from 'native-base';
import {Grid,Row,Col} from 'react-native-easy-grid';
import {TouchableOpacity, Image, TextInput, Linking, Animated} from 'react-native';
import DatePicker from 'react-native-datepicker'
import LoggedView from '../index';
import {LOGIN_SUCCESS,setFetching} from '../../actions/user';
import {connect} from 'react-redux';
import {Validation} from '../../utils/validation';
import {userService} from '../../services/user';
import Loading from '../../presentation/Loading';
import ButtonGradient from '../../presentation/ButtonGradient';
import {Dashboard} from '../../actions/nav'
import Permissions from 'react-native-permissions';
import ImagePicker from 'react-native-image-picker';
import {NavigationActions} from 'react-navigation';
import Icon from '../../presentation/Icon/index';
import moment from 'moment';
import style from './style';

@connect(state => ({
    fetching: state.user.isFetching
}))
/** @extends Component */
export default class NewUserCompletePerfilView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        headerLeft: <Row></Row>,
        headerTintColor:'white',
        headerStyle:{backgroundColor:'#2B2B2B'},
        headerRight: (
            <TouchableOpacity
              onPress={() => navigation.reset([NavigationActions.navigate({ routeName: 'Main' })], 0)}
              style={{ paddingRight: 12 }}>
              <Icon name="SI-6" size={20} style={{color:'white'}} />
            </TouchableOpacity>
        ),
        title:'Complete your profile',
        headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}
      });

    constructor(props){
        super(props);
        this.state = {
            MultimediaOptionsBox:false,
            sexMaleCheckbox:false,
            sexFemaleCheckbox:false,
            conditionsTermsCheckbox:false,
            dateBirth:`${moment(new Date()).format('YYYY-MM-DD')}`,
            avatarData:{},
            name:'',
            password:'',
            idWonder:'',
            email:'',
            codeAffiliate:'',
            errorInput:{},
            bounceValue: new Animated.Value(150),
        };
    }
    isValid = (type) => {
        const { name, idWonder, password, email, codeAffiliate, dateBirth} = this.state;
        this.setState({...this.state,errorInput:{name:''}});
        switch (type) {
            case 'name':
                if(Validation.name(name) === false){
                    this.setState({...this.state,errorInput:{name:'name'}});
                    Toast.show({
                        text:'The name field is empty or has no minimum 4 characters.',
                        type:'danger',
                        duration:4000,
                        position:'bottom'
                    });
                }else{
                    return true;
                }
                break;
            case 'idWonder':
                if(Validation.wonderID(idWonder) === false){
                    this.setState({...this.state,errorInput:{name:'idWonder'}});
                    Toast.show({
                        text:'The WonderID field is empty or does not have at least 4 characters.',
                        type:'danger',
                        duration:4000,
                        position:'bottom'
                    });
                }else{
                    return true;
                }
                break;
            case 'email':
                if(Validation.email(email) === false){
                    this.setState({...this.state,errorInput:{name:'email'}});
                    Toast.show({
                        text:'The email field is empty or does not have a correct format.',
                        type:'danger',
                        duration:4000,
                        position:'bottom'
                    });
                }else{
                    return true;
                }
                break;
            case 'password':
                if(Validation.password(password,8) === false){
                    this.setState({...this.state,errorInput:{name:'password'}});
                    Toast.show({
                        text:'The password field is empty or has no minimum 8 characters.',
                        type:'danger',
                        duration:4000,
                        position:'bottom'
                    });
                }else{
                    return true;
                }
                break;
            case 'sex':
                if(this.state.sexFemaleCheckbox == false && this.state.sexMaleCheckbox == false){
                    this.setState({...this.state,errorInput:{name:'sex'}});
                    Toast.show({
                        text:'You must choose a sexual gender to continue.',
                        type:'danger',
                        duration:4000,
                        position:'bottom'
                    });
                    return false;
                }else{
                    return true;
                }
                break;
            case 'termConditions':
            if(this.state.conditionsTermsCheckbox == false){
                this.setState({...this.state,errorInput:{name:'termConditions'}});
                Toast.show({
                    text:'You must accept the terms and conditions to continue.',
                    type:'danger',
                    duration:4000,
                    position:'bottom'
                });
                return false;
            }else{return true}
                break;
            case 'dateBirth':
            if(moment().diff(dateBirth,'years') < 13){
                this.setState({...this.state,errorInput:{name:'dateBirth'}});
                Toast.show({
                    text:'You must be at least 13 years old.',
                    type:'danger',
                    duration:4000,
                    position:'bottom'
                });
                return false;
            }else{ return true}
            break;
            default:
                break;
        }
    }
    requestContactsPermiss = async () => {
        let result;
        result = await Permissions.request('contacts')
        if(result != 'authorized')
        {
            result = await Permissions.request('contacts',{
                rationale: {
                    title: 'WonderChatt Contacts Permission',
                    message:
                        'WonderChatt needs access to list of contacts ' +
                        'of your phone device to continue with the registration.',
                    },
            });
            if(result == 'authorized'){
                this.props.navigation.dispatch(Dashboard({firstLogin:true}));
            }else{
                this.props.navigation.dispatch(Dashboard({firstLogin:true}));
            }
        }else{
            Toast.show({
                text:'Sorry, a problem occurred in the permission request process to access the list of contacts on your mobile device.',
                type:'warning',
                duration:4000,
                position:'bottom'
            });
            console.log('Error in checking permiss: CONTACS',err);
            this.props.navigation.dispatch(Dashboard({firstLogin:true}));
        }
    }
    requestGalleryPermiss = async () => {
        let result;
        result = await Permissions.check('photo');
        if(result != 'authorized')
        {
            result = await Permissions.request('photo',{
                rationale: {
                    title: 'WonderChatt Gallery Permission',
                    message:
                        'WonderChatt needs access to gallery ' +
                        'of your phone device to continue with the registration.',
                    }
            });
            if(result == 'authorized')
            {
                const options = {
                    title: 'Select Avatar',
                    storageOptions: {
                        skipBackup: true,
                        path: 'images'
                    }};
                ImagePicker.launchImageLibrary(options, (result) => {
                this.setState({...this.state,avatarData:{type:result.type,name:result.fileName,data:result.data,uri:result.uri}});
                this.hadleMultimediaBox();
                });
            }else{
                Toast.show({
                    text:'Sorry, a problem occurred in the permission request process to access the photos on your mobile device.',
                    type:'warning',
                    duration:4000,
                    position:'bottom'
                });
                console.log('Error in checking permiss: PHOTO',err);
            }
        }else{
            const options = {
                title: 'Select Avatar',
                storageOptions: {
                    skipBackup: true,
                    path: 'images',
                },
                };
                ImagePicker.launchImageLibrary(options, (result) => {
                    this.setState({...this.state,avatarData:{type:result.type,name:result.fileName,data:result.data,uri:result.uri}});
                    this.hadleMultimediaBox();
                });
        }
    }
    requestCameraPermission = async () => 
    {
        let result;
        result = await Permissions.check('camera')
        if(result != 'authorized')
        {
            result = await Permissions.request('camera',{
            rationale: {
                title: 'WonderChatt Camera Permission',
                message:
                    'WonderChatt needs access to camera ' +
                    'of your phone device to continue with the registration.',
                }
            });
            if(result == 'authorized')
            {
                const options = {
                    title: 'Select Avatar',
                    storageOptions: {
                        skipBackup: true,
                        path: 'images',
                    },
                    };
                    ImagePicker.launchCamera(options, (result) => {
                        this.setState({...this.state,avatarData:{type:result.type,name:result.fileName,data:result.data,uri:result.uri}});
                        this.hadleMultimediaBox();
                    });
            }else{
                Toast.show({
                    text:'Sorry, there was a problem in the permission request process to use your mobile devices camera',
                    type:'warning',
                    duration:4000,
                    position:'bottom'
                });
                console.log('Error in checking permiss: CAMERA',err);
            }
        }else{
            const options = {
                title: 'Select Avatar',
                storageOptions: {
                    skipBackup: true,
                    path: 'images',
                },
                };
                ImagePicker.launchCamera(options, (result) => {
                    this.setState({...this.state, avatarData:{type:result.type,name:result.fileName,data:result.data,uri:result.uri}});
                    this.hadleMultimediaBox();
                });
        }
    }
    onSubmit = async () => {
        const { name, idWonder, password, email, dateBirth, codeAffiliate} = this.state;
        let result;
        if(this.isValid('name')){
            if(this.isValid('idWonder')){
                if(this.isValid('password')){
                    if(this.isValid('email')){
                        if(this.isValid('termConditions')){
                            if(this.isValid('dateBirth')){
                                if(this.isValid('sex')){
                                    this.props.dispatch(setFetching(true));
                                    result = await userService.existEmail(email);
                                    if(result){
                                        this.props.dispatch(setFetching(false));
                                        Toast.show({
                                            text:'The email entered is in use by another user.',
                                            type:'danger',
                                            duration:4000,
                                            position:'bottom'
                                        });
                                    }else{
                                        result = await userService.existWonder(idWonder);
                                        if(result){
                                            this.props.dispatch(setFetching(false));
                                            Toast.show({
                                                text:'The Wonder ID entered is in use by another user.',
                                                type:'danger',
                                                duration:4000,
                                                position:'bottom'
                                            });
                                        }else{
                                            result = await userService.createNewUser({
                                                file:{uri:this.state.avatarData.uri,name:this.state.avatarData.name,type:this.state.avatarData.type},
                                                name,
                                                idWonder,
                                                password,
                                                email,
                                                dateBirth,
                                                codeAffiliate,
                                                sex: this.state.sexFemaleCheckbox ? 'F' : 'M',
                                                countryCode:this.props.navigation.getParam('countryCode'),
                                                numberPhone:this.props.navigation.getParam('numberPhone')
                                                });
                                                if(result == 200){
                                                result = await userService.login(idWonder,null,password);
                                                if(result){
                                                    this.props.dispatch(setFetching(false));
                                                    this.props.dispatch(LOGIN_SUCCESS(result));
                                                    Toast.show({
                                                        'text':'Congratulations, you were successfully registered at WonderChatt!',
                                                        duration:4000,
                                                        type:'success',
                                                        position:'bottom',
                                                        onClose:(() => this.requestContactsPermiss())
                                                    });
                                                }
                                            }else{
                                                this.props.dispatch(setFetching(false));
                                                Toast.show({
                                                    text:'Sorry, a problem occurred in the process of your registration. Try again.',
                                                    type:'warning',
                                                    duration:4000,
                                                    position:'bottom'
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    hadleMultimediaBox = () => {
        var toValue = 150;

        if(!this.state.MultimediaOptionsBox) {
          toValue = 0;
        }
        Animated.spring(
          this.state.bounceValue,
          {
            toValue: toValue,
            velocity: 3,
            tension: 2,
            friction: 8,
          }
        ).start();
    
        this.setState({...this.state,MultimediaOptionsBox: !this.state.MultimediaOptionsBox});
    }
    renderMultimediaBox = () =>{
        return (
            <Animated.View
            style={[style.multimediaBox,
              {transform: [{translateY: this.state.bounceValue}]}]}
          >
            <Row style={{height:10}}>
                <Col style={{alignItems:'flex-end',padding:10}} >
                    <Icon onPress={() => this.hadleMultimediaBox()} name="SI-6" size={17} style={{color:'black'}} />
                </Col>
            </Row>
            <Row>
                <Col style={[style.multimediaBoxItem,{alignItems:'flex-end'}]}>
                    <TouchableOpacity onPress={() => this.requestCameraPermission()}>
                        <Image source={require('../../assets/photo-camera.png')} />
                    </TouchableOpacity>
                </Col>
                <Col style={style.multimediaBoxItem}>
                    <TouchableOpacity onPress={() => this.requestGalleryPermiss()}>
                        <Image source={require('../../assets/gallery.png')} />
                    </TouchableOpacity>
                </Col>
                </Row>
          </Animated.View>
        );
    }
    render(){
        return(
            <Container>
                <Loading visible={this.props.fetching} />
                <Content>
                    <Grid style={style.containerForm}>
                        <Row style={style.containerItem} >
                            <Col size={0.2} style={style.containerAvatar}>
                                <TouchableOpacity onPress={() => this.hadleMultimediaBox()}>
                                    <Image style={{width:65,height:64}} source={ this.state.avatarData.data == undefined ? require('../../assets/userAvatar.png') : {uri:`data:${this.state.avatarData.type};base64,${this.state.avatarData.data}`}} />
                                </TouchableOpacity>
                            </Col>
                            <Col style={{justifyContent:'center'}}>
                                <Text style={[style.fontBold,style.fontRoboto]}>Name</Text>
                                <TextInput onFocus={() => this.state.MultimediaOptionsBox ? this.hadleMultimediaBox() : null} onBlur={() => this.isValid('name')} autoCorrect={false} returnKeyType='next' textContentType="name"  value={this.state.name} placeholder='Name' underlineColorAndroid={this.state.errorInput.name == 'name' ? 'red' : '#bcbcbc'} onChangeText={(text) => this.setState({...this.state,name:text.replace(/[^a-zA-Z0-9\s]/g, '')})} />
                            </Col>
                        </Row>
                        <Row  style={style.containerItem}>
                            <Col>
                                <Text style={[style.fontBold,style.fontRoboto]}>Wonder ID</Text>
                                <TextInput onFocus={() => this.state.MultimediaOptionsBox ? this.hadleMultimediaBox() : null} onBlur={() => this.isValid('idWonder')} autoCapitalize='none'  autoCorrect={false} returnKeyType='next' textContentType="username" value={this.state.idWonder} onChangeText={(text) => this.setState({...this.state,idWonder:text.replace(/[^a-zA-Z0-9]/g,'')})} placeholder='Wonder ID' underlineColorAndroid={this.state.errorInput.name == 'idWonder' ? 'red' : '#bcbcbc'} />
                            </Col>
                        </Row>
                        <Row style={style.containerItem}>
                            <Col>
                                <Text style={[style.fontBold,style.fontRoboto]}>Email</Text>
                                <TextInput onFocus={() => this.state.MultimediaOptionsBox ? this.hadleMultimediaBox() : null} onBlur={() => this.isValid('email')} autoCapitalize='none' autoCorrect={false} returnKeyType='next'  textContentType="emailAddress" keyboardType="email-address" value={this.state.email} onChangeText={(text) => this.setState({...this.state,email:text})} placeholder="name@domain.com" underlineColorAndroid={this.state.errorInput.name == 'email' ? 'red' : '#bcbcbc'} />
                            </Col>
                        </Row>
                        <Row style={style.containerItem}>
                            <Col>
                                <Text style={[style.fontBold,style.fontRoboto]}>Password</Text>
                                <TextInput onFocus={() => this.state.MultimediaOptionsBox ? this.hadleMultimediaBox() : null} onBlur={() => this.isValid('password')} autoCorrect={false} returnKeyType='next' secureTextEntry  textContentType="password"  value={this.state.password} onChangeText={(text) => this.setState({...this.state,password:text})} placeholder="(Minimun 8 alphanumeric characters)" underlineColorAndroid={this.state.errorInput.name == 'password' ? 'red' : '#bcbcbc'} />
                            </Col>
                        </Row>
                        <Row style={[style.containerItem,style.containerBirthdate]}>
                            <Col size={0.4} style={{justifyContent:'center'}}>
                                <Text style={[style.fontBold,style.fontRoboto]}>Birthdate</Text>
                            </Col>
                            <Col>
                                <DatePicker
                                    onOpenModal={() => this.state.hadleMultimediaBox ? this.hadleMultimediaBox() : null}
                                    style={{width: 200,margin:10}}
                                    date={this.state.dateBirth}
                                    mode="date"
                                    placeholder="select date"
                                    format="YYYY-MM-DD"
                                    confirmBtnText="Confirm"
                                    cancelBtnText="Cancel"
                                    customStyles={{
                                    dateText:{
                                        color:'white',
                                        fontWeight:'bold',
                                        fontSize:19,
                                    },
                                    dateInput: {
                                        borderRadius:3,
                                        borderColor:this.state.errorInput.name == 'dateBirth' ? 'red' : 'transparent',
                                        backgroundColor:'#17D6B6'
                                    }}}
                                    onDateChange={(date) => {this.setState({...this.state,dateBirth:date});this.isValid('dateBirth')}}
                                />
                            </Col>
                        </Row>
                        <Row style={[style.containerSexs,style.containerItem]}>
                            <Col size={0.5}><Text style={[style.fontBold,style.fontRoboto]}>Sex</Text></Col>
                            <CheckBox color={this.state.errorInput.name == 'sex' ? 'red' : '#17D6B6'} style={style.checkBoxsSex} checked={this.state.sexFemaleCheckbox} onPress={() => {if(this.state.MultimediaOptionsBox){if(this.state.sexFemaleCheckbox){this.hadleMultimediaBox();this.setState({...this.state,sexFemaleCheckbox:!this.state.sexFemaleCheckbox,MultimediaOptionsBox:false}) }else{this.hadleMultimediaBox(); this.setState({...this.state,sexFemaleCheckbox:true,sexMaleCheckbox:false,MultimediaOptionsBox:false})}}else{if(this.state.sexFemaleCheckbox){this.setState({...this.state,sexFemaleCheckbox:!this.state.sexFemaleCheckbox})}else{this.setState({...this.state,sexFemaleCheckbox:true,sexMaleCheckbox:false})}}}} />
                            <Text style={style.fontRoboto} onPress={() => {if(this.state.MultimediaOptionsBox){if(this.state.sexFemaleCheckbox){this.hadleMultimediaBox();this.setState({...this.state,sexFemaleCheckbox:!this.state.sexFemaleCheckbox,MultimediaOptionsBox:false}) }else{this.hadleMultimediaBox(); this.setState({...this.state,sexFemaleCheckbox:true,sexMaleCheckbox:false,MultimediaOptionsBox:false})}}else{if(this.state.sexFemaleCheckbox){this.setState({...this.state,sexFemaleCheckbox:!this.state.sexFemaleCheckbox})}else{this.setState({...this.state,sexFemaleCheckbox:true,sexMaleCheckbox:false})}}}}>Female</Text>
                            <CheckBox color={this.state.errorInput.name == 'sex' ? 'red' : '#17D6B6'} style={style.checkBoxsSex} checked={this.state.sexMaleCheckbox}  onPress={() => {if(this.state.MultimediaOptionsBox){if(this.state.sexMaleCheckbox){this.hadleMultimediaBox();this.setState({...this.state,sexMaleCheckbox:!this.state.sexMaleCheckbox,MultimediaOptionsBox:false}) }else{this.hadleMultimediaBox(); this.setState({...this.state,sexMaleCheckbox:true,sexFemaleCheckbox:false,MultimediaOptionsBox:false})}}else{if(this.state.sexMaleCheckbox){this.setState({...this.state,sexMaleCheckbox:!this.state.sexMaleCheckbox})}else{this.setState({...this.state,sexMaleCheckbox:true,sexFemaleCheckbox:false})}}}}/>
                            <Text style={style.fontRoboto} onPress={() => {if(this.state.MultimediaOptionsBox){if(this.state.sexMaleCheckbox){this.hadleMultimediaBox();this.setState({...this.state,sexMaleCheckbox:!this.state.sexMaleCheckbox,MultimediaOptionsBox:false}) }else{this.hadleMultimediaBox(); this.setState({...this.state,sexMaleCheckbox:true,sexFemaleCheckbox:false,MultimediaOptionsBox:false})}}else{if(this.state.sexMaleCheckbox){this.setState({...this.state,sexMaleCheckbox:!this.state.sexMaleCheckbox})}else{this.setState({...this.state,sexMaleCheckbox:true,sexFemaleCheckbox:false})}}}}>Male</Text>
                        </Row>
                        <Row style={style.containerItem}>
                            <Col>
                                <Text style={[style.fontBold,style.fontRoboto]}>Invitation code</Text>
                                <TextInput onFocus={() => this.state.MultimediaOptionsBox ? this.hadleMultimediaBox() : null} autoCorrect={false} returnKeyType='next'  value={this.state.codeAffilliate} onChangeText={(text) => this.setState({...this.state,codeAffilliate:text})} underlineColorAndroid='#bcbcbc' />
                            </Col>
                        </Row>
                        <Row style={style.containerItem}>
                            <Text style={[style.contentCenter]}>
                                <Text style={style.fontRoboto}>To continue, I understand that you have read and accept the following </Text>
                                <Text  onPress={() => Linking.openURL('http://18.235.146.11/#/help')} style={[style.fontItalic,style.textLinkConditions,style.fontRoboto]}>Terms and Conditions of Services</Text>
                                <Text  style={style.fontRoboto}> and </Text>
                                <Text onPress={() => Linking.openURL('http://18.235.146.11/#/help')} style={[style.fontItalic,style.textLinkConditions,style.fontRoboto]}>Privacy Policies.</Text>
                            </Text>
                        </Row>
                        <Row style={style.textCenter}>
                            <CheckBox color={this.state.errorInput.name == 'termConditions' ? 'red' : '#17D6B6'} style={style.checkBoxConditions} checked={this.state.conditionsTermsCheckbox} onPress={() => {if(this.state.MultimediaOptionsBox){this.hadleMultimediaBox(); this.setState({...this.state,conditionsTermsCheckbox:!this.state.conditionsTermsCheckbox})}else{this.setState({...this.state,conditionsTermsCheckbox:!this.state.conditionsTermsCheckbox})}}}/>
                            <Text style={style.fontRoboto} onPress={() => {if(this.state.MultimediaOptionsBox){this.hadleMultimediaBox(); this.setState({...this.state,conditionsTermsCheckbox:!this.state.conditionsTermsCheckbox})}else{this.setState({...this.state,conditionsTermsCheckbox:!this.state.conditionsTermsCheckbox})}}}>I accept the previous conditions</Text>
                        </Row>
                    </Grid>
                    {
                        this.state.conditionsTermsCheckbox ?
                        <ButtonGradient full title='Save' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />  
                        :
                        <ButtonGradient full disabled title='Save' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />

                    }
                </Content>
                {
                    this.renderMultimediaBox()
                }             
            </Container>
        );
    }
}