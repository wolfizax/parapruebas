import React, {Component} from 'react';
import {Container,Content, H3, H2, Text, View, Form, Item, Toast} from 'native-base';
import DeviceInfo from 'react-native-device-info';
import LoggedView from '../index';
import {TextInput,Platform,TouchableOpacity} from 'react-native';
import {Grid,Row} from 'react-native-easy-grid';
import CountryPicker, {getAllCountries} from 'react-native-country-picker-modal'
import {setFetching} from '../../actions/user';
import {NewUserValidationCode} from '../../actions/nav';
import {userService} from '../../services/user';
import Loading from '../../presentation/Loading';
import {Validation} from '../../utils/validation';
import ButtonGradient from '../../presentation/ButtonGradient';
import style from './style';
import Permissions from 'react-native-permissions';
import Icon from '../../presentation/Icon/index';
import { connect } from 'react-redux';

@connect(state => ({
    fetching: state.user.isFetching
}))
/** @extends Component */
export default class NewUserView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        title:'WonderChatt', 
        headerRight:<Row></Row>,
        headerLeft: (
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{ paddingLeft: 12 }}>
          <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>),
        headerTintColor:'white', 
        headerStyle:{backgroundColor:'#2B2B2B'},
        headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}})
    constructor(props){
        super(props)
        let userLocaleCountryCode = DeviceInfo.getDeviceCountry()
        const userCountryData = getAllCountries().filter(country => country.cca2 === userLocaleCountryCode).pop()
        let callingCode = null
        let cca2 = userLocaleCountryCode
        if (!cca2 || !userCountryData) {
            cca2 = 'US'
            callingCode = '1'
        } 
        else {
            callingCode = userCountryData.callingCode
        }
        this.state = {
            cca2,
            callingCode,
            numberPhone:'',
        }
    }
    requestPermissionsSMS = async () => {
        let receiveSms = false;
        let readSms = false;
        let result;
        result = await Permissions.checkMultiple(['receiveSms','readSms'])
        if(result){
            for (const key in result) {
                if (result.hasOwnProperty(key)) {
                    if(result[key] != 'authorized'){
                        if(key == 'receiveSms'){
                            result = await Permissions.request(key,{
                                rationale: {
                                    title: 'WonderChatt Receive SMS Permission',
                                    message:
                                      'WonderChatt needs need access to send messages ' +
                                      'to your phone number to continue with the registration',
                                  },
                            })
                            if(result == 'authorized'){
                                receiveSms = true;
                            }else{
                                console.log('Error in checking permiss: RECEIVE SMS');
                            }
                        }else if(key == 'readSms'){
                            result = await Permissions.request(key,{
                                rationale: {
                                    title: 'WonderChatt Read SMS Permission',
                                    message:
                                      'WonderChatt needs need access to read messages ' +
                                      'to your phone number to continue with the registration',
                                  },
                            })
                            if(result == 'authorized'){
                                readSms = true;
                            }else{
                                console.log('Error in checking permiss: READ SMS',err);
                            }
                        }
                    }else{
                        key == 'receiveSms' ? receiveSms = true : readSms = true
                    }
                }
            }
            if(receiveSms == true && readSms == true){
                this.props.dispatch(setFetching(true));
                result = await userService.sendCodeSingup(this.state.numberPhone,this.state.callingCode);
                this.props.dispatch(setFetching(false));
                if(result){
                    this.props.navigation.dispatch(NewUserValidationCode({numberPhone:this.state.numberPhone,countryCode:this.state.callingCode,codeValidation:result,mainRoute:this.props.navigation.state.key}))
                }
                else{
                    Toast.show({text:'Sorry, a problem occurred while generating your verification code.',buttonText:'Ok',type:"danger"});
                }
            }
        }else{
            console.log('Error in checking permiss',result);
        }
    }
    onSubmit = async () =>{
        if(Validation.isNumerPhone(this.state.numberPhone) == false){
            Toast.show({
                text:'Your telephone number is invalid format. It must have a minimum of 10 digits.',
                duration:4000,
                type:'danger',
                position:'bottom'
            });
        }else{
            this.props.dispatch(setFetching(true));
            let result = await userService.existNumberPhone(this.state.numberPhone,this.state.callingCode);
            this.props.dispatch(setFetching(false));
            if(result === false){
                this.requestPermissionsSMS();
            }else if(result.request._hasError === false){
                Toast.show({
                    text:'The telephone number entered is in use by another user.',
                    duration:4000,
                    type:"danger",
                    position:'bottom'
                });
            }else{
                Toast.show({
                    text:'Sorry, an error occurred in the verification of your phone number. Try again',
                    buttonText:'Ok',
                    position:'bottom',
                    duration:4000,
                    type:'warning'
                });
            } 
        }
    }
    onChanged = (text) => {
        let newText = text.replace(/^0+/, '');
        this.setState({...this.state,numberPhone: newText.replace(/[^0-9]/g, '')})
    }
    render(){
        return (
            <Container>
                <Loading visible={this.props.fetching} />
                <Content padder>
                 <Grid>
                        <Row style={[style.contentCenter,style.fontRoboto]}>
                            <H3>Register</H3>
                        </Row>
                        <Row style={[style.contentCenter,{marginVertical:10}]}>
                            <H2 style={[style.fontBold,style.title,style.textCenter,]}>
                                Register with your phone number
                            </H2>
                        </Row>
                        <Form style={style.contaierFormPhoneInfo}>
                                <Item style={{borderColor:'transparent'}}>
                                    <Text style={[style.fontBold,style.fontRoboto]}>Region</Text>
                                </Item>
                                <Item>
                                    <CountryPicker hideAlphabetFilter filterable  onChange={(value) => this.setState({...this.state,cca2:value.cca2,callingCode:value.callingCode})} cca2={this.state.cca2} showCallingCode closeable />
                                </Item>
                                <Item style={style.contentPhoneInfo}>
                                    <TextInput 
                                    name={'phoneNumber'}
                                    placeholder="Phone Number" 
                                    style={style.inputPhoneText}
                                    autoCapitalize={'none'}
                                    autoCorrect={false} 
                                    returnKeyType='go'
                                    autoFocus
                                    keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'phone-pad'}
                                    textContentType="telephoneNumber"
                                    onChangeText={this.onChanged}
                                    maxLength={11}
                                    value={this.state.numberPhone}
                                    />
                                </Item>
                        </Form>
                        <Row style={[style.contentCenter,style.contentDescription]}>
                            <Text style={[style.descriptionText,style.textCenter,style.fontRoboto]}>Verfication code will be send by SMS to your phone number.</Text>
                        </Row>
                        <Row style={[style.contentCenter,style.contentDescription]}>
                            <Text style={[style.descriptionText,style.textCenter,style.fontRoboto]}>The message with your verification code will be sent in a few seconds.</Text>
                        </Row>
                    </Grid>
                </Content>
                <View>
                    <ButtonGradient full title='Send verification code' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />
                </View>
            </Container>)
    }
}