import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    contaierSMSPermissBackground:{
        backgroundColor:'#F2F2F2'
    },
    logoImage:{
        height:230,
        width:230
    },
    contentCenter:{
        justifyContent:'center',
        margin:10
    },
    fontRoboto:{
        fontFamily:'Roboto'
    },
    title:{
        fontFamily:'Roboto',
        fontWeight:'bold',
        fontSize:25
    },
    textCenter:{
        textAlign:'center'
    },
    fontBold:{
        fontWeight:'bold'
    },
    fontItalic:{
        fontStyle:'italic'
    },
    contentPhoneInfo:{
        marginVertical:15,
    },
    inputPhoneText: {
        borderBottomWidth:0.5,
        borderBottomColor:'#E6E6E6',
        padding:0,
        margin: 0,
        flex: 1,
        fontSize: 20,
        color: '#2B2B2B'
    },
    contentDescription:{
        marginVertical:30
    },
    descriptionText:{
        fontSize:19
    },
    optionsSMSPermiss:{
        color:'#30BFFF',
        padding:20,
        fontSize:25,
        fontStyle:'italic'
    },
    containerForm:{
        padding:10
    },
    containerItem:{
        marginVertical:5,
        justifyContent:'center'
    },
    containerAvatar:{
        marginRight:10
    },
    containerBirthdate:{
        borderBottomColor:'#bcbcbc',
        borderBottomWidth:2,
        paddingBottom:5
    },
    contaierFormPhoneInfo:{
        marginVertical:20
    },
    containerSexs:{
        paddingVertical:10,
        borderBottomColor:'#bcbcbc',
        borderBottomWidth:2,
    },
    checkBoxsSex:{
        marginHorizontal:20
    },
    textLinkConditions:{
        color:'#17D6B6'
    },
    checkBoxConditions:{
        marginRight:20
    },
    multimediaBox: {
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: "#FFFFFF",
        height: 150,
    },
    multimediaBoxItem:{
        justifyContent:'center',
        marginHorizontal:10
    }
});