import React,{Component} from 'react';
import {Container, Content, H1, Form, Label, Text, Toast, View} from 'native-base';
import {TouchableOpacity, Platform, TextInput} from 'react-native';
import {Grid,Row} from 'react-native-easy-grid';
import style from './style';
import SmsListener from 'react-native-android-sms-listener'
import {userService} from '../../services/user';
import {setFetching} from '../../actions/user';
import {connect} from 'react-redux';
import Loading from '../../presentation/Loading';
import {NewUserCompletePerfil} from '../../actions/nav';
import ButtonGradient from '../../presentation/ButtonGradient';
import Icon from '../../presentation/Icon/index';
import LoggedView from '../index';

@connect(state => ({
    fetching: state.user.isFetching
}))
/** @extends Component */
export default class NewUserValidationCode extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        title:'WonderChatt',
        headerTitleStyle: {flex: 1, textAlign: 'center', color:'white'},
        headerStyle:{backgroundColor:'#2B2B2B'},
        headerLeft: (
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{ paddingLeft: 12 }}>
              <Icon name='SI-8' size={20} style={{color: '#fff'}} />
            </TouchableOpacity>
        ),
        headerRight: (
            <TouchableOpacity
              onPress={() => navigation.goBack(navigation.getParam('mainRoute'))}
              style={{ paddingRight: 12 }}>
              <Icon name="SI-6" size={20} style={{color:'white'}} />
            </TouchableOpacity>
        ) 
      });
    constructor(props){
        super(props);
        this.smsSubscription = null;
        this.state = {
            inputCode:'',
            resendCodePress:false,
            resendCodeTime:'200',
        };
    }
    componentDidUpdate(){
        if(this.state.resendCodeTime == '0' && this.state.resendCodePress === false){
            this.countdownCall = setInterval(() =>{
                this.countDown();
            },1000);
        }
    }
    componentDidMount(){
        this.smsSubscription = SmsListener.addListener(message => {
            this.verifyCodeListener(message.body);
        });
        this.countdownCall = setInterval(() =>{
            this.countDown();
        },1000);
    }
    countDown(){
        if(this.state.resendCodeTime > 0){
            this.setState({...this.state,resendCodeTime:this.state.resendCodeTime-1});
        }else{
            clearInterval(this.countdownCall);
            this.setState({...this.state,resendCodePress:!this.state.resendCodePress});
        }
        
    }
    verifyCodeListener = async (bodyMessage) => {
        const verificationCodeRegEx = /his verification code is ([\d]{4})/;
        if(verificationCodeRegEx.test(bodyMessage)){
            let result;
            let messageCode = bodyMessage.match(verificationCodeRegEx)[1];
            this.props.dispatch(setFetching(true))
            result = await userService.verificationCodeSingup(`${this.props.navigation.getParam('countryCode')}${this.props.navigation.getParam('numberPhone')}`,messageCode);
            this.props.dispatch(setFetching(false))
            if(result.data !== undefined){
                this.props.navigation.dispatch(NewUserCompletePerfil({countryCode:this.props.navigation.getParam('countryCode'),numberPhone:this.props.navigation.getParam('numberPhone'),backRoute:this.props.navigation.getParam('mainRoute')}));
            }else{
                if(result.request._hasError === false){
                    Toast.show({text:'The verification code is incorrect, please try again.',buttonText:'Ok',type:"danger",position:'bottom',duration:4000});
                }else{
                    Toast.show({text:'Sorry, an error occurred in the verification of the code. Please, try again.',buttonText:'Ok',type:"danger",position:'bottom',duration:4000});
                }
            }
        }
    }
    onSubmit = async () => {
        let result;
        this.props.dispatch(setFetching(true))
        result = await userService.verificationCodeSingup(`${this.props.navigation.getParam('countryCode')}${this.props.navigation.getParam('numberPhone')}`,this.state.inputCode);
        this.props.dispatch(setFetching(false))
        if(result.data !== undefined){
            this.props.navigation.dispatch(NewUserCompletePerfil({countryCode:this.props.navigation.getParam('countryCode'),numberPhone:this.props.navigation.getParam('numberPhone'),backRoute:this.props.navigation.getParam('mainRoute')}));
        }else{
            if(result.request._hasError === false){
                Toast.show({text:'The verification code is incorrect, please try again.',buttonText:'Ok',type:"danger",position:'bottom',duration:4000});
            }else{
                Toast.show({text:'Sorry, an error occurred in the verification of the code. Please, try again.',buttonText:'Ok',type:"danger",position:'bottom',duration:4000});
            }
        }
    }
    resendCode = async () => {
        let result;
        this.props.dispatch(setFetching(true));
        result = await userService.sendCodeSingup(this.props.navigation.getParam('numberPhone'),this.props.navigation.getParam('countryCode'));
        this.props.dispatch(setFetching(false));
        if(typeof(result) === 'string'){
            this.setState({...this.state,resendCodePress:!this.state.resendCodePress,resendCodeTime:'200'});
            this.props.navigation.setParams({codeValidation: result});
            Toast.show({
                text:'A new validation code has been requested, it will be sent to you in a few minutes.',
                buttonText:'Ok',
                type:'success',
                duration:4000,
                position:'bottom'
            });
        }else{
            Toast.show({text:'Sorry, a problem occurred while generating your new verification code.',buttonText:'Ok',type:"danger",duration:4000,position:'bottom'});
        }
    }
    onChanged = (text) => {
        this.setState({...this.state,inputCode: text.replace(/[^0-9]/g, '')})
    }
    componentWillUnmount(){
        this.smsSubscription.remove();
        clearInterval(this.countdownCall);
    }
    render(){
        return(<Container>
            <Loading visible={this.props.fetching} />
            <Content padder>
                <Grid>
                    <Row style={[style.contentCenter,{marginVertical:30}]}>
                        <H1 style={[style.fontBold,style.textCenter,style.fontRoboto,{paddingHorizontal:30}]}>Verify your phone number via SMS</H1>
                    </Row>
                    <Form style={{marginVertical:20}}>
                        <Label style={[style.fontBold,style.fontRoboto]}>Verification code</Label>
                        <TextInput
                            editable={this.state.resendCodeTime == '0' ? false : true}
                            name={'CodeValidation'}
                            autoCapitalize={'none'}
                            autoCorrect={false} 
                            returnKeyType='go'
                            autoFocus
                            keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'phone-pad'}
                            onChangeText={this.onChanged}
                            maxLength={4}
                            placeholder='####'
                            placeholderTextColor='#A6A6A6'
                            style={{fontSize:25,alignContent:'center',borderBottomColor:'#9B9B9B',borderBottomWidth:2}}
                            value={this.state.inputCode}
                            />
                    </Form>
                    <Row style={[style.contentCenter]}>
                        <Text style={[style.textCenter,style.fontBold]}>Didn't get the code?</Text>
                    </Row>
                    <Row style={style.contentCenter}>
                        <TouchableOpacity disabled={!this.state.resendCodePress} onPress={() => {this.resendCode()}}>
                            <Text style={style.textLinkConditions}>Resend code ({this.state.resendCodeTime})</Text>
                        </TouchableOpacity>
                    </Row>
                </Grid>
            </Content>
            <View>
                <ButtonGradient full title='Validate code' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />
            </View>
        </Container>);
    }
}