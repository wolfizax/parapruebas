import React,{Component} from 'react';
import {Container,Content, Item, Input, Label } from 'native-base';
import HeadGoBack from '../../presentation/HeadGoBack';
import ButtonFooter from '../../presentation/ButtonFooter';
import {addCategory } from '../../actions/category';
import {connect} from 'react-redux';
import MessageModal from './MessageModal';

class NewCategory extends Component{
	constructor(props){
		super(props)
		this.state = {
			input : '',
			modal:false
		}
	}

	handleSucess(){
		if(this._handleChecked(this.props.category,this.state.input)){
			this.setState({modal:!this.state.modal})
		}else{
			this.props.handleAddCategory(this.state.input);
			this.setState({input : ''});
			this.props.navigation.goBack();
		}
		
	}

	_handleChecked(categories,text){

		for(let i = 0;i<categories.length - 1;i++){
			if(categories[i]==text.toLowerCase()) return true;
		}
		return false;
	}

	render(){
		return(
			<Container>
				<HeadGoBack 
					title="Add contact category"
					goBack = {()=>this.props.navigation.goBack()}
				/>
				<Content>
            			<Item floatingLabel style={{marginLeft:10,marginRight:10,marginTop:20}}>
              				<Label style={{fontWeight:'bold'}}>New category</Label>
              				<Input 
              					value={this.state.input}
              					onChangeText = {(input)=>this.setState({input})}
              				/>
            			</Item>  				

				</Content>

				{this.state.input != '' && <ButtonFooter text = 'Save' handleSucess = {this.handleSucess.bind(this)} />}
			
				<MessageModal 
					modal = {this.state.modal}
					handleModal = {()=>this.setState({modal:!this.state.modal})}
				/>
			</Container>
		)
	}
}

const mapStateToProps = (state,props) =>{
    return{
        category: state.category
    }
} 


const mapDispatchToProps = dispatch =>{
    return{
        handleAddCategory(text){       
            dispatch(addCategory(text))
        }
  
    }
}


export default connect(mapStateToProps,mapDispatchToProps)(NewCategory);