import React,{Component} from 'react';
import {View,Text,StyleSheet} from 'react-native';
import {Container,Content} from 'native-base';
import HeadGoBack from '../../presentation/HeadGoBack';
import HeadAvatar from '../../presentation/HeadAvatar';
import Chart from './Chart';
import CardValue from './CardValue';


class Reputation extends Component{
    constructor(props){
        super(props);
         this.state = {
            user : this.props.navigation.getParam('item'),
            qualification:{
                progress : '30%',
                value : '4.4'
            },
            data : [ 50, 10, 40 ]
        }

    }


    render(){
        return(
            <Container>

                <HeadGoBack 
                    title="Reputation"
                    goBack = {()=>this.props.navigation.goBack()}
                />
                
                <HeadAvatar />

                <Content>

                    <View style={styles.inf}>
                        <Text style={styles.bold}>{this.state.user.displayName}</Text>
                        <Text>profession</Text>
                        <Text>{this.state.user.number}</Text>
                    </View>
                
                    <Chart 
                        data = {this.state.data}
                    />
                    <CardValue
                        qualification = {this.state.qualification}
                    />
                    
                    
                </Content>     
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    inf:{
        alignItems:'center'
    },
    bold:{
        fontWeight :'bold',
        fontSize: 26
    }

})


export default Reputation;