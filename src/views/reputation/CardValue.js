import React,{Component} from 'react';
import {View,Text, StyleSheet,Dimensions} from 'react-native'; 
import {CardItem,Card,Body} from 'native-base';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

const CardValue = ({qualification})=>(

    <Card style={styles.ctn}>
        <CardItem>
            <Body style={styles.body}>

                <View style={styles.ctnstart}>
                    <Icon name = "SI-9" style={{fontSize: 100,color:'#17d6b6'}}/>
                </View>
                <View style={styles.ctnload}>

                    <Text style={{fontSize:20,fontWeight:'bold'}}>{qualification.value}</Text>
                    <View  style={styles.progress}>
                        <View style={{width:qualification.progress,height:5,backgroundColor:'#17d6b6'}} />
                    </View>
                            
                    <Text style={{fontSize:18}}>{qualification.progress} ultimos 3 meses</Text>
                </View>
            </Body>
        </CardItem>
    </Card>
)

const styles = StyleSheet.create({
    ctn:{
        marginLeft: 10,
        marginRight: 10
    },
    body:{
        flexDirection:'row'
    },
    ctnstart : {
        alignItems:'center',
        justifyContent:'center',
        borderRightColor:'#eee',
        borderRightWidth:0.5
    },
    ctnload:{
        alignItems:'center',
        justifyContent:'center',
        paddingRight:10,
        paddingLeft:10,
        flex:1,
        fontSize:20
    },
    progress:{
        width:'100%',
        borderColor:'#c7c7c7',
        borderWidth:1,
        height:7,
        marginTop:10,
        marginBottom:10,
        borderRadius:20

    }
})

CardValue.proptypes = {
    qualification : Proptypes.string.isRequired
}


export default CardValue;