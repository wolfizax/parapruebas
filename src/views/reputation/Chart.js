import React,{Component} from 'react';
import {View} from 'react-native';
import { BarChart, Grid,XAxis,YAxis } from 'react-native-svg-charts';
import Proptypes from 'prop-types';

const fill = 'rgb(134, 65, 244)';

const Chart = ({data})=>(
        
  <View style={{ height: 250,  padding: 20  }}>

    <BarChart   
      style={{ flex: 1 }}     
      data={ data }
      svg={{ fill }}
      contentInset={{ top: 20, bottom: 20 }}
    >
      <Grid/>
    </BarChart>
  </View>

)

Chart.proptypes = {
    data : Proptypes.array.isRequired
}


export default Chart;