import React,{Component} from 'react';
import {Container,Content,Tab, Tabs,Text} from 'native-base';
import HeadSearch from '../../presentation/HeadSearch';
import Contacts from '../../presentation/ListNavigationContacts';
import Groups from './Groups';

class SearchContacts extends Component{
	constructor(props){
		super(props);
		this.state = {
			search : '',
            contacts : [
            {
                displayName : 'ricardo perez',
                number : '+584126534991'
            },
             {
                displayName : 'diego valdez',
                number : '+584166534991'
            }
            ],
            groups : [
            {
            	name :' group 1'
            },
            {
            	name :' group 2'
            }
            ]
		}
	}


	render(){
		return(
			<Container>
				<HeadSearch 
                    handleSearch = {(search)=>this.setState({search})} 
                    goBack = {()=>this.props.navigation.goBack()}
                />
                <Content>

				<Tabs tabBarBackgroundColor={'white'} tabBarUnderlineStyle={{backgroundColor:'black',height:1.5}}  >
                	<Tab 
                        heading="Contacts"
                        tabStyle={{backgroundColor:'white'}} 
                        textStyle={{color:'#c7c7c7'}} 
                        activeTabStyle={{ backgroundColor: 'white' }} 
                        activeTextStyle={{color:'black'}}>
                    	
                    		<Contacts
                    			text ={this.state.search}
                    			contacts = {this.state.contacts}
                                navigation = {(item)=>this.props.navigation.navigate('InfContacts',{item})}
                            />
						
                	</Tab>
                	<Tab  
                        heading="Groups"
                        tabStyle={{backgroundColor:'white'}} 
                        textStyle={{color:'#c7c7c7'}} 
                        activeTabStyle={{ backgroundColor: 'white' }} 
                        activeTextStyle={{color:'black'}}>                   	
                		<Groups
                			text ={this.state.search}
                    		groups = {this.state.groups}
                    	/>
                	</Tab>

                </Tabs>

                </Content>
				
			</Container>
			
		)
	}
}


export default SearchContacts;