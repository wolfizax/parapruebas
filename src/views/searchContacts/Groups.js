import React from 'react';
import {View,StyleSheet} from 'react-native';
import {List, ListItem,Left,Right, Body,Text,Thumbnail} from 'native-base';
import {capitalize} from '../../utils/letter';
import Proptypes from 'prop-types';
import Icon from '../../presentation/Icon/index'

const Groups = ({groups,text})=>(
    <List>
        {groups.sort((a,b)=>{return a.name.localeCompare(b.name)}).map((item,i)=>
            {if(item.name.toLowerCase().indexOf(text.toLowerCase())!==-1){ 
                return(
                    <ListItem key = {i}  > 
                        <Left style={styles.left}>
                            <Thumbnail source={require('../../assets/usuario.png')} />
                            <Body style={styles.body}>
                                <Text>{capitalize(item.name)}</Text>
                                <Text note>sub-title</Text>                        
                            </Body> 
                        </Left>                                 
                    </ListItem>
                )
            }}
            
        )}  
    </List>   
)


const styles = StyleSheet.create({
    body:{
        flexDirection:'column',
        marginTop:10
    },
    text : {
        color : '#17D6B6'
    },
    left:{
        flex:1
    },
    right:{
        justifyContent:'center',
        alignItems:'center'
    }
})

Groups.proptypes = {
    groups : Proptypes.array.isRequired,
    text : Proptypes.string.isRequired
}


export default Groups;