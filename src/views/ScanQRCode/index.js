import React, { Component } from 'react';
import {View,StyleSheet,TouchableOpacity} from 'react-native';
import { Container, Content } from 'native-base';
import HeadGoBack from '../../presentation/HeadGoBack';


class ScanQRCode extends Component{
    constructor(props){
        super(props);
        this.state = {
            type: RNCamera.Constants.Type.back,
            flashMode: RNCamera.Constants.FlashMode.auto
        }
    }


    onBarCodeRead(scanResult) {
        console.log(scanResult.data)
    }

    render(){
        return(
            <Container>

                <HeadGoBack 
                    title = "Scan Qr Code"
                    goBack = {()=>this.props.navigation.goBack()}
                />

                <View style={styles.container}>
                    <RNCamera
                        ref={ref => {
                        this.camera = ref;
                    }}
                        barcodeFinderVisible={false}
                        defaultTouchToFocus
                        flashMode={this.state.camera.flashMode}
                        mirrorImage={false}
                        onBarCodeRead={this.onBarCodeRead.bind(this)}
                        style={styles.preview}
                        type={this.state.camera.type}
                    />

                    <View style={styles.footer}>
                        <TouchableOpacity onPress={this.onBarCodeRead.bind(this)}>
                            <View style={styles.cntIcon}></View>
                        </TouchableOpacity>

                    </View>
                </View>
            </Container>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    footer:{
        width:'100%',
        height:55,
        justifyContent:'center',
        alignItems:'center'
    },
    cntIcon:{
        width:50,
        height:50,
        backgroundColor:'#d7d7d7',
        justifyContent:'center',
        alignItems:'center'
    }

})

export default ScanQRCode;