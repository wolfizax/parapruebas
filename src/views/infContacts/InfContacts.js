import React,{Component} from 'react';
import {View,StyleSheet} from 'react-native';
import {Container,Content,ListItem,Text,Thumbnail} from 'native-base';
import {connect} from 'react-redux';
import Head from './Head';
import HeadAvatar from '../../presentation/HeadAvatar';
import Icon from '../../presentation/Icon/index';
import Badge from '../../presentation/BadgeCircle';

class InfContacts extends Component{
    constructor(props){
        super(props);
        this.state = {
            user : this.props.navigation.getParam('item'),
            index:this.props.navigation.getParam('index')
        }
       
    }


	render(){
		return(
			<Container>
				<Head 
					goBack = {()=>this.props.navigation.goBack()}
					navigation = {(routeName)=>this.props.navigation.navigate(routeName,{item:this.state.user,index:this.state.index})}
				/>

				<HeadAvatar/>

				<Content>

					<View style={styles.inf}>
						<Text style={styles.bold}>{this.state.user.displayName}</Text>
						<Text>profession</Text>
						<Text>{this.state.user.number}</Text>
					</View>

				<View style={{paddingLeft:10,paddingRight:15,marginTop:30}}>

                <ListItem style={{flexDirection:'row',justifyContent:'space-between'}} onPress={()=>this.props.navigation.navigate('MomentsContacts')} >
                    <Text>Moments</Text>
                    <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                        {[1,2,3,4].map((item,i)=>
                         <Thumbnail key={i} small source={require('../../assets/bg.jpeg')} style={{marginLeft:10}} />
                        )}
                    </View>

                    <Icon name="SI-17" size={18} color="#3CD22F" style={{marginTop:6,marginLeft:5}} />    

                </ListItem> 
                <ListItem style={{flexDirection:'row',justifyContent:'space-between'}} onPress={()=>this.props.navigation.navigate('Reputation',{item:this.state.user})}>
                    
                    <Text>Reputation</Text>     
                    <Icon name="SI-17" size={18} color="#3CD22F" />    
                    
                </ListItem> 
                <ListItem style={{flexDirection:'row',justifyContent:'space-between'}}  onPress={()=>this.props.navigation.navigate('WonderTales')}>
                   
                    <Text>Wonder Tales</Text>
                    <Badge number = {15} />
                    
                </ListItem> 
                <ListItem style={{flexDirection:'row',justifyContent:'space-between'}} onPress={()=>this.props.navigation.navigate('ViewProfession',{item:this.state.user})}>
                    <Text>View profession</Text>
                    <Icon name = "I-13-Perfil" color="#C7C7C7" size={18} />
                    
                </ListItem> 
                <ListItem style={{flexDirection:'row',justifyContent:'space-between'}}>
                    <Text>Category</Text>
                    <View style={{flexDirection:'row'}}>
                        {
                            [this.state.user.category].map((item,i)=>
                                <View key={i} style={{flexDirection:'row'}}>
                                    <Text style={{color:'#C7C7C7'}}>{item}</Text>
                                    {(i!=0) &&<Text style={{color:'#C7C7C7'}}>/</Text>}
                                </View>
                            )
                        }
                    </View>
                </ListItem> 
                
            	</View>


				</Content>

			</Container>

		)
	}
}

const styles = StyleSheet.create({
	inf:{
		alignItems:'center'
	},
	bold:{
		fontWeight :'bold',
		fontSize: 26
	}

})


const mapStateToProps = (state,props) =>{
    return{
        contacts : state.contacts
    }
} 


export default connect(mapStateToProps,null)(InfContacts);

