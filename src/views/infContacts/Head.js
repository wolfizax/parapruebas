import React from 'react';
import {View,StyleSheet,Text} from 'react-native';
import {Button} from 'native-base';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';


const Head = ({goBack,navigation})=>(

	<View style={styles.head}>
        <Button transparent style={styles.icon} onPress={()=>goBack()}>
            <Icon name="left-arrow" size = {16} color="#3CD22F" />                  
        </Button>
        <Text style={styles.title}>Contact</Text>
        <Button transparent style={styles.icon}  onPress={()=>navigation('OptionsContacts')}>
            <Icon name="list-menu" size = {18} color="#3CD22F" />               
        </Button>
    </View>

)

const styles = StyleSheet.create({
    head:{
        height:60,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:'white',
        color:'white',
        paddingLeft:20,
        paddingRight:20
    },
    icon:{
        marginTop:10
    },
    title:{
        color:'black',
        fontSize:20,
        paddingLeft:2
    }

})

Head.proptypes = {
    goBack : Proptypes.func.isRequired,
    navigation : Proptypes.func.isRequired
}

export default Head;
