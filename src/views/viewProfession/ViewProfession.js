import React,{Component} from 'react';
import {View,StyleSheet,Text} from 'react-native';
import {Container,Content} from 'native-base';
import HeadGoBack from '../../presentation/HeadGoBack';
import HeadAvatar from '../../presentation/HeadAvatar';

class ViewProfession extends Component{
	constructor(props){
        super(props);
        this.state = {
            user : this.props.navigation.getParam('item')
        }
    }

	render(){
		return(
			<Container>

				<HeadGoBack 
					title = "Professional View"
					goBack = {()=>this.props.navigation.goBack()}
				/>

				<Content style={styles.ctn}>

					<View style={styles.ctnDescription}>
						<Text style={[styles.bold,styles.size]}>Profession</Text>
						<Text style={styles.size,styles.gray}>name profession</Text>
					</View>
					<View>

						<Text style={[styles.bold,styles.size]}>Description of Skills</Text>
						<Text>
							lorems ipsu
						</Text>

					</View>
				</Content>

			</Container>

		)
	}
}

const styles = StyleSheet.create({
	ctn:{
		paddingLeft:10,
		paddingRight:10
	},
	inf:{
		alignItems:'center'
	},
	ctnDescription:{
		flexDirection:'row',
		justifyContent:'space-between',
		marginTop:10,
		marginBottom:10

	},
	bold:{
		fontWeight:'bold'
	},
	size:{
		fontSize:20
	},
	gray:{
		color:'#c7c7c7'
	}

})

export default ViewProfession;