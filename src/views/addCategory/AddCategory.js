import React,{Component} from 'react';
import {Container,Content} from 'native-base';
import Head from './Head';
import ListCategory from './ListCategory';
import DeleteModal from './DeleteModal';
import AlertModal from './AlertModal';
import {connect} from 'react-redux';
import { deleteCategory } from '../../actions/category';
import {checked} from '../../utils/categoryChecked';

class AddCategory extends Component{

	constructor(props){
		super(props);
		this.state = {
			modal: false,
			modalAlert:false,
			index : null,
			category : null
		}
	}

	handleDelete (){
		if(!checked(this.props.contacts,this.state.category)){
			this.props.handleDeleteCategory(this.state.index);
			this.setState({index:null,category:null,modal:false});
		}else{
			this.setState({modalAlert:true,modal:false})
		}
		
	}

	render(){
		return(
			<Container>
				<Head
					goBack = {()=>this.props.navigation.goBack()}
					navigation = {(category)=>this.props.navigation.navigate(category)}
				/>
				<Content>
					<ListCategory 
						category = {this.props.category}
						handleModal = {(category,index)=>this.setState({modal:true,category,index})}
						navigation = {(category)=>this.props.navigation.push('EditCategory',{category})}
					/>

				</Content>
				<DeleteModal 
					modal = {this.state.modal}
					handleModal = {()=>this.setState({modal:false})}
					handleDelete = {this.handleDelete.bind(this)}
					nameCategory = {this.state.category}
				/>
				<AlertModal 
					modal = {this.state.modalAlert}
					handleModal = {()=>this.setState({modalAlert:!this.state.modalAlert})}
				/>
			</Container>
		)
	}
}

const mapStateToProps = (state,props) =>{
    return{
        category: state.category,
        contacts : state.contacts
    }
} 

const mapDispatchToProps = dispatch =>{
    return{
        handleDeleteCategory(index){       
            dispatch(deleteCategory(index))
        }
  
    }
}


  
export default connect(mapStateToProps,mapDispatchToProps)(AddCategory);