import React from 'react';
import {StyleSheet,View} from 'react-native';
import {Text,Right,Button} from 'native-base';
import Modal from 'react-native-modal';
import Proptypes from 'prop-types';

const AlertModal = ({modal,handleModal})=>(

	<Modal onBackdropPress={() => handleModal()} onBackButtonPress={() => handleModal()} isVisible={modal}>
        <View style={styles.modalContent}>     
            <View>
                <Text>This category can't be eliminated because it has contacts assigned to it.</Text>
            </View>
        </View>
             
    </Modal>


)
const styles = StyleSheet.create({

	modalContent:{
		width:'80%',
		marginLeft:'10%',
        backgroundColor:'white',
        padding:15,
        borderRadius:5,
        borderColor:"rgba(0, 0, 0, 0.1)"

    },
    modalContentRow:{
        width:'100%',
        flexWrap:'wrap',
        flexDirection:'row',
        alignItems:'center',
        padding:5
    },
    btn:{
        flexDirection:'row'
    }
})


AlertModal.proptypes = {
    modal: Proptypes.bool.isRequired,
    handleModal : Proptypes.func.isRequired,
}



export default AlertModal;