import React from 'react';
import {View,StyleSheet} from 'react-native';
import {List, ListItem,Left,Right, Body,Text,Thumbnail,Button} from 'native-base';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

const ListCategory = ({category,handleModal,navigation})=>(
    <List style={styles.ctn}>
        <ListItem style={styles.list}> 
            <Left><Text style={styles.bold}>Available Categories</Text></Left>         
        </ListItem>
        {category.map((item,i)=>
            {if(item.label != 'all'){
                return(
                    <ListItem key = {i} style={styles.list}> 
                        <Left style={{flex:1}}>
                            <Body><Text>{item.label}</Text></Body> 
                        </Left> 
                        <View style={styles.right}>
                            <Button transparent onPress={()=>navigation(item.label)}><Icon name="SI-7" size={20} color="#3CD22F" style={{marginRight:16}} /></Button>
                            <Button transparent onPress={()=>handleModal(item.label,i)}><Icon name="SI-6" size={20} color="#3CD22F" /></Button>       
                        </View>                                        
                    </ListItem>
                )
            }}
        )}  
    </List>   
)


const styles = StyleSheet.create({
    ctn:{
        marginLeft:15,
        marginRight:15
    },
    list:{
        marginRight:0,
        marginLeft:0
    },
    body:{
        flexDirection:'column',
        marginTop:10
    },
    bold:{
        fontWeight:'bold'
    },
    right:{
        flexDirection:'row'
    }

})

ListCategory.proptypes = {
    category : Proptypes.array.isRequired,
    handleModal : Proptypes.func.isRequired,
    navigation : Proptypes.func.isRequired
}


export default ListCategory;