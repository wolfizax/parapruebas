import React from 'react';
import {View,StyleSheet,Text} from 'react-native';
import {Button} from 'native-base';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

const Head = ({goBack,navigation})=>(

    <View style={styles.head}>
        <Button transparent style={styles.icon} onPress = {()=>goBack()}>
            <Text style={{color:'#3CD22F'}}>cancel</Text>               
        </Button>
        <Text style={styles.title}>Add contact category</Text>
        <Button transparent style={styles.icon} onPress = {()=>navigation('NewCategory')}>
            <Icon name="Enmascarar-grupo-36" size={16} color="#3CD22F" />                
        </Button>
    </View>

)


const styles = StyleSheet.create({
    head:{
        height:50,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:'white',
        color:'white',
        paddingHorizontal:10,
        paddingVertical:4,
        shadowColor: "#000",
        shadowOffset: {
        width: 0,
        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    title:{
        color:'black',
        fontSize:20,
        paddingLeft:3
    }

})

Head.proptypes = {
    goBack : Proptypes.func.isRequired,
    navigation : Proptypes.func.isRequired
}


export default Head;