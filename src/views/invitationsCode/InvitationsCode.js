import React,{Component} from 'react';
import {View} from 'react-native';
import {Container,Content,List, ListItem,Text,Left,Body} from 'native-base';
import HeadGoBack from '../../presentation/HeadGoBack';

class InvitationsCode extends Component{
	constructor(props){
		super(props);
		this.state = {
			invitations : [
					{
						code : '005866',
						number : '+584126534991'
					},
					{
						code : '005867',
						number : '+584146534991'
					},
					{
						code : '003466',
						number : '+584166534991'
					},
			]
		}
	}

	render(){
		return(
			<Container>

				<HeadGoBack 
					title = "External pending invitations"
					goBack = {()=>this.props.navigation.goBack()}
				/>

				<Content>

					<List>
						{this.state.invitations.map((item,i)=>

							<ListItem key ={i} style={{marginLeft:10,marginRight:10}}>
                    			<Left style={{flexDirection:'row'}}>
                    				<View style={{width:40,height:40,borderRadius:14,backgroundColor:'#2B2B2B',justifyContent:'center',alignItems:'center'}}>
                    					<Text style={{color:'white'}}>BE</Text>
                    				</View>
                    				<Body>
                        				<Text>Invitations code {item.code}</Text>
                        				<Text note> {item.number} </Text>
                    				</Body>
                    			</Left>
                    			
                   			</ListItem>
                   		)}
					</List>

				</Content>

			</Container>
		)
	}
}

export default InvitationsCode;