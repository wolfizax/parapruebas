import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    containerCenter:{
        justifyContent:'center'
    },
    logoImage:{
        height:230,
        width:230
    },
    title:{
        fontFamily:'Roboto',
        fontWeight:'bold',
        fontSize:25
    },
    logoContainer:{
        marginTop:100,
    },
    titleContainer:{
        marginTop:20,
    },
    buttonsContainer:{
        paddingTop:30
    },
    linkNewUser:{
        paddingRight:5,
        color:'#30BFFF',
        fontSize:20,
    }
})