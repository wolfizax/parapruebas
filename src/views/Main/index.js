import React,{Component} from 'react';
import {Image,TouchableOpacity} from 'react-native';
import LoggedView from '../index';
import {Container,Content,H1,Text,View} from 'native-base';
import {Row, Grid} from "react-native-easy-grid";
import style from './style';
import ButtonGradient from '../../presentation/ButtonGradient';
import {NewUser, Login, Dashboard} from '../../actions/nav';
import { appInit } from '../../actions/app';
import { connect } from 'react-redux';

@connect(state => ({
    root: state.app.root
}))
/** @extends Component */
export default class MainView extends LoggedView{
    static navigationOptions = {header:null };
    constructor(props){
        super(props);
    }   
    async componentWillMount(){
       await this.props.dispatch(appInit());
       if(!(this.props.root === 'isLogin')){
           this.props.navigation.dispatch(Dashboard());
       }
    }
    render(){
        return(<Container>
            <Content padder>
                <Grid>
                    <Row style={[style.logoContainer,style.containerCenter]}>
                        <Image resizeMode='contain' style={style.logoImage} source={require('../../assets/logo.png')} />
                    </Row>
                    <Row style={[style.titleContainer,style.containerCenter]}>
                        <H1 style={style.title}>WonderChatt</H1>
                    </Row>
                </Grid>
            </Content>
            <View style={{marginBottom:5}}>
                <ButtonGradient full title='Log in' gradientColors={['#1f1f1f', '#1f1f1f']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={() => this.props.navigation.dispatch(Login())} />
            </View>
            <View>
                <ButtonGradient full title='Register' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={() => this.props.navigation.dispatch(NewUser())} />
            </View>
        </Container>);
    }
}