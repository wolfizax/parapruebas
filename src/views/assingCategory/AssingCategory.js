import React,{Component} from 'react';
import {View,Text,StyleSheet} from 'react-native';
import {Container,Content,List,ListItem} from 'native-base';
import HeadGoBack from '../../presentation/HeadGoBack';
import HeadAvatar from '../../presentation/HeadAvatar';
import ButtonFooter from '../../presentation/ButtonFooter';
import {editContactsCategory} from '../../actions/contacts';
import {connect} from 'react-redux';


class AssingCategory extends Component{
	constructor(props){
		super(props);
		this.state = {
			user : this.props.navigation.getParam('item'),

		}
	}

	handleSelect(i,category){
		let data = this.state.user;
      	data.category = category;
      	this.setState({...this.state,data})
	}

	handleSave(){
		this.props.handleEditCategory(this.state.user)
		this.props.navigation.goBack();
	}

	render(){
		return(
			<Container>
				<HeadGoBack 
					title = "Contact"
					goBack = {()=>this.props.navigation.goBack()}
				/>

				<Content>

					<View style={{marginBottom:10,marginLeft:22,marginTop:10}}>
						<View><Text style={styles.bold}>Availables category</Text></View>
					</View>

					<List style={styles.list}>

						{this.props.category.map((item,i)=>
							
							{if(item.label!='all'){
								return(<ListItem style={styles.list} key={i} onPress={()=>this.handleSelect(i,item.label)} style={(item.label == this.state.user.category) ? styles.listTrue : styles.listFalse}><Text style={(item.label == this.state.user.category) ? styles.textTrue : styles.textFalse}>{item.label}</Text></ListItem>)	
							}}
						)}
			            			
            		</List>

				</Content>

				<ButtonFooter text = 'Save' handleSucess = {this.handleSave.bind(this)} />


			</Container>
		)
	}
}

const styles = StyleSheet.create({
  list:{
  	marginLeft:10,
  	paddingRight:24
  },
  bold:{
    fontWeight:'bold',
    fontSize: 26
  },
  listTrue:{
  	backgroundColor:'#3CD22F',
  },
  listFalse:{
  	backgroundColor : 'white'
  },
  textTrue:{
  	color:'white',
  },
  textFalse:{
  	color : 'black'
  },

})

const mapStateToProps = (state,props) =>{
    return{
        category: state.category
    }
} 


const mapDispatchToProps = dispatch =>{
    return{
        handleEditCategory(user){       
            dispatch(editContactsCategory(user))
        }
  
    }
}

  
export default connect(mapStateToProps,mapDispatchToProps)(AssingCategory) 
