import React,{Component} from 'react';
import {Content,Container} from 'native-base';
import HeadSearch from '../../presentation/HeadSearch';
import Chips from '../../presentation/Chips';
import ListaAddContacts from '../../presentation/ListAddContacts';
import Head from './Head';
import ButtonFooter from '../../presentation/ButtonFooter';
import {connect} from 'react-redux';
import {addMembers} from '../../actions/members';


class AddMembers extends Component{
    constructor(props){
        super(props);
        this.state = {
            activate : false,
            search : '',
            activateAddContacts : false,
            members : [],
            contacts : this.props.contacts
        }
    }

    handleAddContacts (user,index) {
        if(this.state.activateAddContacts){
            this.setState({
                members : this.state.members.concat(user),
                contacts : this.state.contacts.filter((item,i)=>{
                            return i !== index;
                        })
            })  
        }
    }

    handleAddContactsActivate(user,index){
        this.setState({
            activateAddContacts:true,
            members :  this.state.members.concat(user),
            contacts : this.state.contacts.filter((item,i)=>{
                        return i !== index;
            })
        })  
    }

    handleRemoveContacts (user,index){
        this.setState({
            contacts : this.state.contacts.concat(user),
            members : this.state.members.filter((item,i)=>{
                            return i !== index;
                        }),
            activateAddContacts : (this.state.members.length - 1 == 0)  ? false : true

        })
       
    }

    handleSucess(){
        this.props.handleAddMembers(this.state.members);
        this.props.navigation.navigate('CreateGroup');
    }


    render(){
        return(
            <Container>
                {this.state.activate ?  <HeadSearch handleSearch = {(search)=>this.setState({search})} activateSearch={()=>this.setState({activate : !this.state.activate,search:''})} /> : <Head activateSearch={()=>this.setState({activate :!this.state.activate})} goBack = {()=>this.props.navigation.goBack()} />}
                               
                {this.state.members && <Chips contacts = {this.state.members} handleRemoveContacts = {this.handleRemoveContacts.bind(this)}/>}
                
                <Content>
                   
                    <ListaAddContacts
                        text = {this.state.search}
                        contacts = {this.state.contacts}
                        handleAddContacts ={this.handleAddContacts.bind(this)}
                        handleAddContactsActivate = {this.handleAddContactsActivate.bind(this)}
                    />
                   
                </Content>

                {this.state.members.length !=0 && <ButtonFooter text="Next" handleSucess = {this.handleSucess.bind(this)}/>}

            </Container>
        )
    }
}


const mapStateToProps = (state,props) =>{
    return{
        contacts : state.contacts
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        handleAddMembers(members){       
            dispatch(addMembers(members))
        }
  
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(AddMembers);