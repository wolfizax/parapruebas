import React from 'react';
import {View,StyleSheet,Text} from 'react-native';
import {Button} from 'native-base';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';


const Head = ({goBack,activateSearch})=>(

	<View style={styles.head}>
        <Button transparent style={{marginTop:3}} onPress={()=>goBack()}>
            <Text style={styles.text}>cancel</Text>               
        </Button>
        <Text style={styles.title}>New Group</Text>
        <Button transparent style={styles.icon}  onPress={()=>activateSearch()}>
            <Icon name="Enmascarar-grupo-63" size = {18} color="#3CD22F" />               
        </Button>
    </View>
)

const styles = StyleSheet.create({
    head:{
        height:50,
        width:'100%',
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        backgroundColor:'white',
        color:'white',
        paddingLeft:10,
        paddingRight:10,
        borderBottomWidth:1,
        borderBottomColor:'#c7c7c7',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 4
    },
    text:{
        color:'#3CD22F'
    },
    icon:{
        color:'#3CD22F',
        marginTop:3
    },
    title:{
        color:'black',
        fontSize:20,
        paddingLeft:3
    }

})

Head.proptypes = {
    goBack : Proptypes.func.isRequired,
    activateSearch : Proptypes.func.isRequired
}

export default Head;
