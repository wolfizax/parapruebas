import React, {Component} from 'react';
import {LOGIN_LOGOUT,setFetching} from '../../actions/user';
import {NavigationActions} from 'react-navigation';
import {Toast} from 'native-base';
import {userService} from '../../services/user';
import {connect} from 'react-redux';
import LoggedView from '../index';

@connect(state => ({
    userId: state.user._id,
    userRole: state.user.role,
    userToken: state.user.token
}))
/** @extends Component */
export default class LogoutView extends LoggedView{
    onLogout = async () => {
        const {userId,userToken,userRole} = this.props;
        this.props.navigation.dispatch(setFetching(true));
        result = await userService.logout(userId,userToken,userRole);
        this.props.navigation.dispatch(setFetching(false));
        if(result === true){
            this.props.navigation.dispatch(LOGIN_LOGOUT());
            this.props.navigation.reset([NavigationActions.navigate({ routeName: 'Main' })], 0);
        }else{
            Toast.show({
                'text':'Sorry, an error occurred and could not log out.',
                duration:4000,
                type:'success',
                position:'bottom',
                onClose:(() => this.props.navigation.goBack())
            });
        }
    }
    componentDidMount(){
        this.onLogout();
    }
    render(){
        return(null)
    }
}