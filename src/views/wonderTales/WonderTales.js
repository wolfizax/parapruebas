import React,{Component} from 'react';
import {View,Text,StyleSheet} from 'react-native';
import {Container,Content} from 'native-base';
import WonderList  from './WonderList';
import HeadGoBack from '../../presentation/HeadGoBack';
import HeadAvatar from '../../presentation/HeadAvatar';

class WonderTales extends Component{
	constructor(props){
		super(props);
		this.state = {
			wonderlist :[
				{
					displayName : 'ricardo perez',
					date:new Date(),
					token : 45
				},
				{
					displayName : 'yessica marchi',
					date:new Date(),
					token : 4
				},
				{
					displayName : 'edgardo avendaño',
					date:new Date(),
					token : 1
				}
			]
		}
	}

	render(){
		return(
			<Container>
				<HeadGoBack 
					title="Wonder Tales"
					goBack = {()=>this.props.navigation.goBack()}
				/>

				<Content>

					<WonderList 
						contacts = {this.state.wonderlist}
					/> 

				</Content>
			</Container>
		)
	}
}



const styles = StyleSheet.create({
	inf:{
		alignItems:'center'
	},
	bold:{
		fontWeight :'bold',
		fontSize: 26
	}

})
export default WonderTales;