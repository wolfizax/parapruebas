import React from 'react';
import {StyleSheet,View} from 'react-native';
import {List, ListItem, Thumbnail,Left, Body,Text} from 'native-base';
import Time from '../../presentation/Time';
import Proptypes from 'prop-types';

const WonderList = ({contacts})=>(

    <List>
        {contacts.map((item,i)=>
            <ListItem key = {i} style={styles.list}>  
                <Left>
                    <Thumbnail source={require('../../assets/usuario.png')} />  
                        <Body style={{flexDirection:'column',marginTop:10}}>
                        <Text>{item.displayName}</Text>             
                    </Body> 
               </Left>
                                                   
                <View style={styles.right}>
                  <Time date ={item.date} color='#c7c7c7'/>
                  {item.token && <Text style={{ color: '#c7c7c7' }}>{item.token} tokens</Text>}
                </View>
                                     
            </ListItem>
        )}
               
    </List>  

)

WonderList.proptypes = {
    contacts : Proptypes.array.isRequired
}

const styles = StyleSheet.create({
  list:{
    marginRight:10,
    marginLeft:10,
  },
  right:{
    justifyContent:'flex-start'
  }
})


export default WonderList;