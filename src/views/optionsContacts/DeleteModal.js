import React from 'react';
import {StyleSheet,View} from 'react-native';
import {Text,Right,Button} from 'native-base';
import Modal from 'react-native-modal';
import Icon from '../../presentation/Icon/index';
import Proptypes from 'prop-types';

const DeleteModal = ({modal,handleModal,handleDelete})=>(

	<Modal onBackdropPress={() => handleModal()} onBackButtonPress={() => handleModal()} isVisible={modal}>
        <View style={styles.modalContent}>     
            <View>
                <Text style={styles.bold}>Deleted contact</Text>
            </View>
            <View>
                <Text>The contact will be deleted,continue?</Text>
            </View>
            <View style={styles.btn}>
                <Button transparent onPress={()=>handleModal()}><Icon name="SI-6" size={16} color="red"/></Button>
                <Button transparent onPress={()=>handleDelete()}><Icon name="Acept" size={16} color="#17D6B6"/></Button>
            </View>
        </View>
             
    </Modal>

)


const styles = StyleSheet.create({

	modalContent:{
		width:'80%',
		marginLeft:'10%',
        backgroundColor:'white',
        padding:15,
        borderRadius:5,
        borderColor:"rgba(0, 0, 0, 0.1)"

    },
    modalContentRow:{
        width:'100%',
        flexWrap:'wrap',
        flexDirection:'row',
        alignItems:'center',
        padding:5
    },
    btn:{
        flexDirection:'row',
        justifyContent:'space-between',
        width:'100%'
    },
    center:{
        alignItems:'center',
        marginBottom:10
    },
    bold:{
        fontWeight:'bold'
    }
})


DeleteModal.proptypes = {
    modal: Proptypes.bool.isRequired,
    handleModal : Proptypes.func.isRequired,
    handleDelete : Proptypes.func.isRequired
}



export default DeleteModal;