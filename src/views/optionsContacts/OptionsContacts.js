import React,{Component} from 'react';
import {StyleSheet} from 'react-native';
import {Container,Content,List, ListItem, Text,Body,Left} from 'native-base';
import {connect} from 'react-redux';
import BlockModal from './BlockModal';
import DeleteModal from './DeleteModal';
import HeadGoBack from '../../presentation/HeadGoBack';
import Icon from '../../presentation/Icon/index';
import { editBlockContact,deleteContact } from '../../actions/contacts';

class OptionsContacts extends Component{
  constructor(props){
    super(props);
    this.state = {
      index: this.props.navigation.getParam('index'),
      user:this.props.navigation.getParam('item'),
      modaldelete : false,
      modalblock : false
    }
  

  }

  handleBlockContacts (){
    this.props.handleBlockContact(this.props.navigation.getParam('index'),!this.props.contacts[this.props.navigation.getParam('index')].block);
    this.setState({modalblock:!this.state.modalblock});
  }

  handleUnlockContacts (){
    this.props.handleBlockContact(this.props.navigation.getParam('index'),!this.props.contacts[this.props.navigation.getParam('index')].block);
    this.setState({modalblock:!this.state.modalblock});
  }

  handleDeleteContact(){
    this.props.handleDeleteContact(this.state.index);
    this.setState({modaldelete:!this.state.modaldelete});
    this.props.navigation.navigate('Dashboard');
  }

  render(){
    return(
      <Container>
        <HeadGoBack 
          title ="Contact"
          goBack = {()=>this.props.navigation.goBack()}
        />

        <Content>

          <List>

            <ListItem style={styles.list} onPress={()=>this.props.navigation.navigate('AssingCategory',{item:this.props.navigation.getParam('item')})}>
              <Left>
                <Icon  name="I-13-Perfil" style={styles.icon} color="black" size={16} />
                <Body><Text>Assing Category</Text></Body>
              </Left>
              
            </ListItem>
            <ListItem style={styles.list}>
              <Left>
                <Icon  name="Share" style={styles.icon} color="black" size={16} />
                <Body><Text>Share</Text></Body>
              </Left>
              
            </ListItem>
            {(this.props.contacts[this.props.navigation.getParam('index')]) &&
            <ListItem style={styles.list} onPress = {()=>this.setState({modalblock:!this.state.modalblock})}>
              <Left>
                <Icon  name="Info" color="black" size={16} style={styles.icon} />
                <Body><Text>{this.props.contacts[this.props.navigation.getParam('index')].block ? "Unlock" : "Block"}</Text></Body>
              </Left>
              
            </ListItem>
            }
            <ListItem style={styles.list} onPress={()=>this.props.navigation.navigate('ReportContact',{item:this.props.navigation.getParam('item')})}>
              <Left>
                <Icon active name="ReportConatct" color="black" size={16} style={styles.icon} />
                <Body><Text>Report</Text></Body>
              </Left>
              
            </ListItem>
            <ListItem style={styles.list} onPress = {()=>this.setState({modaldelete:!this.state.modaldelete})}>
              <Left>
                <Icon active name="DI-27" color="red" size={16} style={styles.icon} />
                <Body><Text>Delete</Text></Body>
              </Left>
              
            </ListItem>

          </List>

        </Content>
        {(this.props.contacts[this.props.navigation.getParam('index')]) &&
        <BlockModal 
          text = {this.props.contacts[this.props.navigation.getParam('index')].block ? "you want to unblock this contact?" : "you want to block this contact?"}
          modal = {this.state.modalblock}
          handleModal = {()=>this.setState({modalblock:!this.state.modalblock})}
          handleBlock = {this.props.contacts[this.props.navigation.getParam('index')].block ? this.handleUnlockContacts.bind(this) : this.handleBlockContacts.bind(this)}
        />
        }
        <DeleteModal 
          modal = {this.state.modaldelete}
          handleModal = {()=>this.setState({modaldelete:!this.state.modaldelete})}
          handleDelete = {this.handleDeleteContact.bind(this)}
        />

      </Container>
    )
  }
}

const styles = StyleSheet.create({
  list:{
    marginRight:10,
    marginLeft:10,
  },
  icon:{
    marginTop:5
  }
})

const mapStateToProps = (state,props) =>{
    return{
        contacts : state.contacts
    }
} 

const mapDispatchToProps = dispatch =>{
    return{
      handleDeleteContact(index){
        dispatch(deleteContact(index))
      },
      handleBlockContact(index,block){       
        dispatch(editBlockContact(index,block))
      },
      
  
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(OptionsContacts);
