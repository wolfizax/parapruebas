import React,{Component} from 'react';
import {View,Text,StyleSheet} from 'react-native';
import {Container,Content} from 'native-base';
import HeadGoBack from '../../presentation/HeadGoBack';
import HeadAvatar from '../../presentation/HeadAvatar';

class InfInviteContacts extends Component{
	constructor(props){
        super(props);
        this.state = {
            user : this.props.navigation.getParam('item')
        }
    }
	render(){
		return(

			<Container>
			
				<HeadGoBack 
					title = "Contacts"
					goBack = {()=>this.props.navigation.goBack()}
				/>

				<HeadAvatar />

				<Content>

					<View style={styles.inf}>
						<Text style={styles.bold}>{this.state.user.displayName}</Text>
						<Text>{this.state.user.number}</Text>
					</View>

					<View style={styles.sent}>
						<Text>Request sent on</Text>
						<Text>date</Text>
					</View>




				</Content>

			</Container>

		)
	}
}

const styles = StyleSheet.create({
	inf:{
		alignItems:'center'
	},
	bold:{
		fontWeight :'bold',
		fontSize: 26
	},
	sent:{
		marginTop:15,
		alignItems:'center'
	}

})

export default InfInviteContacts;