import React, { Component } from 'react';
import LoggedView from '../index';
import { Container, Content, H1 } from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';
import {LeftMenu} from '../../actions/nav';
import HeaderView from '../../presentation/Header';

/** @extends Component */
export default class ExperienceWonderChatt extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        title:'Experience WonderChatt',
      });
      openMenu = () => {
        this.props.navigation.dispatch(LeftMenu());
    }
    render(){
        return(
            <Container>
                <HeaderView title='WonderChatt' leftComponent={{icon:'I-30-Menu',function:this.openMenu}} />
                <Content padder>
                    <Grid>
                        <Row>
                            <H1>
                                Out of service
                            </H1>
                        </Row>
                    </Grid>
                </Content>
            </Container>
        )
    }
}