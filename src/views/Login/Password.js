import React, {Component} from 'react';
import LoggedView from '..';
import { Image, TouchableOpacity } from 'react-native';
import { Container, Content, Form, H3, Input, Item, Text, Toast, View } from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';
import {Validation} from '../../utils/validation';
import { LoginForgotPassword } from '../../actions/nav';
import {userService} from '../../services/user';
import {LOGIN_SUCCESS,setFetching} from '../../actions/user';
import {Dashboard} from '../../actions/nav';
import Loading from '../../presentation/Loading';
import {connect} from 'react-redux';
import ButtonGradient from '../../presentation/ButtonGradient';
import Icon from '../../presentation/Icon/index';
import style from './style';

@connect(state => ({
    fetching: state.user.isFetching
}))
/** @extends Component */
export default class LoginPasswordView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({title:'Login', headerTintColor:'#FFF', headerRight:<Row></Row>, headerStyle:{backgroundColor:'#2B2B2B'},headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'},headerLeft: (
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{ paddingLeft: 12 }}>
         <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>
    )})
    constructor(props){
        super(props);
        this.state = {
            inputPassword:'',
        }
    }
    onSubmit = async () =>{
        if(Validation.requerid(this.state.inputPassword)){
            if(Validation.password(this.state.inputPassword,8)){
                let result;
                this.props.dispatch(setFetching(true));
                result = await userService.login(this.props.navigation.getParam('wonderID'),this.props.navigation.getParam('countryCode'),this.state.inputPassword);
                this.props.dispatch(setFetching(false));
                if(result != false){
                    if(result.isConnected == 1){
                        Toast.show({text:'Sorry, there is an active session on another device.',type:'danger',position:'bottom',duration:4000});
                    }else{
                        this.props.dispatch(LOGIN_SUCCESS(result));
                        this.props.navigation.dispatch(Dashboard());
                    }
                }else{
                    Toast.show({text:'Sorry, the password is incorrect.',type:'danger',position:'bottom',duration:4000});
                }
            }else{Toast.show({text:'Sorry, the password field must contain at least 8 characters.',type:'danger',position:'bottom',duration:4000})}
        }else{Toast.show({text:'Sorry, the password field is empty.',type:'danger',position:'bottom',duration:4000})}
    }
    render(){
        return(
            <Container>
                <Loading visible={this.props.fetching} />
                <Content padder>
                    <Grid>
                        <Row style={style.contentCenter}>
                            <Image resizeMode='contain' style={style.logoImage} source={require('../../assets/logo.png')} />
                        </Row>
                        <Row style={style.contentCenter}>
                            <H3 style={[style.title,style.textCenter,{fontSize:20}]}>
                                Validate Phone Number / Wonder ID
                            </H3>
                        </Row>
                        <Row style={[style.contentCenter,{marginVertical:20}]}>
                            <Text style={[style.fontRoboto,style.textCenter]}>{this.props.navigation.getParam('wonderID')}</Text>
                        </Row>
                        <Form>
                            <Text style={[style.fontBold,style.fontRoboto,style.textCenter]}>Enter your password</Text>
                            <Item regular>
                                <Input autoCapitalize='none' style={{borderColor:'#C7C7C7'}} secureTextEntry onChangeText={(text) => {this.setState({...this.state,inputPassword:text})}} placeholder='Password' />
                                <Icon name='DI-20' size={20} style={{color: '#C7C7C7'}} />
                            </Item>
                        </Form>
                        <Row>
                            <TouchableOpacity onPress={() => this.props.navigation.dispatch(LoginForgotPassword({mainRoute:this.props.navigation.getParam('mainRoute')}))}>
                                <Text style={style.textLink}>I forgot my password</Text>
                            </TouchableOpacity>
                        </Row>
                    </Grid>
                </Content>
                <View>
                    <ButtonGradient full title='Login' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />
                </View>
            </Container>
        )
    }
}