import React, {Component} from 'react';
import {Container,Content, H3, Text, Form, Item, Toast} from 'native-base';
import DeviceInfo from 'react-native-device-info';
import LoggedView from '../index';
import {TextInput,Image,TouchableOpacity,View} from 'react-native';
import {Grid,Row} from 'react-native-easy-grid';
import CountryPicker,{getAllCountries} from 'react-native-country-picker-modal';
import {Validation} from '../../utils/validation';
import {LoginPassword} from '../../actions/nav';
import {userService} from '../../services/user';
import {connect} from 'react-redux';
import Loading from '../../presentation/Loading';
import {setFetching} from '../../actions/user';
import ButtonGradient from '../../presentation/ButtonGradient';
import Icon from '../../presentation/Icon/index';
import style from './style';

@connect(state => ({
    fetching: state.user.isFetching
}))
/** @extends Component */
export default class LoginIdentificationView extends LoggedView{
    static navigationOptions =  ({ navigation }) => ({title:'Login', headerLeft:null, headerStyle:{backgroundColor:'#2B2B2B'},headerRight:<Row></Row>,headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'},headerLeft: (
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{ paddingLeft: 12 }}>
         <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>
    )})
    constructor(props){
        super(props)
        let userLocaleCountryCode = DeviceInfo.getDeviceCountry()
        const userCountryData = getAllCountries().filter(country => country.cca2 === userLocaleCountryCode).pop()
        let callingCode = null
        let cca2 = userLocaleCountryCode
        if (!cca2 || !userCountryData) {
            cca2 = 'US'
            callingCode = '1'
        } 
        else {
            callingCode = userCountryData.callingCode
        }
        this.state = {
            cca2,
            callingCode,
            userInput:'',
        }
    }
    onSubmit = async () =>{
        if(Validation.requerid(this.state.userInput)){
            let result;
            this.props.dispatch(setFetching(true));
            result = await userService.existsUser(this.state.userInput,this.state.callingCode);
            this.props.dispatch(setFetching(false));
            if(result){
                if(result.status){
                    this.props.navigation.dispatch(LoginPassword({wonderID:this.state.userInput,countryCode:this.state.callingCode,mainRoute:this.props.navigation.state.key}));
                }else{
                    Toast.show({text:'Sorry, your account is found deactivated.',duration:4000,position:'bottom',type:'danger'});
                }
            }else{
                if(result === false){
                    Toast.show({text:'Sorry, the entered Username not registered.',duration:4000,position:'bottom',type:'danger'});
                }else{
                    Toast.show({text:'Sorry, an error occurred in the validation process of your Username. Try again.',duration:4000,position:'bottom',type:'warning'});
                }
            }
        }else{
            Toast.show({text:'Sorry, the Username field is empty.',duration:4000,position:'bottom',type:'danger'})
        }
    }
    render(){
        return (
            <Container>
             <Loading visible={this.props.fetching} />
                <Content padder>
                <Grid>
                    <Row style={style.contentCenter}>
                        <Image resizeMode='contain' style={style.logoImage} source={require('../../assets/logo.png')} />
                    </Row>
                    <Row style={[style.contentCenter,{paddingHorizontal:40}]}>
                        <H3 style={[style.title,style.textCenter]} allowFontScaling={true}>
                            Login with your Phone number or Wonder ID
                        </H3>
                    </Row>
                    <Form style={{paddingHorizontal:10}}>
                        <Item style={{borderColor:'transparent'}}>
                                <Text style={[style.fontBold,style.fontRoboto]} >Region</Text>
                            </Item>
                            <Item>
                                <CountryPicker hideAlphabetFilter filterable  onChange={(value) => this.setState({...this.state,cca2:value.cca2,callingCode:value.callingCode})} cca2={this.state.cca2} showCallingCode closeable />
                            </Item>
                            <Item style={{borderColor:'transparent',marginTop:10}}>
                                <Text style={[style.fontBold,style.fontRoboto]}>User</Text>
                            </Item>
                            <Item>
                                <TextInput name={'phoneNumber'}
                                    placeholder="Phone Number / Wonder ID" 
                                    style={style.phoneText}
                                    autoCapitalize={'none'}
                                    autoCorrect={false} 
                                    returnKeyType='go'
                                    autoFocus
                                    onChangeText={(text) => this.setState({...this.state,userInput:text.replace(' ','')})}
                                    value={this.state.userInput}/>
                            </Item>
                        </Form>
                </Grid>
            </Content>
            <View>
                <ButtonGradient full title='Continue' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />
            </View>
     </Container>)
    }
}