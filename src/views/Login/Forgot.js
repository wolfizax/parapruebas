import React, { Component } from 'react';
import { Image, TextInput,TouchableOpacity,Platform } from 'react-native';
import { Container, Content, H3, Text, Form, Item, Toast} from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';
import LoggedView from '../index';
import {Validation} from '../../utils/validation';
import { userService } from '../../services/user';
import DeviceInfo from 'react-native-device-info';
import CountryPicker,{getAllCountries} from 'react-native-country-picker-modal';
import {LoginForgotValidationCode} from '../../actions/nav';
import {connect} from 'react-redux';
import {setFetching} from '../../actions/user';
import Loading from '../../presentation/Loading';
import ButtonGradient from '../../presentation/ButtonGradient';
import Icon from '../../presentation/Icon/index';
import style from './style';

@connect(state => ({
    fetching: state.user.isFetching
}))
/** @extends Component */
export default class LoginForgotPasswordView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({title:'Login', headerTintColor:'#FFF', headerRight:<Row></Row>, headerStyle:{backgroundColor:'#2B2B2B'},headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'},headerLeft: (
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{ paddingLeft: 12 }}>
         <Icon name='SI-8' size={20} style={{color: '#fff'}} />
        </TouchableOpacity>
    )})
    constructor(props){
        super(props);
        let userLocaleCountryCode = DeviceInfo.getDeviceCountry()
        const userCountryData = getAllCountries().filter(country => country.cca2 === userLocaleCountryCode).pop()
        let callingCode = null
        let cca2 = userLocaleCountryCode
        if (!cca2 || !userCountryData) {
            cca2 = 'US'
            callingCode = '1'
        } 
        else {
            callingCode = userCountryData.callingCode
        }
        this.state ={
            cca2,
            callingCode,
            numberPhone:''
        };
    }
    onSubmit = async () =>{
        if(Validation.isNumerPhone(this.state.numberPhone)){
            if(Validation.minLength(this.state.numberPhone,10)){
                let result;
                this.props.dispatch(setFetching(true));
                result = await userService.existNumberPhone(this.state.numberPhone,this.state.callingCode);
                this.props.dispatch(setFetching(false));
                if(result){
                    this.props.dispatch(setFetching(true));
                    result = await userService.sendCodeResetPassword(this.state.numberPhone,this.state.callingCode);
                    this.props.dispatch(setFetching(false));
                    if(result){
                        this.props.navigation.dispatch(LoginForgotValidationCode({countryCode:this.state.callingCode,numberPhone:this.state.numberPhone,codeValidation:result,mainRoute:this.props.navigation.getParam('mainRoute')}));
                    }else{
                        Toast.show({
                            text:'Sorry, a problem occurred in the sending of your recovery code. Try again.',
                            duration:4000,
                            type:'warning',
                            position:'bottom'
                        }); 
                    }
                }else{
                    Toast.show({
                        text:'Sorry, the number phone you entered is not registered.',
                        duration:4000,
                        type:'danger',
                        position:'bottom'
                    });
                }
            }else{
                Toast.show({
                    text:'Your telephone number is invalid format. It must have a minimum of 10 digits.',
                    duration:4000,
                    type:'danger',
                    position:'bottom'
                });
            }
        }else{
            Toast.show({
                text:'Sorry, the phone number entered does not have a correct format. Try again.',
                duration:4000,
                type:'danger',
                position:'bottom'
            });
        }
    }
    render(){
        return(
            <Container>
            <Loading visible={this.props.fetching} />
            <Content padder>
                <Grid>
                    <Row style={style.contentCenter}>
                        <Image resizeMode='contain' style={style.logoImage} source={require('../../assets/logo.png')} />
                    </Row>
                    <Row style={style.contentCenter}>
                        <H3 style={[style.title,style.textCenter]}>
                            To recovery your password, enter your Phone Number
                        </H3>
                    </Row>
                    <Form style={{marginVertical:20}}>
                        <Item style={{borderColor:'transparent'}}>
                            <Text style={[style.fontBold,style.fontRoboto]}>Region</Text>
                        </Item>
                        <Item>
                            <CountryPicker hideAlphabetFilter filterable  onChange={(value) => this.setState({...this.state,cca2:value.cca2,callingCode:value.callingCode})} cca2={this.state.cca2} showCallingCode closeable />
                        </Item>
                        <Item style={{borderColor:'transparent',marginTop:10}}>
                            <Text style={[style.fontBold,style.fontRoboto]}>Phone Number</Text>
                        </Item>
                        <Item>
                            <TextInput 
                                value={this.state.numberPhone} 
                                style={style.inputPhoneText} 
                                onChangeText={(text) => {let newText = text.replace(/^0+/, '');this.setState({...this.state,numberPhone: newText.replace(/[^0-9]/g, '')})}} 
                                maxLength={11} 
                                returnKeyType='go'
                                autoFocus
                                keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'phone-pad'}
                                />
                        </Item>
                    </Form>
                    <Row style={style.contentRight}>
                        <ButtonGradient title='Send' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />
                    </Row>
                </Grid>
            </Content>
        </Container>
        )
    }
}