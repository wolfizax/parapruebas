import React,{Component} from 'react';
import {Container,Content,H1,Form, Item, Label, Input, Toast, View} from 'native-base';
import {TouchableOpacity, Platform, Image} from 'react-native';
import {Grid,Row} from 'react-native-easy-grid';
import style from './style';
import SmsListener from 'react-native-android-sms-listener'
import {LoginForgotChangePassword} from '../../actions/nav';
import ButtonGradient from '../../presentation/ButtonGradient';
import Icon from '../../presentation/Icon/index';
import LoggedView from '../index';

/** @extends Component */
export default class LoginForgotValidationCode extends LoggedView{
    static navigationOptions = ({ navigation }) => ({
        title:'Login', 
        headerTintColor:'#FFF',
        headerStyle:{backgroundColor:'#2B2B2B'},
        headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'},
        headerLeft: <Row></Row>,
        headerRight: (
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              style={{ paddingRight: 12 }}>
              <Icon name="SI-6" size={20} style={{color:'white'}} />
            </TouchableOpacity>
        )
      });
    constructor(props){
        super(props);
        this.smsSubscription = null;
        this.state = {
            inputCode:''
        };
    }
    verifyCodeListener = (bodyMessage) => {
        const verificationCodeRegEx = /his verification code is ([\d]{4})/;
        if(verificationCodeRegEx.test(bodyMessage)){
            let messageCode = bodyMessage.match(verificationCodeRegEx)[1];
            if(this.props.navigation.getParam('codeValidation') == messageCode){
                this.props.navigation.dispatch(LoginForgotChangePassword({countryCode:this.props.navigation.getParam('countryCode'),numberPhone:this.props.navigation.getParam('numberPhone'),backRoute:this.props.navigation.getParam('mainRoute')}));
            }else{
                Toast.show({text:'The verification code is incorrect, please try again.',buttonText:'Ok',type:"danger",position:'bottom',duration:4000});
            }
        }
    }
    onSubmit = () => {
        if(this.props.navigation.getParam('codeValidation') == this.state.inputCode){
            this.props.navigation.dispatch(LoginForgotChangePassword({countryCode:this.props.navigation.getParam('countryCode'),numberPhone:this.props.navigation.getParam('numberPhone'),backRoute:this.props.navigation.getParam('mainRoute')}));
        }else{
            Toast.show({text:'The verification code is incorrect, please try again.',buttonText:'Ok',type:"danger",position:'bottom',duration:4000});
        }
    }
    onChanged = (text) => {
        this.setState({...this.state,inputCode: text.replace(/[^0-9]/g, '')})
    }
    componentDidMount(){
        this.smsSubscription = SmsListener.addListener(message => {
            this.verifyCodeListener(message.body);
        });
    }
    componentWillUnmount(){
        this.smsSubscription.remove();
    }
    render(){
        return(<Container>
            <Content padder>
                <Grid>
                    <Row style={style.contentCenter}>
                        <Image resizeMode='contain' style={style.logoImage} source={require('../../assets/logo.png')} />
                    </Row>
                    <Row style={[style.contentCenter,{marginVertical:20}]}>
                        <H1 style={[style.title,{paddingHorizontal:30},style.textCenter]}>Enter your verification code to change your password</H1>
                    </Row>
                    <Form style={{marginVertical:20}}>
                        <Item stackedLabel>
                            <Label style={[style.fontBold,style.fontRoboto]}>Code verification</Label>
                            <Input 
                                    name={'CodeValidation'}
                                    autoCapitalize={'none'}
                                    autoCorrect={false} 
                                    returnKeyType='go'
                                    autoFocus
                                    keyboardType={Platform.OS === 'ios' ? 'number-pad' : 'phone-pad'}
                                    onChangeText={this.onChanged}
                                    maxLength={4}
                                    value={this.state.inputCode}
                                    />
                        </Item>
                    </Form>
                </Grid>
            </Content>
            <View>
                <ButtonGradient full title='Validate code' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />
            </View>
        </Container>);
    }
}