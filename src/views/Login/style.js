import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    contentCenter:{
        justifyContent:'center',
        margin:10
    },
    logoImage:{
        height:230,
        width:230
    },
    title:{
        fontFamily:'Roboto',
        fontWeight:'bold',
        fontSize:22
    },
    fontRoboto:{
        fontFamily:'Roboto'
    },
    contentRight:{
        justifyContent:'flex-end',
        margin:10
    },
    textCenter:{
        textAlign:'center'
    },
    fontBold:{
        fontWeight:'bold'
    },
    fontItalic:{
        fontStyle:'italic'
    },
    textLink:{
        color:'#30BFFF'
    },
    inputPhoneText: {
        borderBottomWidth:1,
        padding:0,
        margin: 0,
        flex: 1,
        fontSize: 20,
        color: '#2B2B2B',
        borderBottomColor:'#bcbcbc'
    }
});