import React, { Component } from 'react';
import { Image,TouchableOpacity } from 'react-native';
import { Container, Content, H3, Form, Item, Input, Label, Toast} from 'native-base';
import { Grid, Row } from 'react-native-easy-grid';
import LoggedView from '../index';
import {Validation} from '../../utils/validation';
import { userService } from '../../services/user';
import {setFetching} from '../../actions/user';
import {connect} from 'react-redux';
import Loading from '../../presentation/Loading';
import ButtonGradient from '../../presentation/ButtonGradient';
import Icon from '../../presentation/Icon/index';
import style from './style';

@connect(state => ({
    fetching:state.user.isFetching
}))
/** @extends Component */
export default class LoginForgotChangePasswordView extends LoggedView{
    static navigationOptions = ({ navigation }) => ({title:'Login', headerTintColor:'#FFF', headerLeft:<Row></Row>, headerStyle:{backgroundColor:'#2B2B2B'},headerTitleStyle: {flex: 1, textAlign: 'center',color:'white'}, headerRight: (
        <TouchableOpacity
          onPress={() => navigation.popToTop()}
          style={{ paddingRight: 12 }}>
           <Icon name="SI-6" size={20} style={{color:'white'}} />
        </TouchableOpacity>
    )})
    constructor(props){
        super(props);
        this.state ={
            password:''
        };
    }
    onSubmit = async () => {
        if(Validation.password(this.state.password,8)){
            let result;
            this.props.dispatch(setFetching(true));
            result = await userService.resetPassword(this.props.navigation.getParam('numberPhone'),this.props.navigation.getParam('countryCode'),this.state.password);
            this.props.dispatch(setFetching(false));
            if(result){
                Toast.show({text:'Congratulations, your password has been successfully changed.',type:'success',duration:4000,position:'bottom',onClose:(() => this.props.navigation.popToTop())});
            }
            else{
                Toast.show({text:'Sorry, an error occurred in the password change. Try again.',type:'danger',duration:4000,position:'bottom'});
            }
        }else{
            Toast.show({
                text:'Sorry, the new password must contain at least 8 characters.',
                duration:4000,
                position:'bottom',
                type:'danger'
            });
        }
    }
    render(){
        return(
            <Container>
            <Loading visible={this.props.fetching} />
            <Content padder>
                <Grid>
                    <Row style={style.contentCenter}>
                        <Image resizeMode='contain' style={style.logoImage} source={require('../../assets/logo.png')} />
                    </Row>
                    <Row style={style.contentCenter}>
                        <H3 style={[style.fontBold,style.fontRoboto,style.textCenter]}>
                            Please enter your new password.
                        </H3>
                    </Row>
                    <Form style={{marginVertical:20}}>
                        <Label style={[style.fontBold,style.fontRoboto,{paddingLeft:20}]}>New password</Label>
                        <Item>
                            <Input secureTextEntry autoCapitalize='none' value={this.state.password} onChangeText={text => this.setState({...this.state,password:text})} />
                        </Item>
                    </Form>
                    <Row style={style.contentRight}>
                        <ButtonGradient full title='Change password' gradientColors={['#30bfff', '#0dffc7']} gradientStart={{x: 0, y: 0}} gradientEnd={{x: 0.9, y: 0}} onSubmit={this.onSubmit} />
                    </Row>
                </Grid>
            </Content>
        </Container>
        )
    }
}