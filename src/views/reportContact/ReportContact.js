import React,{Component} from 'react';
import {View,Text} from 'react-native';
import {Container,Content,Textarea} from 'native-base';
import HeadGoBack from '../../presentation/HeadGoBack';
import HeadAvatar from '../../presentation/HeadAvatar';
import ButtonFooter from '../../presentation/ButtonFooter';

class ReportContact extends Component{

	constructor(props){
		super(props);
		this.state = {
			text : '',
			user : this.props.navigation.getParam('item')
		}
	}

	handleSend (){
		this.props.navigation.goBack();
	}

	render(){
		return(
			<Container>
				<HeadGoBack 
          			title ="Contact"
          			goBack = {()=>this.props.navigation.goBack()}
        		/>
        		<HeadAvatar />

        		<Content>

        			<View style={{alignItems:'center'}}>
						<Text style={{fontWeight:'bold',fontSize:26}}>{this.state.user.displayName}</Text>
						<Text>profession</Text>
						<Text>{this.state.user.number}</Text>
					</View>
        			<View style={{marginLeft:10,marginRight:10}}>
        				<View><Text>Report Reason</Text></View>
        				<Textarea 
        					style={{borderRadius:3}}
        					rowSpan={5} 
        					bordered 
        					placeholder="Report Reason" 
        					value ={this.state.text}
        					onChangeText={(text)=>this.setState({text})} 
        				/>
        			</View>


        		</Content>

        		{this.state.text != '' && <ButtonFooter text = 'Send' handleSucess = {this.handleSend.bind(this)} />}

			</Container>
		)
	}
}

export default ReportContact;