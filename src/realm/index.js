import Realm from 'realm';

//Models
import UserModel from './models/user';
import {SettingModel} from './models/setting';

const Database = new Realm({
    schema: [UserModel,SettingModel]
});

export default Database;