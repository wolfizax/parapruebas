import Database from '../index';

const SettingModel = {
    name: 'Settings',
    primaryKey:'key',
    properties: {
        key:'string',
        value:'string',
        updateAt:'date'
    }
};

class Setting{
    set(key,value){
        Database.write(() => {
            Database.create('Settings',{key,value:value.toString()},true);
        });
    }
    delete(key){
        Database.write(() => {
            const data = Database.objects('Settings').filtered('key == $0',key);
            if(data && data.length > 0){
                let setting = data[0];
                Database.delete(setting);
            }
        });        
    }
    get(key){
        const data = Database.objects('Settings').filtered('key == $0',key);
        if(data && data.length > 0){ return data[0].value; }
        return '';
    }
    getAll(){
        const data = Database.objects('Settings');
        const settings = {};
        if(data.isEmpty()){
            return null;
        }else{
            data.map((object) => {
                Object.assign(settings,settings,{[object.key]:object.value});
            });
            return settings;
        }
    }
}
export {SettingModel,Setting};