const UserModel = {
    name: 'User',
    primaryKey: 'id',
    properties: {
        id: {type:'int',default:1},
        _id: 'string',
        idWonder: 'string',
        name: 'string',
        email: 'string',
        role: 'string',
        countryCode: 'string',
        numberPhone: {type:'string',optional:true},
        idAvatar:{type:'string',default:'assets/avatar.png'},
        sex: 'string',
        dateBirth: 'string',
        token: 'string',
        skills: 'string',
        profession: {type:'string',optional:true}
    }
};

export default UserModel;