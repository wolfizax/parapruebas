import React, {Component} from 'react';
import { Provider } from 'react-redux';
import {AppNavigator} from './navigator/';
import store from './utils/store';
import { Root } from "native-base";
import OfflineNetwork from './presentation/Offline';

export default class WonderChatt extends Component {

    render() {
        return (
        <Provider store={store}>
            <Root>
            <OfflineNetwork />
            <AppNavigator />
            </Root>
        </Provider>
        );
    }
}