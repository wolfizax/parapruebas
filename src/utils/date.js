import moment from "moment";
let today = moment();

const time =  (date)=>{
    
	let newDate = moment(date);
	let diff = today.diff(newDate,'days')

    if(diff == 0){
    	return moment(date).format('LT');
    }else if(diff ==1){
    	return 'yesterday';
    }else if(diff>1 && diff<7){
    	return moment(date).format('dddd').substring(0,3);
	}else{
    	return moment(date).format("DD/MM/YY");
    }
}

export {time};