export default function convert(time){
    let hours = Math.floor( time / 3600 );  
    let minutes = Math.floor( (time % 3600) / 60 );
    let seconds = Math.floor(time % 60);
    
    minutes = minutes < 10 ? 0 + minutes : minutes;
    seconds = seconds < 10 ? 0 + seconds : seconds;
    let result = {
        hours,
        minutes,
        seconds,
        text:`0${hours}:0${minutes}:0${seconds}`
    }
    return result;
}