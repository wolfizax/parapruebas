import validate from 'validate.js';

function name(value){
	if(validate.single(value,{length:{minimum:4},presence:true}) === undefined){return true;}else{return false}
}
function wonderID(value){
	if(validate.single(value,{length:{minimum:4},presence:true}) === undefined){return true;}else{return false;}
}
function email(value){
	if(validate.single(value,{email:true,presence:true}) === undefined){return true;}else{return false;}
}
function password(value,minimum){
	if(validate.single(value,{length:{minimum},presence:true}) === undefined){return true;}else{return false;}
}
function requerid(value){
	if(validate.single(value,{presence:true}) === undefined){return true;}else{return false;}
}
function isNumerPhone(value){
	if(validate.single(value,{format:{pattern:"[0-9]+"},presence:true}) === undefined){return true;}else{return false;}
}
function minLength(value,length){
	if(validate.single(value,{length:{minimum:length},presence:true}) === undefined){return true;}else{return false;}
}
function maxLength(value,maximun){
	if(validate.single(value,{length:{maximun},presence:true}) === undefined){return true;}else{return false;}
}
function compare(value1,value2){
	if(validate({ value1,value2 }, { value2: { equality: "value1"}}) === undefined){return true;}else{return false;}
}

export const Validation = {
	name,
	wonderID,
	email,
	password,
	requerid,
	minLength,
	maxLength,
	isNumerPhone,
	compare
};