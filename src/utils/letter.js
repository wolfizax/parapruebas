const letter =  (displayName)=>{

    return displayName.split('')[0];
        
}

const capitalize = (string)=>{

	let array = string.split(" ");
	let stringArray = [];

	for(let i=0;i<array.length;i++){
		stringArray.push(array[i].charAt(0).toUpperCase() + array[i].slice(1));
	}

	return stringArray.join(' ');
}

const findAlphabetic = (contacts,category)=>{
	for(let i = 0;i<contacts.length;i++){
		if(contacts[i].category == category || category == 'all') return true;
	}
	return false;

}

const alphabet = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']

export {letter,capitalize,alphabet,findAlphabetic};