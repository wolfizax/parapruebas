import Database from '../realm/index';
import moment from 'moment';

export class localDatabase {
    createOrUpate(model,key,value){
        let result = Database.objects(model).filtered('key == $0',key);
        if(result && result.length == 0){
            Database.write(() => {
                Database.create(model,{key,value:value,updateAt:moment(new Date()).format('YYYY-MM-DD')});
            });
        }else{
            Database.write(() => {
                Database.create(model,{key,value:value,updateAt:moment(new Date()).format('YYYY-MM-DD')},true);
            })
        }
    }
    create(model,key,value){
        Database.write(() => {
            Database.create(model,{key,value:value,updateAt:moment(new Date()).format('YYYY-MM-DD')});
        });
    }
    update(model,key,value){
        Database.write(() => {
            Database.create(model,{key,value,updateAt:moment(new Date()).format('YYYY-MM-DD')},true);
        });
    }
}