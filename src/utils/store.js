import { createStore as reduxCreateStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import Reactotron from '../ReactotronConfig';
import AppReducers from '../reducers/index';
import {navigationMiddleware} from '../navigator/index';
import applyAppStateListener from 'redux-enhancer-react-native-appstate';
const createStore = __DEV__ ? Reactotron.createStore : reduxCreateStore;
let enhancers;

if (__DEV__) {
	enhancers = compose(
		applyAppStateListener(),
		applyMiddleware(thunk,navigationMiddleware),
	);
} else {
	enhancers = compose(
		applyAppStateListener(),
		applyMiddleware(thunk,navigationMiddleware),
	);
}
const store = createStore(AppReducers, enhancers);
export default store;