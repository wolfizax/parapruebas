import {actionTypeSetting} from './action-types';
import { localDatabase } from '../utils/localDatabase';
import {Setting} from '../realm/models/setting';

export function LOAD_SETTINGS(){
    return(dispatch) => {
        let settings = Setting.prototype.getAll();
        dispatch({type:actionTypeSetting.LOAD_SETTINGS,data:settings});
    }
}
export function ADD_USER_BLOCKED_LIST(data){
    return (dispatch) => {
        localDatabase.prototype.createOrUpate('Settings','blockedUsersList',JSON.stringify(data));
        dispatch({type:actionTypeSetting.ADD_USER_BLOCKED_LIST,data});
    }
}
export function REMOVE_USER_BLOCKED_LIST(data){
    return (dispatch) => {
        Setting.prototype.set('blockedUsersList',JSON.stringify(data));
        dispatch({type:actionTypeSetting.REMOVE_USER_BLOCKED_LIST,data});
    }
}
export function DELETE_USER_BLOCKED_LIST(){
    return (dispatch) => {
        Setting.prototype.delete('blockedUsersList');
        dispatch({type:actionTypeSetting.DELETE_USER_BLOCKED_LIST});
    }
}
export function ADD_USER_EMERGENCY_LIST(data){
    return (dispatch) => {
        localDatabase.prototype.createOrUpate('Settings','emergencyUsersList',JSON.stringify(data));
        dispatch({type:actionTypeSetting.ADD_USER_EMERGENCY_LIST,data});
    }
}
export function REMOVE_USER_EMERGENCY_LIST(data){
    return (dispatch) => {
        Setting.prototype.set('emergencyUsersList',JSON.stringify(data));
        dispatch({type:actionTypeSetting.REMOVE_USER_EMERGENCY_LIST,data});
    }
}
export function DELETE_USER_EMERGENCY_LIST(){
    return (dispatch) => {
        Setting.prototype.delete('emergencyUsersList');
        dispatch({type:actionTypeSetting.DELETE_USER_EMERGENCY_LIST});
    }
}
export function CHANGE_MESSAGE_NOTIFICATION_TONE(data){
    return (dispatch) => {
        localDatabase.prototype.createOrUpate('Settings','messageTone',data);
        dispatch({type: actionTypeSetting.CHANGE_MESSAGE_NOTIFICATION_TONE,data});
    }
}
export function CHANGE_MESSAGE_VIBRATION(data){
    return (dispatch) => {
        localDatabase.prototype.createOrUpate('Settings','messageVibration',data);
        dispatch({type: actionTypeSetting.CHANGE_MESSAGE_VIBRATION,data});
    }
}
export function CHANGE_GROUP_NOTIFICATION_TONE(data){
    return (dispatch) => {
        localDatabase.prototype.createOrUpate('Settings','groupTone',data);
        dispatch({type: actionTypeSetting.CHANGE_GROUP_NOTIFICATION_TONE,data});
    }
}
export function CHANGE_GROUP_VIBRATION(data){
    return (dispatch) => {
        localDatabase.prototype.createOrUpate('Settings','groupVibration',data);
        dispatch({type: actionTypeSetting.CHANGE_GROUP_VIBRATION,data});
    }
}
export function CHANGE_CALL_NOTIFICATION_TONE(data){
    return (dispatch) => {
        localDatabase.prototype.createOrUpate('Settings','callTone',data);
        dispatch({type: actionTypeSetting.CHANGE_CALL_NOTIFICATION_TONE,data});
    }
}
export function CHANGE_CALL_VIBRATION(data){
    return (dispatch) => {
        localDatabase.prototype.createOrUpate('Settings','callVibration',data);
        dispatch({type: actionTypeSetting.CHANGE_CALL_VIBRATION,data});
    }
}
export function CHANGE_STORAGE_OPTIONS(data){
    return(dispatch) => {
        localDatabase.prototype.createOrUpate('Settings','storage',JSON.stringify(data));
        dispatch({type:actionTypeSetting.CHANGE_STORAGE_OPTIONS,data});
    }
}