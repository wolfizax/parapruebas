import {actionTypeRoutes} from './action-types';

export function Main(params={}){
    return {
        type:actionTypeRoutes.Main,
        params
    };
}
export function NewUser(params={}){
    return{
        type:actionTypeRoutes.NewUser,
        params
    };
}
export function NewUserValidationCode(params={}){
    return{
        type:actionTypeRoutes.NewUserValidationCode,
        params
    };
}
export function NewUserCompletePerfil(params={}){
    return{
        type:actionTypeRoutes.NewUserCompletePerfil,
        params
    };
}
export function Login(params={}){
    return{
        type:actionTypeRoutes.Login,
        params
    };
}
export function Logout(params={}){
    return{
        type:actionTypeRoutes.Logout,
        params
    };
}
export function LoginPassword(params={}){
    return{
        type:actionTypeRoutes.LoginPassword,
        params
    };
}
export function LoginForgotPassword(params={}){
    return{
        type:actionTypeRoutes.LoginForgotPassword,
        params
    };
}
export function LoginForgotValidationCode(params={}){
    return{
        type:actionTypeRoutes.LoginForgotValidationCode,
        params
    };
}
export function LoginForgotChangePassword(params={}){
    return{
        type:actionTypeRoutes.LoginForgotChangePassword,
        params
    };
}
export function Dashboard(params={}){
    return{
        type:actionTypeRoutes.Dashboard,
        params
    };
}
export function LeftMenu(params={}){
    return{
        type:actionTypeRoutes.LeftMenu,
        params
    };
}
export function UserSetting(params={}){
    return{
        type:actionTypeRoutes.UserSetting,
        params
    };
}
export function PrivacyMenu(params={}){
    return{
        type:actionTypeRoutes.PrivacyMenu,
        params
    }
}
export function PrivacySetting(params={}){
    return{
        type:actionTypeRoutes.PrivacySetting,
        params
    };
}
export function BlockedContacts(params={}){
    return{
        type:actionTypeRoutes.BlockedContacts,
        params
    };
}
export function SecurityMenu(params={}){
    return{
        type:actionTypeRoutes.SecurityMenu,
        params
    };
}
export function EmergencyContacts(params={}){
    return{
        type:actionTypeRoutes.EmergencyContacts,
        params
    };
}
export function AssociateEmail(params={}){
    return{
        type:actionTypeRoutes.AssociateEmail,
        params
    };
}
export function ChangeNumber(params={}){
    return{
        type:actionTypeRoutes.ChangeNumber,
        params
    };
}
export function ChangeNumberForm(params={}){
    return{
        type:actionTypeRoutes.ChangeNumberForm,
        params
    };
}
export function ChangeNumberValidationCode(params={}){
    return{
        type:actionTypeRoutes.ChangeNumberValidationCode,
        params
    };
}
export function SoundSettings(params={}){
    return{
        type:actionTypeRoutes.SoundSettings,
        params
    };
}
export function StorageSettings(params={}){
    return{
        type:actionTypeRoutes.StorageSettings,
        params
    };
}
export function StorageSettingsUseOfData(params={}){
    return{
        type:actionTypeRoutes.StorageSettingsUseOfData,
        params
    };
}
export function StorageSettingsUseOfStorage(params={}){
    return{
        type:actionTypeRoutes.StorageSettingsUseOfStorage,
        params
    };
}
export function HelpMenu(params={}){
    return{
        type:actionTypeRoutes.HelpMenu,
        params
    };
}
export function Profile(params={}){
    return{
        type:actionTypeRoutes.Profile,
        params
    };
}
export function ProfilePersonalInformation(params={}){
    return{
        type:actionTypeRoutes.ProfilePersonalInformation,
        params
    };
}
export function ProfileProfessionalInformation(params={}){
    return{
        type:actionTypeRoutes.ProfileProfessionalInformation,
        params
    };
}
export function MyWalletPoints(params={}){
    return{
        type:actionTypeRoutes.MyWalletPoints,
        params
    };
}
export function ChangePassword(params={}){
    return{
        type:actionTypeRoutes.ChangePassword,
        params
    };
}
export function Chat(params={}){
    return{
        type:actionTypeRoutes.Chat,
        params
    };
}