import {actionTypeApp} from './action-types';
import {RESTORE_SESSION} from './user';
import Database from '../realm/index';
import {LOAD_SETTINGS} from './setting';

export function appInit(){
    return (dispatch) => {
        dispatch(LOAD_SETTINGS());
        try{
            let user = Database.objects('User');
            if(!user.isEmpty()){
                if(user[0].token != ''){
                    dispatch(RESTORE_SESSION(user[0]));
                    dispatch(appReady('isLogin'));
                }else{
                    dispatch(appReady('notLogin'));
                }
            }else{
                dispatch(appReady('notLogin'));
            }
        }
        catch(e){
            dispatch(appReady('notLogin'));
        }
    };
}

export function appReady(state){
    return {
        type: actionTypeApp.READY,
        state
    };
}