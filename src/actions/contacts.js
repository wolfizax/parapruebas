const editContacts = (contacts) => {
    
    return {
        type: 'EDIT_CONTACTS',
        contacts
    }
};

const editContactsCategory = (user) =>{
	 return {
        type: 'EDIT_CONTACTS_CATEGORY',
        user
    }
}

const editBlockContact = (index,block)=>{
	return {
		type : 'EDIT_BLOCK_CONTACTS',
		index,
		block
	}
}

const deleteContact = (index)=>{
    return {
        type:'DELETE_CONTACT',
        index
    }
}



export {editContacts,editContactsCategory,editBlockContact,deleteContact}