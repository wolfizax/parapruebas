const addMembers = (members) => {
    
    return {
        type: 'ADD_MEMBERS',
        members
    }
};


export {addMembers}