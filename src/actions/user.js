import {actionTypeUser} from './action-types';
import Database from '../realm/index';

export function setFetching(status){
    return{
        type:actionTypeUser.isFetching,
        status
    }
}
export function LOGIN_SUCCESS(data){
    return (dispatch) => {
        Database.write(() => {
            let user = Database.objects('User');
            if(user.isEmpty()){
                Database.create('User',{
                    id:1,
                    ...data,
                    skills:JSON.stringify(data.skills)
                });
            }else{
                Database.create('User',{id:1,...data,skills:JSON.stringify(data.skills)},true);
            }
        });
        dispatch({type:actionTypeUser.LOGIN_SUCCESS,data});
    }
}
export function LOGIN_LOGOUT(){
    return (dispatch) => {
        Database.write(() => {
            let user = Database.objects('User')[0];
            user.token = '';
        });
        dispatch({type:actionTypeUser.LOGIN_LOGOUT});
    };
}
export function RESTORE_SESSION(data){
    return (dispatch) => {
        dispatch({type:actionTypeUser.RESTORE_SESSION,data});
    }
}
export function SAVE_PERSONAL_INFORMATION(data){
    return (dispatch) => {
       Database.write(() => {
            let user = Database.objects('User')[0];
            user.name = data.name;
            user.sex = data.sex;
            user.dateBirth = data.dateBirth;
        });
        dispatch({type:actionTypeUser.SAVE_PERSONAL_INFORMATION,data});
    }
}
export function UPDATE_AVATAR(data){
    return (dispatch) => {
        Database.write(() => {
            let user = Database.objects('User')[0];
            user.idAvatar = data;
        });
        dispatch({type:actionTypeUser.UPDATE_AVATAR,data});
    }
}
export function UPDATE_SKILLS_LIST(data){
    return (dispatch) => {
        Database.write(() => {
            let user = Database.objects('User')[0];
            user.skills = JSON.stringify(data);
        });
        dispatch({type:actionTypeUser.UPDATE_SKILLS_LIST,data});
    }
}
export function DELETE_SKILLS_LIST(){
    return (dispatch) => {
        Database.write(() => {
            let user = Database.objects('User')[0];
            user.skills = ''
        });
        dispatch({type:actionTypeUser.DELETE_SKILLS_LIST});
    }
}
export function UPDATE_PROFESSION(data){
    return (dispatch) => {
        Database.write(() => {
            let user = Database.objects('User')[0];
            user.profession = data
        });
        dispatch({type:actionTypeUser.UPDATE_PROFESSION,data});
    }
}