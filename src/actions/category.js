const addCategory = (text) => {
    
    return {
        type: 'ADD_CATEGORY',
        text
    }
};

const deleteCategory = (index) => {
    
    return {
        type: 'DELETE_CATEGORY',
        index
    }
};

const editCategory = (text,category) => {
    
    return {
        type: 'EDIT_CATEGORY',
        text,
        category
    }
};



export {addCategory,deleteCategory,editCategory}