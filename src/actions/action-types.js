export const actionTypeApp = {
    INIT: 'APP_INIT',
    READY: 'APP_READY'
};
export const actionTypeRoutes = {
    Main: 'Main',
    NewUser: 'NewUser',
    NewUserValidationCode: 'NewUserValidationCode',
    NewUserCompletePerfil: 'NewUserCompletePerfil',
    Login: 'Login',
    Logout: 'Logout',
    LoginPassword: 'LoginPassword',
    LoginForgotPassword: 'LoginForgotPassword',
    LoginForgotValidationCode: 'LoginForgotValidationCode',
    LoginForgotChangePassword: 'LoginForgotChangePassword',
    Dashboard: 'Dashboard',
    LeftMenu: 'LeftMenu',
    UserSetting: 'UserSetting',
    PrivacyMenu: 'PrivacyMenu',
    PrivacySetting: 'PrivacySetting',
    BlockedContacts: 'BlockedContacts',
    SecurityMenu: 'SecurityMenu',
    EmergencyContacts: 'EmergencyContacts',
    AssociateEmail: 'AssociateEmail',
    ChangeNumber: 'ChangeNumber',
    ChangeNumberForm: 'ChangeNumberForm',
    ChangeNumberValidationCode: 'ChangeNumberValidationCode',
    SoundSettings: 'SoundSettings',
    StorageSettings: 'StorageSettings',
    StorageSettingsUseOfData: 'StorageSettinsUseofdata',
    StorageSettingsUseOfStorage: 'StorageSettingsUseOfStorage',
    HelpMenu: 'HelpMenu',
    Profile: 'Profile',
    ProfilePersonalInformation: 'ProfilePersonalInformation',
    ProfileProfessionalInformation: 'ProfileProfessionalInformation',
    MyWalletPoints: 'MyWalletPoins',
    ChangePassword: 'ChangePassword',
    Chat:'Chat'
};
export const actionTypeUser = {
    isFetching:'USER_IS_FETCHING',
    LOGIN_SUCCESS: 'LOGIN_SUCCESS',
    LOGIN_LOGOUT: 'LOGIN_LOGOUT',
    RESTORE_SESSION: 'RESTORE_SESSION',
    SAVE_PERSONAL_INFORMATION: 'SAVE_PERSONAL_INFORMATION',
    UPDATE_AVATAR: 'UPDATE_AVATAR',
    UPDATE_SKILLS_LIST: 'UPDATE_SKILLS_LIST',
    DELETE_SKILLS_LIST: 'DELETE_SKILLS_LIST',
    UPDATE_PROFESSION: 'UPDATE_PROFESSION'
};

export const actionTypeSetting = {
    LOAD_SETTINGS: 'LOAD_SETTINGS',
    ADD_USER_BLOCKED_LIST:'ADD_USER_BLOCKED_LIST',
    REMOVE_USER_BLOCKED_LIST:'REMOVE_USER_BLOCKED_LIST',
    DELETE_USER_BLOCKED_LIST:'DELETE_USER_BLOCKED_LIST',
    ADD_USER_EMERGENCY_LIST:'ADD_USER_EMERGENCY_LIST',
    REMOVE_USER_EMERGENCY_LIST:'REMOVE_USER_EMERGENCY_LIST',
    DELETE_USER_EMERGENCY_LIST:'DELETE_USER_EMERGENCY_LIST',
    CHANGE_MESSAGE_NOTIFICATION_TONE:'CHANGE_MESSAGE_NOTIFICATION_TONE',
    CHANGE_MESSAGE_VIBRATION:'CHANGE_MESSAGE_VIBRATION',
    CHANGE_GROUP_NOTIFICATION_TONE:'CHANGE_GROUP_NOTIFICATION_TONE',
    CHANGE_GROUP_VIBRATION:'CHANGE_GROUP_VIBRATION',
    CHANGE_CALL_NOTIFICATION_TONE:'CHANGE_CALL_NOTIFICATION_TONE',
    CHANGE_CALL_VIBRATION:'CHANGE_CALL_VIBRATION',
    CHANGE_STORAGE_OPTIONS:'CHANGE_STORAGE_OPTIONS'
};