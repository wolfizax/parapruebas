import axios from 'axios';
import ConfigService from './config';
const endpointBase = `${ConfigService.BASE_URL}/file`;

function view(file){
    return `${endpointBase}/view?file=${file}`;
}
export const fileService = {
    view
}