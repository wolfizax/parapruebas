import axios from 'axios';
import ConfigService from './config';
const endpointBase = `${ConfigService.BASE_URL}/category-friendship`;

function addEmergencyContact(id,token,role,data){
    const headers  = {headers:{"Authorization":token,id,role}}
    return axios.put(`${endpointBase}/add-emergency`,{iduser:id,idfriend:data.idfriend},headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('Error in service addEmergencyContact',err)})
}
function removeEmergencyContact(id,token,role,data){
    const headers  = {headers:{"Authorization":token,id,role}}
    return axios.put(`${endpointBase}/remove-emergency`,{iduser:id,idfriend:data.idfriend},headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('Error in service removeEmergencyContact',err); return err;})
}
function getEmergencyContacts(id,token,role){
    const headers  = {headers:{"Authorization":token,id,role}}
    return axios.get(`${endpointBase}/list-emergency?iduser=${id}`,headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('Error in service getEmergencyContacts',err);return err;})
}

export const categoryFriendshipService = {
    addEmergencyContact,
    removeEmergencyContact,
    getEmergencyContacts
}