import axios from 'axios';
import ConfigService from './config';
const endpointBase = `${ConfigService.BASE_URL}/user`;

function createNewUser(data){
    let formData = new FormData();
    if(data.file.type != undefined){
        formData.append('file',{
            uri:data.file.uri,
            name:data.file.name,
            type:data.file.type});
    }else{
        formData.append('file','');
    }
    formData.append('name',data.name);
    formData.append('password',data.password);
    formData.append('email',data.email);
    formData.append('sex',data.sex);
    formData.append('codeAffiliate',data.codeAffiliate);
    formData.append('countryCode',data.countryCode);
    formData.append('numberPhone',data.numberPhone);
    formData.append('idWonder',data.idWonder);
    formData.append('dateBirth',data.dateBirth);
    formData.append('idAvatar','assets/avatar.png');
    const config = {headers:{'content-type':'application/x-www-form-urlencoded'}}
    return axios.post(`${endpointBase}/register`,formData,config)
    .then((res) => {return res.status})
    .catch((err) => console.log('Error in service createNewUser',err))
}
function existWonder(wonderid){
    return axios.get(`${endpointBase}/exists-wonder?idWonder=${wonderid}`)
    .then((res) => {return false;})
    .catch(() => {return true;})
}
function existEmail(email){
    return axios.get(`${endpointBase}/exists-email?email=${email}`)
    .then(res => {
        return true;
    })
    .catch(err => {
        return false;
    })
}
function verificationCodeSingup(numberPhone,code){
    return axios.get(`${endpointBase}/exists-code-temp?numberPhone=${numberPhone}&code=${code}`)
    .then(res => {
        return res;
    })
    .catch(err => {
        return err;
    })
}
function existNumberPhone(numberPhone,countryCode){
    return axios.get(`${endpointBase}/exists-number-phone?numberPhone=${numberPhone}&countryCode=${countryCode}`)
    .then(res => {
        return false;
    })
    .catch(err => {
        return err;
    })
}
function sendCodeSingup(numberPhone,countryCode){
    return axios.get(`${endpointBase}/send-code-signup?numberPhone=${numberPhone}&countryCode=${countryCode}`)
    .then(res => {
        return res.data;
    })
    .catch(err => console.log('Error in service: sendCodeSingup',err))
}
function login(idwonder,countryCode,password){
    return axios.post(`${endpointBase}/login`,{idWonder:idwonder,countryCode,password:password})
    .then(res => {return res.data})
    .catch(err => {return false})
}
function sendCodeResetPassword(numberPhone,countryCode){
    return axios.get(`${endpointBase}/send-code-reset-password?numberPhone=${numberPhone}&countryCode=${countryCode}`)
    .then(res => {
        return res.data
    })
    .catch(err => {return false;})
}
function resetPassword(numberPhone,countryCode,password){
    return axios.post(`${endpointBase}/reset-password`,{countryCode:countryCode,numberPhone:numberPhone,password:password})
    .then((res) => {return true})
    .catch((err) => {return false});
}
function existsUser(idWonder,countryCode){
    return axios.get(`${endpointBase}/exists-user?idWonder=${idWonder}&countryCode=${countryCode}`)
    .then((res) => {return res.data})
    .catch(err => {return err})
}
function logout(id,token,role){
    const headers  = {headers:{"Authorization":token,id,role}}
    return axios.post(`${endpointBase}/logout`,{id,interfacex:'APPMovil'},headers)
    .then((result) => {return result.data})
    .catch((err) => {console.log(err);return err})
}
function update(id,token,role,data){
    const headers  = {headers:{"Authorization":token,id,role}};
    return axios.put(`${endpointBase}/update`,{_id:id,...data},headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('cath',err);return err})
}
function updateAvatar(id,token,role,data){
    const headers = {headers:{"Authorization":token,id,role,'content-type':'application/x-www-form-urlencoded'}}
    let formData = new FormData();
    formData.append('file',{
    uri:data.uri,
    name:data.name,
    type:data.type});
    return axios.put(`${endpointBase}/update-avatar`,formData,headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('cath',err);return err})
}
function verifyPassword(id,token,role,password){
    const headers  = {headers:{"Authorization":token,id,role}};
    return axios.post(`${endpointBase}/verify-password`,{_id:id,password},headers)
    .then((res) => {return true})
    .catch((err) => {console.log(err);return false})
}
function chagePassword(id,token,role,data){
    const headers  = {headers:{"Authorization":token,id,role}};
    return axios.put(`${endpointBase}/update-password`,{_id:id,password:data.currentPassword,newPassword:data.newPassword},headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('Error in changePassword service',err);return err})
}
function getProfessionalSkills(id,token,role){
    const headers  = {headers:{"Authorization":token,id,role}};
    return axios.get(`${endpointBase}/list-skill?_id=${id}`,headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('Error in service getProfessionalSkills',err);return err;});
}
function addProfessionalSkill(id,token,role,data){
    const headers  = {headers:{"Authorization":token,id,role}};
    return axios.put(`${endpointBase}/add-skill`,{_id:id,skill:data.skill},headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('Error in service addProfesionalSkill',err);return err;})
}
function removeProfessionalSkill(id,token,role,data){
    const headers  = {headers:{"Authorization":token,id,role}};
    return axios.put(`${endpointBase}/remove-skill`,{_id:id,skill:data.skill},headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('Error in service removeProfessionalSkill',err);return err;})
}
function deactive(id,token,role){
    const headers  = {headers:{"Authorization":token,id,role}};
    return axios.put(`${endpointBase}/deactive`,{id},headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log(err);return err})
}
function query(id,token,role,idwonder){
    const headers  = {headers:{"Authorization":token,id,role}};
    return axios.get(`${endpointBase}/query?idWonder=${idwonder}`,headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('Error in service query',err);return err;})
}
export const userService = {
    createNewUser,
    existWonder,
    existEmail,
    verificationCodeSingup,
    sendCodeSingup,
    existNumberPhone,
    login,
    logout,
    sendCodeResetPassword,
    resetPassword,
    existsUser,
    update,
    updateAvatar,
    chagePassword,
    verifyPassword,
    getProfessionalSkills,
    addProfessionalSkill,
    removeProfessionalSkill,
    deactive,
    query
}