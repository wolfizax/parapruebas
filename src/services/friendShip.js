import axios from 'axios';
import ConfigService from './config';
const endpointBase = `${ConfigService.BASE_URL}/friendship`;

function getUserList(id,token,role){
    const headers  = {headers:{"Authorization":token,id,role}}
    return axios.get(`${endpointBase}/list?iduser=${id}`,headers)
    .then((res) => {return res.data})
    .catch((err) => console.log('Error in service getUserList',err))
}
function getUserBlockedList(id,token,role){
    const headers  = {headers:{"Authorization":token,id,role}}
    return axios.get(`${endpointBase}/list-locked?iduser=${id}`,headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('Error in service getUserBlockedList',err);return err;}) 
}
function blockedContact(id,token,role,data){
    const headers  = {headers:{"Authorization":token,id,role}}
    return axios.put(`${endpointBase}/locked`,{iduser:id,idfriend:data.idfriend},headers)
    .then((res) => {return res.data})
    .catch((err) => {console.log('Error in service blockedContact',err);return err;})
}
function unblockContact(id,token,role,data){
    const headers  = {headers:{"Authorization":token,id,role}}
    return axios.put(`${endpointBase}/unlocked`,{iduser:id,idfriend:data.idfriend},headers)
    .then((res) => {return res.data})
    .catch((err => {console.log('Error in service unblockContact',err);return err;}))
}

export const friendshipService = {
    getUserList,
    getUserBlockedList,
    blockedContact,
    unblockContact
} 