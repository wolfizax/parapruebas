const BASE_URL = __DEV__ ? 'http://52.23.78.30:9999/api' : 'http://52.23.78.30:9999/api';

export default ConfigService = {
    BASE_URL
};