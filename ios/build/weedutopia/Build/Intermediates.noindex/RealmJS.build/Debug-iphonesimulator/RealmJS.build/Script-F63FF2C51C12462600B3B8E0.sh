#!/bin/sh
if [[ -d "$HOME/.asdf/shims" ]]; then
  export PATH=$HOME/.asdf/shims:$PATH
fi

[ -z "$NVM_DIR" ] && export NVM_DIR="$HOME/.nvm"

if [[ -s "$HOME/.nvm/nvm.sh" ]]; then
  . "$HOME/.nvm/nvm.sh"
elif [[ -x "$(command -v brew)" && -s "$(brew --prefix nvm)/nvm.sh" ]]; then
  . "$(brew --prefix nvm)/nvm.sh"
fi

echo "Using Node.js $(node --version)"

cd ..
node scripts/download-realm.js ios --sync

