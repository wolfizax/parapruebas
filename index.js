import './src/ReactotronConfig';
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import WonderChatt from './src';

AppRegistry.registerComponent(appName, () => WonderChatt);